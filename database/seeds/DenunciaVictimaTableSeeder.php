<?php

use Illuminate\Database\Seeder;

class DenunciaVictimaTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('denuncia_victima')->delete();
        
        \DB::table('denuncia_victima')->insert(array (
            0 => 
            array (
                'id' => 1,
                'denuncia_id' => 1,
                'victima_id' => 1,
                'created_at' => '2019-01-29 22:26:06',
                'updated_at' => '2019-01-29 22:26:06',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'denuncia_id' => 2,
                'victima_id' => 2,
                'created_at' => '2019-01-29 22:26:07',
                'updated_at' => '2019-01-29 22:26:07',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'denuncia_id' => 3,
                'victima_id' => 3,
                'created_at' => '2019-01-29 22:26:07',
                'updated_at' => '2019-01-29 22:26:07',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'denuncia_id' => 4,
                'victima_id' => 4,
                'created_at' => '2019-01-29 22:26:07',
                'updated_at' => '2019-01-29 22:26:07',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'denuncia_id' => 5,
                'victima_id' => 5,
                'created_at' => '2019-01-29 22:26:07',
                'updated_at' => '2019-01-29 22:26:07',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'denuncia_id' => 6,
                'victima_id' => 6,
                'created_at' => '2019-01-29 22:26:07',
                'updated_at' => '2019-01-29 22:26:07',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'denuncia_id' => 7,
                'victima_id' => 7,
                'created_at' => '2019-01-29 22:26:07',
                'updated_at' => '2019-01-29 22:26:07',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'denuncia_id' => 7,
                'victima_id' => 8,
                'created_at' => '2019-01-29 22:26:07',
                'updated_at' => '2019-01-29 22:26:07',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'denuncia_id' => 8,
                'victima_id' => 9,
                'created_at' => '2019-01-29 22:26:07',
                'updated_at' => '2019-01-29 22:26:07',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'denuncia_id' => 9,
                'victima_id' => 10,
                'created_at' => '2019-01-29 22:26:08',
                'updated_at' => '2019-01-29 22:26:08',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'denuncia_id' => 10,
                'victima_id' => 11,
                'created_at' => '2019-01-29 22:26:08',
                'updated_at' => '2019-01-29 22:26:08',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'denuncia_id' => 11,
                'victima_id' => 12,
                'created_at' => '2019-01-29 22:26:08',
                'updated_at' => '2019-01-29 22:26:08',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'denuncia_id' => 12,
                'victima_id' => 13,
                'created_at' => '2019-01-29 22:26:08',
                'updated_at' => '2019-01-29 22:26:08',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'denuncia_id' => 13,
                'victima_id' => 14,
                'created_at' => '2019-01-29 22:26:08',
                'updated_at' => '2019-01-29 22:26:08',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'denuncia_id' => 13,
                'victima_id' => 15,
                'created_at' => '2019-01-29 22:26:08',
                'updated_at' => '2019-01-29 22:26:08',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'denuncia_id' => 14,
                'victima_id' => 16,
                'created_at' => '2019-01-29 22:26:08',
                'updated_at' => '2019-01-29 22:26:08',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'denuncia_id' => 15,
                'victima_id' => 17,
                'created_at' => '2019-01-29 22:26:08',
                'updated_at' => '2019-01-29 22:26:08',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'denuncia_id' => 16,
                'victima_id' => 18,
                'created_at' => '2019-01-29 22:26:09',
                'updated_at' => '2019-01-29 22:26:09',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'denuncia_id' => 17,
                'victima_id' => 19,
                'created_at' => '2019-01-29 22:26:09',
                'updated_at' => '2019-01-29 22:26:09',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'denuncia_id' => 18,
                'victima_id' => 20,
                'created_at' => '2019-01-29 22:26:09',
                'updated_at' => '2019-01-29 22:26:09',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'denuncia_id' => 19,
                'victima_id' => 21,
                'created_at' => '2019-01-29 22:26:09',
                'updated_at' => '2019-01-29 22:26:09',
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'denuncia_id' => 20,
                'victima_id' => 22,
                'created_at' => '2019-01-29 22:26:09',
                'updated_at' => '2019-01-29 22:26:09',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'denuncia_id' => 21,
                'victima_id' => 23,
                'created_at' => '2019-01-29 22:26:09',
                'updated_at' => '2019-01-29 22:26:09',
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'denuncia_id' => 21,
                'victima_id' => 24,
                'created_at' => '2019-01-29 22:26:09',
                'updated_at' => '2019-01-29 22:26:09',
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'denuncia_id' => 22,
                'victima_id' => 25,
                'created_at' => '2019-01-29 22:26:10',
                'updated_at' => '2019-01-29 22:26:10',
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'denuncia_id' => 23,
                'victima_id' => 26,
                'created_at' => '2019-01-29 22:26:10',
                'updated_at' => '2019-01-29 22:26:10',
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'denuncia_id' => 24,
                'victima_id' => 27,
                'created_at' => '2019-01-29 22:26:10',
                'updated_at' => '2019-01-29 22:26:10',
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'denuncia_id' => 25,
                'victima_id' => 28,
                'created_at' => '2019-01-29 22:26:10',
                'updated_at' => '2019-01-29 22:26:10',
                'deleted_at' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'denuncia_id' => 26,
                'victima_id' => 29,
                'created_at' => '2019-01-29 22:26:10',
                'updated_at' => '2019-01-29 22:26:10',
                'deleted_at' => NULL,
            ),
            29 => 
            array (
                'id' => 30,
                'denuncia_id' => 27,
                'victima_id' => 30,
                'created_at' => '2019-01-29 22:26:10',
                'updated_at' => '2019-01-29 22:26:10',
                'deleted_at' => NULL,
            ),
            30 => 
            array (
                'id' => 31,
                'denuncia_id' => 28,
                'victima_id' => 31,
                'created_at' => '2019-01-29 22:26:11',
                'updated_at' => '2019-01-29 22:26:11',
                'deleted_at' => NULL,
            ),
            31 => 
            array (
                'id' => 32,
                'denuncia_id' => 29,
                'victima_id' => 32,
                'created_at' => '2019-01-29 22:26:11',
                'updated_at' => '2019-01-29 22:26:11',
                'deleted_at' => NULL,
            ),
            32 => 
            array (
                'id' => 33,
                'denuncia_id' => 30,
                'victima_id' => 33,
                'created_at' => '2019-01-29 22:26:11',
                'updated_at' => '2019-01-29 22:26:11',
                'deleted_at' => NULL,
            ),
            33 => 
            array (
                'id' => 34,
                'denuncia_id' => 31,
                'victima_id' => 34,
                'created_at' => '2019-01-29 22:26:11',
                'updated_at' => '2019-01-29 22:26:11',
                'deleted_at' => NULL,
            ),
            34 => 
            array (
                'id' => 35,
                'denuncia_id' => 32,
                'victima_id' => 35,
                'created_at' => '2019-01-29 22:26:11',
                'updated_at' => '2019-01-29 22:26:11',
                'deleted_at' => NULL,
            ),
            35 => 
            array (
                'id' => 36,
                'denuncia_id' => 33,
                'victima_id' => 36,
                'created_at' => '2019-01-29 22:26:12',
                'updated_at' => '2019-01-29 22:26:12',
                'deleted_at' => NULL,
            ),
            36 => 
            array (
                'id' => 37,
                'denuncia_id' => 34,
                'victima_id' => 37,
                'created_at' => '2019-01-29 22:26:12',
                'updated_at' => '2019-01-29 22:26:12',
                'deleted_at' => NULL,
            ),
            37 => 
            array (
                'id' => 38,
                'denuncia_id' => 36,
                'victima_id' => 38,
                'created_at' => '2019-01-29 22:26:12',
                'updated_at' => '2019-01-29 22:26:12',
                'deleted_at' => NULL,
            ),
            38 => 
            array (
                'id' => 39,
                'denuncia_id' => 37,
                'victima_id' => 39,
                'created_at' => '2019-01-29 22:26:12',
                'updated_at' => '2019-01-29 22:26:12',
                'deleted_at' => NULL,
            ),
            39 => 
            array (
                'id' => 40,
                'denuncia_id' => 37,
                'victima_id' => 40,
                'created_at' => '2019-01-29 22:26:12',
                'updated_at' => '2019-01-29 22:26:12',
                'deleted_at' => NULL,
            ),
            40 => 
            array (
                'id' => 41,
                'denuncia_id' => 38,
                'victima_id' => 41,
                'created_at' => '2019-01-29 22:26:12',
                'updated_at' => '2019-01-29 22:26:12',
                'deleted_at' => NULL,
            ),
            41 => 
            array (
                'id' => 42,
                'denuncia_id' => 39,
                'victima_id' => 42,
                'created_at' => '2019-01-29 22:26:12',
                'updated_at' => '2019-01-29 22:26:12',
                'deleted_at' => NULL,
            ),
            42 => 
            array (
                'id' => 43,
                'denuncia_id' => 40,
                'victima_id' => 43,
                'created_at' => '2019-01-29 22:26:13',
                'updated_at' => '2019-01-29 22:26:13',
                'deleted_at' => NULL,
            ),
            43 => 
            array (
                'id' => 44,
                'denuncia_id' => 41,
                'victima_id' => 44,
                'created_at' => '2019-01-29 22:26:13',
                'updated_at' => '2019-01-29 22:26:13',
                'deleted_at' => NULL,
            ),
            44 => 
            array (
                'id' => 45,
                'denuncia_id' => 42,
                'victima_id' => 45,
                'created_at' => '2019-01-29 22:26:13',
                'updated_at' => '2019-01-29 22:26:13',
                'deleted_at' => NULL,
            ),
            45 => 
            array (
                'id' => 46,
                'denuncia_id' => 43,
                'victima_id' => 46,
                'created_at' => '2019-01-29 22:26:13',
                'updated_at' => '2019-01-29 22:26:13',
                'deleted_at' => NULL,
            ),
            46 => 
            array (
                'id' => 47,
                'denuncia_id' => 44,
                'victima_id' => 47,
                'created_at' => '2019-01-29 22:26:13',
                'updated_at' => '2019-01-29 22:26:13',
                'deleted_at' => NULL,
            ),
            47 => 
            array (
                'id' => 48,
                'denuncia_id' => 47,
                'victima_id' => 48,
                'created_at' => '2019-01-29 22:26:14',
                'updated_at' => '2019-01-29 22:26:14',
                'deleted_at' => NULL,
            ),
            48 => 
            array (
                'id' => 49,
                'denuncia_id' => 52,
                'victima_id' => 49,
                'created_at' => '2019-01-29 22:26:14',
                'updated_at' => '2019-01-29 22:26:14',
                'deleted_at' => NULL,
            ),
            49 => 
            array (
                'id' => 50,
                'denuncia_id' => 53,
                'victima_id' => 50,
                'created_at' => '2019-01-29 22:26:14',
                'updated_at' => '2019-01-29 22:26:14',
                'deleted_at' => NULL,
            ),
            50 => 
            array (
                'id' => 51,
                'denuncia_id' => 54,
                'victima_id' => 51,
                'created_at' => '2019-01-29 22:26:15',
                'updated_at' => '2019-01-29 22:26:15',
                'deleted_at' => NULL,
            ),
            51 => 
            array (
                'id' => 52,
                'denuncia_id' => 54,
                'victima_id' => 52,
                'created_at' => '2019-01-29 22:26:15',
                'updated_at' => '2019-01-29 22:26:15',
                'deleted_at' => NULL,
            ),
            52 => 
            array (
                'id' => 53,
                'denuncia_id' => 55,
                'victima_id' => 53,
                'created_at' => '2019-01-29 22:26:15',
                'updated_at' => '2019-01-29 22:26:15',
                'deleted_at' => NULL,
            ),
            53 => 
            array (
                'id' => 54,
                'denuncia_id' => 56,
                'victima_id' => 54,
                'created_at' => '2019-01-29 22:26:15',
                'updated_at' => '2019-01-29 22:26:15',
                'deleted_at' => NULL,
            ),
            54 => 
            array (
                'id' => 55,
                'denuncia_id' => 58,
                'victima_id' => 55,
                'created_at' => '2019-01-29 22:26:15',
                'updated_at' => '2019-01-29 22:26:15',
                'deleted_at' => NULL,
            ),
            55 => 
            array (
                'id' => 56,
                'denuncia_id' => 58,
                'victima_id' => 56,
                'created_at' => '2019-01-29 22:26:15',
                'updated_at' => '2019-01-29 22:26:15',
                'deleted_at' => NULL,
            ),
            56 => 
            array (
                'id' => 57,
                'denuncia_id' => 58,
                'victima_id' => 57,
                'created_at' => '2019-01-29 22:26:15',
                'updated_at' => '2019-01-29 22:26:15',
                'deleted_at' => NULL,
            ),
            57 => 
            array (
                'id' => 58,
                'denuncia_id' => 59,
                'victima_id' => 58,
                'created_at' => '2019-01-29 22:26:15',
                'updated_at' => '2019-01-29 22:26:15',
                'deleted_at' => NULL,
            ),
            58 => 
            array (
                'id' => 59,
                'denuncia_id' => 60,
                'victima_id' => 59,
                'created_at' => '2019-01-29 22:26:15',
                'updated_at' => '2019-01-29 22:26:15',
                'deleted_at' => NULL,
            ),
            59 => 
            array (
                'id' => 60,
                'denuncia_id' => 61,
                'victima_id' => 60,
                'created_at' => '2019-01-29 22:26:16',
                'updated_at' => '2019-01-29 22:26:16',
                'deleted_at' => NULL,
            ),
            60 => 
            array (
                'id' => 61,
                'denuncia_id' => 62,
                'victima_id' => 61,
                'created_at' => '2019-01-29 22:26:16',
                'updated_at' => '2019-01-29 22:26:16',
                'deleted_at' => NULL,
            ),
            61 => 
            array (
                'id' => 62,
                'denuncia_id' => 63,
                'victima_id' => 62,
                'created_at' => '2019-01-29 22:26:16',
                'updated_at' => '2019-01-29 22:26:16',
                'deleted_at' => NULL,
            ),
            62 => 
            array (
                'id' => 63,
                'denuncia_id' => 64,
                'victima_id' => 63,
                'created_at' => '2019-01-29 22:26:16',
                'updated_at' => '2019-01-29 22:26:16',
                'deleted_at' => NULL,
            ),
            63 => 
            array (
                'id' => 64,
                'denuncia_id' => 65,
                'victima_id' => 64,
                'created_at' => '2019-01-29 22:26:16',
                'updated_at' => '2019-01-29 22:26:16',
                'deleted_at' => NULL,
            ),
            64 => 
            array (
                'id' => 65,
                'denuncia_id' => 66,
                'victima_id' => 65,
                'created_at' => '2019-01-29 22:26:16',
                'updated_at' => '2019-01-29 22:26:16',
                'deleted_at' => NULL,
            ),
            65 => 
            array (
                'id' => 66,
                'denuncia_id' => 67,
                'victima_id' => 66,
                'created_at' => '2019-01-29 22:26:16',
                'updated_at' => '2019-01-29 22:26:16',
                'deleted_at' => NULL,
            ),
            66 => 
            array (
                'id' => 67,
                'denuncia_id' => 68,
                'victima_id' => 67,
                'created_at' => '2019-01-29 22:26:17',
                'updated_at' => '2019-01-29 22:26:17',
                'deleted_at' => NULL,
            ),
            67 => 
            array (
                'id' => 68,
                'denuncia_id' => 69,
                'victima_id' => 68,
                'created_at' => '2019-01-29 22:26:17',
                'updated_at' => '2019-01-29 22:26:17',
                'deleted_at' => NULL,
            ),
            68 => 
            array (
                'id' => 69,
                'denuncia_id' => 70,
                'victima_id' => 69,
                'created_at' => '2019-01-29 22:26:17',
                'updated_at' => '2019-01-29 22:26:17',
                'deleted_at' => NULL,
            ),
            69 => 
            array (
                'id' => 70,
                'denuncia_id' => 71,
                'victima_id' => 70,
                'created_at' => '2019-01-29 22:26:17',
                'updated_at' => '2019-01-29 22:26:17',
                'deleted_at' => NULL,
            ),
            70 => 
            array (
                'id' => 71,
                'denuncia_id' => 72,
                'victima_id' => 71,
                'created_at' => '2019-01-29 22:26:17',
                'updated_at' => '2019-01-29 22:26:17',
                'deleted_at' => NULL,
            ),
            71 => 
            array (
                'id' => 72,
                'denuncia_id' => 73,
                'victima_id' => 72,
                'created_at' => '2019-01-29 22:26:17',
                'updated_at' => '2019-01-29 22:26:17',
                'deleted_at' => NULL,
            ),
            72 => 
            array (
                'id' => 73,
                'denuncia_id' => 74,
                'victima_id' => 73,
                'created_at' => '2019-01-29 22:26:17',
                'updated_at' => '2019-01-29 22:26:17',
                'deleted_at' => NULL,
            ),
            73 => 
            array (
                'id' => 74,
                'denuncia_id' => 75,
                'victima_id' => 74,
                'created_at' => '2019-01-29 22:26:18',
                'updated_at' => '2019-01-29 22:26:18',
                'deleted_at' => NULL,
            ),
            74 => 
            array (
                'id' => 75,
                'denuncia_id' => 76,
                'victima_id' => 75,
                'created_at' => '2019-01-29 22:26:18',
                'updated_at' => '2019-01-29 22:26:18',
                'deleted_at' => NULL,
            ),
            75 => 
            array (
                'id' => 76,
                'denuncia_id' => 77,
                'victima_id' => 76,
                'created_at' => '2019-01-29 22:26:18',
                'updated_at' => '2019-01-29 22:26:18',
                'deleted_at' => NULL,
            ),
            76 => 
            array (
                'id' => 77,
                'denuncia_id' => 78,
                'victima_id' => 77,
                'created_at' => '2019-01-29 22:26:18',
                'updated_at' => '2019-01-29 22:26:18',
                'deleted_at' => NULL,
            ),
            77 => 
            array (
                'id' => 78,
                'denuncia_id' => 79,
                'victima_id' => 78,
                'created_at' => '2019-01-29 22:26:18',
                'updated_at' => '2019-01-29 22:26:18',
                'deleted_at' => NULL,
            ),
            78 => 
            array (
                'id' => 79,
                'denuncia_id' => 80,
                'victima_id' => 79,
                'created_at' => '2019-01-29 22:26:18',
                'updated_at' => '2019-01-29 22:26:18',
                'deleted_at' => NULL,
            ),
            79 => 
            array (
                'id' => 80,
                'denuncia_id' => 82,
                'victima_id' => 80,
                'created_at' => '2019-01-29 22:26:19',
                'updated_at' => '2019-01-29 22:26:19',
                'deleted_at' => NULL,
            ),
            80 => 
            array (
                'id' => 81,
                'denuncia_id' => 83,
                'victima_id' => 81,
                'created_at' => '2019-01-29 22:26:19',
                'updated_at' => '2019-01-29 22:26:19',
                'deleted_at' => NULL,
            ),
            81 => 
            array (
                'id' => 82,
                'denuncia_id' => 84,
                'victima_id' => 82,
                'created_at' => '2019-01-29 22:26:19',
                'updated_at' => '2019-01-29 22:26:19',
                'deleted_at' => NULL,
            ),
            82 => 
            array (
                'id' => 83,
                'denuncia_id' => 85,
                'victima_id' => 83,
                'created_at' => '2019-01-29 22:26:19',
                'updated_at' => '2019-01-29 22:26:19',
                'deleted_at' => NULL,
            ),
            83 => 
            array (
                'id' => 84,
                'denuncia_id' => 86,
                'victima_id' => 84,
                'created_at' => '2019-01-29 22:26:19',
                'updated_at' => '2019-01-29 22:26:19',
                'deleted_at' => NULL,
            ),
            84 => 
            array (
                'id' => 85,
                'denuncia_id' => 86,
                'victima_id' => 85,
                'created_at' => '2019-01-29 22:26:19',
                'updated_at' => '2019-01-29 22:26:19',
                'deleted_at' => NULL,
            ),
            85 => 
            array (
                'id' => 86,
                'denuncia_id' => 87,
                'victima_id' => 86,
                'created_at' => '2019-01-29 22:26:19',
                'updated_at' => '2019-01-29 22:26:19',
                'deleted_at' => NULL,
            ),
            86 => 
            array (
                'id' => 87,
                'denuncia_id' => 88,
                'victima_id' => 87,
                'created_at' => '2019-01-29 22:26:20',
                'updated_at' => '2019-01-29 22:26:20',
                'deleted_at' => NULL,
            ),
            87 => 
            array (
                'id' => 88,
                'denuncia_id' => 89,
                'victima_id' => 88,
                'created_at' => '2019-01-29 22:26:20',
                'updated_at' => '2019-01-29 22:26:20',
                'deleted_at' => NULL,
            ),
            88 => 
            array (
                'id' => 89,
                'denuncia_id' => 90,
                'victima_id' => 89,
                'created_at' => '2019-01-29 22:26:20',
                'updated_at' => '2019-01-29 22:26:20',
                'deleted_at' => NULL,
            ),
            89 => 
            array (
                'id' => 90,
                'denuncia_id' => 91,
                'victima_id' => 90,
                'created_at' => '2019-01-29 22:26:20',
                'updated_at' => '2019-01-29 22:26:20',
                'deleted_at' => NULL,
            ),
            90 => 
            array (
                'id' => 91,
                'denuncia_id' => 92,
                'victima_id' => 91,
                'created_at' => '2019-01-29 22:26:20',
                'updated_at' => '2019-01-29 22:26:20',
                'deleted_at' => NULL,
            ),
            91 => 
            array (
                'id' => 92,
                'denuncia_id' => 92,
                'victima_id' => 92,
                'created_at' => '2019-01-29 22:26:20',
                'updated_at' => '2019-01-29 22:26:20',
                'deleted_at' => NULL,
            ),
            92 => 
            array (
                'id' => 93,
                'denuncia_id' => 93,
                'victima_id' => 93,
                'created_at' => '2019-01-29 22:26:21',
                'updated_at' => '2019-01-29 22:26:21',
                'deleted_at' => NULL,
            ),
            93 => 
            array (
                'id' => 94,
                'denuncia_id' => 94,
                'victima_id' => 94,
                'created_at' => '2019-01-29 22:26:21',
                'updated_at' => '2019-01-29 22:26:21',
                'deleted_at' => NULL,
            ),
            94 => 
            array (
                'id' => 95,
                'denuncia_id' => 95,
                'victima_id' => 95,
                'created_at' => '2019-01-29 22:26:21',
                'updated_at' => '2019-01-29 22:26:21',
                'deleted_at' => NULL,
            ),
            95 => 
            array (
                'id' => 96,
                'denuncia_id' => 95,
                'victima_id' => 96,
                'created_at' => '2019-01-29 22:26:21',
                'updated_at' => '2019-01-29 22:26:21',
                'deleted_at' => NULL,
            ),
            96 => 
            array (
                'id' => 97,
                'denuncia_id' => 96,
                'victima_id' => 97,
                'created_at' => '2019-01-29 22:26:21',
                'updated_at' => '2019-01-29 22:26:21',
                'deleted_at' => NULL,
            ),
            97 => 
            array (
                'id' => 98,
                'denuncia_id' => 97,
                'victima_id' => 98,
                'created_at' => '2019-01-29 22:26:21',
                'updated_at' => '2019-01-29 22:26:21',
                'deleted_at' => NULL,
            ),
            98 => 
            array (
                'id' => 99,
                'denuncia_id' => 98,
                'victima_id' => 99,
                'created_at' => '2019-01-29 22:26:21',
                'updated_at' => '2019-01-29 22:26:21',
                'deleted_at' => NULL,
            ),
            99 => 
            array (
                'id' => 100,
                'denuncia_id' => 99,
                'victima_id' => 100,
                'created_at' => '2019-01-29 22:26:22',
                'updated_at' => '2019-01-29 22:26:22',
                'deleted_at' => NULL,
            ),
            100 => 
            array (
                'id' => 101,
                'denuncia_id' => 100,
                'victima_id' => 101,
                'created_at' => '2019-01-29 22:26:22',
                'updated_at' => '2019-01-29 22:26:22',
                'deleted_at' => NULL,
            ),
            101 => 
            array (
                'id' => 102,
                'denuncia_id' => 101,
                'victima_id' => 102,
                'created_at' => '2019-01-29 22:26:22',
                'updated_at' => '2019-01-29 22:26:22',
                'deleted_at' => NULL,
            ),
            102 => 
            array (
                'id' => 103,
                'denuncia_id' => 102,
                'victima_id' => 103,
                'created_at' => '2019-01-29 22:26:22',
                'updated_at' => '2019-01-29 22:26:22',
                'deleted_at' => NULL,
            ),
            103 => 
            array (
                'id' => 104,
                'denuncia_id' => 103,
                'victima_id' => 104,
                'created_at' => '2019-01-29 22:26:22',
                'updated_at' => '2019-01-29 22:26:22',
                'deleted_at' => NULL,
            ),
            104 => 
            array (
                'id' => 105,
                'denuncia_id' => 104,
                'victima_id' => 105,
                'created_at' => '2019-01-29 22:26:23',
                'updated_at' => '2019-01-29 22:26:23',
                'deleted_at' => NULL,
            ),
            105 => 
            array (
                'id' => 106,
                'denuncia_id' => 105,
                'victima_id' => 106,
                'created_at' => '2019-01-29 22:26:23',
                'updated_at' => '2019-01-29 22:26:23',
                'deleted_at' => NULL,
            ),
            106 => 
            array (
                'id' => 107,
                'denuncia_id' => 106,
                'victima_id' => 107,
                'created_at' => '2019-01-29 22:26:23',
                'updated_at' => '2019-01-29 22:26:23',
                'deleted_at' => NULL,
            ),
            107 => 
            array (
                'id' => 108,
                'denuncia_id' => 107,
                'victima_id' => 108,
                'created_at' => '2019-01-29 22:26:23',
                'updated_at' => '2019-01-29 22:26:23',
                'deleted_at' => NULL,
            ),
            108 => 
            array (
                'id' => 109,
                'denuncia_id' => 108,
                'victima_id' => 109,
                'created_at' => '2019-01-29 22:26:23',
                'updated_at' => '2019-01-29 22:26:23',
                'deleted_at' => NULL,
            ),
            109 => 
            array (
                'id' => 110,
                'denuncia_id' => 108,
                'victima_id' => 110,
                'created_at' => '2019-01-29 22:26:23',
                'updated_at' => '2019-01-29 22:26:23',
                'deleted_at' => NULL,
            ),
            110 => 
            array (
                'id' => 111,
                'denuncia_id' => 109,
                'victima_id' => 111,
                'created_at' => '2019-01-29 22:26:24',
                'updated_at' => '2019-01-29 22:26:24',
                'deleted_at' => NULL,
            ),
            111 => 
            array (
                'id' => 112,
                'denuncia_id' => 110,
                'victima_id' => 112,
                'created_at' => '2019-01-29 22:26:24',
                'updated_at' => '2019-01-29 22:26:24',
                'deleted_at' => NULL,
            ),
            112 => 
            array (
                'id' => 113,
                'denuncia_id' => 111,
                'victima_id' => 113,
                'created_at' => '2019-01-29 22:26:24',
                'updated_at' => '2019-01-29 22:26:24',
                'deleted_at' => NULL,
            ),
            113 => 
            array (
                'id' => 114,
                'denuncia_id' => 112,
                'victima_id' => 114,
                'created_at' => '2019-01-29 22:26:24',
                'updated_at' => '2019-01-29 22:26:24',
                'deleted_at' => NULL,
            ),
            114 => 
            array (
                'id' => 115,
                'denuncia_id' => 112,
                'victima_id' => 115,
                'created_at' => '2019-01-29 22:26:24',
                'updated_at' => '2019-01-29 22:26:24',
                'deleted_at' => NULL,
            ),
            115 => 
            array (
                'id' => 116,
                'denuncia_id' => 113,
                'victima_id' => 116,
                'created_at' => '2019-01-29 22:26:24',
                'updated_at' => '2019-01-29 22:26:24',
                'deleted_at' => NULL,
            ),
            116 => 
            array (
                'id' => 117,
                'denuncia_id' => 114,
                'victima_id' => 117,
                'created_at' => '2019-01-29 22:26:24',
                'updated_at' => '2019-01-29 22:26:24',
                'deleted_at' => NULL,
            ),
            117 => 
            array (
                'id' => 118,
                'denuncia_id' => 115,
                'victima_id' => 118,
                'created_at' => '2019-01-29 22:26:25',
                'updated_at' => '2019-01-29 22:26:25',
                'deleted_at' => NULL,
            ),
            118 => 
            array (
                'id' => 119,
                'denuncia_id' => 116,
                'victima_id' => 119,
                'created_at' => '2019-01-29 22:26:25',
                'updated_at' => '2019-01-29 22:26:25',
                'deleted_at' => NULL,
            ),
            119 => 
            array (
                'id' => 120,
                'denuncia_id' => 118,
                'victima_id' => 120,
                'created_at' => '2019-01-29 22:26:25',
                'updated_at' => '2019-01-29 22:26:25',
                'deleted_at' => NULL,
            ),
            120 => 
            array (
                'id' => 121,
                'denuncia_id' => 119,
                'victima_id' => 121,
                'created_at' => '2019-01-29 22:26:25',
                'updated_at' => '2019-01-29 22:26:25',
                'deleted_at' => NULL,
            ),
            121 => 
            array (
                'id' => 122,
                'denuncia_id' => 120,
                'victima_id' => 122,
                'created_at' => '2019-01-29 22:26:25',
                'updated_at' => '2019-01-29 22:26:25',
                'deleted_at' => NULL,
            ),
            122 => 
            array (
                'id' => 123,
                'denuncia_id' => 121,
                'victima_id' => 123,
                'created_at' => '2019-01-29 22:26:26',
                'updated_at' => '2019-01-29 22:26:26',
                'deleted_at' => NULL,
            ),
            123 => 
            array (
                'id' => 124,
                'denuncia_id' => 122,
                'victima_id' => 124,
                'created_at' => '2019-01-29 22:26:26',
                'updated_at' => '2019-01-29 22:26:26',
                'deleted_at' => NULL,
            ),
            124 => 
            array (
                'id' => 125,
                'denuncia_id' => 123,
                'victima_id' => 125,
                'created_at' => '2019-01-29 22:26:26',
                'updated_at' => '2019-01-29 22:26:26',
                'deleted_at' => NULL,
            ),
            125 => 
            array (
                'id' => 126,
                'denuncia_id' => 124,
                'victima_id' => 126,
                'created_at' => '2019-01-29 22:26:26',
                'updated_at' => '2019-01-29 22:26:26',
                'deleted_at' => NULL,
            ),
            126 => 
            array (
                'id' => 127,
                'denuncia_id' => 125,
                'victima_id' => 127,
                'created_at' => '2019-01-29 22:26:26',
                'updated_at' => '2019-01-29 22:26:26',
                'deleted_at' => NULL,
            ),
            127 => 
            array (
                'id' => 128,
                'denuncia_id' => 126,
                'victima_id' => 128,
                'created_at' => '2019-01-29 22:26:26',
                'updated_at' => '2019-01-29 22:26:26',
                'deleted_at' => NULL,
            ),
            128 => 
            array (
                'id' => 129,
                'denuncia_id' => 127,
                'victima_id' => 129,
                'created_at' => '2019-01-29 22:26:27',
                'updated_at' => '2019-01-29 22:26:27',
                'deleted_at' => NULL,
            ),
            129 => 
            array (
                'id' => 130,
                'denuncia_id' => 128,
                'victima_id' => 130,
                'created_at' => '2019-01-29 22:26:27',
                'updated_at' => '2019-01-29 22:26:27',
                'deleted_at' => NULL,
            ),
            130 => 
            array (
                'id' => 131,
                'denuncia_id' => 129,
                'victima_id' => 131,
                'created_at' => '2019-01-29 22:26:27',
                'updated_at' => '2019-01-29 22:26:27',
                'deleted_at' => NULL,
            ),
            131 => 
            array (
                'id' => 132,
                'denuncia_id' => 130,
                'victima_id' => 132,
                'created_at' => '2019-01-29 22:26:27',
                'updated_at' => '2019-01-29 22:26:27',
                'deleted_at' => NULL,
            ),
            132 => 
            array (
                'id' => 133,
                'denuncia_id' => 131,
                'victima_id' => 133,
                'created_at' => '2019-01-29 22:26:27',
                'updated_at' => '2019-01-29 22:26:27',
                'deleted_at' => NULL,
            ),
            133 => 
            array (
                'id' => 134,
                'denuncia_id' => 132,
                'victima_id' => 134,
                'created_at' => '2019-01-29 22:26:28',
                'updated_at' => '2019-01-29 22:26:28',
                'deleted_at' => NULL,
            ),
            134 => 
            array (
                'id' => 135,
                'denuncia_id' => 133,
                'victima_id' => 135,
                'created_at' => '2019-01-29 22:26:28',
                'updated_at' => '2019-01-29 22:26:28',
                'deleted_at' => NULL,
            ),
            135 => 
            array (
                'id' => 136,
                'denuncia_id' => 134,
                'victima_id' => 136,
                'created_at' => '2019-01-29 22:26:28',
                'updated_at' => '2019-01-29 22:26:28',
                'deleted_at' => NULL,
            ),
            136 => 
            array (
                'id' => 137,
                'denuncia_id' => 135,
                'victima_id' => 137,
                'created_at' => '2019-01-29 22:26:28',
                'updated_at' => '2019-01-29 22:26:28',
                'deleted_at' => NULL,
            ),
            137 => 
            array (
                'id' => 138,
                'denuncia_id' => 136,
                'victima_id' => 138,
                'created_at' => '2019-01-29 22:26:28',
                'updated_at' => '2019-01-29 22:26:28',
                'deleted_at' => NULL,
            ),
            138 => 
            array (
                'id' => 139,
                'denuncia_id' => 137,
                'victima_id' => 139,
                'created_at' => '2019-01-29 22:26:29',
                'updated_at' => '2019-01-29 22:26:29',
                'deleted_at' => NULL,
            ),
            139 => 
            array (
                'id' => 140,
                'denuncia_id' => 138,
                'victima_id' => 140,
                'created_at' => '2019-01-29 22:26:29',
                'updated_at' => '2019-01-29 22:26:29',
                'deleted_at' => NULL,
            ),
            140 => 
            array (
                'id' => 141,
                'denuncia_id' => 139,
                'victima_id' => 141,
                'created_at' => '2019-01-29 22:26:29',
                'updated_at' => '2019-01-29 22:26:29',
                'deleted_at' => NULL,
            ),
            141 => 
            array (
                'id' => 142,
                'denuncia_id' => 140,
                'victima_id' => 142,
                'created_at' => '2019-01-29 22:26:29',
                'updated_at' => '2019-01-29 22:26:29',
                'deleted_at' => NULL,
            ),
            142 => 
            array (
                'id' => 143,
                'denuncia_id' => 141,
                'victima_id' => 143,
                'created_at' => '2019-01-29 22:26:29',
                'updated_at' => '2019-01-29 22:26:29',
                'deleted_at' => NULL,
            ),
            143 => 
            array (
                'id' => 144,
                'denuncia_id' => 142,
                'victima_id' => 144,
                'created_at' => '2019-01-29 22:26:30',
                'updated_at' => '2019-01-29 22:26:30',
                'deleted_at' => NULL,
            ),
            144 => 
            array (
                'id' => 145,
                'denuncia_id' => 143,
                'victima_id' => 145,
                'created_at' => '2019-01-29 22:26:30',
                'updated_at' => '2019-01-29 22:26:30',
                'deleted_at' => NULL,
            ),
            145 => 
            array (
                'id' => 146,
                'denuncia_id' => 144,
                'victima_id' => 146,
                'created_at' => '2019-01-29 22:26:30',
                'updated_at' => '2019-01-29 22:26:30',
                'deleted_at' => NULL,
            ),
            146 => 
            array (
                'id' => 147,
                'denuncia_id' => 146,
                'victima_id' => 147,
                'created_at' => '2019-01-29 22:26:30',
                'updated_at' => '2019-01-29 22:26:30',
                'deleted_at' => NULL,
            ),
            147 => 
            array (
                'id' => 148,
                'denuncia_id' => 147,
                'victima_id' => 148,
                'created_at' => '2019-01-29 22:26:30',
                'updated_at' => '2019-01-29 22:26:30',
                'deleted_at' => NULL,
            ),
            148 => 
            array (
                'id' => 149,
                'denuncia_id' => 148,
                'victima_id' => 149,
                'created_at' => '2019-01-29 22:26:30',
                'updated_at' => '2019-01-29 22:26:30',
                'deleted_at' => NULL,
            ),
            149 => 
            array (
                'id' => 150,
                'denuncia_id' => 149,
                'victima_id' => 150,
                'created_at' => '2019-01-29 22:26:30',
                'updated_at' => '2019-01-29 22:26:30',
                'deleted_at' => NULL,
            ),
            150 => 
            array (
                'id' => 151,
                'denuncia_id' => 150,
                'victima_id' => 151,
                'created_at' => '2019-01-29 22:26:30',
                'updated_at' => '2019-01-29 22:26:30',
                'deleted_at' => NULL,
            ),
            151 => 
            array (
                'id' => 152,
                'denuncia_id' => 151,
                'victima_id' => 152,
                'created_at' => '2019-01-29 22:26:31',
                'updated_at' => '2019-01-29 22:26:31',
                'deleted_at' => NULL,
            ),
            152 => 
            array (
                'id' => 153,
                'denuncia_id' => 152,
                'victima_id' => 153,
                'created_at' => '2019-01-29 22:26:31',
                'updated_at' => '2019-01-29 22:26:31',
                'deleted_at' => NULL,
            ),
            153 => 
            array (
                'id' => 154,
                'denuncia_id' => 152,
                'victima_id' => 154,
                'created_at' => '2019-01-29 22:26:31',
                'updated_at' => '2019-01-29 22:26:31',
                'deleted_at' => NULL,
            ),
            154 => 
            array (
                'id' => 155,
                'denuncia_id' => 153,
                'victima_id' => 155,
                'created_at' => '2019-01-29 22:26:31',
                'updated_at' => '2019-01-29 22:26:31',
                'deleted_at' => NULL,
            ),
            155 => 
            array (
                'id' => 156,
                'denuncia_id' => 153,
                'victima_id' => 156,
                'created_at' => '2019-01-29 22:26:31',
                'updated_at' => '2019-01-29 22:26:31',
                'deleted_at' => NULL,
            ),
            156 => 
            array (
                'id' => 157,
                'denuncia_id' => 154,
                'victima_id' => 157,
                'created_at' => '2019-01-29 22:26:31',
                'updated_at' => '2019-01-29 22:26:31',
                'deleted_at' => NULL,
            ),
            157 => 
            array (
                'id' => 158,
                'denuncia_id' => 158,
                'victima_id' => 158,
                'created_at' => '2019-01-29 22:26:32',
                'updated_at' => '2019-01-29 22:26:32',
                'deleted_at' => NULL,
            ),
            158 => 
            array (
                'id' => 159,
                'denuncia_id' => 159,
                'victima_id' => 159,
                'created_at' => '2019-01-29 22:26:32',
                'updated_at' => '2019-01-29 22:26:32',
                'deleted_at' => NULL,
            ),
            159 => 
            array (
                'id' => 160,
                'denuncia_id' => 160,
                'victima_id' => 160,
                'created_at' => '2019-01-29 22:26:32',
                'updated_at' => '2019-01-29 22:26:32',
                'deleted_at' => NULL,
            ),
            160 => 
            array (
                'id' => 161,
                'denuncia_id' => 161,
                'victima_id' => 161,
                'created_at' => '2019-01-29 22:26:32',
                'updated_at' => '2019-01-29 22:26:32',
                'deleted_at' => NULL,
            ),
            161 => 
            array (
                'id' => 162,
                'denuncia_id' => 162,
                'victima_id' => 162,
                'created_at' => '2019-01-29 22:26:32',
                'updated_at' => '2019-01-29 22:26:32',
                'deleted_at' => NULL,
            ),
            162 => 
            array (
                'id' => 163,
                'denuncia_id' => 163,
                'victima_id' => 163,
                'created_at' => '2019-01-29 22:26:32',
                'updated_at' => '2019-01-29 22:26:32',
                'deleted_at' => NULL,
            ),
            163 => 
            array (
                'id' => 164,
                'denuncia_id' => 164,
                'victima_id' => 164,
                'created_at' => '2019-01-29 22:26:32',
                'updated_at' => '2019-01-29 22:26:32',
                'deleted_at' => NULL,
            ),
            164 => 
            array (
                'id' => 165,
                'denuncia_id' => 165,
                'victima_id' => 165,
                'created_at' => '2019-01-29 22:26:32',
                'updated_at' => '2019-01-29 22:26:32',
                'deleted_at' => NULL,
            ),
            165 => 
            array (
                'id' => 166,
                'denuncia_id' => 166,
                'victima_id' => 166,
                'created_at' => '2019-01-29 22:26:33',
                'updated_at' => '2019-01-29 22:26:33',
                'deleted_at' => NULL,
            ),
            166 => 
            array (
                'id' => 167,
                'denuncia_id' => 167,
                'victima_id' => 167,
                'created_at' => '2019-01-29 22:26:33',
                'updated_at' => '2019-01-29 22:26:33',
                'deleted_at' => NULL,
            ),
            167 => 
            array (
                'id' => 168,
                'denuncia_id' => 168,
                'victima_id' => 168,
                'created_at' => '2019-01-29 22:26:33',
                'updated_at' => '2019-01-29 22:26:33',
                'deleted_at' => NULL,
            ),
            168 => 
            array (
                'id' => 169,
                'denuncia_id' => 169,
                'victima_id' => 169,
                'created_at' => '2019-01-29 22:26:33',
                'updated_at' => '2019-01-29 22:26:33',
                'deleted_at' => NULL,
            ),
            169 => 
            array (
                'id' => 170,
                'denuncia_id' => 170,
                'victima_id' => 170,
                'created_at' => '2019-01-29 22:26:33',
                'updated_at' => '2019-01-29 22:26:33',
                'deleted_at' => NULL,
            ),
            170 => 
            array (
                'id' => 171,
                'denuncia_id' => 171,
                'victima_id' => 171,
                'created_at' => '2019-01-29 22:26:33',
                'updated_at' => '2019-01-29 22:26:33',
                'deleted_at' => NULL,
            ),
            171 => 
            array (
                'id' => 172,
                'denuncia_id' => 172,
                'victima_id' => 172,
                'created_at' => '2019-01-29 22:26:33',
                'updated_at' => '2019-01-29 22:26:33',
                'deleted_at' => NULL,
            ),
            172 => 
            array (
                'id' => 173,
                'denuncia_id' => 173,
                'victima_id' => 173,
                'created_at' => '2019-01-29 22:26:34',
                'updated_at' => '2019-01-29 22:26:34',
                'deleted_at' => NULL,
            ),
            173 => 
            array (
                'id' => 174,
                'denuncia_id' => 174,
                'victima_id' => 174,
                'created_at' => '2019-01-29 22:26:34',
                'updated_at' => '2019-01-29 22:26:34',
                'deleted_at' => NULL,
            ),
            174 => 
            array (
                'id' => 175,
                'denuncia_id' => 175,
                'victima_id' => 175,
                'created_at' => '2019-01-29 22:26:34',
                'updated_at' => '2019-01-29 22:26:34',
                'deleted_at' => NULL,
            ),
            175 => 
            array (
                'id' => 176,
                'denuncia_id' => 176,
                'victima_id' => 176,
                'created_at' => '2019-01-29 22:26:34',
                'updated_at' => '2019-01-29 22:26:34',
                'deleted_at' => NULL,
            ),
            176 => 
            array (
                'id' => 177,
                'denuncia_id' => 177,
                'victima_id' => 177,
                'created_at' => '2019-01-29 22:26:34',
                'updated_at' => '2019-01-29 22:26:34',
                'deleted_at' => NULL,
            ),
            177 => 
            array (
                'id' => 178,
                'denuncia_id' => 178,
                'victima_id' => 178,
                'created_at' => '2019-01-29 22:26:34',
                'updated_at' => '2019-01-29 22:26:34',
                'deleted_at' => NULL,
            ),
            178 => 
            array (
                'id' => 179,
                'denuncia_id' => 179,
                'victima_id' => 179,
                'created_at' => '2019-01-29 22:26:34',
                'updated_at' => '2019-01-29 22:26:34',
                'deleted_at' => NULL,
            ),
            179 => 
            array (
                'id' => 180,
                'denuncia_id' => 180,
                'victima_id' => 180,
                'created_at' => '2019-01-29 22:26:35',
                'updated_at' => '2019-01-29 22:26:35',
                'deleted_at' => NULL,
            ),
            180 => 
            array (
                'id' => 181,
                'denuncia_id' => 181,
                'victima_id' => 181,
                'created_at' => '2019-01-29 22:26:35',
                'updated_at' => '2019-01-29 22:26:35',
                'deleted_at' => NULL,
            ),
            181 => 
            array (
                'id' => 182,
                'denuncia_id' => 182,
                'victima_id' => 182,
                'created_at' => '2019-01-29 22:26:35',
                'updated_at' => '2019-01-29 22:26:35',
                'deleted_at' => NULL,
            ),
            182 => 
            array (
                'id' => 183,
                'denuncia_id' => 183,
                'victima_id' => 183,
                'created_at' => '2019-01-29 22:26:35',
                'updated_at' => '2019-01-29 22:26:35',
                'deleted_at' => NULL,
            ),
            183 => 
            array (
                'id' => 184,
                'denuncia_id' => 184,
                'victima_id' => 184,
                'created_at' => '2019-01-29 22:26:35',
                'updated_at' => '2019-01-29 22:26:35',
                'deleted_at' => NULL,
            ),
            184 => 
            array (
                'id' => 185,
                'denuncia_id' => 185,
                'victima_id' => 185,
                'created_at' => '2019-01-29 22:26:36',
                'updated_at' => '2019-01-29 22:26:36',
                'deleted_at' => NULL,
            ),
            185 => 
            array (
                'id' => 186,
                'denuncia_id' => 186,
                'victima_id' => 186,
                'created_at' => '2019-01-29 22:26:36',
                'updated_at' => '2019-01-29 22:26:36',
                'deleted_at' => NULL,
            ),
            186 => 
            array (
                'id' => 187,
                'denuncia_id' => 187,
                'victima_id' => 187,
                'created_at' => '2019-01-29 22:26:36',
                'updated_at' => '2019-01-29 22:26:36',
                'deleted_at' => NULL,
            ),
            187 => 
            array (
                'id' => 188,
                'denuncia_id' => 188,
                'victima_id' => 188,
                'created_at' => '2019-01-29 22:26:36',
                'updated_at' => '2019-01-29 22:26:36',
                'deleted_at' => NULL,
            ),
            188 => 
            array (
                'id' => 189,
                'denuncia_id' => 189,
                'victima_id' => 189,
                'created_at' => '2019-01-29 22:26:36',
                'updated_at' => '2019-01-29 22:26:36',
                'deleted_at' => NULL,
            ),
            189 => 
            array (
                'id' => 190,
                'denuncia_id' => 190,
                'victima_id' => 190,
                'created_at' => '2019-01-29 22:26:36',
                'updated_at' => '2019-01-29 22:26:36',
                'deleted_at' => NULL,
            ),
            190 => 
            array (
                'id' => 191,
                'denuncia_id' => 191,
                'victima_id' => 191,
                'created_at' => '2019-01-29 22:26:37',
                'updated_at' => '2019-01-29 22:26:37',
                'deleted_at' => NULL,
            ),
            191 => 
            array (
                'id' => 192,
                'denuncia_id' => 192,
                'victima_id' => 192,
                'created_at' => '2019-01-29 22:26:37',
                'updated_at' => '2019-01-29 22:26:37',
                'deleted_at' => NULL,
            ),
            192 => 
            array (
                'id' => 193,
                'denuncia_id' => 193,
                'victima_id' => 193,
                'created_at' => '2019-01-29 22:26:37',
                'updated_at' => '2019-01-29 22:26:37',
                'deleted_at' => NULL,
            ),
            193 => 
            array (
                'id' => 194,
                'denuncia_id' => 195,
                'victima_id' => 194,
                'created_at' => '2019-01-29 22:26:37',
                'updated_at' => '2019-01-29 22:26:37',
                'deleted_at' => NULL,
            ),
            194 => 
            array (
                'id' => 195,
                'denuncia_id' => 196,
                'victima_id' => 195,
                'created_at' => '2019-01-29 22:26:37',
                'updated_at' => '2019-01-29 22:26:37',
                'deleted_at' => NULL,
            ),
            195 => 
            array (
                'id' => 196,
                'denuncia_id' => 197,
                'victima_id' => 196,
                'created_at' => '2019-01-29 22:26:37',
                'updated_at' => '2019-01-29 22:26:37',
                'deleted_at' => NULL,
            ),
            196 => 
            array (
                'id' => 197,
                'denuncia_id' => 198,
                'victima_id' => 197,
                'created_at' => '2019-01-29 22:26:38',
                'updated_at' => '2019-01-29 22:26:38',
                'deleted_at' => NULL,
            ),
            197 => 
            array (
                'id' => 198,
                'denuncia_id' => 199,
                'victima_id' => 198,
                'created_at' => '2019-01-29 22:26:38',
                'updated_at' => '2019-01-29 22:26:38',
                'deleted_at' => NULL,
            ),
            198 => 
            array (
                'id' => 199,
                'denuncia_id' => 200,
                'victima_id' => 199,
                'created_at' => '2019-01-29 22:26:38',
                'updated_at' => '2019-01-29 22:26:38',
                'deleted_at' => NULL,
            ),
            199 => 
            array (
                'id' => 200,
                'denuncia_id' => 201,
                'victima_id' => 200,
                'created_at' => '2019-01-29 22:26:38',
                'updated_at' => '2019-01-29 22:26:38',
                'deleted_at' => NULL,
            ),
            200 => 
            array (
                'id' => 201,
                'denuncia_id' => 202,
                'victima_id' => 201,
                'created_at' => '2019-01-29 22:26:38',
                'updated_at' => '2019-01-29 22:26:38',
                'deleted_at' => NULL,
            ),
            201 => 
            array (
                'id' => 202,
                'denuncia_id' => 203,
                'victima_id' => 202,
                'created_at' => '2019-01-29 22:26:38',
                'updated_at' => '2019-01-29 22:26:38',
                'deleted_at' => NULL,
            ),
            202 => 
            array (
                'id' => 203,
                'denuncia_id' => 204,
                'victima_id' => 203,
                'created_at' => '2019-01-29 22:26:38',
                'updated_at' => '2019-01-29 22:26:38',
                'deleted_at' => NULL,
            ),
            203 => 
            array (
                'id' => 204,
                'denuncia_id' => 205,
                'victima_id' => 204,
                'created_at' => '2019-01-29 22:26:38',
                'updated_at' => '2019-01-29 22:26:38',
                'deleted_at' => NULL,
            ),
            204 => 
            array (
                'id' => 205,
                'denuncia_id' => 206,
                'victima_id' => 205,
                'created_at' => '2019-01-29 22:26:39',
                'updated_at' => '2019-01-29 22:26:39',
                'deleted_at' => NULL,
            ),
            205 => 
            array (
                'id' => 206,
                'denuncia_id' => 207,
                'victima_id' => 206,
                'created_at' => '2019-01-29 22:26:39',
                'updated_at' => '2019-01-29 22:26:39',
                'deleted_at' => NULL,
            ),
            206 => 
            array (
                'id' => 207,
                'denuncia_id' => 208,
                'victima_id' => 207,
                'created_at' => '2019-01-29 22:26:39',
                'updated_at' => '2019-01-29 22:26:39',
                'deleted_at' => NULL,
            ),
            207 => 
            array (
                'id' => 208,
                'denuncia_id' => 209,
                'victima_id' => 208,
                'created_at' => '2019-01-29 22:26:39',
                'updated_at' => '2019-01-29 22:26:39',
                'deleted_at' => NULL,
            ),
            208 => 
            array (
                'id' => 209,
                'denuncia_id' => 210,
                'victima_id' => 209,
                'created_at' => '2019-01-29 22:26:39',
                'updated_at' => '2019-01-29 22:26:39',
                'deleted_at' => NULL,
            ),
            209 => 
            array (
                'id' => 210,
                'denuncia_id' => 211,
                'victima_id' => 210,
                'created_at' => '2019-01-29 22:26:39',
                'updated_at' => '2019-01-29 22:26:39',
                'deleted_at' => NULL,
            ),
            210 => 
            array (
                'id' => 211,
                'denuncia_id' => 211,
                'victima_id' => 211,
                'created_at' => '2019-01-29 22:26:39',
                'updated_at' => '2019-01-29 22:26:39',
                'deleted_at' => NULL,
            ),
            211 => 
            array (
                'id' => 212,
                'denuncia_id' => 212,
                'victima_id' => 212,
                'created_at' => '2019-01-29 22:26:40',
                'updated_at' => '2019-01-29 22:26:40',
                'deleted_at' => NULL,
            ),
            212 => 
            array (
                'id' => 213,
                'denuncia_id' => 213,
                'victima_id' => 213,
                'created_at' => '2019-01-29 22:26:40',
                'updated_at' => '2019-01-29 22:26:40',
                'deleted_at' => NULL,
            ),
            213 => 
            array (
                'id' => 214,
                'denuncia_id' => 214,
                'victima_id' => 214,
                'created_at' => '2019-01-29 22:26:40',
                'updated_at' => '2019-01-29 22:26:40',
                'deleted_at' => NULL,
            ),
            214 => 
            array (
                'id' => 215,
                'denuncia_id' => 214,
                'victima_id' => 215,
                'created_at' => '2019-01-29 22:26:40',
                'updated_at' => '2019-01-29 22:26:40',
                'deleted_at' => NULL,
            ),
            215 => 
            array (
                'id' => 216,
                'denuncia_id' => 215,
                'victima_id' => 216,
                'created_at' => '2019-01-29 22:26:40',
                'updated_at' => '2019-01-29 22:26:40',
                'deleted_at' => NULL,
            ),
            216 => 
            array (
                'id' => 217,
                'denuncia_id' => 217,
                'victima_id' => 217,
                'created_at' => '2019-01-29 22:26:40',
                'updated_at' => '2019-01-29 22:26:40',
                'deleted_at' => NULL,
            ),
            217 => 
            array (
                'id' => 218,
                'denuncia_id' => 218,
                'victima_id' => 218,
                'created_at' => '2019-01-29 22:26:41',
                'updated_at' => '2019-01-29 22:26:41',
                'deleted_at' => NULL,
            ),
            218 => 
            array (
                'id' => 219,
                'denuncia_id' => 219,
                'victima_id' => 219,
                'created_at' => '2019-01-29 22:26:41',
                'updated_at' => '2019-01-29 22:26:41',
                'deleted_at' => NULL,
            ),
            219 => 
            array (
                'id' => 220,
                'denuncia_id' => 220,
                'victima_id' => 220,
                'created_at' => '2019-01-29 22:26:41',
                'updated_at' => '2019-01-29 22:26:41',
                'deleted_at' => NULL,
            ),
            220 => 
            array (
                'id' => 221,
                'denuncia_id' => 221,
                'victima_id' => 221,
                'created_at' => '2019-01-29 22:26:41',
                'updated_at' => '2019-01-29 22:26:41',
                'deleted_at' => NULL,
            ),
            221 => 
            array (
                'id' => 222,
                'denuncia_id' => 222,
                'victima_id' => 222,
                'created_at' => '2019-01-29 22:26:41',
                'updated_at' => '2019-01-29 22:26:41',
                'deleted_at' => NULL,
            ),
            222 => 
            array (
                'id' => 223,
                'denuncia_id' => 223,
                'victima_id' => 223,
                'created_at' => '2019-01-29 22:26:41',
                'updated_at' => '2019-01-29 22:26:41',
                'deleted_at' => NULL,
            ),
            223 => 
            array (
                'id' => 224,
                'denuncia_id' => 223,
                'victima_id' => 224,
                'created_at' => '2019-01-29 22:26:41',
                'updated_at' => '2019-01-29 22:26:41',
                'deleted_at' => NULL,
            ),
            224 => 
            array (
                'id' => 225,
                'denuncia_id' => 224,
                'victima_id' => 225,
                'created_at' => '2019-01-29 22:26:41',
                'updated_at' => '2019-01-29 22:26:41',
                'deleted_at' => NULL,
            ),
            225 => 
            array (
                'id' => 226,
                'denuncia_id' => 225,
                'victima_id' => 226,
                'created_at' => '2019-01-29 22:26:41',
                'updated_at' => '2019-01-29 22:26:41',
                'deleted_at' => NULL,
            ),
            226 => 
            array (
                'id' => 227,
                'denuncia_id' => 226,
                'victima_id' => 227,
                'created_at' => '2019-01-29 22:26:42',
                'updated_at' => '2019-01-29 22:26:42',
                'deleted_at' => NULL,
            ),
            227 => 
            array (
                'id' => 228,
                'denuncia_id' => 227,
                'victima_id' => 228,
                'created_at' => '2019-01-29 22:26:42',
                'updated_at' => '2019-01-29 22:26:42',
                'deleted_at' => NULL,
            ),
            228 => 
            array (
                'id' => 229,
                'denuncia_id' => 227,
                'victima_id' => 229,
                'created_at' => '2019-01-29 22:26:42',
                'updated_at' => '2019-01-29 22:26:42',
                'deleted_at' => NULL,
            ),
            229 => 
            array (
                'id' => 230,
                'denuncia_id' => 228,
                'victima_id' => 230,
                'created_at' => '2019-01-29 22:26:42',
                'updated_at' => '2019-01-29 22:26:42',
                'deleted_at' => NULL,
            ),
            230 => 
            array (
                'id' => 231,
                'denuncia_id' => 229,
                'victima_id' => 231,
                'created_at' => '2019-01-29 22:26:42',
                'updated_at' => '2019-01-29 22:26:42',
                'deleted_at' => NULL,
            ),
            231 => 
            array (
                'id' => 232,
                'denuncia_id' => 230,
                'victima_id' => 232,
                'created_at' => '2019-01-29 22:26:42',
                'updated_at' => '2019-01-29 22:26:42',
                'deleted_at' => NULL,
            ),
            232 => 
            array (
                'id' => 233,
                'denuncia_id' => 232,
                'victima_id' => 233,
                'created_at' => '2019-01-29 22:26:42',
                'updated_at' => '2019-01-29 22:26:42',
                'deleted_at' => NULL,
            ),
            233 => 
            array (
                'id' => 234,
                'denuncia_id' => 233,
                'victima_id' => 234,
                'created_at' => '2019-01-29 22:26:42',
                'updated_at' => '2019-01-29 22:26:42',
                'deleted_at' => NULL,
            ),
            234 => 
            array (
                'id' => 235,
                'denuncia_id' => 234,
                'victima_id' => 235,
                'created_at' => '2019-01-29 22:26:43',
                'updated_at' => '2019-01-29 22:26:43',
                'deleted_at' => NULL,
            ),
            235 => 
            array (
                'id' => 236,
                'denuncia_id' => 235,
                'victima_id' => 236,
                'created_at' => '2019-01-29 22:26:43',
                'updated_at' => '2019-01-29 22:26:43',
                'deleted_at' => NULL,
            ),
            236 => 
            array (
                'id' => 237,
                'denuncia_id' => 236,
                'victima_id' => 237,
                'created_at' => '2019-01-29 22:26:43',
                'updated_at' => '2019-01-29 22:26:43',
                'deleted_at' => NULL,
            ),
            237 => 
            array (
                'id' => 238,
                'denuncia_id' => 237,
                'victima_id' => 238,
                'created_at' => '2019-01-29 22:26:43',
                'updated_at' => '2019-01-29 22:26:43',
                'deleted_at' => NULL,
            ),
            238 => 
            array (
                'id' => 239,
                'denuncia_id' => 238,
                'victima_id' => 239,
                'created_at' => '2019-01-29 22:26:43',
                'updated_at' => '2019-01-29 22:26:43',
                'deleted_at' => NULL,
            ),
            239 => 
            array (
                'id' => 240,
                'denuncia_id' => 239,
                'victima_id' => 240,
                'created_at' => '2019-01-29 22:26:43',
                'updated_at' => '2019-01-29 22:26:43',
                'deleted_at' => NULL,
            ),
            240 => 
            array (
                'id' => 241,
                'denuncia_id' => 240,
                'victima_id' => 241,
                'created_at' => '2019-01-29 22:26:43',
                'updated_at' => '2019-01-29 22:26:43',
                'deleted_at' => NULL,
            ),
            241 => 
            array (
                'id' => 242,
                'denuncia_id' => 241,
                'victima_id' => 242,
                'created_at' => '2019-01-29 22:26:44',
                'updated_at' => '2019-01-29 22:26:44',
                'deleted_at' => NULL,
            ),
            242 => 
            array (
                'id' => 243,
                'denuncia_id' => 242,
                'victima_id' => 243,
                'created_at' => '2019-01-29 22:26:44',
                'updated_at' => '2019-01-29 22:26:44',
                'deleted_at' => NULL,
            ),
            243 => 
            array (
                'id' => 244,
                'denuncia_id' => 243,
                'victima_id' => 244,
                'created_at' => '2019-01-29 22:26:44',
                'updated_at' => '2019-01-29 22:26:44',
                'deleted_at' => NULL,
            ),
            244 => 
            array (
                'id' => 245,
                'denuncia_id' => 244,
                'victima_id' => 245,
                'created_at' => '2019-01-29 22:26:44',
                'updated_at' => '2019-01-29 22:26:44',
                'deleted_at' => NULL,
            ),
            245 => 
            array (
                'id' => 246,
                'denuncia_id' => 245,
                'victima_id' => 246,
                'created_at' => '2019-01-29 22:26:44',
                'updated_at' => '2019-01-29 22:26:44',
                'deleted_at' => NULL,
            ),
            246 => 
            array (
                'id' => 247,
                'denuncia_id' => 246,
                'victima_id' => 247,
                'created_at' => '2019-01-29 22:26:44',
                'updated_at' => '2019-01-29 22:26:44',
                'deleted_at' => NULL,
            ),
            247 => 
            array (
                'id' => 248,
                'denuncia_id' => 247,
                'victima_id' => 248,
                'created_at' => '2019-01-29 22:26:44',
                'updated_at' => '2019-01-29 22:26:44',
                'deleted_at' => NULL,
            ),
            248 => 
            array (
                'id' => 249,
                'denuncia_id' => 248,
                'victima_id' => 249,
                'created_at' => '2019-01-29 22:26:45',
                'updated_at' => '2019-01-29 22:26:45',
                'deleted_at' => NULL,
            ),
            249 => 
            array (
                'id' => 250,
                'denuncia_id' => 249,
                'victima_id' => 250,
                'created_at' => '2019-01-29 22:26:45',
                'updated_at' => '2019-01-29 22:26:45',
                'deleted_at' => NULL,
            ),
            250 => 
            array (
                'id' => 251,
                'denuncia_id' => 250,
                'victima_id' => 251,
                'created_at' => '2019-01-29 22:26:45',
                'updated_at' => '2019-01-29 22:26:45',
                'deleted_at' => NULL,
            ),
            251 => 
            array (
                'id' => 252,
                'denuncia_id' => 251,
                'victima_id' => 252,
                'created_at' => '2019-01-29 22:26:45',
                'updated_at' => '2019-01-29 22:26:45',
                'deleted_at' => NULL,
            ),
            252 => 
            array (
                'id' => 253,
                'denuncia_id' => 252,
                'victima_id' => 253,
                'created_at' => '2019-01-29 22:26:45',
                'updated_at' => '2019-01-29 22:26:45',
                'deleted_at' => NULL,
            ),
            253 => 
            array (
                'id' => 254,
                'denuncia_id' => 253,
                'victima_id' => 254,
                'created_at' => '2019-01-29 22:26:45',
                'updated_at' => '2019-01-29 22:26:45',
                'deleted_at' => NULL,
            ),
            254 => 
            array (
                'id' => 255,
                'denuncia_id' => 254,
                'victima_id' => 255,
                'created_at' => '2019-01-29 22:26:45',
                'updated_at' => '2019-01-29 22:26:45',
                'deleted_at' => NULL,
            ),
            255 => 
            array (
                'id' => 256,
                'denuncia_id' => 255,
                'victima_id' => 256,
                'created_at' => '2019-01-29 22:26:45',
                'updated_at' => '2019-01-29 22:26:45',
                'deleted_at' => NULL,
            ),
            256 => 
            array (
                'id' => 257,
                'denuncia_id' => 255,
                'victima_id' => 257,
                'created_at' => '2019-01-29 22:26:45',
                'updated_at' => '2019-01-29 22:26:45',
                'deleted_at' => NULL,
            ),
            257 => 
            array (
                'id' => 258,
                'denuncia_id' => 256,
                'victima_id' => 258,
                'created_at' => '2019-01-29 22:26:46',
                'updated_at' => '2019-01-29 22:26:46',
                'deleted_at' => NULL,
            ),
            258 => 
            array (
                'id' => 259,
                'denuncia_id' => 257,
                'victima_id' => 259,
                'created_at' => '2019-01-29 22:26:46',
                'updated_at' => '2019-01-29 22:26:46',
                'deleted_at' => NULL,
            ),
            259 => 
            array (
                'id' => 260,
                'denuncia_id' => 258,
                'victima_id' => 260,
                'created_at' => '2019-01-29 22:26:46',
                'updated_at' => '2019-01-29 22:26:46',
                'deleted_at' => NULL,
            ),
            260 => 
            array (
                'id' => 261,
                'denuncia_id' => 259,
                'victima_id' => 261,
                'created_at' => '2019-01-29 22:26:46',
                'updated_at' => '2019-01-29 22:26:46',
                'deleted_at' => NULL,
            ),
            261 => 
            array (
                'id' => 262,
                'denuncia_id' => 260,
                'victima_id' => 262,
                'created_at' => '2019-01-29 22:26:46',
                'updated_at' => '2019-01-29 22:26:46',
                'deleted_at' => NULL,
            ),
            262 => 
            array (
                'id' => 263,
                'denuncia_id' => 261,
                'victima_id' => 263,
                'created_at' => '2019-01-29 22:26:46',
                'updated_at' => '2019-01-29 22:26:46',
                'deleted_at' => NULL,
            ),
            263 => 
            array (
                'id' => 264,
                'denuncia_id' => 262,
                'victima_id' => 264,
                'created_at' => '2019-01-29 22:26:46',
                'updated_at' => '2019-01-29 22:26:46',
                'deleted_at' => NULL,
            ),
            264 => 
            array (
                'id' => 265,
                'denuncia_id' => 262,
                'victima_id' => 265,
                'created_at' => '2019-01-29 22:26:46',
                'updated_at' => '2019-01-29 22:26:46',
                'deleted_at' => NULL,
            ),
            265 => 
            array (
                'id' => 266,
                'denuncia_id' => 262,
                'victima_id' => 266,
                'created_at' => '2019-01-29 22:26:46',
                'updated_at' => '2019-01-29 22:26:46',
                'deleted_at' => NULL,
            ),
            266 => 
            array (
                'id' => 267,
                'denuncia_id' => 262,
                'victima_id' => 267,
                'created_at' => '2019-01-29 22:26:46',
                'updated_at' => '2019-01-29 22:26:46',
                'deleted_at' => NULL,
            ),
            267 => 
            array (
                'id' => 268,
                'denuncia_id' => 262,
                'victima_id' => 268,
                'created_at' => '2019-01-29 22:26:46',
                'updated_at' => '2019-01-29 22:26:46',
                'deleted_at' => NULL,
            ),
            268 => 
            array (
                'id' => 269,
                'denuncia_id' => 262,
                'victima_id' => 269,
                'created_at' => '2019-01-29 22:26:46',
                'updated_at' => '2019-01-29 22:26:46',
                'deleted_at' => NULL,
            ),
            269 => 
            array (
                'id' => 270,
                'denuncia_id' => 262,
                'victima_id' => 270,
                'created_at' => '2019-01-29 22:26:46',
                'updated_at' => '2019-01-29 22:26:46',
                'deleted_at' => NULL,
            ),
            270 => 
            array (
                'id' => 271,
                'denuncia_id' => 263,
                'victima_id' => 271,
                'created_at' => '2019-01-29 22:26:46',
                'updated_at' => '2019-01-29 22:26:46',
                'deleted_at' => NULL,
            ),
            271 => 
            array (
                'id' => 272,
                'denuncia_id' => 264,
                'victima_id' => 272,
                'created_at' => '2019-01-29 22:26:47',
                'updated_at' => '2019-01-29 22:26:47',
                'deleted_at' => NULL,
            ),
            272 => 
            array (
                'id' => 273,
                'denuncia_id' => 265,
                'victima_id' => 273,
                'created_at' => '2019-01-29 22:26:47',
                'updated_at' => '2019-01-29 22:26:47',
                'deleted_at' => NULL,
            ),
            273 => 
            array (
                'id' => 274,
                'denuncia_id' => 266,
                'victima_id' => 274,
                'created_at' => '2019-01-29 22:26:47',
                'updated_at' => '2019-01-29 22:26:47',
                'deleted_at' => NULL,
            ),
            274 => 
            array (
                'id' => 275,
                'denuncia_id' => 267,
                'victima_id' => 275,
                'created_at' => '2019-01-29 22:26:47',
                'updated_at' => '2019-01-29 22:26:47',
                'deleted_at' => NULL,
            ),
            275 => 
            array (
                'id' => 276,
                'denuncia_id' => 268,
                'victima_id' => 276,
                'created_at' => '2019-01-29 22:26:47',
                'updated_at' => '2019-01-29 22:26:47',
                'deleted_at' => NULL,
            ),
            276 => 
            array (
                'id' => 277,
                'denuncia_id' => 269,
                'victima_id' => 277,
                'created_at' => '2019-01-29 22:26:47',
                'updated_at' => '2019-01-29 22:26:47',
                'deleted_at' => NULL,
            ),
            277 => 
            array (
                'id' => 278,
                'denuncia_id' => 270,
                'victima_id' => 278,
                'created_at' => '2019-01-29 22:26:47',
                'updated_at' => '2019-01-29 22:26:47',
                'deleted_at' => NULL,
            ),
            278 => 
            array (
                'id' => 279,
                'denuncia_id' => 271,
                'victima_id' => 279,
                'created_at' => '2019-01-29 22:26:48',
                'updated_at' => '2019-01-29 22:26:48',
                'deleted_at' => NULL,
            ),
            279 => 
            array (
                'id' => 280,
                'denuncia_id' => 272,
                'victima_id' => 280,
                'created_at' => '2019-01-29 22:26:48',
                'updated_at' => '2019-01-29 22:26:48',
                'deleted_at' => NULL,
            ),
            280 => 
            array (
                'id' => 281,
                'denuncia_id' => 273,
                'victima_id' => 281,
                'created_at' => '2019-01-29 22:26:48',
                'updated_at' => '2019-01-29 22:26:48',
                'deleted_at' => NULL,
            ),
            281 => 
            array (
                'id' => 282,
                'denuncia_id' => 274,
                'victima_id' => 282,
                'created_at' => '2019-01-29 22:26:48',
                'updated_at' => '2019-01-29 22:26:48',
                'deleted_at' => NULL,
            ),
            282 => 
            array (
                'id' => 283,
                'denuncia_id' => 275,
                'victima_id' => 283,
                'created_at' => '2019-01-29 22:26:48',
                'updated_at' => '2019-01-29 22:26:48',
                'deleted_at' => NULL,
            ),
            283 => 
            array (
                'id' => 284,
                'denuncia_id' => 276,
                'victima_id' => 284,
                'created_at' => '2019-01-29 22:26:48',
                'updated_at' => '2019-01-29 22:26:48',
                'deleted_at' => NULL,
            ),
            284 => 
            array (
                'id' => 285,
                'denuncia_id' => 277,
                'victima_id' => 285,
                'created_at' => '2019-01-29 22:26:49',
                'updated_at' => '2019-01-29 22:26:49',
                'deleted_at' => NULL,
            ),
            285 => 
            array (
                'id' => 286,
                'denuncia_id' => 278,
                'victima_id' => 286,
                'created_at' => '2019-01-29 22:26:49',
                'updated_at' => '2019-01-29 22:26:49',
                'deleted_at' => NULL,
            ),
            286 => 
            array (
                'id' => 287,
                'denuncia_id' => 279,
                'victima_id' => 287,
                'created_at' => '2019-01-29 22:26:49',
                'updated_at' => '2019-01-29 22:26:49',
                'deleted_at' => NULL,
            ),
            287 => 
            array (
                'id' => 288,
                'denuncia_id' => 280,
                'victima_id' => 288,
                'created_at' => '2019-01-29 22:26:49',
                'updated_at' => '2019-01-29 22:26:49',
                'deleted_at' => NULL,
            ),
            288 => 
            array (
                'id' => 289,
                'denuncia_id' => 281,
                'victima_id' => 289,
                'created_at' => '2019-01-29 22:26:49',
                'updated_at' => '2019-01-29 22:26:49',
                'deleted_at' => NULL,
            ),
            289 => 
            array (
                'id' => 290,
                'denuncia_id' => 282,
                'victima_id' => 290,
                'created_at' => '2019-01-29 22:26:49',
                'updated_at' => '2019-01-29 22:26:49',
                'deleted_at' => NULL,
            ),
            290 => 
            array (
                'id' => 291,
                'denuncia_id' => 283,
                'victima_id' => 291,
                'created_at' => '2019-01-29 22:26:50',
                'updated_at' => '2019-01-29 22:26:50',
                'deleted_at' => NULL,
            ),
            291 => 
            array (
                'id' => 292,
                'denuncia_id' => 284,
                'victima_id' => 292,
                'created_at' => '2019-01-29 22:26:50',
                'updated_at' => '2019-01-29 22:26:50',
                'deleted_at' => NULL,
            ),
            292 => 
            array (
                'id' => 293,
                'denuncia_id' => 285,
                'victima_id' => 293,
                'created_at' => '2019-01-29 22:26:50',
                'updated_at' => '2019-01-29 22:26:50',
                'deleted_at' => NULL,
            ),
            293 => 
            array (
                'id' => 294,
                'denuncia_id' => 290,
                'victima_id' => 294,
                'created_at' => '2019-01-29 22:26:51',
                'updated_at' => '2019-01-29 22:26:51',
                'deleted_at' => NULL,
            ),
            294 => 
            array (
                'id' => 295,
                'denuncia_id' => 292,
                'victima_id' => 295,
                'created_at' => '2019-01-29 22:26:51',
                'updated_at' => '2019-01-29 22:26:51',
                'deleted_at' => NULL,
            ),
            295 => 
            array (
                'id' => 296,
                'denuncia_id' => 293,
                'victima_id' => 296,
                'created_at' => '2019-01-29 22:26:51',
                'updated_at' => '2019-01-29 22:26:51',
                'deleted_at' => NULL,
            ),
            296 => 
            array (
                'id' => 297,
                'denuncia_id' => 294,
                'victima_id' => 297,
                'created_at' => '2019-01-29 22:26:51',
                'updated_at' => '2019-01-29 22:26:51',
                'deleted_at' => NULL,
            ),
            297 => 
            array (
                'id' => 298,
                'denuncia_id' => 295,
                'victima_id' => 298,
                'created_at' => '2019-01-29 22:26:51',
                'updated_at' => '2019-01-29 22:26:51',
                'deleted_at' => NULL,
            ),
            298 => 
            array (
                'id' => 299,
                'denuncia_id' => 296,
                'victima_id' => 299,
                'created_at' => '2019-01-29 22:26:52',
                'updated_at' => '2019-01-29 22:26:52',
                'deleted_at' => NULL,
            ),
            299 => 
            array (
                'id' => 300,
                'denuncia_id' => 297,
                'victima_id' => 300,
                'created_at' => '2019-01-29 22:26:52',
                'updated_at' => '2019-01-29 22:26:52',
                'deleted_at' => NULL,
            ),
            300 => 
            array (
                'id' => 301,
                'denuncia_id' => 298,
                'victima_id' => 301,
                'created_at' => '2019-01-29 22:26:52',
                'updated_at' => '2019-01-29 22:26:52',
                'deleted_at' => NULL,
            ),
            301 => 
            array (
                'id' => 302,
                'denuncia_id' => 298,
                'victima_id' => 302,
                'created_at' => '2019-01-29 22:26:52',
                'updated_at' => '2019-01-29 22:26:52',
                'deleted_at' => NULL,
            ),
            302 => 
            array (
                'id' => 303,
                'denuncia_id' => 299,
                'victima_id' => 303,
                'created_at' => '2019-01-29 22:26:52',
                'updated_at' => '2019-01-29 22:26:52',
                'deleted_at' => NULL,
            ),
            303 => 
            array (
                'id' => 304,
                'denuncia_id' => 300,
                'victima_id' => 304,
                'created_at' => '2019-01-29 22:26:52',
                'updated_at' => '2019-01-29 22:26:52',
                'deleted_at' => NULL,
            ),
            304 => 
            array (
                'id' => 305,
                'denuncia_id' => 301,
                'victima_id' => 305,
                'created_at' => '2019-01-29 22:26:52',
                'updated_at' => '2019-01-29 22:26:52',
                'deleted_at' => NULL,
            ),
            305 => 
            array (
                'id' => 306,
                'denuncia_id' => 302,
                'victima_id' => 306,
                'created_at' => '2019-01-29 22:26:53',
                'updated_at' => '2019-01-29 22:26:53',
                'deleted_at' => NULL,
            ),
            306 => 
            array (
                'id' => 307,
                'denuncia_id' => 303,
                'victima_id' => 307,
                'created_at' => '2019-01-29 22:26:53',
                'updated_at' => '2019-01-29 22:26:53',
                'deleted_at' => NULL,
            ),
            307 => 
            array (
                'id' => 308,
                'denuncia_id' => 304,
                'victima_id' => 308,
                'created_at' => '2019-01-29 22:26:53',
                'updated_at' => '2019-01-29 22:26:53',
                'deleted_at' => NULL,
            ),
            308 => 
            array (
                'id' => 309,
                'denuncia_id' => 305,
                'victima_id' => 309,
                'created_at' => '2019-01-29 22:26:53',
                'updated_at' => '2019-01-29 22:26:53',
                'deleted_at' => NULL,
            ),
            309 => 
            array (
                'id' => 310,
                'denuncia_id' => 306,
                'victima_id' => 310,
                'created_at' => '2019-01-29 22:26:53',
                'updated_at' => '2019-01-29 22:26:53',
                'deleted_at' => NULL,
            ),
            310 => 
            array (
                'id' => 311,
                'denuncia_id' => 307,
                'victima_id' => 311,
                'created_at' => '2019-01-29 22:26:53',
                'updated_at' => '2019-01-29 22:26:53',
                'deleted_at' => NULL,
            ),
            311 => 
            array (
                'id' => 312,
                'denuncia_id' => 308,
                'victima_id' => 312,
                'created_at' => '2019-01-29 22:26:54',
                'updated_at' => '2019-01-29 22:26:54',
                'deleted_at' => NULL,
            ),
            312 => 
            array (
                'id' => 313,
                'denuncia_id' => 309,
                'victima_id' => 313,
                'created_at' => '2019-01-29 22:26:54',
                'updated_at' => '2019-01-29 22:26:54',
                'deleted_at' => NULL,
            ),
            313 => 
            array (
                'id' => 314,
                'denuncia_id' => 310,
                'victima_id' => 314,
                'created_at' => '2019-01-29 22:26:54',
                'updated_at' => '2019-01-29 22:26:54',
                'deleted_at' => NULL,
            ),
            314 => 
            array (
                'id' => 315,
                'denuncia_id' => 311,
                'victima_id' => 315,
                'created_at' => '2019-01-29 22:26:54',
                'updated_at' => '2019-01-29 22:26:54',
                'deleted_at' => NULL,
            ),
            315 => 
            array (
                'id' => 316,
                'denuncia_id' => 312,
                'victima_id' => 316,
                'created_at' => '2019-01-29 22:26:54',
                'updated_at' => '2019-01-29 22:26:54',
                'deleted_at' => NULL,
            ),
            316 => 
            array (
                'id' => 317,
                'denuncia_id' => 313,
                'victima_id' => 317,
                'created_at' => '2019-01-29 22:26:55',
                'updated_at' => '2019-01-29 22:26:55',
                'deleted_at' => NULL,
            ),
            317 => 
            array (
                'id' => 318,
                'denuncia_id' => 314,
                'victima_id' => 318,
                'created_at' => '2019-01-29 22:26:55',
                'updated_at' => '2019-01-29 22:26:55',
                'deleted_at' => NULL,
            ),
            318 => 
            array (
                'id' => 319,
                'denuncia_id' => 315,
                'victima_id' => 319,
                'created_at' => '2019-01-29 22:26:55',
                'updated_at' => '2019-01-29 22:26:55',
                'deleted_at' => NULL,
            ),
            319 => 
            array (
                'id' => 320,
                'denuncia_id' => 316,
                'victima_id' => 320,
                'created_at' => '2019-01-29 22:26:55',
                'updated_at' => '2019-01-29 22:26:55',
                'deleted_at' => NULL,
            ),
            320 => 
            array (
                'id' => 321,
                'denuncia_id' => 317,
                'victima_id' => 321,
                'created_at' => '2019-01-29 22:26:55',
                'updated_at' => '2019-01-29 22:26:55',
                'deleted_at' => NULL,
            ),
            321 => 
            array (
                'id' => 322,
                'denuncia_id' => 318,
                'victima_id' => 322,
                'created_at' => '2019-01-29 22:26:55',
                'updated_at' => '2019-01-29 22:26:55',
                'deleted_at' => NULL,
            ),
            322 => 
            array (
                'id' => 323,
                'denuncia_id' => 319,
                'victima_id' => 323,
                'created_at' => '2019-01-29 22:26:56',
                'updated_at' => '2019-01-29 22:26:56',
                'deleted_at' => NULL,
            ),
            323 => 
            array (
                'id' => 324,
                'denuncia_id' => 320,
                'victima_id' => 324,
                'created_at' => '2019-01-29 22:26:56',
                'updated_at' => '2019-01-29 22:26:56',
                'deleted_at' => NULL,
            ),
            324 => 
            array (
                'id' => 325,
                'denuncia_id' => 321,
                'victima_id' => 325,
                'created_at' => '2019-01-29 22:26:56',
                'updated_at' => '2019-01-29 22:26:56',
                'deleted_at' => NULL,
            ),
            325 => 
            array (
                'id' => 326,
                'denuncia_id' => 322,
                'victima_id' => 326,
                'created_at' => '2019-01-29 22:26:56',
                'updated_at' => '2019-01-29 22:26:56',
                'deleted_at' => NULL,
            ),
            326 => 
            array (
                'id' => 327,
                'denuncia_id' => 323,
                'victima_id' => 327,
                'created_at' => '2019-01-29 22:26:56',
                'updated_at' => '2019-01-29 22:26:56',
                'deleted_at' => NULL,
            ),
            327 => 
            array (
                'id' => 328,
                'denuncia_id' => 326,
                'victima_id' => 328,
                'created_at' => '2019-01-29 22:26:57',
                'updated_at' => '2019-01-29 22:26:57',
                'deleted_at' => NULL,
            ),
            328 => 
            array (
                'id' => 329,
                'denuncia_id' => 327,
                'victima_id' => 329,
                'created_at' => '2019-01-29 22:26:57',
                'updated_at' => '2019-01-29 22:26:57',
                'deleted_at' => NULL,
            ),
            329 => 
            array (
                'id' => 330,
                'denuncia_id' => 328,
                'victima_id' => 330,
                'created_at' => '2019-01-29 22:26:57',
                'updated_at' => '2019-01-29 22:26:57',
                'deleted_at' => NULL,
            ),
            330 => 
            array (
                'id' => 331,
                'denuncia_id' => 329,
                'victima_id' => 331,
                'created_at' => '2019-01-29 22:26:57',
                'updated_at' => '2019-01-29 22:26:57',
                'deleted_at' => NULL,
            ),
            331 => 
            array (
                'id' => 332,
                'denuncia_id' => 330,
                'victima_id' => 332,
                'created_at' => '2019-01-29 22:26:58',
                'updated_at' => '2019-01-29 22:26:58',
                'deleted_at' => NULL,
            ),
            332 => 
            array (
                'id' => 333,
                'denuncia_id' => 331,
                'victima_id' => 333,
                'created_at' => '2019-01-29 22:26:58',
                'updated_at' => '2019-01-29 22:26:58',
                'deleted_at' => NULL,
            ),
            333 => 
            array (
                'id' => 334,
                'denuncia_id' => 332,
                'victima_id' => 334,
                'created_at' => '2019-01-29 22:26:58',
                'updated_at' => '2019-01-29 22:26:58',
                'deleted_at' => NULL,
            ),
            334 => 
            array (
                'id' => 335,
                'denuncia_id' => 333,
                'victima_id' => 335,
                'created_at' => '2019-01-29 22:26:58',
                'updated_at' => '2019-01-29 22:26:58',
                'deleted_at' => NULL,
            ),
            335 => 
            array (
                'id' => 336,
                'denuncia_id' => 334,
                'victima_id' => 336,
                'created_at' => '2019-01-29 22:26:58',
                'updated_at' => '2019-01-29 22:26:58',
                'deleted_at' => NULL,
            ),
            336 => 
            array (
                'id' => 337,
                'denuncia_id' => 335,
                'victima_id' => 337,
                'created_at' => '2019-01-29 22:26:59',
                'updated_at' => '2019-01-29 22:26:59',
                'deleted_at' => NULL,
            ),
            337 => 
            array (
                'id' => 338,
                'denuncia_id' => 336,
                'victima_id' => 338,
                'created_at' => '2019-01-29 22:26:59',
                'updated_at' => '2019-01-29 22:26:59',
                'deleted_at' => NULL,
            ),
            338 => 
            array (
                'id' => 339,
                'denuncia_id' => 337,
                'victima_id' => 339,
                'created_at' => '2019-01-29 22:26:59',
                'updated_at' => '2019-01-29 22:26:59',
                'deleted_at' => NULL,
            ),
            339 => 
            array (
                'id' => 340,
                'denuncia_id' => 338,
                'victima_id' => 340,
                'created_at' => '2019-01-29 22:26:59',
                'updated_at' => '2019-01-29 22:26:59',
                'deleted_at' => NULL,
            ),
            340 => 
            array (
                'id' => 341,
                'denuncia_id' => 339,
                'victima_id' => 341,
                'created_at' => '2019-01-29 22:26:59',
                'updated_at' => '2019-01-29 22:26:59',
                'deleted_at' => NULL,
            ),
            341 => 
            array (
                'id' => 342,
                'denuncia_id' => 341,
                'victima_id' => 342,
                'created_at' => '2019-01-29 22:27:00',
                'updated_at' => '2019-01-29 22:27:00',
                'deleted_at' => NULL,
            ),
            342 => 
            array (
                'id' => 343,
                'denuncia_id' => 342,
                'victima_id' => 343,
                'created_at' => '2019-01-29 22:27:00',
                'updated_at' => '2019-01-29 22:27:00',
                'deleted_at' => NULL,
            ),
            343 => 
            array (
                'id' => 344,
                'denuncia_id' => 343,
                'victima_id' => 344,
                'created_at' => '2019-01-29 22:27:00',
                'updated_at' => '2019-01-29 22:27:00',
                'deleted_at' => NULL,
            ),
            344 => 
            array (
                'id' => 345,
                'denuncia_id' => 344,
                'victima_id' => 345,
                'created_at' => '2019-01-29 22:27:00',
                'updated_at' => '2019-01-29 22:27:00',
                'deleted_at' => NULL,
            ),
            345 => 
            array (
                'id' => 346,
                'denuncia_id' => 344,
                'victima_id' => 346,
                'created_at' => '2019-01-29 22:27:00',
                'updated_at' => '2019-01-29 22:27:00',
                'deleted_at' => NULL,
            ),
            346 => 
            array (
                'id' => 347,
                'denuncia_id' => 345,
                'victima_id' => 347,
                'created_at' => '2019-01-29 22:27:01',
                'updated_at' => '2019-01-29 22:27:01',
                'deleted_at' => NULL,
            ),
            347 => 
            array (
                'id' => 348,
                'denuncia_id' => 346,
                'victima_id' => 348,
                'created_at' => '2019-01-29 22:27:01',
                'updated_at' => '2019-01-29 22:27:01',
                'deleted_at' => NULL,
            ),
            348 => 
            array (
                'id' => 349,
                'denuncia_id' => 346,
                'victima_id' => 349,
                'created_at' => '2019-01-29 22:27:01',
                'updated_at' => '2019-01-29 22:27:01',
                'deleted_at' => NULL,
            ),
            349 => 
            array (
                'id' => 350,
                'denuncia_id' => 346,
                'victima_id' => 350,
                'created_at' => '2019-01-29 22:27:01',
                'updated_at' => '2019-01-29 22:27:01',
                'deleted_at' => NULL,
            ),
            350 => 
            array (
                'id' => 351,
                'denuncia_id' => 347,
                'victima_id' => 351,
                'created_at' => '2019-01-29 22:27:01',
                'updated_at' => '2019-01-29 22:27:01',
                'deleted_at' => NULL,
            ),
            351 => 
            array (
                'id' => 352,
                'denuncia_id' => 348,
                'victima_id' => 352,
                'created_at' => '2019-01-29 22:27:01',
                'updated_at' => '2019-01-29 22:27:01',
                'deleted_at' => NULL,
            ),
            352 => 
            array (
                'id' => 353,
                'denuncia_id' => 349,
                'victima_id' => 353,
                'created_at' => '2019-01-29 22:27:01',
                'updated_at' => '2019-01-29 22:27:01',
                'deleted_at' => NULL,
            ),
            353 => 
            array (
                'id' => 354,
                'denuncia_id' => 350,
                'victima_id' => 354,
                'created_at' => '2019-01-29 22:27:01',
                'updated_at' => '2019-01-29 22:27:01',
                'deleted_at' => NULL,
            ),
            354 => 
            array (
                'id' => 355,
                'denuncia_id' => 351,
                'victima_id' => 355,
                'created_at' => '2019-01-29 22:27:02',
                'updated_at' => '2019-01-29 22:27:02',
                'deleted_at' => NULL,
            ),
            355 => 
            array (
                'id' => 356,
                'denuncia_id' => 352,
                'victima_id' => 356,
                'created_at' => '2019-01-29 22:27:02',
                'updated_at' => '2019-01-29 22:27:02',
                'deleted_at' => NULL,
            ),
            356 => 
            array (
                'id' => 357,
                'denuncia_id' => 353,
                'victima_id' => 357,
                'created_at' => '2019-01-29 22:27:02',
                'updated_at' => '2019-01-29 22:27:02',
                'deleted_at' => NULL,
            ),
            357 => 
            array (
                'id' => 358,
                'denuncia_id' => 354,
                'victima_id' => 358,
                'created_at' => '2019-01-29 22:27:02',
                'updated_at' => '2019-01-29 22:27:02',
                'deleted_at' => NULL,
            ),
            358 => 
            array (
                'id' => 359,
                'denuncia_id' => 355,
                'victima_id' => 359,
                'created_at' => '2019-01-29 22:27:02',
                'updated_at' => '2019-01-29 22:27:02',
                'deleted_at' => NULL,
            ),
            359 => 
            array (
                'id' => 360,
                'denuncia_id' => 355,
                'victima_id' => 360,
                'created_at' => '2019-01-29 22:27:02',
                'updated_at' => '2019-01-29 22:27:02',
                'deleted_at' => NULL,
            ),
            360 => 
            array (
                'id' => 361,
                'denuncia_id' => 356,
                'victima_id' => 361,
                'created_at' => '2019-01-29 22:27:03',
                'updated_at' => '2019-01-29 22:27:03',
                'deleted_at' => NULL,
            ),
            361 => 
            array (
                'id' => 362,
                'denuncia_id' => 357,
                'victima_id' => 362,
                'created_at' => '2019-01-29 22:27:03',
                'updated_at' => '2019-01-29 22:27:03',
                'deleted_at' => NULL,
            ),
            362 => 
            array (
                'id' => 363,
                'denuncia_id' => 357,
                'victima_id' => 363,
                'created_at' => '2019-01-29 22:27:03',
                'updated_at' => '2019-01-29 22:27:03',
                'deleted_at' => NULL,
            ),
            363 => 
            array (
                'id' => 364,
                'denuncia_id' => 358,
                'victima_id' => 364,
                'created_at' => '2019-01-29 22:27:03',
                'updated_at' => '2019-01-29 22:27:03',
                'deleted_at' => NULL,
            ),
            364 => 
            array (
                'id' => 365,
                'denuncia_id' => 359,
                'victima_id' => 365,
                'created_at' => '2019-01-29 22:27:03',
                'updated_at' => '2019-01-29 22:27:03',
                'deleted_at' => NULL,
            ),
            365 => 
            array (
                'id' => 366,
                'denuncia_id' => 360,
                'victima_id' => 366,
                'created_at' => '2019-01-29 22:27:03',
                'updated_at' => '2019-01-29 22:27:03',
                'deleted_at' => NULL,
            ),
            366 => 
            array (
                'id' => 367,
                'denuncia_id' => 361,
                'victima_id' => 367,
                'created_at' => '2019-01-29 22:27:03',
                'updated_at' => '2019-01-29 22:27:03',
                'deleted_at' => NULL,
            ),
            367 => 
            array (
                'id' => 368,
                'denuncia_id' => 362,
                'victima_id' => 368,
                'created_at' => '2019-01-29 22:27:04',
                'updated_at' => '2019-01-29 22:27:04',
                'deleted_at' => NULL,
            ),
            368 => 
            array (
                'id' => 369,
                'denuncia_id' => 363,
                'victima_id' => 369,
                'created_at' => '2019-01-29 22:27:04',
                'updated_at' => '2019-01-29 22:27:04',
                'deleted_at' => NULL,
            ),
            369 => 
            array (
                'id' => 370,
                'denuncia_id' => 364,
                'victima_id' => 370,
                'created_at' => '2019-01-29 22:27:04',
                'updated_at' => '2019-01-29 22:27:04',
                'deleted_at' => NULL,
            ),
            370 => 
            array (
                'id' => 371,
                'denuncia_id' => 366,
                'victima_id' => 371,
                'created_at' => '2019-01-29 22:27:04',
                'updated_at' => '2019-01-29 22:27:04',
                'deleted_at' => NULL,
            ),
            371 => 
            array (
                'id' => 372,
                'denuncia_id' => 367,
                'victima_id' => 372,
                'created_at' => '2019-01-29 22:27:05',
                'updated_at' => '2019-01-29 22:27:05',
                'deleted_at' => NULL,
            ),
            372 => 
            array (
                'id' => 373,
                'denuncia_id' => 368,
                'victima_id' => 373,
                'created_at' => '2019-01-29 22:27:05',
                'updated_at' => '2019-01-29 22:27:05',
                'deleted_at' => NULL,
            ),
            373 => 
            array (
                'id' => 374,
                'denuncia_id' => 369,
                'victima_id' => 374,
                'created_at' => '2019-01-29 22:27:05',
                'updated_at' => '2019-01-29 22:27:05',
                'deleted_at' => NULL,
            ),
            374 => 
            array (
                'id' => 375,
                'denuncia_id' => 370,
                'victima_id' => 375,
                'created_at' => '2019-01-29 22:27:05',
                'updated_at' => '2019-01-29 22:27:05',
                'deleted_at' => NULL,
            ),
            375 => 
            array (
                'id' => 376,
                'denuncia_id' => 371,
                'victima_id' => 376,
                'created_at' => '2019-01-29 22:27:05',
                'updated_at' => '2019-01-29 22:27:05',
                'deleted_at' => NULL,
            ),
            376 => 
            array (
                'id' => 377,
                'denuncia_id' => 372,
                'victima_id' => 377,
                'created_at' => '2019-01-29 22:27:05',
                'updated_at' => '2019-01-29 22:27:05',
                'deleted_at' => NULL,
            ),
            377 => 
            array (
                'id' => 378,
                'denuncia_id' => 373,
                'victima_id' => 378,
                'created_at' => '2019-01-29 22:27:06',
                'updated_at' => '2019-01-29 22:27:06',
                'deleted_at' => NULL,
            ),
            378 => 
            array (
                'id' => 379,
                'denuncia_id' => 374,
                'victima_id' => 379,
                'created_at' => '2019-01-29 22:27:06',
                'updated_at' => '2019-01-29 22:27:06',
                'deleted_at' => NULL,
            ),
            379 => 
            array (
                'id' => 380,
                'denuncia_id' => 376,
                'victima_id' => 380,
                'created_at' => '2019-01-29 22:27:06',
                'updated_at' => '2019-01-29 22:27:06',
                'deleted_at' => NULL,
            ),
            380 => 
            array (
                'id' => 381,
                'denuncia_id' => 378,
                'victima_id' => 381,
                'created_at' => '2019-01-29 22:27:06',
                'updated_at' => '2019-01-29 22:27:06',
                'deleted_at' => NULL,
            ),
            381 => 
            array (
                'id' => 382,
                'denuncia_id' => 379,
                'victima_id' => 382,
                'created_at' => '2019-01-29 22:27:07',
                'updated_at' => '2019-01-29 22:27:07',
                'deleted_at' => NULL,
            ),
            382 => 
            array (
                'id' => 383,
                'denuncia_id' => 380,
                'victima_id' => 383,
                'created_at' => '2019-01-29 22:27:07',
                'updated_at' => '2019-01-29 22:27:07',
                'deleted_at' => NULL,
            ),
            383 => 
            array (
                'id' => 384,
                'denuncia_id' => 381,
                'victima_id' => 384,
                'created_at' => '2019-01-29 22:27:07',
                'updated_at' => '2019-01-29 22:27:07',
                'deleted_at' => NULL,
            ),
            384 => 
            array (
                'id' => 385,
                'denuncia_id' => 382,
                'victima_id' => 385,
                'created_at' => '2019-01-29 22:27:07',
                'updated_at' => '2019-01-29 22:27:07',
                'deleted_at' => NULL,
            ),
            385 => 
            array (
                'id' => 386,
                'denuncia_id' => 383,
                'victima_id' => 386,
                'created_at' => '2019-01-29 22:27:08',
                'updated_at' => '2019-01-29 22:27:08',
                'deleted_at' => NULL,
            ),
            386 => 
            array (
                'id' => 387,
                'denuncia_id' => 384,
                'victima_id' => 387,
                'created_at' => '2019-01-29 22:27:08',
                'updated_at' => '2019-01-29 22:27:08',
                'deleted_at' => NULL,
            ),
            387 => 
            array (
                'id' => 388,
                'denuncia_id' => 385,
                'victima_id' => 388,
                'created_at' => '2019-01-29 22:27:08',
                'updated_at' => '2019-01-29 22:27:08',
                'deleted_at' => NULL,
            ),
            388 => 
            array (
                'id' => 389,
                'denuncia_id' => 386,
                'victima_id' => 389,
                'created_at' => '2019-01-29 22:27:08',
                'updated_at' => '2019-01-29 22:27:08',
                'deleted_at' => NULL,
            ),
            389 => 
            array (
                'id' => 390,
                'denuncia_id' => 388,
                'victima_id' => 390,
                'created_at' => '2019-01-29 22:27:08',
                'updated_at' => '2019-01-29 22:27:08',
                'deleted_at' => NULL,
            ),
            390 => 
            array (
                'id' => 391,
                'denuncia_id' => 389,
                'victima_id' => 391,
                'created_at' => '2019-01-29 22:27:09',
                'updated_at' => '2019-01-29 22:27:09',
                'deleted_at' => NULL,
            ),
            391 => 
            array (
                'id' => 392,
                'denuncia_id' => 390,
                'victima_id' => 392,
                'created_at' => '2019-01-29 22:27:09',
                'updated_at' => '2019-01-29 22:27:09',
                'deleted_at' => NULL,
            ),
            392 => 
            array (
                'id' => 393,
                'denuncia_id' => 391,
                'victima_id' => 393,
                'created_at' => '2019-01-29 22:27:09',
                'updated_at' => '2019-01-29 22:27:09',
                'deleted_at' => NULL,
            ),
            393 => 
            array (
                'id' => 394,
                'denuncia_id' => 392,
                'victima_id' => 394,
                'created_at' => '2019-01-29 22:27:09',
                'updated_at' => '2019-01-29 22:27:09',
                'deleted_at' => NULL,
            ),
            394 => 
            array (
                'id' => 395,
                'denuncia_id' => 393,
                'victima_id' => 395,
                'created_at' => '2019-01-29 22:27:09',
                'updated_at' => '2019-01-29 22:27:09',
                'deleted_at' => NULL,
            ),
            395 => 
            array (
                'id' => 396,
                'denuncia_id' => 394,
                'victima_id' => 396,
                'created_at' => '2019-01-29 22:27:09',
                'updated_at' => '2019-01-29 22:27:09',
                'deleted_at' => NULL,
            ),
            396 => 
            array (
                'id' => 397,
                'denuncia_id' => 395,
                'victima_id' => 397,
                'created_at' => '2019-01-29 22:27:10',
                'updated_at' => '2019-01-29 22:27:10',
                'deleted_at' => NULL,
            ),
            397 => 
            array (
                'id' => 398,
                'denuncia_id' => 397,
                'victima_id' => 398,
                'created_at' => '2019-01-29 22:27:10',
                'updated_at' => '2019-01-29 22:27:10',
                'deleted_at' => NULL,
            ),
            398 => 
            array (
                'id' => 399,
                'denuncia_id' => 398,
                'victima_id' => 399,
                'created_at' => '2019-01-29 22:27:10',
                'updated_at' => '2019-01-29 22:27:10',
                'deleted_at' => NULL,
            ),
            399 => 
            array (
                'id' => 400,
                'denuncia_id' => 399,
                'victima_id' => 400,
                'created_at' => '2019-01-29 22:27:10',
                'updated_at' => '2019-01-29 22:27:10',
                'deleted_at' => NULL,
            ),
            400 => 
            array (
                'id' => 401,
                'denuncia_id' => 400,
                'victima_id' => 401,
                'created_at' => '2019-01-29 22:27:10',
                'updated_at' => '2019-01-29 22:27:10',
                'deleted_at' => NULL,
            ),
            401 => 
            array (
                'id' => 402,
                'denuncia_id' => 400,
                'victima_id' => 402,
                'created_at' => '2019-01-29 22:27:10',
                'updated_at' => '2019-01-29 22:27:10',
                'deleted_at' => NULL,
            ),
            402 => 
            array (
                'id' => 403,
                'denuncia_id' => 402,
                'victima_id' => 403,
                'created_at' => '2019-01-29 22:27:11',
                'updated_at' => '2019-01-29 22:27:11',
                'deleted_at' => NULL,
            ),
            403 => 
            array (
                'id' => 404,
                'denuncia_id' => 403,
                'victima_id' => 404,
                'created_at' => '2019-01-29 22:27:11',
                'updated_at' => '2019-01-29 22:27:11',
                'deleted_at' => NULL,
            ),
            404 => 
            array (
                'id' => 405,
                'denuncia_id' => 404,
                'victima_id' => 405,
                'created_at' => '2019-01-29 22:27:11',
                'updated_at' => '2019-01-29 22:27:11',
                'deleted_at' => NULL,
            ),
            405 => 
            array (
                'id' => 406,
                'denuncia_id' => 405,
                'victima_id' => 406,
                'created_at' => '2019-01-29 22:27:11',
                'updated_at' => '2019-01-29 22:27:11',
                'deleted_at' => NULL,
            ),
            406 => 
            array (
                'id' => 407,
                'denuncia_id' => 406,
                'victima_id' => 407,
                'created_at' => '2019-01-29 22:27:11',
                'updated_at' => '2019-01-29 22:27:11',
                'deleted_at' => NULL,
            ),
            407 => 
            array (
                'id' => 408,
                'denuncia_id' => 407,
                'victima_id' => 408,
                'created_at' => '2019-01-29 22:27:12',
                'updated_at' => '2019-01-29 22:27:12',
                'deleted_at' => NULL,
            ),
            408 => 
            array (
                'id' => 409,
                'denuncia_id' => 408,
                'victima_id' => 409,
                'created_at' => '2019-01-29 22:27:12',
                'updated_at' => '2019-01-29 22:27:12',
                'deleted_at' => NULL,
            ),
            409 => 
            array (
                'id' => 410,
                'denuncia_id' => 409,
                'victima_id' => 410,
                'created_at' => '2019-01-29 22:27:12',
                'updated_at' => '2019-01-29 22:27:12',
                'deleted_at' => NULL,
            ),
            410 => 
            array (
                'id' => 411,
                'denuncia_id' => 410,
                'victima_id' => 411,
                'created_at' => '2019-01-29 22:27:12',
                'updated_at' => '2019-01-29 22:27:12',
                'deleted_at' => NULL,
            ),
            411 => 
            array (
                'id' => 412,
                'denuncia_id' => 412,
                'victima_id' => 412,
                'created_at' => '2019-01-29 22:27:12',
                'updated_at' => '2019-01-29 22:27:12',
                'deleted_at' => NULL,
            ),
            412 => 
            array (
                'id' => 413,
                'denuncia_id' => 412,
                'victima_id' => 413,
                'created_at' => '2019-01-29 22:27:12',
                'updated_at' => '2019-01-29 22:27:12',
                'deleted_at' => NULL,
            ),
            413 => 
            array (
                'id' => 414,
                'denuncia_id' => 413,
                'victima_id' => 414,
                'created_at' => '2019-01-29 22:27:13',
                'updated_at' => '2019-01-29 22:27:13',
                'deleted_at' => NULL,
            ),
            414 => 
            array (
                'id' => 415,
                'denuncia_id' => 413,
                'victima_id' => 415,
                'created_at' => '2019-01-29 22:27:13',
                'updated_at' => '2019-01-29 22:27:13',
                'deleted_at' => NULL,
            ),
            415 => 
            array (
                'id' => 416,
                'denuncia_id' => 415,
                'victima_id' => 416,
                'created_at' => '2019-01-29 22:27:13',
                'updated_at' => '2019-01-29 22:27:13',
                'deleted_at' => NULL,
            ),
            416 => 
            array (
                'id' => 417,
                'denuncia_id' => 417,
                'victima_id' => 417,
                'created_at' => '2019-01-29 22:27:13',
                'updated_at' => '2019-01-29 22:27:13',
                'deleted_at' => NULL,
            ),
            417 => 
            array (
                'id' => 418,
                'denuncia_id' => 418,
                'victima_id' => 418,
                'created_at' => '2019-01-29 22:27:14',
                'updated_at' => '2019-01-29 22:27:14',
                'deleted_at' => NULL,
            ),
            418 => 
            array (
                'id' => 419,
                'denuncia_id' => 423,
                'victima_id' => 419,
                'created_at' => '2019-01-29 22:27:15',
                'updated_at' => '2019-01-29 22:27:15',
                'deleted_at' => NULL,
            ),
            419 => 
            array (
                'id' => 420,
                'denuncia_id' => 424,
                'victima_id' => 420,
                'created_at' => '2019-01-29 22:27:15',
                'updated_at' => '2019-01-29 22:27:15',
                'deleted_at' => NULL,
            ),
            420 => 
            array (
                'id' => 421,
                'denuncia_id' => 425,
                'victima_id' => 421,
                'created_at' => '2019-01-29 22:27:15',
                'updated_at' => '2019-01-29 22:27:15',
                'deleted_at' => NULL,
            ),
            421 => 
            array (
                'id' => 422,
                'denuncia_id' => 426,
                'victima_id' => 422,
                'created_at' => '2019-01-29 22:27:15',
                'updated_at' => '2019-01-29 22:27:15',
                'deleted_at' => NULL,
            ),
            422 => 
            array (
                'id' => 423,
                'denuncia_id' => 427,
                'victima_id' => 423,
                'created_at' => '2019-01-29 22:27:15',
                'updated_at' => '2019-01-29 22:27:15',
                'deleted_at' => NULL,
            ),
            423 => 
            array (
                'id' => 424,
                'denuncia_id' => 428,
                'victima_id' => 424,
                'created_at' => '2019-01-29 22:27:16',
                'updated_at' => '2019-01-29 22:27:16',
                'deleted_at' => NULL,
            ),
            424 => 
            array (
                'id' => 425,
                'denuncia_id' => 429,
                'victima_id' => 425,
                'created_at' => '2019-01-29 22:27:16',
                'updated_at' => '2019-01-29 22:27:16',
                'deleted_at' => NULL,
            ),
            425 => 
            array (
                'id' => 426,
                'denuncia_id' => 430,
                'victima_id' => 426,
                'created_at' => '2019-01-29 22:27:16',
                'updated_at' => '2019-01-29 22:27:16',
                'deleted_at' => NULL,
            ),
            426 => 
            array (
                'id' => 427,
                'denuncia_id' => 432,
                'victima_id' => 427,
                'created_at' => '2019-01-29 22:27:16',
                'updated_at' => '2019-01-29 22:27:16',
                'deleted_at' => NULL,
            ),
            427 => 
            array (
                'id' => 428,
                'denuncia_id' => 433,
                'victima_id' => 428,
                'created_at' => '2019-01-29 22:27:16',
                'updated_at' => '2019-01-29 22:27:16',
                'deleted_at' => NULL,
            ),
            428 => 
            array (
                'id' => 429,
                'denuncia_id' => 434,
                'victima_id' => 429,
                'created_at' => '2019-01-29 22:27:17',
                'updated_at' => '2019-01-29 22:27:17',
                'deleted_at' => NULL,
            ),
            429 => 
            array (
                'id' => 430,
                'denuncia_id' => 435,
                'victima_id' => 430,
                'created_at' => '2019-01-29 22:27:17',
                'updated_at' => '2019-01-29 22:27:17',
                'deleted_at' => NULL,
            ),
            430 => 
            array (
                'id' => 431,
                'denuncia_id' => 436,
                'victima_id' => 431,
                'created_at' => '2019-01-29 22:27:17',
                'updated_at' => '2019-01-29 22:27:17',
                'deleted_at' => NULL,
            ),
            431 => 
            array (
                'id' => 432,
                'denuncia_id' => 437,
                'victima_id' => 432,
                'created_at' => '2019-01-29 22:27:17',
                'updated_at' => '2019-01-29 22:27:17',
                'deleted_at' => NULL,
            ),
            432 => 
            array (
                'id' => 433,
                'denuncia_id' => 438,
                'victima_id' => 433,
                'created_at' => '2019-01-29 22:27:18',
                'updated_at' => '2019-01-29 22:27:18',
                'deleted_at' => NULL,
            ),
            433 => 
            array (
                'id' => 434,
                'denuncia_id' => 439,
                'victima_id' => 434,
                'created_at' => '2019-01-29 22:27:18',
                'updated_at' => '2019-01-29 22:27:18',
                'deleted_at' => NULL,
            ),
            434 => 
            array (
                'id' => 435,
                'denuncia_id' => 440,
                'victima_id' => 435,
                'created_at' => '2019-01-29 22:27:18',
                'updated_at' => '2019-01-29 22:27:18',
                'deleted_at' => NULL,
            ),
            435 => 
            array (
                'id' => 436,
                'denuncia_id' => 441,
                'victima_id' => 436,
                'created_at' => '2019-01-29 22:27:18',
                'updated_at' => '2019-01-29 22:27:18',
                'deleted_at' => NULL,
            ),
            436 => 
            array (
                'id' => 437,
                'denuncia_id' => 442,
                'victima_id' => 437,
                'created_at' => '2019-01-29 22:27:18',
                'updated_at' => '2019-01-29 22:27:18',
                'deleted_at' => NULL,
            ),
            437 => 
            array (
                'id' => 438,
                'denuncia_id' => 443,
                'victima_id' => 438,
                'created_at' => '2019-01-29 22:27:19',
                'updated_at' => '2019-01-29 22:27:19',
                'deleted_at' => NULL,
            ),
            438 => 
            array (
                'id' => 439,
                'denuncia_id' => 444,
                'victima_id' => 439,
                'created_at' => '2019-01-29 22:27:19',
                'updated_at' => '2019-01-29 22:27:19',
                'deleted_at' => NULL,
            ),
            439 => 
            array (
                'id' => 440,
                'denuncia_id' => 445,
                'victima_id' => 440,
                'created_at' => '2019-01-29 22:27:19',
                'updated_at' => '2019-01-29 22:27:19',
                'deleted_at' => NULL,
            ),
            440 => 
            array (
                'id' => 441,
                'denuncia_id' => 446,
                'victima_id' => 441,
                'created_at' => '2019-01-29 22:27:19',
                'updated_at' => '2019-01-29 22:27:19',
                'deleted_at' => NULL,
            ),
            441 => 
            array (
                'id' => 442,
                'denuncia_id' => 448,
                'victima_id' => 442,
                'created_at' => '2019-01-29 22:27:19',
                'updated_at' => '2019-01-29 22:27:19',
                'deleted_at' => NULL,
            ),
            442 => 
            array (
                'id' => 443,
                'denuncia_id' => 449,
                'victima_id' => 443,
                'created_at' => '2019-01-29 22:27:20',
                'updated_at' => '2019-01-29 22:27:20',
                'deleted_at' => NULL,
            ),
            443 => 
            array (
                'id' => 444,
                'denuncia_id' => 451,
                'victima_id' => 444,
                'created_at' => '2019-01-29 22:27:20',
                'updated_at' => '2019-01-29 22:27:20',
                'deleted_at' => NULL,
            ),
            444 => 
            array (
                'id' => 445,
                'denuncia_id' => 452,
                'victima_id' => 445,
                'created_at' => '2019-01-29 22:27:20',
                'updated_at' => '2019-01-29 22:27:20',
                'deleted_at' => NULL,
            ),
            445 => 
            array (
                'id' => 446,
                'denuncia_id' => 453,
                'victima_id' => 446,
                'created_at' => '2019-01-29 22:27:20',
                'updated_at' => '2019-01-29 22:27:20',
                'deleted_at' => NULL,
            ),
            446 => 
            array (
                'id' => 447,
                'denuncia_id' => 454,
                'victima_id' => 447,
                'created_at' => '2019-01-29 22:27:20',
                'updated_at' => '2019-01-29 22:27:20',
                'deleted_at' => NULL,
            ),
            447 => 
            array (
                'id' => 448,
                'denuncia_id' => 455,
                'victima_id' => 448,
                'created_at' => '2019-01-29 22:27:21',
                'updated_at' => '2019-01-29 22:27:21',
                'deleted_at' => NULL,
            ),
            448 => 
            array (
                'id' => 449,
                'denuncia_id' => 456,
                'victima_id' => 449,
                'created_at' => '2019-01-29 22:27:21',
                'updated_at' => '2019-01-29 22:27:21',
                'deleted_at' => NULL,
            ),
            449 => 
            array (
                'id' => 450,
                'denuncia_id' => 457,
                'victima_id' => 450,
                'created_at' => '2019-01-29 22:27:21',
                'updated_at' => '2019-01-29 22:27:21',
                'deleted_at' => NULL,
            ),
            450 => 
            array (
                'id' => 451,
                'denuncia_id' => 458,
                'victima_id' => 451,
                'created_at' => '2019-01-29 22:27:21',
                'updated_at' => '2019-01-29 22:27:21',
                'deleted_at' => NULL,
            ),
            451 => 
            array (
                'id' => 452,
                'denuncia_id' => 459,
                'victima_id' => 452,
                'created_at' => '2019-01-29 22:27:21',
                'updated_at' => '2019-01-29 22:27:21',
                'deleted_at' => NULL,
            ),
            452 => 
            array (
                'id' => 453,
                'denuncia_id' => 460,
                'victima_id' => 453,
                'created_at' => '2019-01-29 22:27:22',
                'updated_at' => '2019-01-29 22:27:22',
                'deleted_at' => NULL,
            ),
            453 => 
            array (
                'id' => 454,
                'denuncia_id' => 461,
                'victima_id' => 454,
                'created_at' => '2019-01-29 22:27:22',
                'updated_at' => '2019-01-29 22:27:22',
                'deleted_at' => NULL,
            ),
            454 => 
            array (
                'id' => 455,
                'denuncia_id' => 462,
                'victima_id' => 455,
                'created_at' => '2019-01-29 22:27:22',
                'updated_at' => '2019-01-29 22:27:22',
                'deleted_at' => NULL,
            ),
            455 => 
            array (
                'id' => 456,
                'denuncia_id' => 463,
                'victima_id' => 456,
                'created_at' => '2019-01-29 22:27:22',
                'updated_at' => '2019-01-29 22:27:22',
                'deleted_at' => NULL,
            ),
            456 => 
            array (
                'id' => 457,
                'denuncia_id' => 464,
                'victima_id' => 457,
                'created_at' => '2019-01-29 22:27:22',
                'updated_at' => '2019-01-29 22:27:22',
                'deleted_at' => NULL,
            ),
            457 => 
            array (
                'id' => 458,
                'denuncia_id' => 466,
                'victima_id' => 458,
                'created_at' => '2019-01-29 22:27:23',
                'updated_at' => '2019-01-29 22:27:23',
                'deleted_at' => NULL,
            ),
            458 => 
            array (
                'id' => 459,
                'denuncia_id' => 467,
                'victima_id' => 459,
                'created_at' => '2019-01-29 22:27:23',
                'updated_at' => '2019-01-29 22:27:23',
                'deleted_at' => NULL,
            ),
            459 => 
            array (
                'id' => 460,
                'denuncia_id' => 469,
                'victima_id' => 460,
                'created_at' => '2019-01-29 22:27:24',
                'updated_at' => '2019-01-29 22:27:24',
                'deleted_at' => NULL,
            ),
            460 => 
            array (
                'id' => 461,
                'denuncia_id' => 470,
                'victima_id' => 461,
                'created_at' => '2019-01-29 22:27:24',
                'updated_at' => '2019-01-29 22:27:24',
                'deleted_at' => NULL,
            ),
            461 => 
            array (
                'id' => 462,
                'denuncia_id' => 471,
                'victima_id' => 462,
                'created_at' => '2019-01-29 22:27:24',
                'updated_at' => '2019-01-29 22:27:24',
                'deleted_at' => NULL,
            ),
            462 => 
            array (
                'id' => 463,
                'denuncia_id' => 471,
                'victima_id' => 463,
                'created_at' => '2019-01-29 22:27:24',
                'updated_at' => '2019-01-29 22:27:24',
                'deleted_at' => NULL,
            ),
            463 => 
            array (
                'id' => 464,
                'denuncia_id' => 472,
                'victima_id' => 464,
                'created_at' => '2019-01-29 22:27:24',
                'updated_at' => '2019-01-29 22:27:24',
                'deleted_at' => NULL,
            ),
            464 => 
            array (
                'id' => 465,
                'denuncia_id' => 473,
                'victima_id' => 465,
                'created_at' => '2019-01-29 22:27:24',
                'updated_at' => '2019-01-29 22:27:24',
                'deleted_at' => NULL,
            ),
            465 => 
            array (
                'id' => 466,
                'denuncia_id' => 474,
                'victima_id' => 466,
                'created_at' => '2019-01-29 22:27:24',
                'updated_at' => '2019-01-29 22:27:24',
                'deleted_at' => NULL,
            ),
            466 => 
            array (
                'id' => 467,
                'denuncia_id' => 475,
                'victima_id' => 467,
                'created_at' => '2019-01-29 22:27:25',
                'updated_at' => '2019-01-29 22:27:25',
                'deleted_at' => NULL,
            ),
            467 => 
            array (
                'id' => 468,
                'denuncia_id' => 476,
                'victima_id' => 468,
                'created_at' => '2019-01-29 22:27:25',
                'updated_at' => '2019-01-29 22:27:25',
                'deleted_at' => NULL,
            ),
            468 => 
            array (
                'id' => 469,
                'denuncia_id' => 477,
                'victima_id' => 469,
                'created_at' => '2019-01-29 22:27:25',
                'updated_at' => '2019-01-29 22:27:25',
                'deleted_at' => NULL,
            ),
            469 => 
            array (
                'id' => 470,
                'denuncia_id' => 478,
                'victima_id' => 470,
                'created_at' => '2019-01-29 22:27:25',
                'updated_at' => '2019-01-29 22:27:25',
                'deleted_at' => NULL,
            ),
            470 => 
            array (
                'id' => 471,
                'denuncia_id' => 479,
                'victima_id' => 471,
                'created_at' => '2019-01-29 22:27:25',
                'updated_at' => '2019-01-29 22:27:25',
                'deleted_at' => NULL,
            ),
            471 => 
            array (
                'id' => 472,
                'denuncia_id' => 480,
                'victima_id' => 472,
                'created_at' => '2019-01-29 22:27:26',
                'updated_at' => '2019-01-29 22:27:26',
                'deleted_at' => NULL,
            ),
            472 => 
            array (
                'id' => 473,
                'denuncia_id' => 480,
                'victima_id' => 473,
                'created_at' => '2019-01-29 22:27:26',
                'updated_at' => '2019-01-29 22:27:26',
                'deleted_at' => NULL,
            ),
            473 => 
            array (
                'id' => 474,
                'denuncia_id' => 481,
                'victima_id' => 474,
                'created_at' => '2019-01-29 22:27:26',
                'updated_at' => '2019-01-29 22:27:26',
                'deleted_at' => NULL,
            ),
            474 => 
            array (
                'id' => 475,
                'denuncia_id' => 482,
                'victima_id' => 475,
                'created_at' => '2019-01-29 22:27:26',
                'updated_at' => '2019-01-29 22:27:26',
                'deleted_at' => NULL,
            ),
            475 => 
            array (
                'id' => 476,
                'denuncia_id' => 483,
                'victima_id' => 476,
                'created_at' => '2019-01-29 22:27:26',
                'updated_at' => '2019-01-29 22:27:26',
                'deleted_at' => NULL,
            ),
            476 => 
            array (
                'id' => 477,
                'denuncia_id' => 485,
                'victima_id' => 477,
                'created_at' => '2019-01-29 22:27:27',
                'updated_at' => '2019-01-29 22:27:27',
                'deleted_at' => NULL,
            ),
            477 => 
            array (
                'id' => 478,
                'denuncia_id' => 486,
                'victima_id' => 478,
                'created_at' => '2019-01-29 22:27:27',
                'updated_at' => '2019-01-29 22:27:27',
                'deleted_at' => NULL,
            ),
            478 => 
            array (
                'id' => 479,
                'denuncia_id' => 487,
                'victima_id' => 479,
                'created_at' => '2019-01-29 22:27:27',
                'updated_at' => '2019-01-29 22:27:27',
                'deleted_at' => NULL,
            ),
            479 => 
            array (
                'id' => 480,
                'denuncia_id' => 488,
                'victima_id' => 480,
                'created_at' => '2019-01-29 22:27:27',
                'updated_at' => '2019-01-29 22:27:27',
                'deleted_at' => NULL,
            ),
            480 => 
            array (
                'id' => 481,
                'denuncia_id' => 490,
                'victima_id' => 481,
                'created_at' => '2019-01-29 22:27:28',
                'updated_at' => '2019-01-29 22:27:28',
                'deleted_at' => NULL,
            ),
            481 => 
            array (
                'id' => 482,
                'denuncia_id' => 491,
                'victima_id' => 482,
                'created_at' => '2019-01-29 22:27:28',
                'updated_at' => '2019-01-29 22:27:28',
                'deleted_at' => NULL,
            ),
            482 => 
            array (
                'id' => 483,
                'denuncia_id' => 492,
                'victima_id' => 483,
                'created_at' => '2019-01-29 22:27:28',
                'updated_at' => '2019-01-29 22:27:28',
                'deleted_at' => NULL,
            ),
            483 => 
            array (
                'id' => 484,
                'denuncia_id' => 493,
                'victima_id' => 484,
                'created_at' => '2019-01-29 22:27:28',
                'updated_at' => '2019-01-29 22:27:28',
                'deleted_at' => NULL,
            ),
            484 => 
            array (
                'id' => 485,
                'denuncia_id' => 493,
                'victima_id' => 485,
                'created_at' => '2019-01-29 22:27:28',
                'updated_at' => '2019-01-29 22:27:28',
                'deleted_at' => NULL,
            ),
            485 => 
            array (
                'id' => 486,
                'denuncia_id' => 494,
                'victima_id' => 486,
                'created_at' => '2019-01-29 22:27:28',
                'updated_at' => '2019-01-29 22:27:28',
                'deleted_at' => NULL,
            ),
            486 => 
            array (
                'id' => 487,
                'denuncia_id' => 495,
                'victima_id' => 487,
                'created_at' => '2019-01-29 22:27:29',
                'updated_at' => '2019-01-29 22:27:29',
                'deleted_at' => NULL,
            ),
            487 => 
            array (
                'id' => 488,
                'denuncia_id' => 496,
                'victima_id' => 488,
                'created_at' => '2019-01-29 22:27:29',
                'updated_at' => '2019-01-29 22:27:29',
                'deleted_at' => NULL,
            ),
            488 => 
            array (
                'id' => 489,
                'denuncia_id' => 497,
                'victima_id' => 489,
                'created_at' => '2019-01-29 22:27:29',
                'updated_at' => '2019-01-29 22:27:29',
                'deleted_at' => NULL,
            ),
            489 => 
            array (
                'id' => 490,
                'denuncia_id' => 498,
                'victima_id' => 490,
                'created_at' => '2019-01-29 22:27:29',
                'updated_at' => '2019-01-29 22:27:29',
                'deleted_at' => NULL,
            ),
            490 => 
            array (
                'id' => 491,
                'denuncia_id' => 499,
                'victima_id' => 491,
                'created_at' => '2019-01-29 22:27:29',
                'updated_at' => '2019-01-29 22:27:29',
                'deleted_at' => NULL,
            ),
            491 => 
            array (
                'id' => 492,
                'denuncia_id' => 500,
                'victima_id' => 492,
                'created_at' => '2019-01-29 22:27:30',
                'updated_at' => '2019-01-29 22:27:30',
                'deleted_at' => NULL,
            ),
            492 => 
            array (
                'id' => 493,
                'denuncia_id' => 501,
                'victima_id' => 493,
                'created_at' => '2019-01-29 22:27:30',
                'updated_at' => '2019-01-29 22:27:30',
                'deleted_at' => NULL,
            ),
            493 => 
            array (
                'id' => 494,
                'denuncia_id' => 502,
                'victima_id' => 494,
                'created_at' => '2019-01-29 22:27:30',
                'updated_at' => '2019-01-29 22:27:30',
                'deleted_at' => NULL,
            ),
            494 => 
            array (
                'id' => 495,
                'denuncia_id' => 503,
                'victima_id' => 495,
                'created_at' => '2019-01-29 22:27:30',
                'updated_at' => '2019-01-29 22:27:30',
                'deleted_at' => NULL,
            ),
            495 => 
            array (
                'id' => 496,
                'denuncia_id' => 504,
                'victima_id' => 496,
                'created_at' => '2019-01-29 22:27:30',
                'updated_at' => '2019-01-29 22:27:30',
                'deleted_at' => NULL,
            ),
            496 => 
            array (
                'id' => 497,
                'denuncia_id' => 505,
                'victima_id' => 497,
                'created_at' => '2019-01-29 22:27:31',
                'updated_at' => '2019-01-29 22:27:31',
                'deleted_at' => NULL,
            ),
            497 => 
            array (
                'id' => 498,
                'denuncia_id' => 506,
                'victima_id' => 498,
                'created_at' => '2019-01-29 22:27:31',
                'updated_at' => '2019-01-29 22:27:31',
                'deleted_at' => NULL,
            ),
            498 => 
            array (
                'id' => 499,
                'denuncia_id' => 507,
                'victima_id' => 499,
                'created_at' => '2019-01-29 22:27:31',
                'updated_at' => '2019-01-29 22:27:31',
                'deleted_at' => NULL,
            ),
            499 => 
            array (
                'id' => 500,
                'denuncia_id' => 508,
                'victima_id' => 500,
                'created_at' => '2019-01-29 22:27:31',
                'updated_at' => '2019-01-29 22:27:31',
                'deleted_at' => NULL,
            ),
        ));
        \DB::table('denuncia_victima')->insert(array (
            0 => 
            array (
                'id' => 501,
                'denuncia_id' => 509,
                'victima_id' => 501,
                'created_at' => '2019-01-29 22:27:31',
                'updated_at' => '2019-01-29 22:27:31',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 502,
                'denuncia_id' => 510,
                'victima_id' => 502,
                'created_at' => '2019-01-29 22:27:32',
                'updated_at' => '2019-01-29 22:27:32',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 503,
                'denuncia_id' => 511,
                'victima_id' => 503,
                'created_at' => '2019-01-29 22:27:32',
                'updated_at' => '2019-01-29 22:27:32',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 504,
                'denuncia_id' => 512,
                'victima_id' => 504,
                'created_at' => '2019-01-29 22:27:32',
                'updated_at' => '2019-01-29 22:27:32',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 505,
                'denuncia_id' => 513,
                'victima_id' => 505,
                'created_at' => '2019-01-29 22:27:32',
                'updated_at' => '2019-01-29 22:27:32',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 506,
                'denuncia_id' => 514,
                'victima_id' => 506,
                'created_at' => '2019-01-29 22:27:32',
                'updated_at' => '2019-01-29 22:27:32',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 507,
                'denuncia_id' => 515,
                'victima_id' => 507,
                'created_at' => '2019-01-29 22:27:32',
                'updated_at' => '2019-01-29 22:27:32',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 508,
                'denuncia_id' => 516,
                'victima_id' => 508,
                'created_at' => '2019-01-29 22:27:33',
                'updated_at' => '2019-01-29 22:27:33',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 509,
                'denuncia_id' => 517,
                'victima_id' => 509,
                'created_at' => '2019-01-29 22:27:33',
                'updated_at' => '2019-01-29 22:27:33',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 510,
                'denuncia_id' => 518,
                'victima_id' => 510,
                'created_at' => '2019-01-29 22:27:33',
                'updated_at' => '2019-01-29 22:27:33',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 511,
                'denuncia_id' => 519,
                'victima_id' => 511,
                'created_at' => '2019-01-29 22:27:33',
                'updated_at' => '2019-01-29 22:27:33',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 512,
                'denuncia_id' => 520,
                'victima_id' => 512,
                'created_at' => '2019-01-29 22:27:34',
                'updated_at' => '2019-01-29 22:27:34',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 513,
                'denuncia_id' => 521,
                'victima_id' => 513,
                'created_at' => '2019-01-29 22:27:34',
                'updated_at' => '2019-01-29 22:27:34',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 514,
                'denuncia_id' => 523,
                'victima_id' => 514,
                'created_at' => '2019-01-29 22:27:34',
                'updated_at' => '2019-01-29 22:27:34',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 515,
                'denuncia_id' => 524,
                'victima_id' => 515,
                'created_at' => '2019-01-29 22:27:34',
                'updated_at' => '2019-01-29 22:27:34',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 516,
                'denuncia_id' => 525,
                'victima_id' => 516,
                'created_at' => '2019-01-29 22:27:34',
                'updated_at' => '2019-01-29 22:27:34',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 517,
                'denuncia_id' => 526,
                'victima_id' => 517,
                'created_at' => '2019-01-29 22:27:35',
                'updated_at' => '2019-01-29 22:27:35',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 518,
                'denuncia_id' => 527,
                'victima_id' => 518,
                'created_at' => '2019-01-29 22:27:35',
                'updated_at' => '2019-01-29 22:27:35',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 519,
                'denuncia_id' => 528,
                'victima_id' => 519,
                'created_at' => '2019-01-29 22:27:35',
                'updated_at' => '2019-01-29 22:27:35',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 520,
                'denuncia_id' => 529,
                'victima_id' => 520,
                'created_at' => '2019-01-29 22:27:35',
                'updated_at' => '2019-01-29 22:27:35',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 521,
                'denuncia_id' => 530,
                'victima_id' => 521,
                'created_at' => '2019-01-29 22:27:36',
                'updated_at' => '2019-01-29 22:27:36',
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 522,
                'denuncia_id' => 531,
                'victima_id' => 522,
                'created_at' => '2019-01-29 22:27:36',
                'updated_at' => '2019-01-29 22:27:36',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 523,
                'denuncia_id' => 532,
                'victima_id' => 523,
                'created_at' => '2019-01-29 22:27:36',
                'updated_at' => '2019-01-29 22:27:36',
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id' => 524,
                'denuncia_id' => 533,
                'victima_id' => 524,
                'created_at' => '2019-01-29 22:27:36',
                'updated_at' => '2019-01-29 22:27:36',
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id' => 525,
                'denuncia_id' => 534,
                'victima_id' => 525,
                'created_at' => '2019-01-29 22:27:36',
                'updated_at' => '2019-01-29 22:27:36',
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id' => 526,
                'denuncia_id' => 535,
                'victima_id' => 526,
                'created_at' => '2019-01-29 22:27:37',
                'updated_at' => '2019-01-29 22:27:37',
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'id' => 527,
                'denuncia_id' => 536,
                'victima_id' => 527,
                'created_at' => '2019-01-29 22:27:37',
                'updated_at' => '2019-01-29 22:27:37',
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id' => 528,
                'denuncia_id' => 537,
                'victima_id' => 528,
                'created_at' => '2019-01-29 22:27:37',
                'updated_at' => '2019-01-29 22:27:37',
                'deleted_at' => NULL,
            ),
            28 => 
            array (
                'id' => 529,
                'denuncia_id' => 538,
                'victima_id' => 529,
                'created_at' => '2019-01-29 22:27:37',
                'updated_at' => '2019-01-29 22:27:37',
                'deleted_at' => NULL,
            ),
            29 => 
            array (
                'id' => 530,
                'denuncia_id' => 539,
                'victima_id' => 530,
                'created_at' => '2019-01-29 22:27:38',
                'updated_at' => '2019-01-29 22:27:38',
                'deleted_at' => NULL,
            ),
            30 => 
            array (
                'id' => 531,
                'denuncia_id' => 540,
                'victima_id' => 531,
                'created_at' => '2019-01-29 22:27:38',
                'updated_at' => '2019-01-29 22:27:38',
                'deleted_at' => NULL,
            ),
            31 => 
            array (
                'id' => 532,
                'denuncia_id' => 541,
                'victima_id' => 532,
                'created_at' => '2019-01-29 22:27:38',
                'updated_at' => '2019-01-29 22:27:38',
                'deleted_at' => NULL,
            ),
            32 => 
            array (
                'id' => 533,
                'denuncia_id' => 542,
                'victima_id' => 533,
                'created_at' => '2019-01-29 22:27:38',
                'updated_at' => '2019-01-29 22:27:38',
                'deleted_at' => NULL,
            ),
            33 => 
            array (
                'id' => 534,
                'denuncia_id' => 543,
                'victima_id' => 534,
                'created_at' => '2019-01-29 22:27:38',
                'updated_at' => '2019-01-29 22:27:38',
                'deleted_at' => NULL,
            ),
            34 => 
            array (
                'id' => 535,
                'denuncia_id' => 544,
                'victima_id' => 535,
                'created_at' => '2019-01-29 22:27:39',
                'updated_at' => '2019-01-29 22:27:39',
                'deleted_at' => NULL,
            ),
            35 => 
            array (
                'id' => 536,
                'denuncia_id' => 545,
                'victima_id' => 536,
                'created_at' => '2019-01-29 22:27:39',
                'updated_at' => '2019-01-29 22:27:39',
                'deleted_at' => NULL,
            ),
            36 => 
            array (
                'id' => 537,
                'denuncia_id' => 546,
                'victima_id' => 537,
                'created_at' => '2019-01-29 22:27:39',
                'updated_at' => '2019-01-29 22:27:39',
                'deleted_at' => NULL,
            ),
            37 => 
            array (
                'id' => 538,
                'denuncia_id' => 547,
                'victima_id' => 538,
                'created_at' => '2019-01-29 22:27:39',
                'updated_at' => '2019-01-29 22:27:39',
                'deleted_at' => NULL,
            ),
            38 => 
            array (
                'id' => 539,
                'denuncia_id' => 548,
                'victima_id' => 539,
                'created_at' => '2019-01-29 22:27:39',
                'updated_at' => '2019-01-29 22:27:39',
                'deleted_at' => NULL,
            ),
            39 => 
            array (
                'id' => 540,
                'denuncia_id' => 549,
                'victima_id' => 540,
                'created_at' => '2019-01-29 22:27:39',
                'updated_at' => '2019-01-29 22:27:39',
                'deleted_at' => NULL,
            ),
            40 => 
            array (
                'id' => 541,
                'denuncia_id' => 550,
                'victima_id' => 541,
                'created_at' => '2019-01-29 22:27:40',
                'updated_at' => '2019-01-29 22:27:40',
                'deleted_at' => NULL,
            ),
            41 => 
            array (
                'id' => 542,
                'denuncia_id' => 551,
                'victima_id' => 542,
                'created_at' => '2019-01-29 22:27:40',
                'updated_at' => '2019-01-29 22:27:40',
                'deleted_at' => NULL,
            ),
            42 => 
            array (
                'id' => 543,
                'denuncia_id' => 552,
                'victima_id' => 543,
                'created_at' => '2019-01-29 22:27:40',
                'updated_at' => '2019-01-29 22:27:40',
                'deleted_at' => NULL,
            ),
            43 => 
            array (
                'id' => 544,
                'denuncia_id' => 553,
                'victima_id' => 544,
                'created_at' => '2019-01-29 22:27:40',
                'updated_at' => '2019-01-29 22:27:40',
                'deleted_at' => NULL,
            ),
            44 => 
            array (
                'id' => 545,
                'denuncia_id' => 554,
                'victima_id' => 545,
                'created_at' => '2019-01-29 22:27:40',
                'updated_at' => '2019-01-29 22:27:40',
                'deleted_at' => NULL,
            ),
            45 => 
            array (
                'id' => 546,
                'denuncia_id' => 555,
                'victima_id' => 546,
                'created_at' => '2019-01-29 22:27:40',
                'updated_at' => '2019-01-29 22:27:40',
                'deleted_at' => NULL,
            ),
            46 => 
            array (
                'id' => 547,
                'denuncia_id' => 556,
                'victima_id' => 547,
                'created_at' => '2019-01-29 22:27:41',
                'updated_at' => '2019-01-29 22:27:41',
                'deleted_at' => NULL,
            ),
            47 => 
            array (
                'id' => 548,
                'denuncia_id' => 557,
                'victima_id' => 548,
                'created_at' => '2019-01-29 22:27:41',
                'updated_at' => '2019-01-29 22:27:41',
                'deleted_at' => NULL,
            ),
            48 => 
            array (
                'id' => 549,
                'denuncia_id' => 558,
                'victima_id' => 549,
                'created_at' => '2019-01-29 22:27:41',
                'updated_at' => '2019-01-29 22:27:41',
                'deleted_at' => NULL,
            ),
            49 => 
            array (
                'id' => 550,
                'denuncia_id' => 559,
                'victima_id' => 550,
                'created_at' => '2019-01-29 22:27:41',
                'updated_at' => '2019-01-29 22:27:41',
                'deleted_at' => NULL,
            ),
            50 => 
            array (
                'id' => 551,
                'denuncia_id' => 560,
                'victima_id' => 551,
                'created_at' => '2019-01-29 22:27:41',
                'updated_at' => '2019-01-29 22:27:41',
                'deleted_at' => NULL,
            ),
            51 => 
            array (
                'id' => 552,
                'denuncia_id' => 561,
                'victima_id' => 552,
                'created_at' => '2019-01-29 22:27:42',
                'updated_at' => '2019-01-29 22:27:42',
                'deleted_at' => NULL,
            ),
            52 => 
            array (
                'id' => 553,
                'denuncia_id' => 562,
                'victima_id' => 553,
                'created_at' => '2019-01-29 22:27:42',
                'updated_at' => '2019-01-29 22:27:42',
                'deleted_at' => NULL,
            ),
            53 => 
            array (
                'id' => 554,
                'denuncia_id' => 563,
                'victima_id' => 554,
                'created_at' => '2019-01-29 22:27:42',
                'updated_at' => '2019-01-29 22:27:42',
                'deleted_at' => NULL,
            ),
            54 => 
            array (
                'id' => 555,
                'denuncia_id' => 564,
                'victima_id' => 555,
                'created_at' => '2019-01-29 22:27:42',
                'updated_at' => '2019-01-29 22:27:42',
                'deleted_at' => NULL,
            ),
            55 => 
            array (
                'id' => 556,
                'denuncia_id' => 565,
                'victima_id' => 556,
                'created_at' => '2019-01-29 22:27:42',
                'updated_at' => '2019-01-29 22:27:42',
                'deleted_at' => NULL,
            ),
            56 => 
            array (
                'id' => 557,
                'denuncia_id' => 566,
                'victima_id' => 557,
                'created_at' => '2019-01-29 22:27:43',
                'updated_at' => '2019-01-29 22:27:43',
                'deleted_at' => NULL,
            ),
            57 => 
            array (
                'id' => 558,
                'denuncia_id' => 567,
                'victima_id' => 558,
                'created_at' => '2019-01-29 22:27:43',
                'updated_at' => '2019-01-29 22:27:43',
                'deleted_at' => NULL,
            ),
            58 => 
            array (
                'id' => 559,
                'denuncia_id' => 568,
                'victima_id' => 559,
                'created_at' => '2019-01-29 22:27:43',
                'updated_at' => '2019-01-29 22:27:43',
                'deleted_at' => NULL,
            ),
            59 => 
            array (
                'id' => 560,
                'denuncia_id' => 569,
                'victima_id' => 560,
                'created_at' => '2019-01-29 22:27:43',
                'updated_at' => '2019-01-29 22:27:43',
                'deleted_at' => NULL,
            ),
            60 => 
            array (
                'id' => 561,
                'denuncia_id' => 570,
                'victima_id' => 561,
                'created_at' => '2019-01-29 22:27:43',
                'updated_at' => '2019-01-29 22:27:43',
                'deleted_at' => NULL,
            ),
            61 => 
            array (
                'id' => 562,
                'denuncia_id' => 571,
                'victima_id' => 562,
                'created_at' => '2019-01-29 22:27:44',
                'updated_at' => '2019-01-29 22:27:44',
                'deleted_at' => NULL,
            ),
            62 => 
            array (
                'id' => 563,
                'denuncia_id' => 572,
                'victima_id' => 563,
                'created_at' => '2019-01-29 22:27:44',
                'updated_at' => '2019-01-29 22:27:44',
                'deleted_at' => NULL,
            ),
            63 => 
            array (
                'id' => 564,
                'denuncia_id' => 573,
                'victima_id' => 564,
                'created_at' => '2019-01-29 22:27:44',
                'updated_at' => '2019-01-29 22:27:44',
                'deleted_at' => NULL,
            ),
            64 => 
            array (
                'id' => 565,
                'denuncia_id' => 574,
                'victima_id' => 565,
                'created_at' => '2019-01-29 22:27:44',
                'updated_at' => '2019-01-29 22:27:44',
                'deleted_at' => NULL,
            ),
            65 => 
            array (
                'id' => 566,
                'denuncia_id' => 577,
                'victima_id' => 566,
                'created_at' => '2019-01-29 22:27:45',
                'updated_at' => '2019-01-29 22:27:45',
                'deleted_at' => NULL,
            ),
            66 => 
            array (
                'id' => 567,
                'denuncia_id' => 578,
                'victima_id' => 567,
                'created_at' => '2019-01-29 22:27:45',
                'updated_at' => '2019-01-29 22:27:45',
                'deleted_at' => NULL,
            ),
            67 => 
            array (
                'id' => 568,
                'denuncia_id' => 579,
                'victima_id' => 568,
                'created_at' => '2019-01-29 22:27:45',
                'updated_at' => '2019-01-29 22:27:45',
                'deleted_at' => NULL,
            ),
            68 => 
            array (
                'id' => 569,
                'denuncia_id' => 579,
                'victima_id' => 569,
                'created_at' => '2019-01-29 22:27:45',
                'updated_at' => '2019-01-29 22:27:45',
                'deleted_at' => NULL,
            ),
            69 => 
            array (
                'id' => 570,
                'denuncia_id' => 580,
                'victima_id' => 570,
                'created_at' => '2019-01-29 22:27:45',
                'updated_at' => '2019-01-29 22:27:45',
                'deleted_at' => NULL,
            ),
            70 => 
            array (
                'id' => 571,
                'denuncia_id' => 581,
                'victima_id' => 571,
                'created_at' => '2019-01-29 22:27:46',
                'updated_at' => '2019-01-29 22:27:46',
                'deleted_at' => NULL,
            ),
            71 => 
            array (
                'id' => 572,
                'denuncia_id' => 582,
                'victima_id' => 572,
                'created_at' => '2019-01-29 22:27:46',
                'updated_at' => '2019-01-29 22:27:46',
                'deleted_at' => NULL,
            ),
            72 => 
            array (
                'id' => 573,
                'denuncia_id' => 583,
                'victima_id' => 573,
                'created_at' => '2019-01-29 22:27:46',
                'updated_at' => '2019-01-29 22:27:46',
                'deleted_at' => NULL,
            ),
            73 => 
            array (
                'id' => 574,
                'denuncia_id' => 584,
                'victima_id' => 574,
                'created_at' => '2019-01-29 22:27:46',
                'updated_at' => '2019-01-29 22:27:46',
                'deleted_at' => NULL,
            ),
            74 => 
            array (
                'id' => 575,
                'denuncia_id' => 585,
                'victima_id' => 575,
                'created_at' => '2019-01-29 22:27:46',
                'updated_at' => '2019-01-29 22:27:46',
                'deleted_at' => NULL,
            ),
            75 => 
            array (
                'id' => 576,
                'denuncia_id' => 585,
                'victima_id' => 576,
                'created_at' => '2019-01-29 22:27:46',
                'updated_at' => '2019-01-29 22:27:46',
                'deleted_at' => NULL,
            ),
            76 => 
            array (
                'id' => 577,
                'denuncia_id' => 586,
                'victima_id' => 577,
                'created_at' => '2019-01-29 22:27:47',
                'updated_at' => '2019-01-29 22:27:47',
                'deleted_at' => NULL,
            ),
            77 => 
            array (
                'id' => 578,
                'denuncia_id' => 587,
                'victima_id' => 578,
                'created_at' => '2019-01-29 22:27:47',
                'updated_at' => '2019-01-29 22:27:47',
                'deleted_at' => NULL,
            ),
            78 => 
            array (
                'id' => 579,
                'denuncia_id' => 587,
                'victima_id' => 579,
                'created_at' => '2019-01-29 22:27:47',
                'updated_at' => '2019-01-29 22:27:47',
                'deleted_at' => NULL,
            ),
            79 => 
            array (
                'id' => 580,
                'denuncia_id' => 588,
                'victima_id' => 580,
                'created_at' => '2019-01-29 22:27:47',
                'updated_at' => '2019-01-29 22:27:47',
                'deleted_at' => NULL,
            ),
            80 => 
            array (
                'id' => 581,
                'denuncia_id' => 589,
                'victima_id' => 581,
                'created_at' => '2019-01-29 22:27:47',
                'updated_at' => '2019-01-29 22:27:47',
                'deleted_at' => NULL,
            ),
            81 => 
            array (
                'id' => 582,
                'denuncia_id' => 590,
                'victima_id' => 582,
                'created_at' => '2019-01-29 22:27:47',
                'updated_at' => '2019-01-29 22:27:47',
                'deleted_at' => NULL,
            ),
            82 => 
            array (
                'id' => 583,
                'denuncia_id' => 591,
                'victima_id' => 583,
                'created_at' => '2019-01-29 22:27:48',
                'updated_at' => '2019-01-29 22:27:48',
                'deleted_at' => NULL,
            ),
            83 => 
            array (
                'id' => 584,
                'denuncia_id' => 592,
                'victima_id' => 584,
                'created_at' => '2019-01-29 22:27:48',
                'updated_at' => '2019-01-29 22:27:48',
                'deleted_at' => NULL,
            ),
            84 => 
            array (
                'id' => 585,
                'denuncia_id' => 593,
                'victima_id' => 585,
                'created_at' => '2019-01-29 22:27:48',
                'updated_at' => '2019-01-29 22:27:48',
                'deleted_at' => NULL,
            ),
            85 => 
            array (
                'id' => 586,
                'denuncia_id' => 594,
                'victima_id' => 586,
                'created_at' => '2019-01-29 22:27:48',
                'updated_at' => '2019-01-29 22:27:48',
                'deleted_at' => NULL,
            ),
            86 => 
            array (
                'id' => 587,
                'denuncia_id' => 595,
                'victima_id' => 587,
                'created_at' => '2019-01-29 22:27:48',
                'updated_at' => '2019-01-29 22:27:48',
                'deleted_at' => NULL,
            ),
            87 => 
            array (
                'id' => 588,
                'denuncia_id' => 596,
                'victima_id' => 588,
                'created_at' => '2019-01-29 22:27:49',
                'updated_at' => '2019-01-29 22:27:49',
                'deleted_at' => NULL,
            ),
            88 => 
            array (
                'id' => 589,
                'denuncia_id' => 597,
                'victima_id' => 589,
                'created_at' => '2019-01-29 22:27:49',
                'updated_at' => '2019-01-29 22:27:49',
                'deleted_at' => NULL,
            ),
            89 => 
            array (
                'id' => 590,
                'denuncia_id' => 598,
                'victima_id' => 590,
                'created_at' => '2019-01-29 22:27:49',
                'updated_at' => '2019-01-29 22:27:49',
                'deleted_at' => NULL,
            ),
            90 => 
            array (
                'id' => 591,
                'denuncia_id' => 600,
                'victima_id' => 591,
                'created_at' => '2019-01-29 22:27:49',
                'updated_at' => '2019-01-29 22:27:49',
                'deleted_at' => NULL,
            ),
            91 => 
            array (
                'id' => 592,
                'denuncia_id' => 602,
                'victima_id' => 592,
                'created_at' => '2019-01-29 22:27:50',
                'updated_at' => '2019-01-29 22:27:50',
                'deleted_at' => NULL,
            ),
            92 => 
            array (
                'id' => 593,
                'denuncia_id' => 603,
                'victima_id' => 593,
                'created_at' => '2019-01-29 22:27:50',
                'updated_at' => '2019-01-29 22:27:50',
                'deleted_at' => NULL,
            ),
            93 => 
            array (
                'id' => 594,
                'denuncia_id' => 604,
                'victima_id' => 594,
                'created_at' => '2019-01-29 22:27:50',
                'updated_at' => '2019-01-29 22:27:50',
                'deleted_at' => NULL,
            ),
            94 => 
            array (
                'id' => 595,
                'denuncia_id' => 605,
                'victima_id' => 595,
                'created_at' => '2019-01-29 22:27:50',
                'updated_at' => '2019-01-29 22:27:50',
                'deleted_at' => NULL,
            ),
            95 => 
            array (
                'id' => 596,
                'denuncia_id' => 606,
                'victima_id' => 596,
                'created_at' => '2019-01-29 22:27:50',
                'updated_at' => '2019-01-29 22:27:50',
                'deleted_at' => NULL,
            ),
            96 => 
            array (
                'id' => 597,
                'denuncia_id' => 606,
                'victima_id' => 597,
                'created_at' => '2019-01-29 22:27:50',
                'updated_at' => '2019-01-29 22:27:50',
                'deleted_at' => NULL,
            ),
            97 => 
            array (
                'id' => 598,
                'denuncia_id' => 607,
                'victima_id' => 598,
                'created_at' => '2019-01-29 22:27:51',
                'updated_at' => '2019-01-29 22:27:51',
                'deleted_at' => NULL,
            ),
            98 => 
            array (
                'id' => 599,
                'denuncia_id' => 609,
                'victima_id' => 599,
                'created_at' => '2019-01-29 22:27:51',
                'updated_at' => '2019-01-29 22:27:51',
                'deleted_at' => NULL,
            ),
            99 => 
            array (
                'id' => 600,
                'denuncia_id' => 611,
                'victima_id' => 600,
                'created_at' => '2019-01-29 22:27:51',
                'updated_at' => '2019-01-29 22:27:51',
                'deleted_at' => NULL,
            ),
            100 => 
            array (
                'id' => 601,
                'denuncia_id' => 612,
                'victima_id' => 601,
                'created_at' => '2019-01-29 22:27:52',
                'updated_at' => '2019-01-29 22:27:52',
                'deleted_at' => NULL,
            ),
            101 => 
            array (
                'id' => 602,
                'denuncia_id' => 613,
                'victima_id' => 602,
                'created_at' => '2019-01-29 22:27:52',
                'updated_at' => '2019-01-29 22:27:52',
                'deleted_at' => NULL,
            ),
            102 => 
            array (
                'id' => 603,
                'denuncia_id' => 614,
                'victima_id' => 603,
                'created_at' => '2019-01-29 22:27:52',
                'updated_at' => '2019-01-29 22:27:52',
                'deleted_at' => NULL,
            ),
            103 => 
            array (
                'id' => 604,
                'denuncia_id' => 615,
                'victima_id' => 604,
                'created_at' => '2019-01-29 22:27:52',
                'updated_at' => '2019-01-29 22:27:52',
                'deleted_at' => NULL,
            ),
            104 => 
            array (
                'id' => 605,
                'denuncia_id' => 616,
                'victima_id' => 605,
                'created_at' => '2019-01-29 22:27:52',
                'updated_at' => '2019-01-29 22:27:52',
                'deleted_at' => NULL,
            ),
            105 => 
            array (
                'id' => 606,
                'denuncia_id' => 617,
                'victima_id' => 606,
                'created_at' => '2019-01-29 22:27:52',
                'updated_at' => '2019-01-29 22:27:52',
                'deleted_at' => NULL,
            ),
            106 => 
            array (
                'id' => 607,
                'denuncia_id' => 618,
                'victima_id' => 607,
                'created_at' => '2019-01-29 22:27:53',
                'updated_at' => '2019-01-29 22:27:53',
                'deleted_at' => NULL,
            ),
            107 => 
            array (
                'id' => 608,
                'denuncia_id' => 619,
                'victima_id' => 608,
                'created_at' => '2019-01-29 22:27:53',
                'updated_at' => '2019-01-29 22:27:53',
                'deleted_at' => NULL,
            ),
            108 => 
            array (
                'id' => 609,
                'denuncia_id' => 620,
                'victima_id' => 609,
                'created_at' => '2019-01-29 22:27:53',
                'updated_at' => '2019-01-29 22:27:53',
                'deleted_at' => NULL,
            ),
            109 => 
            array (
                'id' => 610,
                'denuncia_id' => 621,
                'victima_id' => 610,
                'created_at' => '2019-01-29 22:27:53',
                'updated_at' => '2019-01-29 22:27:53',
                'deleted_at' => NULL,
            ),
            110 => 
            array (
                'id' => 611,
                'denuncia_id' => 622,
                'victima_id' => 611,
                'created_at' => '2019-01-29 22:27:53',
                'updated_at' => '2019-01-29 22:27:53',
                'deleted_at' => NULL,
            ),
            111 => 
            array (
                'id' => 612,
                'denuncia_id' => 623,
                'victima_id' => 612,
                'created_at' => '2019-01-29 22:27:53',
                'updated_at' => '2019-01-29 22:27:53',
                'deleted_at' => NULL,
            ),
            112 => 
            array (
                'id' => 613,
                'denuncia_id' => 624,
                'victima_id' => 613,
                'created_at' => '2019-01-29 22:27:54',
                'updated_at' => '2019-01-29 22:27:54',
                'deleted_at' => NULL,
            ),
            113 => 
            array (
                'id' => 614,
                'denuncia_id' => 625,
                'victima_id' => 614,
                'created_at' => '2019-01-29 22:27:54',
                'updated_at' => '2019-01-29 22:27:54',
                'deleted_at' => NULL,
            ),
            114 => 
            array (
                'id' => 615,
                'denuncia_id' => 626,
                'victima_id' => 615,
                'created_at' => '2019-01-29 22:27:54',
                'updated_at' => '2019-01-29 22:27:54',
                'deleted_at' => NULL,
            ),
            115 => 
            array (
                'id' => 616,
                'denuncia_id' => 627,
                'victima_id' => 616,
                'created_at' => '2019-01-29 22:27:54',
                'updated_at' => '2019-01-29 22:27:54',
                'deleted_at' => NULL,
            ),
            116 => 
            array (
                'id' => 617,
                'denuncia_id' => 628,
                'victima_id' => 617,
                'created_at' => '2019-01-29 22:27:54',
                'updated_at' => '2019-01-29 22:27:54',
                'deleted_at' => NULL,
            ),
            117 => 
            array (
                'id' => 618,
                'denuncia_id' => 629,
                'victima_id' => 618,
                'created_at' => '2019-01-29 22:27:55',
                'updated_at' => '2019-01-29 22:27:55',
                'deleted_at' => NULL,
            ),
            118 => 
            array (
                'id' => 619,
                'denuncia_id' => 630,
                'victima_id' => 619,
                'created_at' => '2019-01-29 22:27:55',
                'updated_at' => '2019-01-29 22:27:55',
                'deleted_at' => NULL,
            ),
            119 => 
            array (
                'id' => 620,
                'denuncia_id' => 631,
                'victima_id' => 620,
                'created_at' => '2019-01-29 22:27:55',
                'updated_at' => '2019-01-29 22:27:55',
                'deleted_at' => NULL,
            ),
            120 => 
            array (
                'id' => 621,
                'denuncia_id' => 632,
                'victima_id' => 621,
                'created_at' => '2019-01-29 22:27:55',
                'updated_at' => '2019-01-29 22:27:55',
                'deleted_at' => NULL,
            ),
            121 => 
            array (
                'id' => 622,
                'denuncia_id' => 633,
                'victima_id' => 622,
                'created_at' => '2019-01-29 22:27:55',
                'updated_at' => '2019-01-29 22:27:55',
                'deleted_at' => NULL,
            ),
            122 => 
            array (
                'id' => 623,
                'denuncia_id' => 634,
                'victima_id' => 623,
                'created_at' => '2019-01-29 22:27:55',
                'updated_at' => '2019-01-29 22:27:55',
                'deleted_at' => NULL,
            ),
            123 => 
            array (
                'id' => 624,
                'denuncia_id' => 635,
                'victima_id' => 624,
                'created_at' => '2019-01-29 22:27:56',
                'updated_at' => '2019-01-29 22:27:56',
                'deleted_at' => NULL,
            ),
            124 => 
            array (
                'id' => 625,
                'denuncia_id' => 636,
                'victima_id' => 625,
                'created_at' => '2019-01-29 22:27:56',
                'updated_at' => '2019-01-29 22:27:56',
                'deleted_at' => NULL,
            ),
            125 => 
            array (
                'id' => 626,
                'denuncia_id' => 637,
                'victima_id' => 626,
                'created_at' => '2019-01-29 22:27:56',
                'updated_at' => '2019-01-29 22:27:56',
                'deleted_at' => NULL,
            ),
            126 => 
            array (
                'id' => 627,
                'denuncia_id' => 638,
                'victima_id' => 627,
                'created_at' => '2019-01-29 22:27:56',
                'updated_at' => '2019-01-29 22:27:56',
                'deleted_at' => NULL,
            ),
            127 => 
            array (
                'id' => 628,
                'denuncia_id' => 639,
                'victima_id' => 628,
                'created_at' => '2019-01-29 22:27:56',
                'updated_at' => '2019-01-29 22:27:56',
                'deleted_at' => NULL,
            ),
            128 => 
            array (
                'id' => 629,
                'denuncia_id' => 640,
                'victima_id' => 629,
                'created_at' => '2019-01-29 22:27:56',
                'updated_at' => '2019-01-29 22:27:56',
                'deleted_at' => NULL,
            ),
            129 => 
            array (
                'id' => 630,
                'denuncia_id' => 641,
                'victima_id' => 630,
                'created_at' => '2019-01-29 22:27:57',
                'updated_at' => '2019-01-29 22:27:57',
                'deleted_at' => NULL,
            ),
            130 => 
            array (
                'id' => 631,
                'denuncia_id' => 642,
                'victima_id' => 631,
                'created_at' => '2019-01-29 22:27:57',
                'updated_at' => '2019-01-29 22:27:57',
                'deleted_at' => NULL,
            ),
            131 => 
            array (
                'id' => 632,
                'denuncia_id' => 643,
                'victima_id' => 632,
                'created_at' => '2019-01-29 22:27:57',
                'updated_at' => '2019-01-29 22:27:57',
                'deleted_at' => NULL,
            ),
            132 => 
            array (
                'id' => 633,
                'denuncia_id' => 644,
                'victima_id' => 633,
                'created_at' => '2019-01-29 22:27:57',
                'updated_at' => '2019-01-29 22:27:57',
                'deleted_at' => NULL,
            ),
            133 => 
            array (
                'id' => 634,
                'denuncia_id' => 645,
                'victima_id' => 634,
                'created_at' => '2019-01-29 22:27:57',
                'updated_at' => '2019-01-29 22:27:57',
                'deleted_at' => NULL,
            ),
            134 => 
            array (
                'id' => 635,
                'denuncia_id' => 645,
                'victima_id' => 635,
                'created_at' => '2019-01-29 22:27:57',
                'updated_at' => '2019-01-29 22:27:57',
                'deleted_at' => NULL,
            ),
            135 => 
            array (
                'id' => 636,
                'denuncia_id' => 646,
                'victima_id' => 636,
                'created_at' => '2019-01-29 22:27:58',
                'updated_at' => '2019-01-29 22:27:58',
                'deleted_at' => NULL,
            ),
            136 => 
            array (
                'id' => 637,
                'denuncia_id' => 647,
                'victima_id' => 637,
                'created_at' => '2019-01-29 22:27:58',
                'updated_at' => '2019-01-29 22:27:58',
                'deleted_at' => NULL,
            ),
            137 => 
            array (
                'id' => 638,
                'denuncia_id' => 648,
                'victima_id' => 638,
                'created_at' => '2019-01-29 22:27:58',
                'updated_at' => '2019-01-29 22:27:58',
                'deleted_at' => NULL,
            ),
            138 => 
            array (
                'id' => 639,
                'denuncia_id' => 649,
                'victima_id' => 639,
                'created_at' => '2019-01-29 22:27:58',
                'updated_at' => '2019-01-29 22:27:58',
                'deleted_at' => NULL,
            ),
            139 => 
            array (
                'id' => 640,
                'denuncia_id' => 650,
                'victima_id' => 640,
                'created_at' => '2019-01-29 22:27:58',
                'updated_at' => '2019-01-29 22:27:58',
                'deleted_at' => NULL,
            ),
            140 => 
            array (
                'id' => 641,
                'denuncia_id' => 651,
                'victima_id' => 641,
                'created_at' => '2019-01-29 22:27:59',
                'updated_at' => '2019-01-29 22:27:59',
                'deleted_at' => NULL,
            ),
            141 => 
            array (
                'id' => 642,
                'denuncia_id' => 652,
                'victima_id' => 642,
                'created_at' => '2019-01-29 22:27:59',
                'updated_at' => '2019-01-29 22:27:59',
                'deleted_at' => NULL,
            ),
            142 => 
            array (
                'id' => 643,
                'denuncia_id' => 653,
                'victima_id' => 643,
                'created_at' => '2019-01-29 22:27:59',
                'updated_at' => '2019-01-29 22:27:59',
                'deleted_at' => NULL,
            ),
            143 => 
            array (
                'id' => 644,
                'denuncia_id' => 654,
                'victima_id' => 644,
                'created_at' => '2019-01-29 22:27:59',
                'updated_at' => '2019-01-29 22:27:59',
                'deleted_at' => NULL,
            ),
            144 => 
            array (
                'id' => 645,
                'denuncia_id' => 655,
                'victima_id' => 645,
                'created_at' => '2019-01-29 22:27:59',
                'updated_at' => '2019-01-29 22:27:59',
                'deleted_at' => NULL,
            ),
            145 => 
            array (
                'id' => 646,
                'denuncia_id' => 656,
                'victima_id' => 646,
                'created_at' => '2019-01-29 22:28:00',
                'updated_at' => '2019-01-29 22:28:00',
                'deleted_at' => NULL,
            ),
            146 => 
            array (
                'id' => 647,
                'denuncia_id' => 657,
                'victima_id' => 647,
                'created_at' => '2019-01-29 22:28:00',
                'updated_at' => '2019-01-29 22:28:00',
                'deleted_at' => NULL,
            ),
            147 => 
            array (
                'id' => 648,
                'denuncia_id' => 658,
                'victima_id' => 648,
                'created_at' => '2019-01-29 22:28:00',
                'updated_at' => '2019-01-29 22:28:00',
                'deleted_at' => NULL,
            ),
            148 => 
            array (
                'id' => 649,
                'denuncia_id' => 659,
                'victima_id' => 649,
                'created_at' => '2019-01-29 22:28:00',
                'updated_at' => '2019-01-29 22:28:00',
                'deleted_at' => NULL,
            ),
            149 => 
            array (
                'id' => 650,
                'denuncia_id' => 660,
                'victima_id' => 650,
                'created_at' => '2019-01-29 22:28:00',
                'updated_at' => '2019-01-29 22:28:00',
                'deleted_at' => NULL,
            ),
            150 => 
            array (
                'id' => 651,
                'denuncia_id' => 661,
                'victima_id' => 651,
                'created_at' => '2019-01-29 22:28:00',
                'updated_at' => '2019-01-29 22:28:00',
                'deleted_at' => NULL,
            ),
            151 => 
            array (
                'id' => 652,
                'denuncia_id' => 662,
                'victima_id' => 652,
                'created_at' => '2019-01-29 22:28:01',
                'updated_at' => '2019-01-29 22:28:01',
                'deleted_at' => NULL,
            ),
            152 => 
            array (
                'id' => 653,
                'denuncia_id' => 662,
                'victima_id' => 653,
                'created_at' => '2019-01-29 22:28:01',
                'updated_at' => '2019-01-29 22:28:01',
                'deleted_at' => NULL,
            ),
            153 => 
            array (
                'id' => 654,
                'denuncia_id' => 663,
                'victima_id' => 654,
                'created_at' => '2019-01-29 22:28:01',
                'updated_at' => '2019-01-29 22:28:01',
                'deleted_at' => NULL,
            ),
            154 => 
            array (
                'id' => 655,
                'denuncia_id' => 665,
                'victima_id' => 655,
                'created_at' => '2019-01-29 22:28:01',
                'updated_at' => '2019-01-29 22:28:01',
                'deleted_at' => NULL,
            ),
            155 => 
            array (
                'id' => 656,
                'denuncia_id' => 666,
                'victima_id' => 656,
                'created_at' => '2019-01-29 22:28:01',
                'updated_at' => '2019-01-29 22:28:01',
                'deleted_at' => NULL,
            ),
            156 => 
            array (
                'id' => 657,
                'denuncia_id' => 667,
                'victima_id' => 657,
                'created_at' => '2019-01-29 22:28:02',
                'updated_at' => '2019-01-29 22:28:02',
                'deleted_at' => NULL,
            ),
            157 => 
            array (
                'id' => 658,
                'denuncia_id' => 668,
                'victima_id' => 658,
                'created_at' => '2019-01-29 22:28:02',
                'updated_at' => '2019-01-29 22:28:02',
                'deleted_at' => NULL,
            ),
            158 => 
            array (
                'id' => 659,
                'denuncia_id' => 669,
                'victima_id' => 659,
                'created_at' => '2019-01-29 22:28:02',
                'updated_at' => '2019-01-29 22:28:02',
                'deleted_at' => NULL,
            ),
            159 => 
            array (
                'id' => 660,
                'denuncia_id' => 670,
                'victima_id' => 660,
                'created_at' => '2019-01-29 22:28:02',
                'updated_at' => '2019-01-29 22:28:02',
                'deleted_at' => NULL,
            ),
            160 => 
            array (
                'id' => 661,
                'denuncia_id' => 671,
                'victima_id' => 661,
                'created_at' => '2019-01-29 22:28:02',
                'updated_at' => '2019-01-29 22:28:02',
                'deleted_at' => NULL,
            ),
            161 => 
            array (
                'id' => 662,
                'denuncia_id' => 672,
                'victima_id' => 662,
                'created_at' => '2019-01-29 22:28:03',
                'updated_at' => '2019-01-29 22:28:03',
                'deleted_at' => NULL,
            ),
            162 => 
            array (
                'id' => 663,
                'denuncia_id' => 673,
                'victima_id' => 663,
                'created_at' => '2019-01-29 22:28:03',
                'updated_at' => '2019-01-29 22:28:03',
                'deleted_at' => NULL,
            ),
            163 => 
            array (
                'id' => 664,
                'denuncia_id' => 674,
                'victima_id' => 664,
                'created_at' => '2019-01-29 22:28:03',
                'updated_at' => '2019-01-29 22:28:03',
                'deleted_at' => NULL,
            ),
            164 => 
            array (
                'id' => 665,
                'denuncia_id' => 675,
                'victima_id' => 665,
                'created_at' => '2019-01-29 22:28:03',
                'updated_at' => '2019-01-29 22:28:03',
                'deleted_at' => NULL,
            ),
            165 => 
            array (
                'id' => 666,
                'denuncia_id' => 676,
                'victima_id' => 666,
                'created_at' => '2019-01-29 22:28:03',
                'updated_at' => '2019-01-29 22:28:03',
                'deleted_at' => NULL,
            ),
            166 => 
            array (
                'id' => 667,
                'denuncia_id' => 677,
                'victima_id' => 667,
                'created_at' => '2019-01-29 22:28:03',
                'updated_at' => '2019-01-29 22:28:03',
                'deleted_at' => NULL,
            ),
            167 => 
            array (
                'id' => 668,
                'denuncia_id' => 678,
                'victima_id' => 668,
                'created_at' => '2019-01-29 22:28:04',
                'updated_at' => '2019-01-29 22:28:04',
                'deleted_at' => NULL,
            ),
            168 => 
            array (
                'id' => 669,
                'denuncia_id' => 679,
                'victima_id' => 669,
                'created_at' => '2019-01-29 22:28:04',
                'updated_at' => '2019-01-29 22:28:04',
                'deleted_at' => NULL,
            ),
            169 => 
            array (
                'id' => 670,
                'denuncia_id' => 680,
                'victima_id' => 670,
                'created_at' => '2019-01-29 22:28:04',
                'updated_at' => '2019-01-29 22:28:04',
                'deleted_at' => NULL,
            ),
            170 => 
            array (
                'id' => 671,
                'denuncia_id' => 681,
                'victima_id' => 671,
                'created_at' => '2019-01-29 22:28:04',
                'updated_at' => '2019-01-29 22:28:04',
                'deleted_at' => NULL,
            ),
            171 => 
            array (
                'id' => 672,
                'denuncia_id' => 682,
                'victima_id' => 672,
                'created_at' => '2019-01-29 22:28:04',
                'updated_at' => '2019-01-29 22:28:04',
                'deleted_at' => NULL,
            ),
            172 => 
            array (
                'id' => 673,
                'denuncia_id' => 683,
                'victima_id' => 673,
                'created_at' => '2019-01-29 22:28:04',
                'updated_at' => '2019-01-29 22:28:04',
                'deleted_at' => NULL,
            ),
            173 => 
            array (
                'id' => 674,
                'denuncia_id' => 684,
                'victima_id' => 674,
                'created_at' => '2019-01-29 22:28:05',
                'updated_at' => '2019-01-29 22:28:05',
                'deleted_at' => NULL,
            ),
            174 => 
            array (
                'id' => 675,
                'denuncia_id' => 685,
                'victima_id' => 675,
                'created_at' => '2019-01-29 22:28:05',
                'updated_at' => '2019-01-29 22:28:05',
                'deleted_at' => NULL,
            ),
            175 => 
            array (
                'id' => 676,
                'denuncia_id' => 686,
                'victima_id' => 676,
                'created_at' => '2019-01-29 22:28:05',
                'updated_at' => '2019-01-29 22:28:05',
                'deleted_at' => NULL,
            ),
            176 => 
            array (
                'id' => 677,
                'denuncia_id' => 687,
                'victima_id' => 677,
                'created_at' => '2019-01-29 22:28:05',
                'updated_at' => '2019-01-29 22:28:05',
                'deleted_at' => NULL,
            ),
            177 => 
            array (
                'id' => 678,
                'denuncia_id' => 689,
                'victima_id' => 678,
                'created_at' => '2019-01-29 22:28:06',
                'updated_at' => '2019-01-29 22:28:06',
                'deleted_at' => NULL,
            ),
            178 => 
            array (
                'id' => 679,
                'denuncia_id' => 690,
                'victima_id' => 679,
                'created_at' => '2019-01-29 22:28:06',
                'updated_at' => '2019-01-29 22:28:06',
                'deleted_at' => NULL,
            ),
            179 => 
            array (
                'id' => 680,
                'denuncia_id' => 691,
                'victima_id' => 680,
                'created_at' => '2019-01-29 22:28:06',
                'updated_at' => '2019-01-29 22:28:06',
                'deleted_at' => NULL,
            ),
            180 => 
            array (
                'id' => 681,
                'denuncia_id' => 692,
                'victima_id' => 681,
                'created_at' => '2019-01-29 22:28:06',
                'updated_at' => '2019-01-29 22:28:06',
                'deleted_at' => NULL,
            ),
            181 => 
            array (
                'id' => 682,
                'denuncia_id' => 693,
                'victima_id' => 682,
                'created_at' => '2019-01-29 22:28:07',
                'updated_at' => '2019-01-29 22:28:07',
                'deleted_at' => NULL,
            ),
            182 => 
            array (
                'id' => 683,
                'denuncia_id' => 694,
                'victima_id' => 683,
                'created_at' => '2019-01-29 22:28:07',
                'updated_at' => '2019-01-29 22:28:07',
                'deleted_at' => NULL,
            ),
            183 => 
            array (
                'id' => 684,
                'denuncia_id' => 695,
                'victima_id' => 684,
                'created_at' => '2019-01-29 22:28:07',
                'updated_at' => '2019-01-29 22:28:07',
                'deleted_at' => NULL,
            ),
            184 => 
            array (
                'id' => 685,
                'denuncia_id' => 696,
                'victima_id' => 685,
                'created_at' => '2019-01-29 22:28:07',
                'updated_at' => '2019-01-29 22:28:07',
                'deleted_at' => NULL,
            ),
            185 => 
            array (
                'id' => 686,
                'denuncia_id' => 697,
                'victima_id' => 686,
                'created_at' => '2019-01-29 22:28:08',
                'updated_at' => '2019-01-29 22:28:08',
                'deleted_at' => NULL,
            ),
            186 => 
            array (
                'id' => 687,
                'denuncia_id' => 698,
                'victima_id' => 687,
                'created_at' => '2019-01-29 22:28:08',
                'updated_at' => '2019-01-29 22:28:08',
                'deleted_at' => NULL,
            ),
            187 => 
            array (
                'id' => 688,
                'denuncia_id' => 699,
                'victima_id' => 688,
                'created_at' => '2019-01-29 22:28:08',
                'updated_at' => '2019-01-29 22:28:08',
                'deleted_at' => NULL,
            ),
            188 => 
            array (
                'id' => 689,
                'denuncia_id' => 700,
                'victima_id' => 689,
                'created_at' => '2019-01-29 22:28:08',
                'updated_at' => '2019-01-29 22:28:08',
                'deleted_at' => NULL,
            ),
            189 => 
            array (
                'id' => 690,
                'denuncia_id' => 701,
                'victima_id' => 690,
                'created_at' => '2019-01-29 22:28:08',
                'updated_at' => '2019-01-29 22:28:08',
                'deleted_at' => NULL,
            ),
            190 => 
            array (
                'id' => 691,
                'denuncia_id' => 702,
                'victima_id' => 691,
                'created_at' => '2019-01-29 22:28:09',
                'updated_at' => '2019-01-29 22:28:09',
                'deleted_at' => NULL,
            ),
            191 => 
            array (
                'id' => 692,
                'denuncia_id' => 703,
                'victima_id' => 692,
                'created_at' => '2019-01-29 22:28:09',
                'updated_at' => '2019-01-29 22:28:09',
                'deleted_at' => NULL,
            ),
            192 => 
            array (
                'id' => 693,
                'denuncia_id' => 704,
                'victima_id' => 693,
                'created_at' => '2019-01-29 22:28:09',
                'updated_at' => '2019-01-29 22:28:09',
                'deleted_at' => NULL,
            ),
            193 => 
            array (
                'id' => 694,
                'denuncia_id' => 705,
                'victima_id' => 694,
                'created_at' => '2019-01-29 22:28:09',
                'updated_at' => '2019-01-29 22:28:09',
                'deleted_at' => NULL,
            ),
            194 => 
            array (
                'id' => 695,
                'denuncia_id' => 706,
                'victima_id' => 695,
                'created_at' => '2019-01-29 22:28:09',
                'updated_at' => '2019-01-29 22:28:09',
                'deleted_at' => NULL,
            ),
            195 => 
            array (
                'id' => 696,
                'denuncia_id' => 707,
                'victima_id' => 696,
                'created_at' => '2019-01-29 22:28:09',
                'updated_at' => '2019-01-29 22:28:09',
                'deleted_at' => NULL,
            ),
            196 => 
            array (
                'id' => 697,
                'denuncia_id' => 708,
                'victima_id' => 697,
                'created_at' => '2019-01-29 22:28:09',
                'updated_at' => '2019-01-29 22:28:09',
                'deleted_at' => NULL,
            ),
            197 => 
            array (
                'id' => 698,
                'denuncia_id' => 709,
                'victima_id' => 698,
                'created_at' => '2019-01-29 22:28:10',
                'updated_at' => '2019-01-29 22:28:10',
                'deleted_at' => NULL,
            ),
            198 => 
            array (
                'id' => 699,
                'denuncia_id' => 710,
                'victima_id' => 699,
                'created_at' => '2019-01-29 22:28:10',
                'updated_at' => '2019-01-29 22:28:10',
                'deleted_at' => NULL,
            ),
            199 => 
            array (
                'id' => 700,
                'denuncia_id' => 711,
                'victima_id' => 700,
                'created_at' => '2019-01-29 22:28:10',
                'updated_at' => '2019-01-29 22:28:10',
                'deleted_at' => NULL,
            ),
            200 => 
            array (
                'id' => 701,
                'denuncia_id' => 711,
                'victima_id' => 701,
                'created_at' => '2019-01-29 22:28:10',
                'updated_at' => '2019-01-29 22:28:10',
                'deleted_at' => NULL,
            ),
            201 => 
            array (
                'id' => 702,
                'denuncia_id' => 712,
                'victima_id' => 702,
                'created_at' => '2019-01-29 22:28:10',
                'updated_at' => '2019-01-29 22:28:10',
                'deleted_at' => NULL,
            ),
            202 => 
            array (
                'id' => 703,
                'denuncia_id' => 713,
                'victima_id' => 703,
                'created_at' => '2019-01-29 22:28:10',
                'updated_at' => '2019-01-29 22:28:10',
                'deleted_at' => NULL,
            ),
            203 => 
            array (
                'id' => 704,
                'denuncia_id' => 714,
                'victima_id' => 704,
                'created_at' => '2019-01-29 22:28:10',
                'updated_at' => '2019-01-29 22:28:10',
                'deleted_at' => NULL,
            ),
            204 => 
            array (
                'id' => 705,
                'denuncia_id' => 715,
                'victima_id' => 705,
                'created_at' => '2019-01-29 22:28:11',
                'updated_at' => '2019-01-29 22:28:11',
                'deleted_at' => NULL,
            ),
            205 => 
            array (
                'id' => 706,
                'denuncia_id' => 716,
                'victima_id' => 706,
                'created_at' => '2019-01-29 22:28:11',
                'updated_at' => '2019-01-29 22:28:11',
                'deleted_at' => NULL,
            ),
            206 => 
            array (
                'id' => 707,
                'denuncia_id' => 717,
                'victima_id' => 707,
                'created_at' => '2019-01-29 22:28:11',
                'updated_at' => '2019-01-29 22:28:11',
                'deleted_at' => NULL,
            ),
            207 => 
            array (
                'id' => 708,
                'denuncia_id' => 718,
                'victima_id' => 708,
                'created_at' => '2019-01-29 22:28:11',
                'updated_at' => '2019-01-29 22:28:11',
                'deleted_at' => NULL,
            ),
            208 => 
            array (
                'id' => 709,
                'denuncia_id' => 719,
                'victima_id' => 709,
                'created_at' => '2019-01-29 22:28:11',
                'updated_at' => '2019-01-29 22:28:11',
                'deleted_at' => NULL,
            ),
            209 => 
            array (
                'id' => 710,
                'denuncia_id' => 720,
                'victima_id' => 710,
                'created_at' => '2019-01-29 22:28:11',
                'updated_at' => '2019-01-29 22:28:11',
                'deleted_at' => NULL,
            ),
            210 => 
            array (
                'id' => 711,
                'denuncia_id' => 721,
                'victima_id' => 711,
                'created_at' => '2019-01-29 22:28:12',
                'updated_at' => '2019-01-29 22:28:12',
                'deleted_at' => NULL,
            ),
            211 => 
            array (
                'id' => 712,
                'denuncia_id' => 722,
                'victima_id' => 712,
                'created_at' => '2019-01-29 22:28:12',
                'updated_at' => '2019-01-29 22:28:12',
                'deleted_at' => NULL,
            ),
            212 => 
            array (
                'id' => 713,
                'denuncia_id' => 723,
                'victima_id' => 713,
                'created_at' => '2019-01-29 22:28:12',
                'updated_at' => '2019-01-29 22:28:12',
                'deleted_at' => NULL,
            ),
            213 => 
            array (
                'id' => 714,
                'denuncia_id' => 724,
                'victima_id' => 714,
                'created_at' => '2019-01-29 22:28:12',
                'updated_at' => '2019-01-29 22:28:12',
                'deleted_at' => NULL,
            ),
            214 => 
            array (
                'id' => 715,
                'denuncia_id' => 725,
                'victima_id' => 715,
                'created_at' => '2019-01-29 22:28:12',
                'updated_at' => '2019-01-29 22:28:12',
                'deleted_at' => NULL,
            ),
            215 => 
            array (
                'id' => 716,
                'denuncia_id' => 726,
                'victima_id' => 716,
                'created_at' => '2019-01-29 22:28:13',
                'updated_at' => '2019-01-29 22:28:13',
                'deleted_at' => NULL,
            ),
            216 => 
            array (
                'id' => 717,
                'denuncia_id' => 727,
                'victima_id' => 717,
                'created_at' => '2019-01-29 22:28:13',
                'updated_at' => '2019-01-29 22:28:13',
                'deleted_at' => NULL,
            ),
            217 => 
            array (
                'id' => 718,
                'denuncia_id' => 728,
                'victima_id' => 718,
                'created_at' => '2019-01-29 22:28:13',
                'updated_at' => '2019-01-29 22:28:13',
                'deleted_at' => NULL,
            ),
            218 => 
            array (
                'id' => 719,
                'denuncia_id' => 729,
                'victima_id' => 719,
                'created_at' => '2019-01-29 22:28:13',
                'updated_at' => '2019-01-29 22:28:13',
                'deleted_at' => NULL,
            ),
            219 => 
            array (
                'id' => 720,
                'denuncia_id' => 730,
                'victima_id' => 720,
                'created_at' => '2019-01-29 22:28:13',
                'updated_at' => '2019-01-29 22:28:13',
                'deleted_at' => NULL,
            ),
            220 => 
            array (
                'id' => 721,
                'denuncia_id' => 731,
                'victima_id' => 721,
                'created_at' => '2019-01-29 22:28:14',
                'updated_at' => '2019-01-29 22:28:14',
                'deleted_at' => NULL,
            ),
            221 => 
            array (
                'id' => 722,
                'denuncia_id' => 732,
                'victima_id' => 722,
                'created_at' => '2019-01-29 22:28:14',
                'updated_at' => '2019-01-29 22:28:14',
                'deleted_at' => NULL,
            ),
            222 => 
            array (
                'id' => 723,
                'denuncia_id' => 733,
                'victima_id' => 723,
                'created_at' => '2019-01-29 22:28:14',
                'updated_at' => '2019-01-29 22:28:14',
                'deleted_at' => NULL,
            ),
            223 => 
            array (
                'id' => 724,
                'denuncia_id' => 734,
                'victima_id' => 724,
                'created_at' => '2019-01-29 22:28:14',
                'updated_at' => '2019-01-29 22:28:14',
                'deleted_at' => NULL,
            ),
            224 => 
            array (
                'id' => 725,
                'denuncia_id' => 735,
                'victima_id' => 725,
                'created_at' => '2019-01-29 22:28:14',
                'updated_at' => '2019-01-29 22:28:14',
                'deleted_at' => NULL,
            ),
            225 => 
            array (
                'id' => 726,
                'denuncia_id' => 736,
                'victima_id' => 726,
                'created_at' => '2019-01-29 22:28:14',
                'updated_at' => '2019-01-29 22:28:14',
                'deleted_at' => NULL,
            ),
            226 => 
            array (
                'id' => 727,
                'denuncia_id' => 737,
                'victima_id' => 727,
                'created_at' => '2019-01-29 22:28:15',
                'updated_at' => '2019-01-29 22:28:15',
                'deleted_at' => NULL,
            ),
            227 => 
            array (
                'id' => 728,
                'denuncia_id' => 738,
                'victima_id' => 728,
                'created_at' => '2019-01-29 22:28:15',
                'updated_at' => '2019-01-29 22:28:15',
                'deleted_at' => NULL,
            ),
            228 => 
            array (
                'id' => 729,
                'denuncia_id' => 739,
                'victima_id' => 729,
                'created_at' => '2019-01-29 22:28:15',
                'updated_at' => '2019-01-29 22:28:15',
                'deleted_at' => NULL,
            ),
            229 => 
            array (
                'id' => 730,
                'denuncia_id' => 740,
                'victima_id' => 730,
                'created_at' => '2019-01-29 22:28:15',
                'updated_at' => '2019-01-29 22:28:15',
                'deleted_at' => NULL,
            ),
            230 => 
            array (
                'id' => 731,
                'denuncia_id' => 741,
                'victima_id' => 731,
                'created_at' => '2019-01-29 22:28:15',
                'updated_at' => '2019-01-29 22:28:15',
                'deleted_at' => NULL,
            ),
            231 => 
            array (
                'id' => 732,
                'denuncia_id' => 742,
                'victima_id' => 732,
                'created_at' => '2019-01-29 22:28:16',
                'updated_at' => '2019-01-29 22:28:16',
                'deleted_at' => NULL,
            ),
            232 => 
            array (
                'id' => 733,
                'denuncia_id' => 743,
                'victima_id' => 733,
                'created_at' => '2019-01-29 22:28:16',
                'updated_at' => '2019-01-29 22:28:16',
                'deleted_at' => NULL,
            ),
            233 => 
            array (
                'id' => 734,
                'denuncia_id' => 744,
                'victima_id' => 734,
                'created_at' => '2019-01-29 22:28:16',
                'updated_at' => '2019-01-29 22:28:16',
                'deleted_at' => NULL,
            ),
            234 => 
            array (
                'id' => 735,
                'denuncia_id' => 745,
                'victima_id' => 735,
                'created_at' => '2019-01-29 22:28:16',
                'updated_at' => '2019-01-29 22:28:16',
                'deleted_at' => NULL,
            ),
            235 => 
            array (
                'id' => 736,
                'denuncia_id' => 746,
                'victima_id' => 736,
                'created_at' => '2019-01-29 22:28:16',
                'updated_at' => '2019-01-29 22:28:16',
                'deleted_at' => NULL,
            ),
            236 => 
            array (
                'id' => 737,
                'denuncia_id' => 747,
                'victima_id' => 737,
                'created_at' => '2019-01-29 22:28:16',
                'updated_at' => '2019-01-29 22:28:16',
                'deleted_at' => NULL,
            ),
            237 => 
            array (
                'id' => 738,
                'denuncia_id' => 748,
                'victima_id' => 738,
                'created_at' => '2019-01-29 22:28:17',
                'updated_at' => '2019-01-29 22:28:17',
                'deleted_at' => NULL,
            ),
            238 => 
            array (
                'id' => 739,
                'denuncia_id' => 749,
                'victima_id' => 739,
                'created_at' => '2019-01-29 22:28:17',
                'updated_at' => '2019-01-29 22:28:17',
                'deleted_at' => NULL,
            ),
            239 => 
            array (
                'id' => 740,
                'denuncia_id' => 750,
                'victima_id' => 740,
                'created_at' => '2019-01-29 22:28:17',
                'updated_at' => '2019-01-29 22:28:17',
                'deleted_at' => NULL,
            ),
            240 => 
            array (
                'id' => 741,
                'denuncia_id' => 752,
                'victima_id' => 741,
                'created_at' => '2019-01-29 22:28:17',
                'updated_at' => '2019-01-29 22:28:17',
                'deleted_at' => NULL,
            ),
            241 => 
            array (
                'id' => 742,
                'denuncia_id' => 753,
                'victima_id' => 742,
                'created_at' => '2019-01-29 22:28:17',
                'updated_at' => '2019-01-29 22:28:17',
                'deleted_at' => NULL,
            ),
            242 => 
            array (
                'id' => 743,
                'denuncia_id' => 754,
                'victima_id' => 743,
                'created_at' => '2019-01-29 22:28:18',
                'updated_at' => '2019-01-29 22:28:18',
                'deleted_at' => NULL,
            ),
            243 => 
            array (
                'id' => 744,
                'denuncia_id' => 755,
                'victima_id' => 744,
                'created_at' => '2019-01-29 22:28:18',
                'updated_at' => '2019-01-29 22:28:18',
                'deleted_at' => NULL,
            ),
            244 => 
            array (
                'id' => 745,
                'denuncia_id' => 756,
                'victima_id' => 745,
                'created_at' => '2019-01-29 22:28:18',
                'updated_at' => '2019-01-29 22:28:18',
                'deleted_at' => NULL,
            ),
            245 => 
            array (
                'id' => 746,
                'denuncia_id' => 757,
                'victima_id' => 746,
                'created_at' => '2019-01-29 22:28:18',
                'updated_at' => '2019-01-29 22:28:18',
                'deleted_at' => NULL,
            ),
            246 => 
            array (
                'id' => 747,
                'denuncia_id' => 758,
                'victima_id' => 747,
                'created_at' => '2019-01-29 22:28:18',
                'updated_at' => '2019-01-29 22:28:18',
                'deleted_at' => NULL,
            ),
            247 => 
            array (
                'id' => 748,
                'denuncia_id' => 759,
                'victima_id' => 748,
                'created_at' => '2019-01-29 22:28:18',
                'updated_at' => '2019-01-29 22:28:18',
                'deleted_at' => NULL,
            ),
            248 => 
            array (
                'id' => 749,
                'denuncia_id' => 760,
                'victima_id' => 749,
                'created_at' => '2019-01-29 22:28:19',
                'updated_at' => '2019-01-29 22:28:19',
                'deleted_at' => NULL,
            ),
            249 => 
            array (
                'id' => 750,
                'denuncia_id' => 762,
                'victima_id' => 750,
                'created_at' => '2019-01-29 22:28:19',
                'updated_at' => '2019-01-29 22:28:19',
                'deleted_at' => NULL,
            ),
            250 => 
            array (
                'id' => 751,
                'denuncia_id' => 763,
                'victima_id' => 751,
                'created_at' => '2019-01-29 22:28:19',
                'updated_at' => '2019-01-29 22:28:19',
                'deleted_at' => NULL,
            ),
            251 => 
            array (
                'id' => 752,
                'denuncia_id' => 764,
                'victima_id' => 752,
                'created_at' => '2019-01-29 22:28:19',
                'updated_at' => '2019-01-29 22:28:19',
                'deleted_at' => NULL,
            ),
            252 => 
            array (
                'id' => 753,
                'denuncia_id' => 765,
                'victima_id' => 753,
                'created_at' => '2019-01-29 22:28:20',
                'updated_at' => '2019-01-29 22:28:20',
                'deleted_at' => NULL,
            ),
            253 => 
            array (
                'id' => 754,
                'denuncia_id' => 766,
                'victima_id' => 754,
                'created_at' => '2019-01-29 22:28:20',
                'updated_at' => '2019-01-29 22:28:20',
                'deleted_at' => NULL,
            ),
            254 => 
            array (
                'id' => 755,
                'denuncia_id' => 767,
                'victima_id' => 755,
                'created_at' => '2019-01-29 22:28:20',
                'updated_at' => '2019-01-29 22:28:20',
                'deleted_at' => NULL,
            ),
            255 => 
            array (
                'id' => 756,
                'denuncia_id' => 768,
                'victima_id' => 756,
                'created_at' => '2019-01-29 22:28:20',
                'updated_at' => '2019-01-29 22:28:20',
                'deleted_at' => NULL,
            ),
            256 => 
            array (
                'id' => 757,
                'denuncia_id' => 770,
                'victima_id' => 757,
                'created_at' => '2019-01-29 22:28:20',
                'updated_at' => '2019-01-29 22:28:20',
                'deleted_at' => NULL,
            ),
            257 => 
            array (
                'id' => 758,
                'denuncia_id' => 771,
                'victima_id' => 758,
                'created_at' => '2019-01-29 22:28:21',
                'updated_at' => '2019-01-29 22:28:21',
                'deleted_at' => NULL,
            ),
            258 => 
            array (
                'id' => 759,
                'denuncia_id' => 772,
                'victima_id' => 759,
                'created_at' => '2019-01-29 22:28:21',
                'updated_at' => '2019-01-29 22:28:21',
                'deleted_at' => NULL,
            ),
            259 => 
            array (
                'id' => 760,
                'denuncia_id' => 773,
                'victima_id' => 760,
                'created_at' => '2019-01-29 22:28:21',
                'updated_at' => '2019-01-29 22:28:21',
                'deleted_at' => NULL,
            ),
            260 => 
            array (
                'id' => 761,
                'denuncia_id' => 774,
                'victima_id' => 761,
                'created_at' => '2019-01-29 22:28:21',
                'updated_at' => '2019-01-29 22:28:21',
                'deleted_at' => NULL,
            ),
            261 => 
            array (
                'id' => 762,
                'denuncia_id' => 775,
                'victima_id' => 762,
                'created_at' => '2019-01-29 22:28:21',
                'updated_at' => '2019-01-29 22:28:21',
                'deleted_at' => NULL,
            ),
            262 => 
            array (
                'id' => 763,
                'denuncia_id' => 776,
                'victima_id' => 763,
                'created_at' => '2019-01-29 22:28:21',
                'updated_at' => '2019-01-29 22:28:21',
                'deleted_at' => NULL,
            ),
            263 => 
            array (
                'id' => 764,
                'denuncia_id' => 777,
                'victima_id' => 764,
                'created_at' => '2019-01-29 22:28:22',
                'updated_at' => '2019-01-29 22:28:22',
                'deleted_at' => NULL,
            ),
            264 => 
            array (
                'id' => 765,
                'denuncia_id' => 778,
                'victima_id' => 765,
                'created_at' => '2019-01-29 22:28:22',
                'updated_at' => '2019-01-29 22:28:22',
                'deleted_at' => NULL,
            ),
            265 => 
            array (
                'id' => 766,
                'denuncia_id' => 779,
                'victima_id' => 766,
                'created_at' => '2019-01-29 22:28:22',
                'updated_at' => '2019-01-29 22:28:22',
                'deleted_at' => NULL,
            ),
            266 => 
            array (
                'id' => 767,
                'denuncia_id' => 780,
                'victima_id' => 767,
                'created_at' => '2019-01-29 22:28:22',
                'updated_at' => '2019-01-29 22:28:22',
                'deleted_at' => NULL,
            ),
            267 => 
            array (
                'id' => 768,
                'denuncia_id' => 781,
                'victima_id' => 768,
                'created_at' => '2019-01-29 22:28:22',
                'updated_at' => '2019-01-29 22:28:22',
                'deleted_at' => NULL,
            ),
            268 => 
            array (
                'id' => 769,
                'denuncia_id' => 782,
                'victima_id' => 769,
                'created_at' => '2019-01-29 22:28:23',
                'updated_at' => '2019-01-29 22:28:23',
                'deleted_at' => NULL,
            ),
            269 => 
            array (
                'id' => 770,
                'denuncia_id' => 783,
                'victima_id' => 770,
                'created_at' => '2019-01-29 22:28:23',
                'updated_at' => '2019-01-29 22:28:23',
                'deleted_at' => NULL,
            ),
            270 => 
            array (
                'id' => 771,
                'denuncia_id' => 783,
                'victima_id' => 771,
                'created_at' => '2019-01-29 22:28:23',
                'updated_at' => '2019-01-29 22:28:23',
                'deleted_at' => NULL,
            ),
            271 => 
            array (
                'id' => 772,
                'denuncia_id' => 784,
                'victima_id' => 772,
                'created_at' => '2019-01-29 22:28:23',
                'updated_at' => '2019-01-29 22:28:23',
                'deleted_at' => NULL,
            ),
            272 => 
            array (
                'id' => 773,
                'denuncia_id' => 785,
                'victima_id' => 773,
                'created_at' => '2019-01-29 22:28:23',
                'updated_at' => '2019-01-29 22:28:23',
                'deleted_at' => NULL,
            ),
            273 => 
            array (
                'id' => 774,
                'denuncia_id' => 786,
                'victima_id' => 774,
                'created_at' => '2019-01-29 22:28:23',
                'updated_at' => '2019-01-29 22:28:23',
                'deleted_at' => NULL,
            ),
            274 => 
            array (
                'id' => 775,
                'denuncia_id' => 787,
                'victima_id' => 775,
                'created_at' => '2019-01-29 22:28:23',
                'updated_at' => '2019-01-29 22:28:23',
                'deleted_at' => NULL,
            ),
            275 => 
            array (
                'id' => 776,
                'denuncia_id' => 788,
                'victima_id' => 776,
                'created_at' => '2019-01-29 22:28:24',
                'updated_at' => '2019-01-29 22:28:24',
                'deleted_at' => NULL,
            ),
            276 => 
            array (
                'id' => 777,
                'denuncia_id' => 789,
                'victima_id' => 777,
                'created_at' => '2019-01-29 22:28:24',
                'updated_at' => '2019-01-29 22:28:24',
                'deleted_at' => NULL,
            ),
            277 => 
            array (
                'id' => 778,
                'denuncia_id' => 790,
                'victima_id' => 778,
                'created_at' => '2019-01-29 22:28:24',
                'updated_at' => '2019-01-29 22:28:24',
                'deleted_at' => NULL,
            ),
            278 => 
            array (
                'id' => 779,
                'denuncia_id' => 791,
                'victima_id' => 779,
                'created_at' => '2019-01-29 22:28:24',
                'updated_at' => '2019-01-29 22:28:24',
                'deleted_at' => NULL,
            ),
            279 => 
            array (
                'id' => 780,
                'denuncia_id' => 792,
                'victima_id' => 780,
                'created_at' => '2019-01-29 22:28:24',
                'updated_at' => '2019-01-29 22:28:24',
                'deleted_at' => NULL,
            ),
            280 => 
            array (
                'id' => 781,
                'denuncia_id' => 793,
                'victima_id' => 781,
                'created_at' => '2019-01-29 22:28:25',
                'updated_at' => '2019-01-29 22:28:25',
                'deleted_at' => NULL,
            ),
            281 => 
            array (
                'id' => 782,
                'denuncia_id' => 794,
                'victima_id' => 782,
                'created_at' => '2019-01-29 22:28:25',
                'updated_at' => '2019-01-29 22:28:25',
                'deleted_at' => NULL,
            ),
            282 => 
            array (
                'id' => 783,
                'denuncia_id' => 795,
                'victima_id' => 783,
                'created_at' => '2019-01-29 22:28:25',
                'updated_at' => '2019-01-29 22:28:25',
                'deleted_at' => NULL,
            ),
            283 => 
            array (
                'id' => 784,
                'denuncia_id' => 796,
                'victima_id' => 784,
                'created_at' => '2019-01-29 22:28:25',
                'updated_at' => '2019-01-29 22:28:25',
                'deleted_at' => NULL,
            ),
            284 => 
            array (
                'id' => 785,
                'denuncia_id' => 797,
                'victima_id' => 785,
                'created_at' => '2019-01-29 22:28:25',
                'updated_at' => '2019-01-29 22:28:25',
                'deleted_at' => NULL,
            ),
            285 => 
            array (
                'id' => 786,
                'denuncia_id' => 798,
                'victima_id' => 786,
                'created_at' => '2019-01-29 22:28:25',
                'updated_at' => '2019-01-29 22:28:25',
                'deleted_at' => NULL,
            ),
            286 => 
            array (
                'id' => 787,
                'denuncia_id' => 799,
                'victima_id' => 787,
                'created_at' => '2019-01-29 22:28:26',
                'updated_at' => '2019-01-29 22:28:26',
                'deleted_at' => NULL,
            ),
            287 => 
            array (
                'id' => 788,
                'denuncia_id' => 800,
                'victima_id' => 788,
                'created_at' => '2019-01-29 22:28:26',
                'updated_at' => '2019-01-29 22:28:26',
                'deleted_at' => NULL,
            ),
            288 => 
            array (
                'id' => 789,
                'denuncia_id' => 802,
                'victima_id' => 789,
                'created_at' => '2019-01-29 22:28:26',
                'updated_at' => '2019-01-29 22:28:26',
                'deleted_at' => NULL,
            ),
            289 => 
            array (
                'id' => 790,
                'denuncia_id' => 803,
                'victima_id' => 790,
                'created_at' => '2019-01-29 22:28:26',
                'updated_at' => '2019-01-29 22:28:26',
                'deleted_at' => NULL,
            ),
            290 => 
            array (
                'id' => 791,
                'denuncia_id' => 805,
                'victima_id' => 791,
                'created_at' => '2019-01-29 22:28:27',
                'updated_at' => '2019-01-29 22:28:27',
                'deleted_at' => NULL,
            ),
            291 => 
            array (
                'id' => 792,
                'denuncia_id' => 806,
                'victima_id' => 792,
                'created_at' => '2019-01-29 22:28:27',
                'updated_at' => '2019-01-29 22:28:27',
                'deleted_at' => NULL,
            ),
            292 => 
            array (
                'id' => 793,
                'denuncia_id' => 807,
                'victima_id' => 793,
                'created_at' => '2019-01-29 22:28:27',
                'updated_at' => '2019-01-29 22:28:27',
                'deleted_at' => NULL,
            ),
            293 => 
            array (
                'id' => 794,
                'denuncia_id' => 808,
                'victima_id' => 794,
                'created_at' => '2019-01-29 22:28:27',
                'updated_at' => '2019-01-29 22:28:27',
                'deleted_at' => NULL,
            ),
            294 => 
            array (
                'id' => 795,
                'denuncia_id' => 809,
                'victima_id' => 795,
                'created_at' => '2019-01-29 22:28:27',
                'updated_at' => '2019-01-29 22:28:27',
                'deleted_at' => NULL,
            ),
            295 => 
            array (
                'id' => 796,
                'denuncia_id' => 810,
                'victima_id' => 796,
                'created_at' => '2019-01-29 22:28:28',
                'updated_at' => '2019-01-29 22:28:28',
                'deleted_at' => NULL,
            ),
            296 => 
            array (
                'id' => 797,
                'denuncia_id' => 811,
                'victima_id' => 797,
                'created_at' => '2019-01-29 22:28:28',
                'updated_at' => '2019-01-29 22:28:28',
                'deleted_at' => NULL,
            ),
            297 => 
            array (
                'id' => 798,
                'denuncia_id' => 813,
                'victima_id' => 798,
                'created_at' => '2019-01-29 22:28:28',
                'updated_at' => '2019-01-29 22:28:28',
                'deleted_at' => NULL,
            ),
            298 => 
            array (
                'id' => 799,
                'denuncia_id' => 813,
                'victima_id' => 799,
                'created_at' => '2019-01-29 22:28:28',
                'updated_at' => '2019-01-29 22:28:28',
                'deleted_at' => NULL,
            ),
            299 => 
            array (
                'id' => 800,
                'denuncia_id' => 814,
                'victima_id' => 800,
                'created_at' => '2019-01-29 22:28:28',
                'updated_at' => '2019-01-29 22:28:28',
                'deleted_at' => NULL,
            ),
            300 => 
            array (
                'id' => 801,
                'denuncia_id' => 815,
                'victima_id' => 801,
                'created_at' => '2019-01-29 22:28:29',
                'updated_at' => '2019-01-29 22:28:29',
                'deleted_at' => NULL,
            ),
            301 => 
            array (
                'id' => 802,
                'denuncia_id' => 816,
                'victima_id' => 802,
                'created_at' => '2019-01-29 22:28:29',
                'updated_at' => '2019-01-29 22:28:29',
                'deleted_at' => NULL,
            ),
            302 => 
            array (
                'id' => 803,
                'denuncia_id' => 817,
                'victima_id' => 803,
                'created_at' => '2019-01-29 22:28:29',
                'updated_at' => '2019-01-29 22:28:29',
                'deleted_at' => NULL,
            ),
            303 => 
            array (
                'id' => 804,
                'denuncia_id' => 818,
                'victima_id' => 804,
                'created_at' => '2019-01-29 22:28:29',
                'updated_at' => '2019-01-29 22:28:29',
                'deleted_at' => NULL,
            ),
            304 => 
            array (
                'id' => 805,
                'denuncia_id' => 819,
                'victima_id' => 805,
                'created_at' => '2019-01-29 22:28:30',
                'updated_at' => '2019-01-29 22:28:30',
                'deleted_at' => NULL,
            ),
            305 => 
            array (
                'id' => 806,
                'denuncia_id' => 820,
                'victima_id' => 806,
                'created_at' => '2019-01-29 22:28:30',
                'updated_at' => '2019-01-29 22:28:30',
                'deleted_at' => NULL,
            ),
            306 => 
            array (
                'id' => 807,
                'denuncia_id' => 821,
                'victima_id' => 807,
                'created_at' => '2019-01-29 22:28:30',
                'updated_at' => '2019-01-29 22:28:30',
                'deleted_at' => NULL,
            ),
            307 => 
            array (
                'id' => 808,
                'denuncia_id' => 822,
                'victima_id' => 808,
                'created_at' => '2019-01-29 22:28:30',
                'updated_at' => '2019-01-29 22:28:30',
                'deleted_at' => NULL,
            ),
            308 => 
            array (
                'id' => 809,
                'denuncia_id' => 823,
                'victima_id' => 809,
                'created_at' => '2019-01-29 22:28:30',
                'updated_at' => '2019-01-29 22:28:30',
                'deleted_at' => NULL,
            ),
            309 => 
            array (
                'id' => 810,
                'denuncia_id' => 824,
                'victima_id' => 810,
                'created_at' => '2019-01-29 22:28:30',
                'updated_at' => '2019-01-29 22:28:30',
                'deleted_at' => NULL,
            ),
            310 => 
            array (
                'id' => 811,
                'denuncia_id' => 825,
                'victima_id' => 811,
                'created_at' => '2019-01-29 22:28:31',
                'updated_at' => '2019-01-29 22:28:31',
                'deleted_at' => NULL,
            ),
            311 => 
            array (
                'id' => 812,
                'denuncia_id' => 826,
                'victima_id' => 812,
                'created_at' => '2019-01-29 22:28:31',
                'updated_at' => '2019-01-29 22:28:31',
                'deleted_at' => NULL,
            ),
            312 => 
            array (
                'id' => 813,
                'denuncia_id' => 828,
                'victima_id' => 813,
                'created_at' => '2019-01-29 22:28:31',
                'updated_at' => '2019-01-29 22:28:31',
                'deleted_at' => NULL,
            ),
            313 => 
            array (
                'id' => 814,
                'denuncia_id' => 829,
                'victima_id' => 814,
                'created_at' => '2019-01-29 22:28:31',
                'updated_at' => '2019-01-29 22:28:31',
                'deleted_at' => NULL,
            ),
            314 => 
            array (
                'id' => 815,
                'denuncia_id' => 830,
                'victima_id' => 815,
                'created_at' => '2019-01-29 22:28:31',
                'updated_at' => '2019-01-29 22:28:31',
                'deleted_at' => NULL,
            ),
            315 => 
            array (
                'id' => 816,
                'denuncia_id' => 831,
                'victima_id' => 816,
                'created_at' => '2019-01-29 22:28:32',
                'updated_at' => '2019-01-29 22:28:32',
                'deleted_at' => NULL,
            ),
            316 => 
            array (
                'id' => 817,
                'denuncia_id' => 832,
                'victima_id' => 817,
                'created_at' => '2019-01-29 22:28:32',
                'updated_at' => '2019-01-29 22:28:32',
                'deleted_at' => NULL,
            ),
            317 => 
            array (
                'id' => 818,
                'denuncia_id' => 833,
                'victima_id' => 818,
                'created_at' => '2019-01-29 22:28:32',
                'updated_at' => '2019-01-29 22:28:32',
                'deleted_at' => NULL,
            ),
            318 => 
            array (
                'id' => 819,
                'denuncia_id' => 834,
                'victima_id' => 819,
                'created_at' => '2019-01-29 22:28:32',
                'updated_at' => '2019-01-29 22:28:32',
                'deleted_at' => NULL,
            ),
            319 => 
            array (
                'id' => 820,
                'denuncia_id' => 835,
                'victima_id' => 820,
                'created_at' => '2019-01-29 22:28:32',
                'updated_at' => '2019-01-29 22:28:32',
                'deleted_at' => NULL,
            ),
            320 => 
            array (
                'id' => 821,
                'denuncia_id' => 836,
                'victima_id' => 821,
                'created_at' => '2019-01-29 22:28:32',
                'updated_at' => '2019-01-29 22:28:32',
                'deleted_at' => NULL,
            ),
            321 => 
            array (
                'id' => 822,
                'denuncia_id' => 837,
                'victima_id' => 822,
                'created_at' => '2019-01-29 22:28:32',
                'updated_at' => '2019-01-29 22:28:32',
                'deleted_at' => NULL,
            ),
            322 => 
            array (
                'id' => 823,
                'denuncia_id' => 838,
                'victima_id' => 823,
                'created_at' => '2019-01-29 22:28:33',
                'updated_at' => '2019-01-29 22:28:33',
                'deleted_at' => NULL,
            ),
            323 => 
            array (
                'id' => 824,
                'denuncia_id' => 839,
                'victima_id' => 824,
                'created_at' => '2019-01-29 22:28:33',
                'updated_at' => '2019-01-29 22:28:33',
                'deleted_at' => NULL,
            ),
            324 => 
            array (
                'id' => 825,
                'denuncia_id' => 840,
                'victima_id' => 825,
                'created_at' => '2019-01-29 22:28:33',
                'updated_at' => '2019-01-29 22:28:33',
                'deleted_at' => NULL,
            ),
            325 => 
            array (
                'id' => 826,
                'denuncia_id' => 841,
                'victima_id' => 826,
                'created_at' => '2019-01-29 22:28:33',
                'updated_at' => '2019-01-29 22:28:33',
                'deleted_at' => NULL,
            ),
            326 => 
            array (
                'id' => 827,
                'denuncia_id' => 842,
                'victima_id' => 827,
                'created_at' => '2019-01-29 22:28:33',
                'updated_at' => '2019-01-29 22:28:33',
                'deleted_at' => NULL,
            ),
            327 => 
            array (
                'id' => 828,
                'denuncia_id' => 844,
                'victima_id' => 828,
                'created_at' => '2019-01-29 22:28:34',
                'updated_at' => '2019-01-29 22:28:34',
                'deleted_at' => NULL,
            ),
            328 => 
            array (
                'id' => 829,
                'denuncia_id' => 846,
                'victima_id' => 829,
                'created_at' => '2019-01-29 22:28:34',
                'updated_at' => '2019-01-29 22:28:34',
                'deleted_at' => NULL,
            ),
            329 => 
            array (
                'id' => 830,
                'denuncia_id' => 849,
                'victima_id' => 830,
                'created_at' => '2019-01-29 22:28:35',
                'updated_at' => '2019-01-29 22:28:35',
                'deleted_at' => NULL,
            ),
            330 => 
            array (
                'id' => 831,
                'denuncia_id' => 850,
                'victima_id' => 831,
                'created_at' => '2019-01-29 22:28:35',
                'updated_at' => '2019-01-29 22:28:35',
                'deleted_at' => NULL,
            ),
            331 => 
            array (
                'id' => 832,
                'denuncia_id' => 851,
                'victima_id' => 832,
                'created_at' => '2019-01-29 22:28:35',
                'updated_at' => '2019-01-29 22:28:35',
                'deleted_at' => NULL,
            ),
            332 => 
            array (
                'id' => 833,
                'denuncia_id' => 852,
                'victima_id' => 833,
                'created_at' => '2019-01-29 22:28:35',
                'updated_at' => '2019-01-29 22:28:35',
                'deleted_at' => NULL,
            ),
            333 => 
            array (
                'id' => 834,
                'denuncia_id' => 853,
                'victima_id' => 834,
                'created_at' => '2019-01-29 22:28:35',
                'updated_at' => '2019-01-29 22:28:35',
                'deleted_at' => NULL,
            ),
            334 => 
            array (
                'id' => 835,
                'denuncia_id' => 854,
                'victima_id' => 835,
                'created_at' => '2019-01-29 22:28:36',
                'updated_at' => '2019-01-29 22:28:36',
                'deleted_at' => NULL,
            ),
            335 => 
            array (
                'id' => 836,
                'denuncia_id' => 855,
                'victima_id' => 836,
                'created_at' => '2019-01-29 22:28:36',
                'updated_at' => '2019-01-29 22:28:36',
                'deleted_at' => NULL,
            ),
            336 => 
            array (
                'id' => 837,
                'denuncia_id' => 856,
                'victima_id' => 837,
                'created_at' => '2019-01-29 22:28:36',
                'updated_at' => '2019-01-29 22:28:36',
                'deleted_at' => NULL,
            ),
            337 => 
            array (
                'id' => 838,
                'denuncia_id' => 860,
                'victima_id' => 838,
                'created_at' => '2019-01-29 22:28:37',
                'updated_at' => '2019-01-29 22:28:37',
                'deleted_at' => NULL,
            ),
            338 => 
            array (
                'id' => 839,
                'denuncia_id' => 861,
                'victima_id' => 839,
                'created_at' => '2019-01-29 22:28:37',
                'updated_at' => '2019-01-29 22:28:37',
                'deleted_at' => NULL,
            ),
            339 => 
            array (
                'id' => 840,
                'denuncia_id' => 862,
                'victima_id' => 840,
                'created_at' => '2019-01-29 22:28:37',
                'updated_at' => '2019-01-29 22:28:37',
                'deleted_at' => NULL,
            ),
            340 => 
            array (
                'id' => 841,
                'denuncia_id' => 863,
                'victima_id' => 841,
                'created_at' => '2019-01-29 22:28:37',
                'updated_at' => '2019-01-29 22:28:37',
                'deleted_at' => NULL,
            ),
            341 => 
            array (
                'id' => 842,
                'denuncia_id' => 864,
                'victima_id' => 842,
                'created_at' => '2019-01-29 22:28:37',
                'updated_at' => '2019-01-29 22:28:37',
                'deleted_at' => NULL,
            ),
            342 => 
            array (
                'id' => 843,
                'denuncia_id' => 864,
                'victima_id' => 843,
                'created_at' => '2019-01-29 22:28:37',
                'updated_at' => '2019-01-29 22:28:37',
                'deleted_at' => NULL,
            ),
            343 => 
            array (
                'id' => 844,
                'denuncia_id' => 865,
                'victima_id' => 844,
                'created_at' => '2019-01-29 22:28:38',
                'updated_at' => '2019-01-29 22:28:38',
                'deleted_at' => NULL,
            ),
            344 => 
            array (
                'id' => 845,
                'denuncia_id' => 866,
                'victima_id' => 845,
                'created_at' => '2019-01-29 22:28:38',
                'updated_at' => '2019-01-29 22:28:38',
                'deleted_at' => NULL,
            ),
            345 => 
            array (
                'id' => 846,
                'denuncia_id' => 867,
                'victima_id' => 846,
                'created_at' => '2019-01-29 22:28:38',
                'updated_at' => '2019-01-29 22:28:38',
                'deleted_at' => NULL,
            ),
            346 => 
            array (
                'id' => 847,
                'denuncia_id' => 868,
                'victima_id' => 847,
                'created_at' => '2019-01-29 22:28:38',
                'updated_at' => '2019-01-29 22:28:38',
                'deleted_at' => NULL,
            ),
            347 => 
            array (
                'id' => 848,
                'denuncia_id' => 870,
                'victima_id' => 848,
                'created_at' => '2019-01-29 22:28:39',
                'updated_at' => '2019-01-29 22:28:39',
                'deleted_at' => NULL,
            ),
            348 => 
            array (
                'id' => 849,
                'denuncia_id' => 872,
                'victima_id' => 849,
                'created_at' => '2019-01-29 22:28:39',
                'updated_at' => '2019-01-29 22:28:39',
                'deleted_at' => NULL,
            ),
            349 => 
            array (
                'id' => 850,
                'denuncia_id' => 872,
                'victima_id' => 850,
                'created_at' => '2019-01-29 22:28:39',
                'updated_at' => '2019-01-29 22:28:39',
                'deleted_at' => NULL,
            ),
            350 => 
            array (
                'id' => 851,
                'denuncia_id' => 873,
                'victima_id' => 851,
                'created_at' => '2019-01-29 22:28:39',
                'updated_at' => '2019-01-29 22:28:39',
                'deleted_at' => NULL,
            ),
            351 => 
            array (
                'id' => 852,
                'denuncia_id' => 874,
                'victima_id' => 852,
                'created_at' => '2019-01-29 22:28:39',
                'updated_at' => '2019-01-29 22:28:39',
                'deleted_at' => NULL,
            ),
            352 => 
            array (
                'id' => 853,
                'denuncia_id' => 875,
                'victima_id' => 853,
                'created_at' => '2019-01-29 22:28:40',
                'updated_at' => '2019-01-29 22:28:40',
                'deleted_at' => NULL,
            ),
            353 => 
            array (
                'id' => 854,
                'denuncia_id' => 876,
                'victima_id' => 854,
                'created_at' => '2019-01-29 22:28:40',
                'updated_at' => '2019-01-29 22:28:40',
                'deleted_at' => NULL,
            ),
            354 => 
            array (
                'id' => 855,
                'denuncia_id' => 877,
                'victima_id' => 855,
                'created_at' => '2019-01-29 22:28:40',
                'updated_at' => '2019-01-29 22:28:40',
                'deleted_at' => NULL,
            ),
            355 => 
            array (
                'id' => 856,
                'denuncia_id' => 878,
                'victima_id' => 856,
                'created_at' => '2019-01-29 22:28:40',
                'updated_at' => '2019-01-29 22:28:40',
                'deleted_at' => NULL,
            ),
            356 => 
            array (
                'id' => 857,
                'denuncia_id' => 879,
                'victima_id' => 857,
                'created_at' => '2019-01-29 22:28:40',
                'updated_at' => '2019-01-29 22:28:40',
                'deleted_at' => NULL,
            ),
            357 => 
            array (
                'id' => 858,
                'denuncia_id' => 880,
                'victima_id' => 858,
                'created_at' => '2019-01-29 22:28:40',
                'updated_at' => '2019-01-29 22:28:40',
                'deleted_at' => NULL,
            ),
            358 => 
            array (
                'id' => 859,
                'denuncia_id' => 881,
                'victima_id' => 859,
                'created_at' => '2019-01-29 22:28:41',
                'updated_at' => '2019-01-29 22:28:41',
                'deleted_at' => NULL,
            ),
            359 => 
            array (
                'id' => 860,
                'denuncia_id' => 882,
                'victima_id' => 860,
                'created_at' => '2019-01-29 22:28:41',
                'updated_at' => '2019-01-29 22:28:41',
                'deleted_at' => NULL,
            ),
            360 => 
            array (
                'id' => 861,
                'denuncia_id' => 883,
                'victima_id' => 861,
                'created_at' => '2019-01-29 22:28:41',
                'updated_at' => '2019-01-29 22:28:41',
                'deleted_at' => NULL,
            ),
            361 => 
            array (
                'id' => 862,
                'denuncia_id' => 884,
                'victima_id' => 862,
                'created_at' => '2019-01-29 22:28:41',
                'updated_at' => '2019-01-29 22:28:41',
                'deleted_at' => NULL,
            ),
            362 => 
            array (
                'id' => 863,
                'denuncia_id' => 885,
                'victima_id' => 863,
                'created_at' => '2019-01-29 22:28:41',
                'updated_at' => '2019-01-29 22:28:41',
                'deleted_at' => NULL,
            ),
            363 => 
            array (
                'id' => 864,
                'denuncia_id' => 886,
                'victima_id' => 864,
                'created_at' => '2019-01-29 22:28:41',
                'updated_at' => '2019-01-29 22:28:41',
                'deleted_at' => NULL,
            ),
            364 => 
            array (
                'id' => 865,
                'denuncia_id' => 887,
                'victima_id' => 865,
                'created_at' => '2019-01-29 22:28:42',
                'updated_at' => '2019-01-29 22:28:42',
                'deleted_at' => NULL,
            ),
            365 => 
            array (
                'id' => 866,
                'denuncia_id' => 888,
                'victima_id' => 866,
                'created_at' => '2019-01-29 22:28:42',
                'updated_at' => '2019-01-29 22:28:42',
                'deleted_at' => NULL,
            ),
            366 => 
            array (
                'id' => 867,
                'denuncia_id' => 889,
                'victima_id' => 867,
                'created_at' => '2019-01-29 22:28:42',
                'updated_at' => '2019-01-29 22:28:42',
                'deleted_at' => NULL,
            ),
            367 => 
            array (
                'id' => 868,
                'denuncia_id' => 890,
                'victima_id' => 868,
                'created_at' => '2019-01-29 22:28:42',
                'updated_at' => '2019-01-29 22:28:42',
                'deleted_at' => NULL,
            ),
            368 => 
            array (
                'id' => 869,
                'denuncia_id' => 892,
                'victima_id' => 869,
                'created_at' => '2019-01-29 22:28:42',
                'updated_at' => '2019-01-29 22:28:42',
                'deleted_at' => NULL,
            ),
            369 => 
            array (
                'id' => 870,
                'denuncia_id' => 893,
                'victima_id' => 870,
                'created_at' => '2019-01-29 22:28:42',
                'updated_at' => '2019-01-29 22:28:42',
                'deleted_at' => NULL,
            ),
            370 => 
            array (
                'id' => 871,
                'denuncia_id' => 894,
                'victima_id' => 871,
                'created_at' => '2019-01-29 22:28:42',
                'updated_at' => '2019-01-29 22:28:42',
                'deleted_at' => NULL,
            ),
            371 => 
            array (
                'id' => 872,
                'denuncia_id' => 895,
                'victima_id' => 872,
                'created_at' => '2019-01-29 22:28:43',
                'updated_at' => '2019-01-29 22:28:43',
                'deleted_at' => NULL,
            ),
            372 => 
            array (
                'id' => 873,
                'denuncia_id' => 896,
                'victima_id' => 873,
                'created_at' => '2019-01-29 22:28:43',
                'updated_at' => '2019-01-29 22:28:43',
                'deleted_at' => NULL,
            ),
            373 => 
            array (
                'id' => 874,
                'denuncia_id' => 897,
                'victima_id' => 874,
                'created_at' => '2019-01-29 22:28:43',
                'updated_at' => '2019-01-29 22:28:43',
                'deleted_at' => NULL,
            ),
            374 => 
            array (
                'id' => 875,
                'denuncia_id' => 898,
                'victima_id' => 875,
                'created_at' => '2019-01-29 22:28:43',
                'updated_at' => '2019-01-29 22:28:43',
                'deleted_at' => NULL,
            ),
            375 => 
            array (
                'id' => 876,
                'denuncia_id' => 899,
                'victima_id' => 876,
                'created_at' => '2019-01-29 22:28:43',
                'updated_at' => '2019-01-29 22:28:43',
                'deleted_at' => NULL,
            ),
            376 => 
            array (
                'id' => 877,
                'denuncia_id' => 900,
                'victima_id' => 877,
                'created_at' => '2019-01-29 22:28:43',
                'updated_at' => '2019-01-29 22:28:43',
                'deleted_at' => NULL,
            ),
            377 => 
            array (
                'id' => 878,
                'denuncia_id' => 901,
                'victima_id' => 878,
                'created_at' => '2019-01-29 22:28:43',
                'updated_at' => '2019-01-29 22:28:43',
                'deleted_at' => NULL,
            ),
            378 => 
            array (
                'id' => 879,
                'denuncia_id' => 903,
                'victima_id' => 879,
                'created_at' => '2019-01-29 22:28:44',
                'updated_at' => '2019-01-29 22:28:44',
                'deleted_at' => NULL,
            ),
            379 => 
            array (
                'id' => 880,
                'denuncia_id' => 903,
                'victima_id' => 880,
                'created_at' => '2019-01-29 22:28:44',
                'updated_at' => '2019-01-29 22:28:44',
                'deleted_at' => NULL,
            ),
            380 => 
            array (
                'id' => 881,
                'denuncia_id' => 903,
                'victima_id' => 881,
                'created_at' => '2019-01-29 22:28:44',
                'updated_at' => '2019-01-29 22:28:44',
                'deleted_at' => NULL,
            ),
            381 => 
            array (
                'id' => 882,
                'denuncia_id' => 904,
                'victima_id' => 882,
                'created_at' => '2019-01-29 22:28:44',
                'updated_at' => '2019-01-29 22:28:44',
                'deleted_at' => NULL,
            ),
            382 => 
            array (
                'id' => 883,
                'denuncia_id' => 905,
                'victima_id' => 883,
                'created_at' => '2019-01-29 22:28:44',
                'updated_at' => '2019-01-29 22:28:44',
                'deleted_at' => NULL,
            ),
            383 => 
            array (
                'id' => 884,
                'denuncia_id' => 906,
                'victima_id' => 884,
                'created_at' => '2019-01-29 22:28:44',
                'updated_at' => '2019-01-29 22:28:44',
                'deleted_at' => NULL,
            ),
            384 => 
            array (
                'id' => 885,
                'denuncia_id' => 908,
                'victima_id' => 885,
                'created_at' => '2019-01-29 22:28:44',
                'updated_at' => '2019-01-29 22:28:44',
                'deleted_at' => NULL,
            ),
            385 => 
            array (
                'id' => 886,
                'denuncia_id' => 909,
                'victima_id' => 886,
                'created_at' => '2019-01-29 22:28:45',
                'updated_at' => '2019-01-29 22:28:45',
                'deleted_at' => NULL,
            ),
            386 => 
            array (
                'id' => 887,
                'denuncia_id' => 910,
                'victima_id' => 887,
                'created_at' => '2019-01-29 22:28:45',
                'updated_at' => '2019-01-29 22:28:45',
                'deleted_at' => NULL,
            ),
            387 => 
            array (
                'id' => 888,
                'denuncia_id' => 911,
                'victima_id' => 888,
                'created_at' => '2019-01-29 22:28:45',
                'updated_at' => '2019-01-29 22:28:45',
                'deleted_at' => NULL,
            ),
            388 => 
            array (
                'id' => 889,
                'denuncia_id' => 912,
                'victima_id' => 889,
                'created_at' => '2019-01-29 22:28:45',
                'updated_at' => '2019-01-29 22:28:45',
                'deleted_at' => NULL,
            ),
            389 => 
            array (
                'id' => 890,
                'denuncia_id' => 913,
                'victima_id' => 890,
                'created_at' => '2019-01-29 22:28:45',
                'updated_at' => '2019-01-29 22:28:45',
                'deleted_at' => NULL,
            ),
            390 => 
            array (
                'id' => 891,
                'denuncia_id' => 914,
                'victima_id' => 891,
                'created_at' => '2019-01-29 22:28:45',
                'updated_at' => '2019-01-29 22:28:45',
                'deleted_at' => NULL,
            ),
            391 => 
            array (
                'id' => 892,
                'denuncia_id' => 915,
                'victima_id' => 892,
                'created_at' => '2019-01-29 22:28:45',
                'updated_at' => '2019-01-29 22:28:45',
                'deleted_at' => NULL,
            ),
            392 => 
            array (
                'id' => 893,
                'denuncia_id' => 916,
                'victima_id' => 893,
                'created_at' => '2019-01-29 22:28:45',
                'updated_at' => '2019-01-29 22:28:45',
                'deleted_at' => NULL,
            ),
            393 => 
            array (
                'id' => 894,
                'denuncia_id' => 917,
                'victima_id' => 894,
                'created_at' => '2019-01-29 22:28:46',
                'updated_at' => '2019-01-29 22:28:46',
                'deleted_at' => NULL,
            ),
            394 => 
            array (
                'id' => 895,
                'denuncia_id' => 918,
                'victima_id' => 895,
                'created_at' => '2019-01-29 22:28:46',
                'updated_at' => '2019-01-29 22:28:46',
                'deleted_at' => NULL,
            ),
            395 => 
            array (
                'id' => 896,
                'denuncia_id' => 920,
                'victima_id' => 896,
                'created_at' => '2019-01-29 22:28:46',
                'updated_at' => '2019-01-29 22:28:46',
                'deleted_at' => NULL,
            ),
            396 => 
            array (
                'id' => 897,
                'denuncia_id' => 921,
                'victima_id' => 897,
                'created_at' => '2019-01-29 22:28:46',
                'updated_at' => '2019-01-29 22:28:46',
                'deleted_at' => NULL,
            ),
            397 => 
            array (
                'id' => 898,
                'denuncia_id' => 922,
                'victima_id' => 898,
                'created_at' => '2019-01-29 22:28:46',
                'updated_at' => '2019-01-29 22:28:46',
                'deleted_at' => NULL,
            ),
            398 => 
            array (
                'id' => 899,
                'denuncia_id' => 923,
                'victima_id' => 899,
                'created_at' => '2019-01-29 22:28:47',
                'updated_at' => '2019-01-29 22:28:47',
                'deleted_at' => NULL,
            ),
            399 => 
            array (
                'id' => 900,
                'denuncia_id' => 924,
                'victima_id' => 900,
                'created_at' => '2019-01-29 22:28:47',
                'updated_at' => '2019-01-29 22:28:47',
                'deleted_at' => NULL,
            ),
            400 => 
            array (
                'id' => 901,
                'denuncia_id' => 925,
                'victima_id' => 901,
                'created_at' => '2019-01-29 22:28:47',
                'updated_at' => '2019-01-29 22:28:47',
                'deleted_at' => NULL,
            ),
            401 => 
            array (
                'id' => 902,
                'denuncia_id' => 926,
                'victima_id' => 902,
                'created_at' => '2019-01-29 22:28:47',
                'updated_at' => '2019-01-29 22:28:47',
                'deleted_at' => NULL,
            ),
            402 => 
            array (
                'id' => 903,
                'denuncia_id' => 927,
                'victima_id' => 903,
                'created_at' => '2019-01-29 22:28:47',
                'updated_at' => '2019-01-29 22:28:47',
                'deleted_at' => NULL,
            ),
            403 => 
            array (
                'id' => 904,
                'denuncia_id' => 928,
                'victima_id' => 904,
                'created_at' => '2019-01-29 22:28:48',
                'updated_at' => '2019-01-29 22:28:48',
                'deleted_at' => NULL,
            ),
            404 => 
            array (
                'id' => 905,
                'denuncia_id' => 929,
                'victima_id' => 905,
                'created_at' => '2019-01-29 22:28:48',
                'updated_at' => '2019-01-29 22:28:48',
                'deleted_at' => NULL,
            ),
            405 => 
            array (
                'id' => 906,
                'denuncia_id' => 930,
                'victima_id' => 906,
                'created_at' => '2019-01-29 22:28:48',
                'updated_at' => '2019-01-29 22:28:48',
                'deleted_at' => NULL,
            ),
            406 => 
            array (
                'id' => 907,
                'denuncia_id' => 931,
                'victima_id' => 907,
                'created_at' => '2019-01-29 22:28:48',
                'updated_at' => '2019-01-29 22:28:48',
                'deleted_at' => NULL,
            ),
            407 => 
            array (
                'id' => 908,
                'denuncia_id' => 932,
                'victima_id' => 908,
                'created_at' => '2019-01-29 22:28:48',
                'updated_at' => '2019-01-29 22:28:48',
                'deleted_at' => NULL,
            ),
            408 => 
            array (
                'id' => 909,
                'denuncia_id' => 933,
                'victima_id' => 909,
                'created_at' => '2019-01-29 22:28:48',
                'updated_at' => '2019-01-29 22:28:48',
                'deleted_at' => NULL,
            ),
            409 => 
            array (
                'id' => 910,
                'denuncia_id' => 934,
                'victima_id' => 910,
                'created_at' => '2019-01-29 22:28:48',
                'updated_at' => '2019-01-29 22:28:48',
                'deleted_at' => NULL,
            ),
            410 => 
            array (
                'id' => 911,
                'denuncia_id' => 934,
                'victima_id' => 911,
                'created_at' => '2019-01-29 22:28:48',
                'updated_at' => '2019-01-29 22:28:48',
                'deleted_at' => NULL,
            ),
            411 => 
            array (
                'id' => 912,
                'denuncia_id' => 935,
                'victima_id' => 912,
                'created_at' => '2019-01-29 22:28:48',
                'updated_at' => '2019-01-29 22:28:48',
                'deleted_at' => NULL,
            ),
            412 => 
            array (
                'id' => 913,
                'denuncia_id' => 936,
                'victima_id' => 913,
                'created_at' => '2019-01-29 22:28:49',
                'updated_at' => '2019-01-29 22:28:49',
                'deleted_at' => NULL,
            ),
            413 => 
            array (
                'id' => 914,
                'denuncia_id' => 936,
                'victima_id' => 914,
                'created_at' => '2019-01-29 22:28:49',
                'updated_at' => '2019-01-29 22:28:49',
                'deleted_at' => NULL,
            ),
            414 => 
            array (
                'id' => 915,
                'denuncia_id' => 937,
                'victima_id' => 915,
                'created_at' => '2019-01-29 22:28:49',
                'updated_at' => '2019-01-29 22:28:49',
                'deleted_at' => NULL,
            ),
            415 => 
            array (
                'id' => 916,
                'denuncia_id' => 938,
                'victima_id' => 916,
                'created_at' => '2019-01-29 22:28:49',
                'updated_at' => '2019-01-29 22:28:49',
                'deleted_at' => NULL,
            ),
            416 => 
            array (
                'id' => 917,
                'denuncia_id' => 939,
                'victima_id' => 917,
                'created_at' => '2019-01-29 22:28:49',
                'updated_at' => '2019-01-29 22:28:49',
                'deleted_at' => NULL,
            ),
            417 => 
            array (
                'id' => 918,
                'denuncia_id' => 940,
                'victima_id' => 918,
                'created_at' => '2019-01-29 22:28:49',
                'updated_at' => '2019-01-29 22:28:49',
                'deleted_at' => NULL,
            ),
            418 => 
            array (
                'id' => 919,
                'denuncia_id' => 941,
                'victima_id' => 919,
                'created_at' => '2019-01-29 22:28:49',
                'updated_at' => '2019-01-29 22:28:49',
                'deleted_at' => NULL,
            ),
            419 => 
            array (
                'id' => 920,
                'denuncia_id' => 942,
                'victima_id' => 920,
                'created_at' => '2019-01-29 22:28:50',
                'updated_at' => '2019-01-29 22:28:50',
                'deleted_at' => NULL,
            ),
            420 => 
            array (
                'id' => 921,
                'denuncia_id' => 943,
                'victima_id' => 921,
                'created_at' => '2019-01-29 22:28:50',
                'updated_at' => '2019-01-29 22:28:50',
                'deleted_at' => NULL,
            ),
            421 => 
            array (
                'id' => 922,
                'denuncia_id' => 944,
                'victima_id' => 922,
                'created_at' => '2019-01-29 22:28:50',
                'updated_at' => '2019-01-29 22:28:50',
                'deleted_at' => NULL,
            ),
            422 => 
            array (
                'id' => 923,
                'denuncia_id' => 948,
                'victima_id' => 923,
                'created_at' => '2019-01-29 22:28:50',
                'updated_at' => '2019-01-29 22:28:50',
                'deleted_at' => NULL,
            ),
            423 => 
            array (
                'id' => 924,
                'denuncia_id' => 949,
                'victima_id' => 924,
                'created_at' => '2019-01-29 22:28:51',
                'updated_at' => '2019-01-29 22:28:51',
                'deleted_at' => NULL,
            ),
            424 => 
            array (
                'id' => 925,
                'denuncia_id' => 950,
                'victima_id' => 925,
                'created_at' => '2019-01-29 22:28:51',
                'updated_at' => '2019-01-29 22:28:51',
                'deleted_at' => NULL,
            ),
            425 => 
            array (
                'id' => 926,
                'denuncia_id' => 950,
                'victima_id' => 926,
                'created_at' => '2019-01-29 22:28:51',
                'updated_at' => '2019-01-29 22:28:51',
                'deleted_at' => NULL,
            ),
            426 => 
            array (
                'id' => 927,
                'denuncia_id' => 950,
                'victima_id' => 927,
                'created_at' => '2019-01-29 22:28:51',
                'updated_at' => '2019-01-29 22:28:51',
                'deleted_at' => NULL,
            ),
            427 => 
            array (
                'id' => 928,
                'denuncia_id' => 951,
                'victima_id' => 928,
                'created_at' => '2019-01-29 22:28:51',
                'updated_at' => '2019-01-29 22:28:51',
                'deleted_at' => NULL,
            ),
            428 => 
            array (
                'id' => 929,
                'denuncia_id' => 952,
                'victima_id' => 929,
                'created_at' => '2019-01-29 22:28:51',
                'updated_at' => '2019-01-29 22:28:51',
                'deleted_at' => NULL,
            ),
            429 => 
            array (
                'id' => 930,
                'denuncia_id' => 954,
                'victima_id' => 930,
                'created_at' => '2019-01-29 22:28:52',
                'updated_at' => '2019-01-29 22:28:52',
                'deleted_at' => NULL,
            ),
            430 => 
            array (
                'id' => 931,
                'denuncia_id' => 958,
                'victima_id' => 931,
                'created_at' => '2019-01-29 22:28:52',
                'updated_at' => '2019-01-29 22:28:52',
                'deleted_at' => NULL,
            ),
            431 => 
            array (
                'id' => 932,
                'denuncia_id' => 959,
                'victima_id' => 932,
                'created_at' => '2019-01-29 22:28:52',
                'updated_at' => '2019-01-29 22:28:52',
                'deleted_at' => NULL,
            ),
            432 => 
            array (
                'id' => 933,
                'denuncia_id' => 960,
                'victima_id' => 933,
                'created_at' => '2019-01-29 22:28:53',
                'updated_at' => '2019-01-29 22:28:53',
                'deleted_at' => NULL,
            ),
            433 => 
            array (
                'id' => 934,
                'denuncia_id' => 961,
                'victima_id' => 934,
                'created_at' => '2019-01-29 22:28:53',
                'updated_at' => '2019-01-29 22:28:53',
                'deleted_at' => NULL,
            ),
            434 => 
            array (
                'id' => 935,
                'denuncia_id' => 962,
                'victima_id' => 935,
                'created_at' => '2019-01-29 22:28:53',
                'updated_at' => '2019-01-29 22:28:53',
                'deleted_at' => NULL,
            ),
            435 => 
            array (
                'id' => 936,
                'denuncia_id' => 963,
                'victima_id' => 936,
                'created_at' => '2019-01-29 22:28:53',
                'updated_at' => '2019-01-29 22:28:53',
                'deleted_at' => NULL,
            ),
            436 => 
            array (
                'id' => 937,
                'denuncia_id' => 964,
                'victima_id' => 937,
                'created_at' => '2019-01-29 22:28:53',
                'updated_at' => '2019-01-29 22:28:53',
                'deleted_at' => NULL,
            ),
            437 => 
            array (
                'id' => 938,
                'denuncia_id' => 965,
                'victima_id' => 938,
                'created_at' => '2019-01-29 22:28:53',
                'updated_at' => '2019-01-29 22:28:53',
                'deleted_at' => NULL,
            ),
            438 => 
            array (
                'id' => 939,
                'denuncia_id' => 965,
                'victima_id' => 939,
                'created_at' => '2019-01-29 22:28:53',
                'updated_at' => '2019-01-29 22:28:53',
                'deleted_at' => NULL,
            ),
            439 => 
            array (
                'id' => 940,
                'denuncia_id' => 966,
                'victima_id' => 940,
                'created_at' => '2019-01-29 22:28:53',
                'updated_at' => '2019-01-29 22:28:53',
                'deleted_at' => NULL,
            ),
            440 => 
            array (
                'id' => 941,
                'denuncia_id' => 967,
                'victima_id' => 941,
                'created_at' => '2019-01-29 22:28:53',
                'updated_at' => '2019-01-29 22:28:53',
                'deleted_at' => NULL,
            ),
            441 => 
            array (
                'id' => 942,
                'denuncia_id' => 968,
                'victima_id' => 942,
                'created_at' => '2019-01-29 22:28:54',
                'updated_at' => '2019-01-29 22:28:54',
                'deleted_at' => NULL,
            ),
            442 => 
            array (
                'id' => 943,
                'denuncia_id' => 969,
                'victima_id' => 943,
                'created_at' => '2019-01-29 22:28:54',
                'updated_at' => '2019-01-29 22:28:54',
                'deleted_at' => NULL,
            ),
            443 => 
            array (
                'id' => 944,
                'denuncia_id' => 970,
                'victima_id' => 944,
                'created_at' => '2019-01-29 22:28:54',
                'updated_at' => '2019-01-29 22:28:54',
                'deleted_at' => NULL,
            ),
            444 => 
            array (
                'id' => 945,
                'denuncia_id' => 971,
                'victima_id' => 945,
                'created_at' => '2019-01-29 22:28:54',
                'updated_at' => '2019-01-29 22:28:54',
                'deleted_at' => NULL,
            ),
            445 => 
            array (
                'id' => 946,
                'denuncia_id' => 972,
                'victima_id' => 946,
                'created_at' => '2019-01-29 22:28:54',
                'updated_at' => '2019-01-29 22:28:54',
                'deleted_at' => NULL,
            ),
            446 => 
            array (
                'id' => 947,
                'denuncia_id' => 972,
                'victima_id' => 947,
                'created_at' => '2019-01-29 22:28:54',
                'updated_at' => '2019-01-29 22:28:54',
                'deleted_at' => NULL,
            ),
            447 => 
            array (
                'id' => 948,
                'denuncia_id' => 973,
                'victima_id' => 948,
                'created_at' => '2019-01-29 22:28:54',
                'updated_at' => '2019-01-29 22:28:54',
                'deleted_at' => NULL,
            ),
            448 => 
            array (
                'id' => 949,
                'denuncia_id' => 974,
                'victima_id' => 949,
                'created_at' => '2019-01-29 22:28:54',
                'updated_at' => '2019-01-29 22:28:54',
                'deleted_at' => NULL,
            ),
            449 => 
            array (
                'id' => 950,
                'denuncia_id' => 975,
                'victima_id' => 950,
                'created_at' => '2019-01-29 22:28:55',
                'updated_at' => '2019-01-29 22:28:55',
                'deleted_at' => NULL,
            ),
            450 => 
            array (
                'id' => 951,
                'denuncia_id' => 976,
                'victima_id' => 951,
                'created_at' => '2019-01-29 22:28:55',
                'updated_at' => '2019-01-29 22:28:55',
                'deleted_at' => NULL,
            ),
            451 => 
            array (
                'id' => 952,
                'denuncia_id' => 977,
                'victima_id' => 952,
                'created_at' => '2019-01-29 22:28:55',
                'updated_at' => '2019-01-29 22:28:55',
                'deleted_at' => NULL,
            ),
            452 => 
            array (
                'id' => 953,
                'denuncia_id' => 978,
                'victima_id' => 953,
                'created_at' => '2019-01-29 22:28:55',
                'updated_at' => '2019-01-29 22:28:55',
                'deleted_at' => NULL,
            ),
            453 => 
            array (
                'id' => 954,
                'denuncia_id' => 979,
                'victima_id' => 954,
                'created_at' => '2019-01-29 22:28:55',
                'updated_at' => '2019-01-29 22:28:55',
                'deleted_at' => NULL,
            ),
            454 => 
            array (
                'id' => 955,
                'denuncia_id' => 980,
                'victima_id' => 955,
                'created_at' => '2019-01-29 22:28:55',
                'updated_at' => '2019-01-29 22:28:55',
                'deleted_at' => NULL,
            ),
            455 => 
            array (
                'id' => 956,
                'denuncia_id' => 981,
                'victima_id' => 956,
                'created_at' => '2019-01-29 22:28:56',
                'updated_at' => '2019-01-29 22:28:56',
                'deleted_at' => NULL,
            ),
            456 => 
            array (
                'id' => 957,
                'denuncia_id' => 982,
                'victima_id' => 957,
                'created_at' => '2019-01-29 22:28:56',
                'updated_at' => '2019-01-29 22:28:56',
                'deleted_at' => NULL,
            ),
            457 => 
            array (
                'id' => 958,
                'denuncia_id' => 987,
                'victima_id' => 958,
                'created_at' => '2019-01-29 22:28:56',
                'updated_at' => '2019-01-29 22:28:56',
                'deleted_at' => NULL,
            ),
            458 => 
            array (
                'id' => 959,
                'denuncia_id' => 988,
                'victima_id' => 959,
                'created_at' => '2019-01-29 22:28:56',
                'updated_at' => '2019-01-29 22:28:56',
                'deleted_at' => NULL,
            ),
            459 => 
            array (
                'id' => 960,
                'denuncia_id' => 989,
                'victima_id' => 960,
                'created_at' => '2019-01-29 22:28:57',
                'updated_at' => '2019-01-29 22:28:57',
                'deleted_at' => NULL,
            ),
            460 => 
            array (
                'id' => 961,
                'denuncia_id' => 990,
                'victima_id' => 961,
                'created_at' => '2019-01-29 22:28:57',
                'updated_at' => '2019-01-29 22:28:57',
                'deleted_at' => NULL,
            ),
            461 => 
            array (
                'id' => 962,
                'denuncia_id' => 990,
                'victima_id' => 962,
                'created_at' => '2019-01-29 22:28:57',
                'updated_at' => '2019-01-29 22:28:57',
                'deleted_at' => NULL,
            ),
            462 => 
            array (
                'id' => 963,
                'denuncia_id' => 990,
                'victima_id' => 963,
                'created_at' => '2019-01-29 22:28:57',
                'updated_at' => '2019-01-29 22:28:57',
                'deleted_at' => NULL,
            ),
            463 => 
            array (
                'id' => 964,
                'denuncia_id' => 991,
                'victima_id' => 964,
                'created_at' => '2019-01-29 22:28:57',
                'updated_at' => '2019-01-29 22:28:57',
                'deleted_at' => NULL,
            ),
            464 => 
            array (
                'id' => 965,
                'denuncia_id' => 992,
                'victima_id' => 965,
                'created_at' => '2019-01-29 22:28:57',
                'updated_at' => '2019-01-29 22:28:57',
                'deleted_at' => NULL,
            ),
            465 => 
            array (
                'id' => 966,
                'denuncia_id' => 993,
                'victima_id' => 966,
                'created_at' => '2019-01-29 22:28:57',
                'updated_at' => '2019-01-29 22:28:57',
                'deleted_at' => NULL,
            ),
            466 => 
            array (
                'id' => 967,
                'denuncia_id' => 994,
                'victima_id' => 967,
                'created_at' => '2019-01-29 22:28:57',
                'updated_at' => '2019-01-29 22:28:57',
                'deleted_at' => NULL,
            ),
            467 => 
            array (
                'id' => 968,
                'denuncia_id' => 995,
                'victima_id' => 968,
                'created_at' => '2019-01-29 22:28:57',
                'updated_at' => '2019-01-29 22:28:57',
                'deleted_at' => NULL,
            ),
            468 => 
            array (
                'id' => 969,
                'denuncia_id' => 996,
                'victima_id' => 969,
                'created_at' => '2019-01-29 22:28:57',
                'updated_at' => '2019-01-29 22:28:57',
                'deleted_at' => NULL,
            ),
            469 => 
            array (
                'id' => 970,
                'denuncia_id' => 997,
                'victima_id' => 970,
                'created_at' => '2019-01-29 22:28:58',
                'updated_at' => '2019-01-29 22:28:58',
                'deleted_at' => NULL,
            ),
            470 => 
            array (
                'id' => 971,
                'denuncia_id' => 998,
                'victima_id' => 971,
                'created_at' => '2019-01-29 22:28:58',
                'updated_at' => '2019-01-29 22:28:58',
                'deleted_at' => NULL,
            ),
            471 => 
            array (
                'id' => 972,
                'denuncia_id' => 999,
                'victima_id' => 972,
                'created_at' => '2019-01-29 22:28:58',
                'updated_at' => '2019-01-29 22:28:58',
                'deleted_at' => NULL,
            ),
            472 => 
            array (
                'id' => 973,
                'denuncia_id' => 1000,
                'victima_id' => 973,
                'created_at' => '2019-01-29 22:28:58',
                'updated_at' => '2019-01-29 22:28:58',
                'deleted_at' => NULL,
            ),
            473 => 
            array (
                'id' => 974,
                'denuncia_id' => 1001,
                'victima_id' => 974,
                'created_at' => '2019-01-29 22:28:58',
                'updated_at' => '2019-01-29 22:28:58',
                'deleted_at' => NULL,
            ),
            474 => 
            array (
                'id' => 975,
                'denuncia_id' => 1002,
                'victima_id' => 975,
                'created_at' => '2019-01-29 22:28:58',
                'updated_at' => '2019-01-29 22:28:58',
                'deleted_at' => NULL,
            ),
            475 => 
            array (
                'id' => 976,
                'denuncia_id' => 1003,
                'victima_id' => 976,
                'created_at' => '2019-01-29 22:28:58',
                'updated_at' => '2019-01-29 22:28:58',
                'deleted_at' => NULL,
            ),
            476 => 
            array (
                'id' => 977,
                'denuncia_id' => 1004,
                'victima_id' => 977,
                'created_at' => '2019-01-29 22:28:59',
                'updated_at' => '2019-01-29 22:28:59',
                'deleted_at' => NULL,
            ),
            477 => 
            array (
                'id' => 978,
                'denuncia_id' => 1005,
                'victima_id' => 978,
                'created_at' => '2019-01-29 22:28:59',
                'updated_at' => '2019-01-29 22:28:59',
                'deleted_at' => NULL,
            ),
            478 => 
            array (
                'id' => 979,
                'denuncia_id' => 1006,
                'victima_id' => 979,
                'created_at' => '2019-01-29 22:28:59',
                'updated_at' => '2019-01-29 22:28:59',
                'deleted_at' => NULL,
            ),
            479 => 
            array (
                'id' => 980,
                'denuncia_id' => 1007,
                'victima_id' => 980,
                'created_at' => '2019-01-29 22:28:59',
                'updated_at' => '2019-01-29 22:28:59',
                'deleted_at' => NULL,
            ),
            480 => 
            array (
                'id' => 981,
                'denuncia_id' => 1008,
                'victima_id' => 981,
                'created_at' => '2019-01-29 22:28:59',
                'updated_at' => '2019-01-29 22:28:59',
                'deleted_at' => NULL,
            ),
            481 => 
            array (
                'id' => 982,
                'denuncia_id' => 1009,
                'victima_id' => 982,
                'created_at' => '2019-01-29 22:28:59',
                'updated_at' => '2019-01-29 22:28:59',
                'deleted_at' => NULL,
            ),
            482 => 
            array (
                'id' => 983,
                'denuncia_id' => 1010,
                'victima_id' => 983,
                'created_at' => '2019-01-29 22:29:00',
                'updated_at' => '2019-01-29 22:29:00',
                'deleted_at' => NULL,
            ),
            483 => 
            array (
                'id' => 984,
                'denuncia_id' => 1011,
                'victima_id' => 984,
                'created_at' => '2019-01-29 22:29:00',
                'updated_at' => '2019-01-29 22:29:00',
                'deleted_at' => NULL,
            ),
            484 => 
            array (
                'id' => 985,
                'denuncia_id' => 1012,
                'victima_id' => 985,
                'created_at' => '2019-01-29 22:29:00',
                'updated_at' => '2019-01-29 22:29:00',
                'deleted_at' => NULL,
            ),
            485 => 
            array (
                'id' => 986,
                'denuncia_id' => 1013,
                'victima_id' => 986,
                'created_at' => '2019-01-29 22:29:00',
                'updated_at' => '2019-01-29 22:29:00',
                'deleted_at' => NULL,
            ),
            486 => 
            array (
                'id' => 987,
                'denuncia_id' => 1014,
                'victima_id' => 987,
                'created_at' => '2019-01-29 22:29:00',
                'updated_at' => '2019-01-29 22:29:00',
                'deleted_at' => NULL,
            ),
            487 => 
            array (
                'id' => 988,
                'denuncia_id' => 1015,
                'victima_id' => 988,
                'created_at' => '2019-01-29 22:29:00',
                'updated_at' => '2019-01-29 22:29:00',
                'deleted_at' => NULL,
            ),
            488 => 
            array (
                'id' => 989,
                'denuncia_id' => 1016,
                'victima_id' => 989,
                'created_at' => '2019-01-29 22:29:00',
                'updated_at' => '2019-01-29 22:29:00',
                'deleted_at' => NULL,
            ),
            489 => 
            array (
                'id' => 990,
                'denuncia_id' => 1017,
                'victima_id' => 990,
                'created_at' => '2019-01-29 22:29:00',
                'updated_at' => '2019-01-29 22:29:00',
                'deleted_at' => NULL,
            ),
            490 => 
            array (
                'id' => 991,
                'denuncia_id' => 1018,
                'victima_id' => 991,
                'created_at' => '2019-01-29 22:29:01',
                'updated_at' => '2019-01-29 22:29:01',
                'deleted_at' => NULL,
            ),
            491 => 
            array (
                'id' => 992,
                'denuncia_id' => 1019,
                'victima_id' => 992,
                'created_at' => '2019-01-29 22:29:01',
                'updated_at' => '2019-01-29 22:29:01',
                'deleted_at' => NULL,
            ),
            492 => 
            array (
                'id' => 993,
                'denuncia_id' => 1020,
                'victima_id' => 993,
                'created_at' => '2019-01-29 22:29:01',
                'updated_at' => '2019-01-29 22:29:01',
                'deleted_at' => NULL,
            ),
            493 => 
            array (
                'id' => 994,
                'denuncia_id' => 1021,
                'victima_id' => 994,
                'created_at' => '2019-01-29 22:29:01',
                'updated_at' => '2019-01-29 22:29:01',
                'deleted_at' => NULL,
            ),
            494 => 
            array (
                'id' => 995,
                'denuncia_id' => 1022,
                'victima_id' => 995,
                'created_at' => '2019-01-29 22:29:01',
                'updated_at' => '2019-01-29 22:29:01',
                'deleted_at' => NULL,
            ),
            495 => 
            array (
                'id' => 996,
                'denuncia_id' => 1023,
                'victima_id' => 996,
                'created_at' => '2019-01-29 22:29:01',
                'updated_at' => '2019-01-29 22:29:01',
                'deleted_at' => NULL,
            ),
            496 => 
            array (
                'id' => 997,
                'denuncia_id' => 1024,
                'victima_id' => 997,
                'created_at' => '2019-01-29 22:29:02',
                'updated_at' => '2019-01-29 22:29:02',
                'deleted_at' => NULL,
            ),
            497 => 
            array (
                'id' => 998,
                'denuncia_id' => 1026,
                'victima_id' => 998,
                'created_at' => '2019-01-29 22:29:02',
                'updated_at' => '2019-01-29 22:29:02',
                'deleted_at' => NULL,
            ),
            498 => 
            array (
                'id' => 999,
                'denuncia_id' => 1027,
                'victima_id' => 999,
                'created_at' => '2019-01-29 22:29:02',
                'updated_at' => '2019-01-29 22:29:02',
                'deleted_at' => NULL,
            ),
            499 => 
            array (
                'id' => 1000,
                'denuncia_id' => 1028,
                'victima_id' => 1000,
                'created_at' => '2019-01-29 22:29:02',
                'updated_at' => '2019-01-29 22:29:02',
                'deleted_at' => NULL,
            ),
        ));
        \DB::table('denuncia_victima')->insert(array (
            0 => 
            array (
                'id' => 1001,
                'denuncia_id' => 1029,
                'victima_id' => 1001,
                'created_at' => '2019-01-29 22:29:02',
                'updated_at' => '2019-01-29 22:29:02',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 1002,
                'denuncia_id' => 1030,
                'victima_id' => 1002,
                'created_at' => '2019-01-29 22:29:02',
                'updated_at' => '2019-01-29 22:29:02',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 1003,
                'denuncia_id' => 1031,
                'victima_id' => 1003,
                'created_at' => '2019-01-29 22:29:02',
                'updated_at' => '2019-01-29 22:29:02',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 1004,
                'denuncia_id' => 1032,
                'victima_id' => 1004,
                'created_at' => '2019-01-29 22:29:03',
                'updated_at' => '2019-01-29 22:29:03',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 1005,
                'denuncia_id' => 1033,
                'victima_id' => 1005,
                'created_at' => '2019-01-29 22:29:03',
                'updated_at' => '2019-01-29 22:29:03',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 1006,
                'denuncia_id' => 1034,
                'victima_id' => 1006,
                'created_at' => '2019-01-29 22:29:03',
                'updated_at' => '2019-01-29 22:29:03',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 1007,
                'denuncia_id' => 1035,
                'victima_id' => 1007,
                'created_at' => '2019-01-29 22:29:03',
                'updated_at' => '2019-01-29 22:29:03',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 1008,
                'denuncia_id' => 1035,
                'victima_id' => 1008,
                'created_at' => '2019-01-29 22:29:03',
                'updated_at' => '2019-01-29 22:29:03',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 1009,
                'denuncia_id' => 1036,
                'victima_id' => 1009,
                'created_at' => '2019-01-29 22:29:03',
                'updated_at' => '2019-01-29 22:29:03',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 1010,
                'denuncia_id' => 1037,
                'victima_id' => 1010,
                'created_at' => '2019-01-29 22:29:03',
                'updated_at' => '2019-01-29 22:29:03',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 1011,
                'denuncia_id' => 1038,
                'victima_id' => 1011,
                'created_at' => '2019-01-29 22:29:03',
                'updated_at' => '2019-01-29 22:29:03',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 1012,
                'denuncia_id' => 1039,
                'victima_id' => 1012,
                'created_at' => '2019-01-29 22:29:04',
                'updated_at' => '2019-01-29 22:29:04',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 1013,
                'denuncia_id' => 1040,
                'victima_id' => 1013,
                'created_at' => '2019-01-29 22:29:04',
                'updated_at' => '2019-01-29 22:29:04',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 1014,
                'denuncia_id' => 1041,
                'victima_id' => 1014,
                'created_at' => '2019-01-29 22:29:04',
                'updated_at' => '2019-01-29 22:29:04',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 1015,
                'denuncia_id' => 1042,
                'victima_id' => 1015,
                'created_at' => '2019-01-29 22:29:04',
                'updated_at' => '2019-01-29 22:29:04',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 1016,
                'denuncia_id' => 1043,
                'victima_id' => 1016,
                'created_at' => '2019-01-29 22:29:04',
                'updated_at' => '2019-01-29 22:29:04',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 1017,
                'denuncia_id' => 1044,
                'victima_id' => 1017,
                'created_at' => '2019-01-29 22:29:04',
                'updated_at' => '2019-01-29 22:29:04',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 1018,
                'denuncia_id' => 1045,
                'victima_id' => 1018,
                'created_at' => '2019-01-29 22:29:04',
                'updated_at' => '2019-01-29 22:29:04',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 1019,
                'denuncia_id' => 1046,
                'victima_id' => 1019,
                'created_at' => '2019-01-29 22:29:04',
                'updated_at' => '2019-01-29 22:29:04',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 1020,
                'denuncia_id' => 1047,
                'victima_id' => 1020,
                'created_at' => '2019-01-29 22:29:05',
                'updated_at' => '2019-01-29 22:29:05',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 1021,
                'denuncia_id' => 1048,
                'victima_id' => 1021,
                'created_at' => '2019-01-29 22:29:05',
                'updated_at' => '2019-01-29 22:29:05',
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 1022,
                'denuncia_id' => 1048,
                'victima_id' => 1022,
                'created_at' => '2019-01-29 22:29:05',
                'updated_at' => '2019-01-29 22:29:05',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 1023,
                'denuncia_id' => 1049,
                'victima_id' => 1023,
                'created_at' => '2019-01-29 22:29:05',
                'updated_at' => '2019-01-29 22:29:05',
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id' => 1024,
                'denuncia_id' => 1050,
                'victima_id' => 1024,
                'created_at' => '2019-01-29 22:29:05',
                'updated_at' => '2019-01-29 22:29:05',
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id' => 1025,
                'denuncia_id' => 1051,
                'victima_id' => 1025,
                'created_at' => '2019-01-29 22:29:05',
                'updated_at' => '2019-01-29 22:29:05',
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id' => 1026,
                'denuncia_id' => 1052,
                'victima_id' => 1026,
                'created_at' => '2019-01-29 22:29:05',
                'updated_at' => '2019-01-29 22:29:05',
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'id' => 1027,
                'denuncia_id' => 1053,
                'victima_id' => 1027,
                'created_at' => '2019-01-29 22:29:05',
                'updated_at' => '2019-01-29 22:29:05',
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id' => 1028,
                'denuncia_id' => 1054,
                'victima_id' => 1028,
                'created_at' => '2019-01-29 22:29:05',
                'updated_at' => '2019-01-29 22:29:05',
                'deleted_at' => NULL,
            ),
            28 => 
            array (
                'id' => 1029,
                'denuncia_id' => 1055,
                'victima_id' => 1029,
                'created_at' => '2019-01-29 22:29:06',
                'updated_at' => '2019-01-29 22:29:06',
                'deleted_at' => NULL,
            ),
            29 => 
            array (
                'id' => 1030,
                'denuncia_id' => 1056,
                'victima_id' => 1030,
                'created_at' => '2019-01-29 22:29:06',
                'updated_at' => '2019-01-29 22:29:06',
                'deleted_at' => NULL,
            ),
            30 => 
            array (
                'id' => 1031,
                'denuncia_id' => 1057,
                'victima_id' => 1031,
                'created_at' => '2019-01-29 22:29:06',
                'updated_at' => '2019-01-29 22:29:06',
                'deleted_at' => NULL,
            ),
            31 => 
            array (
                'id' => 1032,
                'denuncia_id' => 1058,
                'victima_id' => 1032,
                'created_at' => '2019-01-29 22:29:06',
                'updated_at' => '2019-01-29 22:29:06',
                'deleted_at' => NULL,
            ),
            32 => 
            array (
                'id' => 1033,
                'denuncia_id' => 1060,
                'victima_id' => 1033,
                'created_at' => '2019-01-29 22:29:06',
                'updated_at' => '2019-01-29 22:29:06',
                'deleted_at' => NULL,
            ),
            33 => 
            array (
                'id' => 1034,
                'denuncia_id' => 1061,
                'victima_id' => 1034,
                'created_at' => '2019-01-29 22:29:07',
                'updated_at' => '2019-01-29 22:29:07',
                'deleted_at' => NULL,
            ),
            34 => 
            array (
                'id' => 1035,
                'denuncia_id' => 1062,
                'victima_id' => 1035,
                'created_at' => '2019-01-29 22:29:07',
                'updated_at' => '2019-01-29 22:29:07',
                'deleted_at' => NULL,
            ),
            35 => 
            array (
                'id' => 1036,
                'denuncia_id' => 1064,
                'victima_id' => 1036,
                'created_at' => '2019-01-29 22:29:07',
                'updated_at' => '2019-01-29 22:29:07',
                'deleted_at' => NULL,
            ),
            36 => 
            array (
                'id' => 1037,
                'denuncia_id' => 1067,
                'victima_id' => 1037,
                'created_at' => '2019-01-29 22:29:07',
                'updated_at' => '2019-01-29 22:29:07',
                'deleted_at' => NULL,
            ),
            37 => 
            array (
                'id' => 1038,
                'denuncia_id' => 1068,
                'victima_id' => 1038,
                'created_at' => '2019-01-29 22:29:08',
                'updated_at' => '2019-01-29 22:29:08',
                'deleted_at' => NULL,
            ),
            38 => 
            array (
                'id' => 1039,
                'denuncia_id' => 1070,
                'victima_id' => 1039,
                'created_at' => '2019-01-29 22:29:08',
                'updated_at' => '2019-01-29 22:29:08',
                'deleted_at' => NULL,
            ),
            39 => 
            array (
                'id' => 1040,
                'denuncia_id' => 1071,
                'victima_id' => 1040,
                'created_at' => '2019-01-29 22:29:08',
                'updated_at' => '2019-01-29 22:29:08',
                'deleted_at' => NULL,
            ),
            40 => 
            array (
                'id' => 1041,
                'denuncia_id' => 1072,
                'victima_id' => 1041,
                'created_at' => '2019-01-29 22:29:08',
                'updated_at' => '2019-01-29 22:29:08',
                'deleted_at' => NULL,
            ),
            41 => 
            array (
                'id' => 1042,
                'denuncia_id' => 1073,
                'victima_id' => 1042,
                'created_at' => '2019-01-29 22:29:08',
                'updated_at' => '2019-01-29 22:29:08',
                'deleted_at' => NULL,
            ),
            42 => 
            array (
                'id' => 1043,
                'denuncia_id' => 1074,
                'victima_id' => 1043,
                'created_at' => '2019-01-29 22:29:08',
                'updated_at' => '2019-01-29 22:29:08',
                'deleted_at' => NULL,
            ),
            43 => 
            array (
                'id' => 1044,
                'denuncia_id' => 1075,
                'victima_id' => 1044,
                'created_at' => '2019-01-29 22:29:08',
                'updated_at' => '2019-01-29 22:29:08',
                'deleted_at' => NULL,
            ),
            44 => 
            array (
                'id' => 1045,
                'denuncia_id' => 1078,
                'victima_id' => 1045,
                'created_at' => '2019-01-29 22:29:09',
                'updated_at' => '2019-01-29 22:29:09',
                'deleted_at' => NULL,
            ),
            45 => 
            array (
                'id' => 1046,
                'denuncia_id' => 1078,
                'victima_id' => 1046,
                'created_at' => '2019-01-29 22:29:09',
                'updated_at' => '2019-01-29 22:29:09',
                'deleted_at' => NULL,
            ),
            46 => 
            array (
                'id' => 1047,
                'denuncia_id' => 1080,
                'victima_id' => 1047,
                'created_at' => '2019-01-29 22:29:09',
                'updated_at' => '2019-01-29 22:29:09',
                'deleted_at' => NULL,
            ),
            47 => 
            array (
                'id' => 1048,
                'denuncia_id' => 1081,
                'victima_id' => 1048,
                'created_at' => '2019-01-29 22:29:09',
                'updated_at' => '2019-01-29 22:29:09',
                'deleted_at' => NULL,
            ),
            48 => 
            array (
                'id' => 1049,
                'denuncia_id' => 1082,
                'victima_id' => 1049,
                'created_at' => '2019-01-29 22:29:09',
                'updated_at' => '2019-01-29 22:29:09',
                'deleted_at' => NULL,
            ),
            49 => 
            array (
                'id' => 1050,
                'denuncia_id' => 1083,
                'victima_id' => 1050,
                'created_at' => '2019-01-29 22:29:10',
                'updated_at' => '2019-01-29 22:29:10',
                'deleted_at' => NULL,
            ),
            50 => 
            array (
                'id' => 1051,
                'denuncia_id' => 1084,
                'victima_id' => 1051,
                'created_at' => '2019-01-29 22:29:10',
                'updated_at' => '2019-01-29 22:29:10',
                'deleted_at' => NULL,
            ),
            51 => 
            array (
                'id' => 1052,
                'denuncia_id' => 1085,
                'victima_id' => 1052,
                'created_at' => '2019-01-29 22:29:10',
                'updated_at' => '2019-01-29 22:29:10',
                'deleted_at' => NULL,
            ),
            52 => 
            array (
                'id' => 1053,
                'denuncia_id' => 1086,
                'victima_id' => 1053,
                'created_at' => '2019-01-29 22:29:10',
                'updated_at' => '2019-01-29 22:29:10',
                'deleted_at' => NULL,
            ),
            53 => 
            array (
                'id' => 1054,
                'denuncia_id' => 1087,
                'victima_id' => 1054,
                'created_at' => '2019-01-29 22:29:10',
                'updated_at' => '2019-01-29 22:29:10',
                'deleted_at' => NULL,
            ),
            54 => 
            array (
                'id' => 1055,
                'denuncia_id' => 1088,
                'victima_id' => 1055,
                'created_at' => '2019-01-29 22:29:10',
                'updated_at' => '2019-01-29 22:29:10',
                'deleted_at' => NULL,
            ),
            55 => 
            array (
                'id' => 1056,
                'denuncia_id' => 1089,
                'victima_id' => 1056,
                'created_at' => '2019-01-29 22:29:10',
                'updated_at' => '2019-01-29 22:29:10',
                'deleted_at' => NULL,
            ),
            56 => 
            array (
                'id' => 1057,
                'denuncia_id' => 1090,
                'victima_id' => 1057,
                'created_at' => '2019-01-29 22:29:11',
                'updated_at' => '2019-01-29 22:29:11',
                'deleted_at' => NULL,
            ),
            57 => 
            array (
                'id' => 1058,
                'denuncia_id' => 1091,
                'victima_id' => 1058,
                'created_at' => '2019-01-29 22:29:11',
                'updated_at' => '2019-01-29 22:29:11',
                'deleted_at' => NULL,
            ),
            58 => 
            array (
                'id' => 1059,
                'denuncia_id' => 1092,
                'victima_id' => 1059,
                'created_at' => '2019-01-29 22:29:11',
                'updated_at' => '2019-01-29 22:29:11',
                'deleted_at' => NULL,
            ),
            59 => 
            array (
                'id' => 1060,
                'denuncia_id' => 1093,
                'victima_id' => 1060,
                'created_at' => '2019-01-29 22:29:11',
                'updated_at' => '2019-01-29 22:29:11',
                'deleted_at' => NULL,
            ),
            60 => 
            array (
                'id' => 1061,
                'denuncia_id' => 1094,
                'victima_id' => 1061,
                'created_at' => '2019-01-29 22:29:11',
                'updated_at' => '2019-01-29 22:29:11',
                'deleted_at' => NULL,
            ),
            61 => 
            array (
                'id' => 1062,
                'denuncia_id' => 1095,
                'victima_id' => 1062,
                'created_at' => '2019-01-29 22:29:11',
                'updated_at' => '2019-01-29 22:29:11',
                'deleted_at' => NULL,
            ),
            62 => 
            array (
                'id' => 1063,
                'denuncia_id' => 1096,
                'victima_id' => 1063,
                'created_at' => '2019-01-29 22:29:11',
                'updated_at' => '2019-01-29 22:29:11',
                'deleted_at' => NULL,
            ),
            63 => 
            array (
                'id' => 1064,
                'denuncia_id' => 1097,
                'victima_id' => 1064,
                'created_at' => '2019-01-29 22:29:12',
                'updated_at' => '2019-01-29 22:29:12',
                'deleted_at' => NULL,
            ),
            64 => 
            array (
                'id' => 1065,
                'denuncia_id' => 1098,
                'victima_id' => 1065,
                'created_at' => '2019-01-29 22:29:12',
                'updated_at' => '2019-01-29 22:29:12',
                'deleted_at' => NULL,
            ),
            65 => 
            array (
                'id' => 1066,
                'denuncia_id' => 1099,
                'victima_id' => 1066,
                'created_at' => '2019-01-29 22:29:12',
                'updated_at' => '2019-01-29 22:29:12',
                'deleted_at' => NULL,
            ),
            66 => 
            array (
                'id' => 1067,
                'denuncia_id' => 1100,
                'victima_id' => 1067,
                'created_at' => '2019-01-29 22:29:12',
                'updated_at' => '2019-01-29 22:29:12',
                'deleted_at' => NULL,
            ),
            67 => 
            array (
                'id' => 1068,
                'denuncia_id' => 1101,
                'victima_id' => 1068,
                'created_at' => '2019-01-29 22:29:12',
                'updated_at' => '2019-01-29 22:29:12',
                'deleted_at' => NULL,
            ),
            68 => 
            array (
                'id' => 1069,
                'denuncia_id' => 1102,
                'victima_id' => 1069,
                'created_at' => '2019-01-29 22:29:12',
                'updated_at' => '2019-01-29 22:29:12',
                'deleted_at' => NULL,
            ),
            69 => 
            array (
                'id' => 1070,
                'denuncia_id' => 1103,
                'victima_id' => 1070,
                'created_at' => '2019-01-29 22:29:12',
                'updated_at' => '2019-01-29 22:29:12',
                'deleted_at' => NULL,
            ),
            70 => 
            array (
                'id' => 1071,
                'denuncia_id' => 1104,
                'victima_id' => 1071,
                'created_at' => '2019-01-29 22:29:13',
                'updated_at' => '2019-01-29 22:29:13',
                'deleted_at' => NULL,
            ),
            71 => 
            array (
                'id' => 1072,
                'denuncia_id' => 1105,
                'victima_id' => 1072,
                'created_at' => '2019-01-29 22:29:13',
                'updated_at' => '2019-01-29 22:29:13',
                'deleted_at' => NULL,
            ),
            72 => 
            array (
                'id' => 1073,
                'denuncia_id' => 1106,
                'victima_id' => 1073,
                'created_at' => '2019-01-29 22:29:13',
                'updated_at' => '2019-01-29 22:29:13',
                'deleted_at' => NULL,
            ),
            73 => 
            array (
                'id' => 1074,
                'denuncia_id' => 1107,
                'victima_id' => 1074,
                'created_at' => '2019-01-29 22:29:13',
                'updated_at' => '2019-01-29 22:29:13',
                'deleted_at' => NULL,
            ),
            74 => 
            array (
                'id' => 1075,
                'denuncia_id' => 1108,
                'victima_id' => 1075,
                'created_at' => '2019-01-29 22:29:13',
                'updated_at' => '2019-01-29 22:29:13',
                'deleted_at' => NULL,
            ),
            75 => 
            array (
                'id' => 1076,
                'denuncia_id' => 1109,
                'victima_id' => 1076,
                'created_at' => '2019-01-29 22:29:13',
                'updated_at' => '2019-01-29 22:29:13',
                'deleted_at' => NULL,
            ),
            76 => 
            array (
                'id' => 1077,
                'denuncia_id' => 1110,
                'victima_id' => 1077,
                'created_at' => '2019-01-29 22:29:13',
                'updated_at' => '2019-01-29 22:29:13',
                'deleted_at' => NULL,
            ),
            77 => 
            array (
                'id' => 1078,
                'denuncia_id' => 1111,
                'victima_id' => 1078,
                'created_at' => '2019-01-29 22:29:14',
                'updated_at' => '2019-01-29 22:29:14',
                'deleted_at' => NULL,
            ),
            78 => 
            array (
                'id' => 1079,
                'denuncia_id' => 1112,
                'victima_id' => 1079,
                'created_at' => '2019-01-29 22:29:14',
                'updated_at' => '2019-01-29 22:29:14',
                'deleted_at' => NULL,
            ),
            79 => 
            array (
                'id' => 1080,
                'denuncia_id' => 1113,
                'victima_id' => 1080,
                'created_at' => '2019-01-29 22:29:14',
                'updated_at' => '2019-01-29 22:29:14',
                'deleted_at' => NULL,
            ),
            80 => 
            array (
                'id' => 1081,
                'denuncia_id' => 1114,
                'victima_id' => 1081,
                'created_at' => '2019-01-29 22:29:14',
                'updated_at' => '2019-01-29 22:29:14',
                'deleted_at' => NULL,
            ),
            81 => 
            array (
                'id' => 1082,
                'denuncia_id' => 1115,
                'victima_id' => 1082,
                'created_at' => '2019-01-29 22:29:14',
                'updated_at' => '2019-01-29 22:29:14',
                'deleted_at' => NULL,
            ),
            82 => 
            array (
                'id' => 1083,
                'denuncia_id' => 1116,
                'victima_id' => 1083,
                'created_at' => '2019-01-29 22:29:15',
                'updated_at' => '2019-01-29 22:29:15',
                'deleted_at' => NULL,
            ),
            83 => 
            array (
                'id' => 1084,
                'denuncia_id' => 1117,
                'victima_id' => 1084,
                'created_at' => '2019-01-29 22:29:15',
                'updated_at' => '2019-01-29 22:29:15',
                'deleted_at' => NULL,
            ),
            84 => 
            array (
                'id' => 1085,
                'denuncia_id' => 1118,
                'victima_id' => 1085,
                'created_at' => '2019-01-29 22:29:15',
                'updated_at' => '2019-01-29 22:29:15',
                'deleted_at' => NULL,
            ),
            85 => 
            array (
                'id' => 1086,
                'denuncia_id' => 1119,
                'victima_id' => 1086,
                'created_at' => '2019-01-29 22:29:15',
                'updated_at' => '2019-01-29 22:29:15',
                'deleted_at' => NULL,
            ),
            86 => 
            array (
                'id' => 1087,
                'denuncia_id' => 1120,
                'victima_id' => 1087,
                'created_at' => '2019-01-29 22:29:15',
                'updated_at' => '2019-01-29 22:29:15',
                'deleted_at' => NULL,
            ),
            87 => 
            array (
                'id' => 1088,
                'denuncia_id' => 1121,
                'victima_id' => 1088,
                'created_at' => '2019-01-29 22:29:15',
                'updated_at' => '2019-01-29 22:29:15',
                'deleted_at' => NULL,
            ),
            88 => 
            array (
                'id' => 1089,
                'denuncia_id' => 1122,
                'victima_id' => 1089,
                'created_at' => '2019-01-29 22:29:15',
                'updated_at' => '2019-01-29 22:29:15',
                'deleted_at' => NULL,
            ),
            89 => 
            array (
                'id' => 1090,
                'denuncia_id' => 1123,
                'victima_id' => 1090,
                'created_at' => '2019-01-29 22:29:15',
                'updated_at' => '2019-01-29 22:29:15',
                'deleted_at' => NULL,
            ),
            90 => 
            array (
                'id' => 1091,
                'denuncia_id' => 1124,
                'victima_id' => 1091,
                'created_at' => '2019-01-29 22:29:16',
                'updated_at' => '2019-01-29 22:29:16',
                'deleted_at' => NULL,
            ),
            91 => 
            array (
                'id' => 1092,
                'denuncia_id' => 1125,
                'victima_id' => 1092,
                'created_at' => '2019-01-29 22:29:16',
                'updated_at' => '2019-01-29 22:29:16',
                'deleted_at' => NULL,
            ),
            92 => 
            array (
                'id' => 1093,
                'denuncia_id' => 1126,
                'victima_id' => 1093,
                'created_at' => '2019-01-29 22:29:16',
                'updated_at' => '2019-01-29 22:29:16',
                'deleted_at' => NULL,
            ),
            93 => 
            array (
                'id' => 1094,
                'denuncia_id' => 1127,
                'victima_id' => 1094,
                'created_at' => '2019-01-29 22:29:16',
                'updated_at' => '2019-01-29 22:29:16',
                'deleted_at' => NULL,
            ),
            94 => 
            array (
                'id' => 1095,
                'denuncia_id' => 1128,
                'victima_id' => 1095,
                'created_at' => '2019-01-29 22:29:16',
                'updated_at' => '2019-01-29 22:29:16',
                'deleted_at' => NULL,
            ),
            95 => 
            array (
                'id' => 1096,
                'denuncia_id' => 1129,
                'victima_id' => 1096,
                'created_at' => '2019-01-29 22:29:16',
                'updated_at' => '2019-01-29 22:29:16',
                'deleted_at' => NULL,
            ),
            96 => 
            array (
                'id' => 1097,
                'denuncia_id' => 1130,
                'victima_id' => 1097,
                'created_at' => '2019-01-29 22:29:16',
                'updated_at' => '2019-01-29 22:29:16',
                'deleted_at' => NULL,
            ),
            97 => 
            array (
                'id' => 1098,
                'denuncia_id' => 1131,
                'victima_id' => 1098,
                'created_at' => '2019-01-29 22:29:17',
                'updated_at' => '2019-01-29 22:29:17',
                'deleted_at' => NULL,
            ),
            98 => 
            array (
                'id' => 1099,
                'denuncia_id' => 1132,
                'victima_id' => 1099,
                'created_at' => '2019-01-29 22:29:17',
                'updated_at' => '2019-01-29 22:29:17',
                'deleted_at' => NULL,
            ),
            99 => 
            array (
                'id' => 1100,
                'denuncia_id' => 1133,
                'victima_id' => 1100,
                'created_at' => '2019-01-29 22:29:17',
                'updated_at' => '2019-01-29 22:29:17',
                'deleted_at' => NULL,
            ),
            100 => 
            array (
                'id' => 1101,
                'denuncia_id' => 1134,
                'victima_id' => 1101,
                'created_at' => '2019-01-29 22:29:17',
                'updated_at' => '2019-01-29 22:29:17',
                'deleted_at' => NULL,
            ),
            101 => 
            array (
                'id' => 1102,
                'denuncia_id' => 1135,
                'victima_id' => 1102,
                'created_at' => '2019-01-29 22:29:17',
                'updated_at' => '2019-01-29 22:29:17',
                'deleted_at' => NULL,
            ),
            102 => 
            array (
                'id' => 1103,
                'denuncia_id' => 1136,
                'victima_id' => 1103,
                'created_at' => '2019-01-29 22:29:17',
                'updated_at' => '2019-01-29 22:29:17',
                'deleted_at' => NULL,
            ),
            103 => 
            array (
                'id' => 1104,
                'denuncia_id' => 1136,
                'victima_id' => 1104,
                'created_at' => '2019-01-29 22:29:17',
                'updated_at' => '2019-01-29 22:29:17',
                'deleted_at' => NULL,
            ),
            104 => 
            array (
                'id' => 1105,
                'denuncia_id' => 1137,
                'victima_id' => 1105,
                'created_at' => '2019-01-29 22:29:17',
                'updated_at' => '2019-01-29 22:29:17',
                'deleted_at' => NULL,
            ),
            105 => 
            array (
                'id' => 1106,
                'denuncia_id' => 1138,
                'victima_id' => 1106,
                'created_at' => '2019-01-29 22:29:17',
                'updated_at' => '2019-01-29 22:29:17',
                'deleted_at' => NULL,
            ),
            106 => 
            array (
                'id' => 1107,
                'denuncia_id' => 1139,
                'victima_id' => 1107,
                'created_at' => '2019-01-29 22:29:18',
                'updated_at' => '2019-01-29 22:29:18',
                'deleted_at' => NULL,
            ),
            107 => 
            array (
                'id' => 1108,
                'denuncia_id' => 1140,
                'victima_id' => 1108,
                'created_at' => '2019-01-29 22:29:18',
                'updated_at' => '2019-01-29 22:29:18',
                'deleted_at' => NULL,
            ),
            108 => 
            array (
                'id' => 1109,
                'denuncia_id' => 1141,
                'victima_id' => 1109,
                'created_at' => '2019-01-29 22:29:18',
                'updated_at' => '2019-01-29 22:29:18',
                'deleted_at' => NULL,
            ),
            109 => 
            array (
                'id' => 1110,
                'denuncia_id' => 1142,
                'victima_id' => 1110,
                'created_at' => '2019-01-29 22:29:18',
                'updated_at' => '2019-01-29 22:29:18',
                'deleted_at' => NULL,
            ),
            110 => 
            array (
                'id' => 1111,
                'denuncia_id' => 1143,
                'victima_id' => 1111,
                'created_at' => '2019-01-29 22:29:18',
                'updated_at' => '2019-01-29 22:29:18',
                'deleted_at' => NULL,
            ),
            111 => 
            array (
                'id' => 1112,
                'denuncia_id' => 1144,
                'victima_id' => 1112,
                'created_at' => '2019-01-29 22:29:18',
                'updated_at' => '2019-01-29 22:29:18',
                'deleted_at' => NULL,
            ),
            112 => 
            array (
                'id' => 1113,
                'denuncia_id' => 1146,
                'victima_id' => 1113,
                'created_at' => '2019-01-29 22:29:19',
                'updated_at' => '2019-01-29 22:29:19',
                'deleted_at' => NULL,
            ),
            113 => 
            array (
                'id' => 1114,
                'denuncia_id' => 1147,
                'victima_id' => 1114,
                'created_at' => '2019-01-29 22:29:19',
                'updated_at' => '2019-01-29 22:29:19',
                'deleted_at' => NULL,
            ),
            114 => 
            array (
                'id' => 1115,
                'denuncia_id' => 1148,
                'victima_id' => 1115,
                'created_at' => '2019-01-29 22:29:19',
                'updated_at' => '2019-01-29 22:29:19',
                'deleted_at' => NULL,
            ),
            115 => 
            array (
                'id' => 1116,
                'denuncia_id' => 1149,
                'victima_id' => 1116,
                'created_at' => '2019-01-29 22:29:19',
                'updated_at' => '2019-01-29 22:29:19',
                'deleted_at' => NULL,
            ),
            116 => 
            array (
                'id' => 1117,
                'denuncia_id' => 1150,
                'victima_id' => 1117,
                'created_at' => '2019-01-29 22:29:19',
                'updated_at' => '2019-01-29 22:29:19',
                'deleted_at' => NULL,
            ),
            117 => 
            array (
                'id' => 1118,
                'denuncia_id' => 1152,
                'victima_id' => 1118,
                'created_at' => '2019-01-29 22:29:19',
                'updated_at' => '2019-01-29 22:29:19',
                'deleted_at' => NULL,
            ),
            118 => 
            array (
                'id' => 1119,
                'denuncia_id' => 1153,
                'victima_id' => 1119,
                'created_at' => '2019-01-29 22:29:19',
                'updated_at' => '2019-01-29 22:29:19',
                'deleted_at' => NULL,
            ),
            119 => 
            array (
                'id' => 1120,
                'denuncia_id' => 1154,
                'victima_id' => 1120,
                'created_at' => '2019-01-29 22:29:20',
                'updated_at' => '2019-01-29 22:29:20',
                'deleted_at' => NULL,
            ),
            120 => 
            array (
                'id' => 1121,
                'denuncia_id' => 1155,
                'victima_id' => 1121,
                'created_at' => '2019-01-29 22:29:20',
                'updated_at' => '2019-01-29 22:29:20',
                'deleted_at' => NULL,
            ),
            121 => 
            array (
                'id' => 1122,
                'denuncia_id' => 1155,
                'victima_id' => 1122,
                'created_at' => '2019-01-29 22:29:20',
                'updated_at' => '2019-01-29 22:29:20',
                'deleted_at' => NULL,
            ),
            122 => 
            array (
                'id' => 1123,
                'denuncia_id' => 1157,
                'victima_id' => 1123,
                'created_at' => '2019-01-29 22:29:20',
                'updated_at' => '2019-01-29 22:29:20',
                'deleted_at' => NULL,
            ),
            123 => 
            array (
                'id' => 1124,
                'denuncia_id' => 1158,
                'victima_id' => 1124,
                'created_at' => '2019-01-29 22:29:20',
                'updated_at' => '2019-01-29 22:29:20',
                'deleted_at' => NULL,
            ),
            124 => 
            array (
                'id' => 1125,
                'denuncia_id' => 1159,
                'victima_id' => 1125,
                'created_at' => '2019-01-29 22:29:20',
                'updated_at' => '2019-01-29 22:29:20',
                'deleted_at' => NULL,
            ),
            125 => 
            array (
                'id' => 1126,
                'denuncia_id' => 1160,
                'victima_id' => 1126,
                'created_at' => '2019-01-29 22:29:20',
                'updated_at' => '2019-01-29 22:29:20',
                'deleted_at' => NULL,
            ),
            126 => 
            array (
                'id' => 1127,
                'denuncia_id' => 1161,
                'victima_id' => 1127,
                'created_at' => '2019-01-29 22:29:21',
                'updated_at' => '2019-01-29 22:29:21',
                'deleted_at' => NULL,
            ),
            127 => 
            array (
                'id' => 1128,
                'denuncia_id' => 1162,
                'victima_id' => 1128,
                'created_at' => '2019-01-29 22:29:21',
                'updated_at' => '2019-01-29 22:29:21',
                'deleted_at' => NULL,
            ),
            128 => 
            array (
                'id' => 1129,
                'denuncia_id' => 1164,
                'victima_id' => 1129,
                'created_at' => '2019-01-29 22:29:21',
                'updated_at' => '2019-01-29 22:29:21',
                'deleted_at' => NULL,
            ),
            129 => 
            array (
                'id' => 1130,
                'denuncia_id' => 1165,
                'victima_id' => 1130,
                'created_at' => '2019-01-29 22:29:21',
                'updated_at' => '2019-01-29 22:29:21',
                'deleted_at' => NULL,
            ),
            130 => 
            array (
                'id' => 1131,
                'denuncia_id' => 1166,
                'victima_id' => 1131,
                'created_at' => '2019-01-29 22:29:21',
                'updated_at' => '2019-01-29 22:29:21',
                'deleted_at' => NULL,
            ),
            131 => 
            array (
                'id' => 1132,
                'denuncia_id' => 1167,
                'victima_id' => 1132,
                'created_at' => '2019-01-29 22:29:22',
                'updated_at' => '2019-01-29 22:29:22',
                'deleted_at' => NULL,
            ),
            132 => 
            array (
                'id' => 1133,
                'denuncia_id' => 1167,
                'victima_id' => 1133,
                'created_at' => '2019-01-29 22:29:22',
                'updated_at' => '2019-01-29 22:29:22',
                'deleted_at' => NULL,
            ),
            133 => 
            array (
                'id' => 1134,
                'denuncia_id' => 1167,
                'victima_id' => 1134,
                'created_at' => '2019-01-29 22:29:22',
                'updated_at' => '2019-01-29 22:29:22',
                'deleted_at' => NULL,
            ),
            134 => 
            array (
                'id' => 1135,
                'denuncia_id' => 1168,
                'victima_id' => 1135,
                'created_at' => '2019-01-29 22:29:22',
                'updated_at' => '2019-01-29 22:29:22',
                'deleted_at' => NULL,
            ),
            135 => 
            array (
                'id' => 1136,
                'denuncia_id' => 1169,
                'victima_id' => 1136,
                'created_at' => '2019-01-29 22:29:22',
                'updated_at' => '2019-01-29 22:29:22',
                'deleted_at' => NULL,
            ),
            136 => 
            array (
                'id' => 1137,
                'denuncia_id' => 1170,
                'victima_id' => 1137,
                'created_at' => '2019-01-29 22:29:22',
                'updated_at' => '2019-01-29 22:29:22',
                'deleted_at' => NULL,
            ),
            137 => 
            array (
                'id' => 1138,
                'denuncia_id' => 1171,
                'victima_id' => 1138,
                'created_at' => '2019-01-29 22:29:22',
                'updated_at' => '2019-01-29 22:29:22',
                'deleted_at' => NULL,
            ),
            138 => 
            array (
                'id' => 1139,
                'denuncia_id' => 1172,
                'victima_id' => 1139,
                'created_at' => '2019-01-29 22:29:23',
                'updated_at' => '2019-01-29 22:29:23',
                'deleted_at' => NULL,
            ),
            139 => 
            array (
                'id' => 1140,
                'denuncia_id' => 1173,
                'victima_id' => 1140,
                'created_at' => '2019-01-29 22:29:23',
                'updated_at' => '2019-01-29 22:29:23',
                'deleted_at' => NULL,
            ),
            140 => 
            array (
                'id' => 1141,
                'denuncia_id' => 1174,
                'victima_id' => 1141,
                'created_at' => '2019-01-29 22:29:23',
                'updated_at' => '2019-01-29 22:29:23',
                'deleted_at' => NULL,
            ),
            141 => 
            array (
                'id' => 1142,
                'denuncia_id' => 1175,
                'victima_id' => 1142,
                'created_at' => '2019-01-29 22:29:23',
                'updated_at' => '2019-01-29 22:29:23',
                'deleted_at' => NULL,
            ),
            142 => 
            array (
                'id' => 1143,
                'denuncia_id' => 1175,
                'victima_id' => 1143,
                'created_at' => '2019-01-29 22:29:23',
                'updated_at' => '2019-01-29 22:29:23',
                'deleted_at' => NULL,
            ),
            143 => 
            array (
                'id' => 1144,
                'denuncia_id' => 1175,
                'victima_id' => 1144,
                'created_at' => '2019-01-29 22:29:23',
                'updated_at' => '2019-01-29 22:29:23',
                'deleted_at' => NULL,
            ),
            144 => 
            array (
                'id' => 1145,
                'denuncia_id' => 1175,
                'victima_id' => 1145,
                'created_at' => '2019-01-29 22:29:23',
                'updated_at' => '2019-01-29 22:29:23',
                'deleted_at' => NULL,
            ),
            145 => 
            array (
                'id' => 1146,
                'denuncia_id' => 1176,
                'victima_id' => 1146,
                'created_at' => '2019-01-29 22:29:23',
                'updated_at' => '2019-01-29 22:29:23',
                'deleted_at' => NULL,
            ),
            146 => 
            array (
                'id' => 1147,
                'denuncia_id' => 1177,
                'victima_id' => 1147,
                'created_at' => '2019-01-29 22:29:24',
                'updated_at' => '2019-01-29 22:29:24',
                'deleted_at' => NULL,
            ),
            147 => 
            array (
                'id' => 1148,
                'denuncia_id' => 1178,
                'victima_id' => 1148,
                'created_at' => '2019-01-29 22:29:24',
                'updated_at' => '2019-01-29 22:29:24',
                'deleted_at' => NULL,
            ),
            148 => 
            array (
                'id' => 1149,
                'denuncia_id' => 1179,
                'victima_id' => 1149,
                'created_at' => '2019-01-29 22:29:24',
                'updated_at' => '2019-01-29 22:29:24',
                'deleted_at' => NULL,
            ),
            149 => 
            array (
                'id' => 1150,
                'denuncia_id' => 1180,
                'victima_id' => 1150,
                'created_at' => '2019-01-29 22:29:24',
                'updated_at' => '2019-01-29 22:29:24',
                'deleted_at' => NULL,
            ),
            150 => 
            array (
                'id' => 1151,
                'denuncia_id' => 1181,
                'victima_id' => 1151,
                'created_at' => '2019-01-29 22:29:24',
                'updated_at' => '2019-01-29 22:29:24',
                'deleted_at' => NULL,
            ),
            151 => 
            array (
                'id' => 1152,
                'denuncia_id' => 1183,
                'victima_id' => 1152,
                'created_at' => '2019-01-29 22:29:25',
                'updated_at' => '2019-01-29 22:29:25',
                'deleted_at' => NULL,
            ),
            152 => 
            array (
                'id' => 1153,
                'denuncia_id' => 1185,
                'victima_id' => 1153,
                'created_at' => '2019-01-29 22:29:25',
                'updated_at' => '2019-01-29 22:29:25',
                'deleted_at' => NULL,
            ),
            153 => 
            array (
                'id' => 1154,
                'denuncia_id' => 1186,
                'victima_id' => 1154,
                'created_at' => '2019-01-29 22:29:25',
                'updated_at' => '2019-01-29 22:29:25',
                'deleted_at' => NULL,
            ),
            154 => 
            array (
                'id' => 1155,
                'denuncia_id' => 1188,
                'victima_id' => 1155,
                'created_at' => '2019-01-29 22:29:25',
                'updated_at' => '2019-01-29 22:29:25',
                'deleted_at' => NULL,
            ),
            155 => 
            array (
                'id' => 1156,
                'denuncia_id' => 1189,
                'victima_id' => 1156,
                'created_at' => '2019-01-29 22:29:26',
                'updated_at' => '2019-01-29 22:29:26',
                'deleted_at' => NULL,
            ),
            156 => 
            array (
                'id' => 1157,
                'denuncia_id' => 1190,
                'victima_id' => 1157,
                'created_at' => '2019-01-29 22:29:26',
                'updated_at' => '2019-01-29 22:29:26',
                'deleted_at' => NULL,
            ),
            157 => 
            array (
                'id' => 1158,
                'denuncia_id' => 1191,
                'victima_id' => 1158,
                'created_at' => '2019-01-29 22:29:26',
                'updated_at' => '2019-01-29 22:29:26',
                'deleted_at' => NULL,
            ),
            158 => 
            array (
                'id' => 1159,
                'denuncia_id' => 1192,
                'victima_id' => 1159,
                'created_at' => '2019-01-29 22:29:26',
                'updated_at' => '2019-01-29 22:29:26',
                'deleted_at' => NULL,
            ),
            159 => 
            array (
                'id' => 1160,
                'denuncia_id' => 1193,
                'victima_id' => 1160,
                'created_at' => '2019-01-29 22:29:26',
                'updated_at' => '2019-01-29 22:29:26',
                'deleted_at' => NULL,
            ),
            160 => 
            array (
                'id' => 1161,
                'denuncia_id' => 1194,
                'victima_id' => 1161,
                'created_at' => '2019-01-29 22:29:26',
                'updated_at' => '2019-01-29 22:29:26',
                'deleted_at' => NULL,
            ),
            161 => 
            array (
                'id' => 1162,
                'denuncia_id' => 1194,
                'victima_id' => 1162,
                'created_at' => '2019-01-29 22:29:26',
                'updated_at' => '2019-01-29 22:29:26',
                'deleted_at' => NULL,
            ),
            162 => 
            array (
                'id' => 1163,
                'denuncia_id' => 1195,
                'victima_id' => 1163,
                'created_at' => '2019-01-29 22:29:26',
                'updated_at' => '2019-01-29 22:29:26',
                'deleted_at' => NULL,
            ),
            163 => 
            array (
                'id' => 1164,
                'denuncia_id' => 1195,
                'victima_id' => 1164,
                'created_at' => '2019-01-29 22:29:26',
                'updated_at' => '2019-01-29 22:29:26',
                'deleted_at' => NULL,
            ),
            164 => 
            array (
                'id' => 1165,
                'denuncia_id' => 1195,
                'victima_id' => 1165,
                'created_at' => '2019-01-29 22:29:26',
                'updated_at' => '2019-01-29 22:29:26',
                'deleted_at' => NULL,
            ),
            165 => 
            array (
                'id' => 1166,
                'denuncia_id' => 1198,
                'victima_id' => 1166,
                'created_at' => '2019-01-29 22:29:27',
                'updated_at' => '2019-01-29 22:29:27',
                'deleted_at' => NULL,
            ),
            166 => 
            array (
                'id' => 1167,
                'denuncia_id' => 1199,
                'victima_id' => 1167,
                'created_at' => '2019-01-29 22:29:27',
                'updated_at' => '2019-01-29 22:29:27',
                'deleted_at' => NULL,
            ),
            167 => 
            array (
                'id' => 1168,
                'denuncia_id' => 1200,
                'victima_id' => 1168,
                'created_at' => '2019-01-29 22:29:27',
                'updated_at' => '2019-01-29 22:29:27',
                'deleted_at' => NULL,
            ),
            168 => 
            array (
                'id' => 1169,
                'denuncia_id' => 1201,
                'victima_id' => 1169,
                'created_at' => '2019-01-29 22:29:28',
                'updated_at' => '2019-01-29 22:29:28',
                'deleted_at' => NULL,
            ),
            169 => 
            array (
                'id' => 1170,
                'denuncia_id' => 1201,
                'victima_id' => 1170,
                'created_at' => '2019-01-29 22:29:28',
                'updated_at' => '2019-01-29 22:29:28',
                'deleted_at' => NULL,
            ),
            170 => 
            array (
                'id' => 1171,
                'denuncia_id' => 1202,
                'victima_id' => 1171,
                'created_at' => '2019-01-29 22:29:28',
                'updated_at' => '2019-01-29 22:29:28',
                'deleted_at' => NULL,
            ),
            171 => 
            array (
                'id' => 1172,
                'denuncia_id' => 1203,
                'victima_id' => 1172,
                'created_at' => '2019-01-29 22:29:28',
                'updated_at' => '2019-01-29 22:29:28',
                'deleted_at' => NULL,
            ),
            172 => 
            array (
                'id' => 1173,
                'denuncia_id' => 1204,
                'victima_id' => 1173,
                'created_at' => '2019-01-29 22:29:28',
                'updated_at' => '2019-01-29 22:29:28',
                'deleted_at' => NULL,
            ),
            173 => 
            array (
                'id' => 1174,
                'denuncia_id' => 1205,
                'victima_id' => 1174,
                'created_at' => '2019-01-29 22:29:28',
                'updated_at' => '2019-01-29 22:29:28',
                'deleted_at' => NULL,
            ),
            174 => 
            array (
                'id' => 1175,
                'denuncia_id' => 1206,
                'victima_id' => 1175,
                'created_at' => '2019-01-29 22:29:28',
                'updated_at' => '2019-01-29 22:29:28',
                'deleted_at' => NULL,
            ),
            175 => 
            array (
                'id' => 1176,
                'denuncia_id' => 1207,
                'victima_id' => 1176,
                'created_at' => '2019-01-29 22:29:28',
                'updated_at' => '2019-01-29 22:29:28',
                'deleted_at' => NULL,
            ),
            176 => 
            array (
                'id' => 1177,
                'denuncia_id' => 1208,
                'victima_id' => 1177,
                'created_at' => '2019-01-29 22:29:29',
                'updated_at' => '2019-01-29 22:29:29',
                'deleted_at' => NULL,
            ),
            177 => 
            array (
                'id' => 1178,
                'denuncia_id' => 1209,
                'victima_id' => 1178,
                'created_at' => '2019-01-29 22:29:29',
                'updated_at' => '2019-01-29 22:29:29',
                'deleted_at' => NULL,
            ),
            178 => 
            array (
                'id' => 1179,
                'denuncia_id' => 1210,
                'victima_id' => 1179,
                'created_at' => '2019-01-29 22:29:29',
                'updated_at' => '2019-01-29 22:29:29',
                'deleted_at' => NULL,
            ),
            179 => 
            array (
                'id' => 1180,
                'denuncia_id' => 1211,
                'victima_id' => 1180,
                'created_at' => '2019-01-29 22:29:29',
                'updated_at' => '2019-01-29 22:29:29',
                'deleted_at' => NULL,
            ),
            180 => 
            array (
                'id' => 1181,
                'denuncia_id' => 1211,
                'victima_id' => 1181,
                'created_at' => '2019-01-29 22:29:29',
                'updated_at' => '2019-01-29 22:29:29',
                'deleted_at' => NULL,
            ),
            181 => 
            array (
                'id' => 1182,
                'denuncia_id' => 1212,
                'victima_id' => 1182,
                'created_at' => '2019-01-29 22:29:29',
                'updated_at' => '2019-01-29 22:29:29',
                'deleted_at' => NULL,
            ),
            182 => 
            array (
                'id' => 1183,
                'denuncia_id' => 1213,
                'victima_id' => 1183,
                'created_at' => '2019-01-29 22:29:29',
                'updated_at' => '2019-01-29 22:29:29',
                'deleted_at' => NULL,
            ),
            183 => 
            array (
                'id' => 1184,
                'denuncia_id' => 1214,
                'victima_id' => 1184,
                'created_at' => '2019-01-29 22:29:29',
                'updated_at' => '2019-01-29 22:29:29',
                'deleted_at' => NULL,
            ),
            184 => 
            array (
                'id' => 1185,
                'denuncia_id' => 1215,
                'victima_id' => 1185,
                'created_at' => '2019-01-29 22:29:30',
                'updated_at' => '2019-01-29 22:29:30',
                'deleted_at' => NULL,
            ),
            185 => 
            array (
                'id' => 1186,
                'denuncia_id' => 1216,
                'victima_id' => 1186,
                'created_at' => '2019-01-29 22:29:30',
                'updated_at' => '2019-01-29 22:29:30',
                'deleted_at' => NULL,
            ),
            186 => 
            array (
                'id' => 1187,
                'denuncia_id' => 1218,
                'victima_id' => 1187,
                'created_at' => '2019-01-29 22:29:30',
                'updated_at' => '2019-01-29 22:29:30',
                'deleted_at' => NULL,
            ),
            187 => 
            array (
                'id' => 1188,
                'denuncia_id' => 1219,
                'victima_id' => 1188,
                'created_at' => '2019-01-29 22:29:30',
                'updated_at' => '2019-01-29 22:29:30',
                'deleted_at' => NULL,
            ),
            188 => 
            array (
                'id' => 1189,
                'denuncia_id' => 1220,
                'victima_id' => 1189,
                'created_at' => '2019-01-29 22:29:30',
                'updated_at' => '2019-01-29 22:29:30',
                'deleted_at' => NULL,
            ),
            189 => 
            array (
                'id' => 1190,
                'denuncia_id' => 1221,
                'victima_id' => 1190,
                'created_at' => '2019-01-29 22:29:31',
                'updated_at' => '2019-01-29 22:29:31',
                'deleted_at' => NULL,
            ),
            190 => 
            array (
                'id' => 1191,
                'denuncia_id' => 1222,
                'victima_id' => 1191,
                'created_at' => '2019-01-29 22:29:31',
                'updated_at' => '2019-01-29 22:29:31',
                'deleted_at' => NULL,
            ),
            191 => 
            array (
                'id' => 1192,
                'denuncia_id' => 1223,
                'victima_id' => 1192,
                'created_at' => '2019-01-29 22:29:31',
                'updated_at' => '2019-01-29 22:29:31',
                'deleted_at' => NULL,
            ),
            192 => 
            array (
                'id' => 1193,
                'denuncia_id' => 1224,
                'victima_id' => 1193,
                'created_at' => '2019-01-29 22:29:31',
                'updated_at' => '2019-01-29 22:29:31',
                'deleted_at' => NULL,
            ),
            193 => 
            array (
                'id' => 1194,
                'denuncia_id' => 1225,
                'victima_id' => 1194,
                'created_at' => '2019-01-29 22:29:31',
                'updated_at' => '2019-01-29 22:29:31',
                'deleted_at' => NULL,
            ),
            194 => 
            array (
                'id' => 1195,
                'denuncia_id' => 1226,
                'victima_id' => 1195,
                'created_at' => '2019-01-29 22:29:31',
                'updated_at' => '2019-01-29 22:29:31',
                'deleted_at' => NULL,
            ),
            195 => 
            array (
                'id' => 1196,
                'denuncia_id' => 1227,
                'victima_id' => 1196,
                'created_at' => '2019-01-29 22:29:32',
                'updated_at' => '2019-01-29 22:29:32',
                'deleted_at' => NULL,
            ),
            196 => 
            array (
                'id' => 1197,
                'denuncia_id' => 1228,
                'victima_id' => 1197,
                'created_at' => '2019-01-29 22:29:32',
                'updated_at' => '2019-01-29 22:29:32',
                'deleted_at' => NULL,
            ),
            197 => 
            array (
                'id' => 1198,
                'denuncia_id' => 1229,
                'victima_id' => 1198,
                'created_at' => '2019-01-29 22:29:32',
                'updated_at' => '2019-01-29 22:29:32',
                'deleted_at' => NULL,
            ),
            198 => 
            array (
                'id' => 1199,
                'denuncia_id' => 1230,
                'victima_id' => 1199,
                'created_at' => '2019-01-29 22:29:32',
                'updated_at' => '2019-01-29 22:29:32',
                'deleted_at' => NULL,
            ),
            199 => 
            array (
                'id' => 1200,
                'denuncia_id' => 1231,
                'victima_id' => 1200,
                'created_at' => '2019-01-29 22:29:32',
                'updated_at' => '2019-01-29 22:29:32',
                'deleted_at' => NULL,
            ),
            200 => 
            array (
                'id' => 1201,
                'denuncia_id' => 1232,
                'victima_id' => 1201,
                'created_at' => '2019-01-29 22:29:33',
                'updated_at' => '2019-01-29 22:29:33',
                'deleted_at' => NULL,
            ),
            201 => 
            array (
                'id' => 1202,
                'denuncia_id' => 1232,
                'victima_id' => 1202,
                'created_at' => '2019-01-29 22:29:33',
                'updated_at' => '2019-01-29 22:29:33',
                'deleted_at' => NULL,
            ),
            202 => 
            array (
                'id' => 1203,
                'denuncia_id' => 1232,
                'victima_id' => 1203,
                'created_at' => '2019-01-29 22:29:33',
                'updated_at' => '2019-01-29 22:29:33',
                'deleted_at' => NULL,
            ),
            203 => 
            array (
                'id' => 1204,
                'denuncia_id' => 1232,
                'victima_id' => 1204,
                'created_at' => '2019-01-29 22:29:33',
                'updated_at' => '2019-01-29 22:29:33',
                'deleted_at' => NULL,
            ),
            204 => 
            array (
                'id' => 1205,
                'denuncia_id' => 1232,
                'victima_id' => 1205,
                'created_at' => '2019-01-29 22:29:33',
                'updated_at' => '2019-01-29 22:29:33',
                'deleted_at' => NULL,
            ),
            205 => 
            array (
                'id' => 1206,
                'denuncia_id' => 1233,
                'victima_id' => 1206,
                'created_at' => '2019-01-29 22:29:33',
                'updated_at' => '2019-01-29 22:29:33',
                'deleted_at' => NULL,
            ),
            206 => 
            array (
                'id' => 1207,
                'denuncia_id' => 1234,
                'victima_id' => 1207,
                'created_at' => '2019-01-29 22:29:33',
                'updated_at' => '2019-01-29 22:29:33',
                'deleted_at' => NULL,
            ),
            207 => 
            array (
                'id' => 1208,
                'denuncia_id' => 1235,
                'victima_id' => 1208,
                'created_at' => '2019-01-29 22:29:33',
                'updated_at' => '2019-01-29 22:29:33',
                'deleted_at' => NULL,
            ),
            208 => 
            array (
                'id' => 1209,
                'denuncia_id' => 1236,
                'victima_id' => 1209,
                'created_at' => '2019-01-29 22:29:33',
                'updated_at' => '2019-01-29 22:29:33',
                'deleted_at' => NULL,
            ),
            209 => 
            array (
                'id' => 1210,
                'denuncia_id' => 1237,
                'victima_id' => 1210,
                'created_at' => '2019-01-29 22:29:34',
                'updated_at' => '2019-01-29 22:29:34',
                'deleted_at' => NULL,
            ),
            210 => 
            array (
                'id' => 1211,
                'denuncia_id' => 1238,
                'victima_id' => 1211,
                'created_at' => '2019-01-29 22:29:34',
                'updated_at' => '2019-01-29 22:29:34',
                'deleted_at' => NULL,
            ),
            211 => 
            array (
                'id' => 1212,
                'denuncia_id' => 1239,
                'victima_id' => 1212,
                'created_at' => '2019-01-29 22:29:34',
                'updated_at' => '2019-01-29 22:29:34',
                'deleted_at' => NULL,
            ),
            212 => 
            array (
                'id' => 1213,
                'denuncia_id' => 1241,
                'victima_id' => 1213,
                'created_at' => '2019-01-29 22:29:34',
                'updated_at' => '2019-01-29 22:29:34',
                'deleted_at' => NULL,
            ),
            213 => 
            array (
                'id' => 1214,
                'denuncia_id' => 1242,
                'victima_id' => 1214,
                'created_at' => '2019-01-29 22:29:34',
                'updated_at' => '2019-01-29 22:29:34',
                'deleted_at' => NULL,
            ),
            214 => 
            array (
                'id' => 1215,
                'denuncia_id' => 1243,
                'victima_id' => 1215,
                'created_at' => '2019-01-29 22:29:34',
                'updated_at' => '2019-01-29 22:29:34',
                'deleted_at' => NULL,
            ),
            215 => 
            array (
                'id' => 1216,
                'denuncia_id' => 1244,
                'victima_id' => 1216,
                'created_at' => '2019-01-29 22:29:34',
                'updated_at' => '2019-01-29 22:29:34',
                'deleted_at' => NULL,
            ),
            216 => 
            array (
                'id' => 1217,
                'denuncia_id' => 1245,
                'victima_id' => 1217,
                'created_at' => '2019-01-29 22:29:35',
                'updated_at' => '2019-01-29 22:29:35',
                'deleted_at' => NULL,
            ),
            217 => 
            array (
                'id' => 1218,
                'denuncia_id' => 1246,
                'victima_id' => 1218,
                'created_at' => '2019-01-29 22:29:35',
                'updated_at' => '2019-01-29 22:29:35',
                'deleted_at' => NULL,
            ),
            218 => 
            array (
                'id' => 1219,
                'denuncia_id' => 1247,
                'victima_id' => 1219,
                'created_at' => '2019-01-29 22:29:35',
                'updated_at' => '2019-01-29 22:29:35',
                'deleted_at' => NULL,
            ),
            219 => 
            array (
                'id' => 1220,
                'denuncia_id' => 1248,
                'victima_id' => 1220,
                'created_at' => '2019-01-29 22:29:35',
                'updated_at' => '2019-01-29 22:29:35',
                'deleted_at' => NULL,
            ),
            220 => 
            array (
                'id' => 1221,
                'denuncia_id' => 1249,
                'victima_id' => 1221,
                'created_at' => '2019-01-29 22:29:35',
                'updated_at' => '2019-01-29 22:29:35',
                'deleted_at' => NULL,
            ),
            221 => 
            array (
                'id' => 1222,
                'denuncia_id' => 1250,
                'victima_id' => 1222,
                'created_at' => '2019-01-29 22:29:36',
                'updated_at' => '2019-01-29 22:29:36',
                'deleted_at' => NULL,
            ),
            222 => 
            array (
                'id' => 1223,
                'denuncia_id' => 1251,
                'victima_id' => 1223,
                'created_at' => '2019-01-29 22:29:36',
                'updated_at' => '2019-01-29 22:29:36',
                'deleted_at' => NULL,
            ),
            223 => 
            array (
                'id' => 1224,
                'denuncia_id' => 1252,
                'victima_id' => 1224,
                'created_at' => '2019-01-29 22:29:36',
                'updated_at' => '2019-01-29 22:29:36',
                'deleted_at' => NULL,
            ),
            224 => 
            array (
                'id' => 1225,
                'denuncia_id' => 1253,
                'victima_id' => 1225,
                'created_at' => '2019-01-29 22:29:36',
                'updated_at' => '2019-01-29 22:29:36',
                'deleted_at' => NULL,
            ),
            225 => 
            array (
                'id' => 1226,
                'denuncia_id' => 1254,
                'victima_id' => 1226,
                'created_at' => '2019-01-29 22:29:36',
                'updated_at' => '2019-01-29 22:29:36',
                'deleted_at' => NULL,
            ),
            226 => 
            array (
                'id' => 1227,
                'denuncia_id' => 1255,
                'victima_id' => 1227,
                'created_at' => '2019-01-29 22:29:37',
                'updated_at' => '2019-01-29 22:29:37',
                'deleted_at' => NULL,
            ),
            227 => 
            array (
                'id' => 1228,
                'denuncia_id' => 1255,
                'victima_id' => 1228,
                'created_at' => '2019-01-29 22:29:37',
                'updated_at' => '2019-01-29 22:29:37',
                'deleted_at' => NULL,
            ),
            228 => 
            array (
                'id' => 1229,
                'denuncia_id' => 1256,
                'victima_id' => 1229,
                'created_at' => '2019-01-29 22:29:37',
                'updated_at' => '2019-01-29 22:29:37',
                'deleted_at' => NULL,
            ),
            229 => 
            array (
                'id' => 1230,
                'denuncia_id' => 1257,
                'victima_id' => 1230,
                'created_at' => '2019-01-29 22:29:37',
                'updated_at' => '2019-01-29 22:29:37',
                'deleted_at' => NULL,
            ),
            230 => 
            array (
                'id' => 1231,
                'denuncia_id' => 1258,
                'victima_id' => 1231,
                'created_at' => '2019-01-29 22:29:37',
                'updated_at' => '2019-01-29 22:29:37',
                'deleted_at' => NULL,
            ),
            231 => 
            array (
                'id' => 1232,
                'denuncia_id' => 1259,
                'victima_id' => 1232,
                'created_at' => '2019-01-29 22:29:37',
                'updated_at' => '2019-01-29 22:29:37',
                'deleted_at' => NULL,
            ),
            232 => 
            array (
                'id' => 1233,
                'denuncia_id' => 1260,
                'victima_id' => 1233,
                'created_at' => '2019-01-29 22:29:37',
                'updated_at' => '2019-01-29 22:29:37',
                'deleted_at' => NULL,
            ),
            233 => 
            array (
                'id' => 1234,
                'denuncia_id' => 1261,
                'victima_id' => 1234,
                'created_at' => '2019-01-29 22:29:37',
                'updated_at' => '2019-01-29 22:29:37',
                'deleted_at' => NULL,
            ),
            234 => 
            array (
                'id' => 1235,
                'denuncia_id' => 1262,
                'victima_id' => 1235,
                'created_at' => '2019-01-29 22:29:38',
                'updated_at' => '2019-01-29 22:29:38',
                'deleted_at' => NULL,
            ),
            235 => 
            array (
                'id' => 1236,
                'denuncia_id' => 1263,
                'victima_id' => 1236,
                'created_at' => '2019-01-29 22:29:38',
                'updated_at' => '2019-01-29 22:29:38',
                'deleted_at' => NULL,
            ),
            236 => 
            array (
                'id' => 1237,
                'denuncia_id' => 1264,
                'victima_id' => 1237,
                'created_at' => '2019-01-29 22:29:38',
                'updated_at' => '2019-01-29 22:29:38',
                'deleted_at' => NULL,
            ),
            237 => 
            array (
                'id' => 1238,
                'denuncia_id' => 1265,
                'victima_id' => 1238,
                'created_at' => '2019-01-29 22:29:38',
                'updated_at' => '2019-01-29 22:29:38',
                'deleted_at' => NULL,
            ),
            238 => 
            array (
                'id' => 1239,
                'denuncia_id' => 1266,
                'victima_id' => 1239,
                'created_at' => '2019-01-29 22:29:38',
                'updated_at' => '2019-01-29 22:29:38',
                'deleted_at' => NULL,
            ),
            239 => 
            array (
                'id' => 1240,
                'denuncia_id' => 1267,
                'victima_id' => 1240,
                'created_at' => '2019-01-29 22:29:38',
                'updated_at' => '2019-01-29 22:29:38',
                'deleted_at' => NULL,
            ),
            240 => 
            array (
                'id' => 1241,
                'denuncia_id' => 1268,
                'victima_id' => 1241,
                'created_at' => '2019-01-29 22:29:39',
                'updated_at' => '2019-01-29 22:29:39',
                'deleted_at' => NULL,
            ),
            241 => 
            array (
                'id' => 1242,
                'denuncia_id' => 1269,
                'victima_id' => 1242,
                'created_at' => '2019-01-29 22:29:39',
                'updated_at' => '2019-01-29 22:29:39',
                'deleted_at' => NULL,
            ),
            242 => 
            array (
                'id' => 1243,
                'denuncia_id' => 1270,
                'victima_id' => 1243,
                'created_at' => '2019-01-29 22:29:39',
                'updated_at' => '2019-01-29 22:29:39',
                'deleted_at' => NULL,
            ),
            243 => 
            array (
                'id' => 1244,
                'denuncia_id' => 1271,
                'victima_id' => 1244,
                'created_at' => '2019-01-29 22:29:39',
                'updated_at' => '2019-01-29 22:29:39',
                'deleted_at' => NULL,
            ),
            244 => 
            array (
                'id' => 1245,
                'denuncia_id' => 1272,
                'victima_id' => 1245,
                'created_at' => '2019-01-29 22:29:39',
                'updated_at' => '2019-01-29 22:29:39',
                'deleted_at' => NULL,
            ),
            245 => 
            array (
                'id' => 1246,
                'denuncia_id' => 1273,
                'victima_id' => 1246,
                'created_at' => '2019-01-29 22:29:40',
                'updated_at' => '2019-01-29 22:29:40',
                'deleted_at' => NULL,
            ),
            246 => 
            array (
                'id' => 1247,
                'denuncia_id' => 1274,
                'victima_id' => 1247,
                'created_at' => '2019-01-29 22:29:40',
                'updated_at' => '2019-01-29 22:29:40',
                'deleted_at' => NULL,
            ),
            247 => 
            array (
                'id' => 1248,
                'denuncia_id' => 1274,
                'victima_id' => 1248,
                'created_at' => '2019-01-29 22:29:40',
                'updated_at' => '2019-01-29 22:29:40',
                'deleted_at' => NULL,
            ),
            248 => 
            array (
                'id' => 1249,
                'denuncia_id' => 1275,
                'victima_id' => 1249,
                'created_at' => '2019-01-29 22:29:40',
                'updated_at' => '2019-01-29 22:29:40',
                'deleted_at' => NULL,
            ),
            249 => 
            array (
                'id' => 1250,
                'denuncia_id' => 1275,
                'victima_id' => 1250,
                'created_at' => '2019-01-29 22:29:40',
                'updated_at' => '2019-01-29 22:29:40',
                'deleted_at' => NULL,
            ),
            250 => 
            array (
                'id' => 1251,
                'denuncia_id' => 1276,
                'victima_id' => 1251,
                'created_at' => '2019-01-29 22:29:40',
                'updated_at' => '2019-01-29 22:29:40',
                'deleted_at' => NULL,
            ),
            251 => 
            array (
                'id' => 1252,
                'denuncia_id' => 1277,
                'victima_id' => 1252,
                'created_at' => '2019-01-29 22:29:40',
                'updated_at' => '2019-01-29 22:29:40',
                'deleted_at' => NULL,
            ),
            252 => 
            array (
                'id' => 1253,
                'denuncia_id' => 1278,
                'victima_id' => 1253,
                'created_at' => '2019-01-29 22:29:40',
                'updated_at' => '2019-01-29 22:29:40',
                'deleted_at' => NULL,
            ),
            253 => 
            array (
                'id' => 1254,
                'denuncia_id' => 1279,
                'victima_id' => 1254,
                'created_at' => '2019-01-29 22:29:41',
                'updated_at' => '2019-01-29 22:29:41',
                'deleted_at' => NULL,
            ),
            254 => 
            array (
                'id' => 1255,
                'denuncia_id' => 1280,
                'victima_id' => 1255,
                'created_at' => '2019-01-29 22:29:41',
                'updated_at' => '2019-01-29 22:29:41',
                'deleted_at' => NULL,
            ),
            255 => 
            array (
                'id' => 1256,
                'denuncia_id' => 1281,
                'victima_id' => 1256,
                'created_at' => '2019-01-29 22:29:41',
                'updated_at' => '2019-01-29 22:29:41',
                'deleted_at' => NULL,
            ),
            256 => 
            array (
                'id' => 1257,
                'denuncia_id' => 1282,
                'victima_id' => 1257,
                'created_at' => '2019-01-29 22:29:41',
                'updated_at' => '2019-01-29 22:29:41',
                'deleted_at' => NULL,
            ),
            257 => 
            array (
                'id' => 1258,
                'denuncia_id' => 1283,
                'victima_id' => 1258,
                'created_at' => '2019-01-29 22:29:42',
                'updated_at' => '2019-01-29 22:29:42',
                'deleted_at' => NULL,
            ),
            258 => 
            array (
                'id' => 1259,
                'denuncia_id' => 1284,
                'victima_id' => 1259,
                'created_at' => '2019-01-29 22:29:42',
                'updated_at' => '2019-01-29 22:29:42',
                'deleted_at' => NULL,
            ),
            259 => 
            array (
                'id' => 1260,
                'denuncia_id' => 1285,
                'victima_id' => 1260,
                'created_at' => '2019-01-29 22:29:42',
                'updated_at' => '2019-01-29 22:29:42',
                'deleted_at' => NULL,
            ),
            260 => 
            array (
                'id' => 1261,
                'denuncia_id' => 1286,
                'victima_id' => 1261,
                'created_at' => '2019-01-29 22:29:42',
                'updated_at' => '2019-01-29 22:29:42',
                'deleted_at' => NULL,
            ),
            261 => 
            array (
                'id' => 1262,
                'denuncia_id' => 1287,
                'victima_id' => 1262,
                'created_at' => '2019-01-29 22:29:42',
                'updated_at' => '2019-01-29 22:29:42',
                'deleted_at' => NULL,
            ),
            262 => 
            array (
                'id' => 1263,
                'denuncia_id' => 1288,
                'victima_id' => 1263,
                'created_at' => '2019-01-29 22:29:42',
                'updated_at' => '2019-01-29 22:29:42',
                'deleted_at' => NULL,
            ),
            263 => 
            array (
                'id' => 1264,
                'denuncia_id' => 1289,
                'victima_id' => 1264,
                'created_at' => '2019-01-29 22:29:42',
                'updated_at' => '2019-01-29 22:29:42',
                'deleted_at' => NULL,
            ),
            264 => 
            array (
                'id' => 1265,
                'denuncia_id' => 1290,
                'victima_id' => 1265,
                'created_at' => '2019-01-29 22:29:43',
                'updated_at' => '2019-01-29 22:29:43',
                'deleted_at' => NULL,
            ),
            265 => 
            array (
                'id' => 1266,
                'denuncia_id' => 1292,
                'victima_id' => 1266,
                'created_at' => '2019-01-29 22:29:43',
                'updated_at' => '2019-01-29 22:29:43',
                'deleted_at' => NULL,
            ),
            266 => 
            array (
                'id' => 1267,
                'denuncia_id' => 1293,
                'victima_id' => 1267,
                'created_at' => '2019-01-29 22:29:43',
                'updated_at' => '2019-01-29 22:29:43',
                'deleted_at' => NULL,
            ),
            267 => 
            array (
                'id' => 1268,
                'denuncia_id' => 1294,
                'victima_id' => 1268,
                'created_at' => '2019-01-29 22:29:43',
                'updated_at' => '2019-01-29 22:29:43',
                'deleted_at' => NULL,
            ),
            268 => 
            array (
                'id' => 1269,
                'denuncia_id' => 1295,
                'victima_id' => 1269,
                'created_at' => '2019-01-29 22:29:43',
                'updated_at' => '2019-01-29 22:29:43',
                'deleted_at' => NULL,
            ),
            269 => 
            array (
                'id' => 1270,
                'denuncia_id' => 1296,
                'victima_id' => 1270,
                'created_at' => '2019-01-29 22:29:44',
                'updated_at' => '2019-01-29 22:29:44',
                'deleted_at' => NULL,
            ),
            270 => 
            array (
                'id' => 1271,
                'denuncia_id' => 1297,
                'victima_id' => 1271,
                'created_at' => '2019-01-29 22:29:44',
                'updated_at' => '2019-01-29 22:29:44',
                'deleted_at' => NULL,
            ),
            271 => 
            array (
                'id' => 1272,
                'denuncia_id' => 1298,
                'victima_id' => 1272,
                'created_at' => '2019-01-29 22:29:44',
                'updated_at' => '2019-01-29 22:29:44',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}