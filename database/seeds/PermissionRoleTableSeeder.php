<?php

use Illuminate\Database\Seeder;

class PermissionRoleTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permission_role')->delete();
        
        \DB::table('permission_role')->insert(array (
            0 => 
            array (
                'id' => 1,
                'permission_id' => 51,
                'role_id' => 2,
                'created_at' => '2019-02-10 20:01:53',
                'updated_at' => '2019-02-10 20:01:53',
            ),
            1 => 
            array (
                'id' => 2,
                'permission_id' => 52,
                'role_id' => 2,
                'created_at' => '2019-02-10 20:01:53',
                'updated_at' => '2019-02-10 20:01:53',
            ),
            2 => 
            array (
                'id' => 3,
                'permission_id' => 53,
                'role_id' => 2,
                'created_at' => '2019-02-10 20:01:53',
                'updated_at' => '2019-02-10 20:01:53',
            ),
            3 => 
            array (
                'id' => 4,
                'permission_id' => 54,
                'role_id' => 2,
                'created_at' => '2019-02-10 20:01:53',
                'updated_at' => '2019-02-10 20:01:53',
            ),
            4 => 
            array (
                'id' => 6,
                'permission_id' => 56,
                'role_id' => 2,
                'created_at' => '2019-02-10 20:01:53',
                'updated_at' => '2019-02-10 20:01:53',
            ),
            5 => 
            array (
                'id' => 7,
                'permission_id' => 57,
                'role_id' => 2,
                'created_at' => '2019-02-10 20:01:53',
                'updated_at' => '2019-02-10 20:01:53',
            ),
            6 => 
            array (
                'id' => 8,
                'permission_id' => 58,
                'role_id' => 2,
                'created_at' => '2019-02-10 20:01:53',
                'updated_at' => '2019-02-10 20:01:53',
            ),
            7 => 
            array (
                'id' => 9,
                'permission_id' => 59,
                'role_id' => 2,
                'created_at' => '2019-02-10 20:01:53',
                'updated_at' => '2019-02-10 20:01:53',
            ),
            8 => 
            array (
                'id' => 11,
                'permission_id' => 61,
                'role_id' => 2,
                'created_at' => '2019-02-10 20:01:53',
                'updated_at' => '2019-02-10 20:01:53',
            ),
            9 => 
            array (
                'id' => 12,
                'permission_id' => 62,
                'role_id' => 2,
                'created_at' => '2019-02-10 20:01:53',
                'updated_at' => '2019-02-10 20:01:53',
            ),
            10 => 
            array (
                'id' => 13,
                'permission_id' => 63,
                'role_id' => 2,
                'created_at' => '2019-02-10 20:01:53',
                'updated_at' => '2019-02-10 20:01:53',
            ),
            11 => 
            array (
                'id' => 14,
                'permission_id' => 64,
                'role_id' => 2,
                'created_at' => '2019-02-10 20:01:53',
                'updated_at' => '2019-02-10 20:01:53',
            ),
            12 => 
            array (
                'id' => 15,
                'permission_id' => 67,
                'role_id' => 2,
                'created_at' => '2019-02-10 20:01:53',
                'updated_at' => '2019-02-10 20:01:53',
            ),
            13 => 
            array (
                'id' => 16,
                'permission_id' => 68,
                'role_id' => 2,
                'created_at' => '2019-02-10 20:01:53',
                'updated_at' => '2019-02-10 20:01:53',
            ),
            14 => 
            array (
                'id' => 17,
                'permission_id' => 69,
                'role_id' => 2,
                'created_at' => '2019-02-10 20:01:53',
                'updated_at' => '2019-02-10 20:01:53',
            ),
            15 => 
            array (
                'id' => 18,
                'permission_id' => 70,
                'role_id' => 2,
                'created_at' => '2019-02-10 20:01:53',
                'updated_at' => '2019-02-10 20:01:53',
            ),
            16 => 
            array (
                'id' => 19,
                'permission_id' => 71,
                'role_id' => 2,
                'created_at' => '2019-02-10 20:01:53',
                'updated_at' => '2019-02-10 20:01:53',
            ),
            17 => 
            array (
                'id' => 20,
                'permission_id' => 72,
                'role_id' => 2,
                'created_at' => '2019-02-10 20:01:53',
                'updated_at' => '2019-02-10 20:01:53',
            ),
            18 => 
            array (
                'id' => 21,
                'permission_id' => 73,
                'role_id' => 2,
                'created_at' => '2019-02-10 20:01:53',
                'updated_at' => '2019-02-10 20:01:53',
            ),
            19 => 
            array (
                'id' => 22,
                'permission_id' => 74,
                'role_id' => 2,
                'created_at' => '2019-02-10 20:01:53',
                'updated_at' => '2019-02-10 20:01:53',
            ),
            20 => 
            array (
                'id' => 23,
                'permission_id' => 75,
                'role_id' => 2,
                'created_at' => '2019-02-10 20:01:53',
                'updated_at' => '2019-02-10 20:01:53',
            ),
            21 => 
            array (
                'id' => 24,
                'permission_id' => 51,
                'role_id' => 3,
                'created_at' => '2019-02-12 21:50:33',
                'updated_at' => '2019-02-12 21:50:33',
            ),
            22 => 
            array (
                'id' => 25,
                'permission_id' => 52,
                'role_id' => 3,
                'created_at' => '2019-02-12 21:50:33',
                'updated_at' => '2019-02-12 21:50:33',
            ),
            23 => 
            array (
                'id' => 26,
                'permission_id' => 56,
                'role_id' => 3,
                'created_at' => '2019-02-12 21:50:33',
                'updated_at' => '2019-02-12 21:50:33',
            ),
            24 => 
            array (
                'id' => 27,
                'permission_id' => 57,
                'role_id' => 3,
                'created_at' => '2019-02-12 21:50:33',
                'updated_at' => '2019-02-12 21:50:33',
            ),
            25 => 
            array (
                'id' => 28,
                'permission_id' => 61,
                'role_id' => 3,
                'created_at' => '2019-02-12 21:50:33',
                'updated_at' => '2019-02-12 21:50:33',
            ),
            26 => 
            array (
                'id' => 29,
                'permission_id' => 62,
                'role_id' => 3,
                'created_at' => '2019-02-12 21:50:33',
                'updated_at' => '2019-02-12 21:50:33',
            ),
            27 => 
            array (
                'id' => 30,
                'permission_id' => 63,
                'role_id' => 3,
                'created_at' => '2019-02-12 21:50:33',
                'updated_at' => '2019-02-12 21:50:33',
            ),
            28 => 
            array (
                'id' => 32,
                'permission_id' => 71,
                'role_id' => 3,
                'created_at' => '2019-02-13 21:45:44',
                'updated_at' => '2019-02-13 21:45:44',
            ),
            29 => 
            array (
                'id' => 33,
                'permission_id' => 1,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            30 => 
            array (
                'id' => 34,
                'permission_id' => 2,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            31 => 
            array (
                'id' => 35,
                'permission_id' => 3,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            32 => 
            array (
                'id' => 36,
                'permission_id' => 4,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            33 => 
            array (
                'id' => 37,
                'permission_id' => 11,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            34 => 
            array (
                'id' => 38,
                'permission_id' => 12,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            35 => 
            array (
                'id' => 39,
                'permission_id' => 13,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            36 => 
            array (
                'id' => 40,
                'permission_id' => 14,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            37 => 
            array (
                'id' => 41,
                'permission_id' => 16,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            38 => 
            array (
                'id' => 42,
                'permission_id' => 17,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            39 => 
            array (
                'id' => 43,
                'permission_id' => 18,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            40 => 
            array (
                'id' => 44,
                'permission_id' => 19,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            41 => 
            array (
                'id' => 45,
                'permission_id' => 21,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            42 => 
            array (
                'id' => 46,
                'permission_id' => 22,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            43 => 
            array (
                'id' => 47,
                'permission_id' => 23,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            44 => 
            array (
                'id' => 48,
                'permission_id' => 24,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            45 => 
            array (
                'id' => 49,
                'permission_id' => 26,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            46 => 
            array (
                'id' => 50,
                'permission_id' => 27,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            47 => 
            array (
                'id' => 51,
                'permission_id' => 28,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            48 => 
            array (
                'id' => 52,
                'permission_id' => 29,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            49 => 
            array (
                'id' => 53,
                'permission_id' => 31,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            50 => 
            array (
                'id' => 54,
                'permission_id' => 32,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            51 => 
            array (
                'id' => 55,
                'permission_id' => 33,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            52 => 
            array (
                'id' => 56,
                'permission_id' => 34,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            53 => 
            array (
                'id' => 57,
                'permission_id' => 36,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            54 => 
            array (
                'id' => 58,
                'permission_id' => 37,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            55 => 
            array (
                'id' => 59,
                'permission_id' => 38,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            56 => 
            array (
                'id' => 60,
                'permission_id' => 39,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            57 => 
            array (
                'id' => 61,
                'permission_id' => 41,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            58 => 
            array (
                'id' => 62,
                'permission_id' => 42,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            59 => 
            array (
                'id' => 63,
                'permission_id' => 43,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            60 => 
            array (
                'id' => 64,
                'permission_id' => 44,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            61 => 
            array (
                'id' => 65,
                'permission_id' => 46,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            62 => 
            array (
                'id' => 66,
                'permission_id' => 47,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            63 => 
            array (
                'id' => 67,
                'permission_id' => 48,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            64 => 
            array (
                'id' => 68,
                'permission_id' => 49,
                'role_id' => 2,
                'created_at' => '2019-02-14 22:23:03',
                'updated_at' => '2019-02-14 22:23:03',
            ),
            65 => 
            array (
                'id' => 69,
                'permission_id' => 82,
                'role_id' => 3,
                'created_at' => '2019-02-28 16:16:52',
                'updated_at' => '2019-02-28 16:16:52',
            ),
            66 => 
            array (
                'id' => 70,
                'permission_id' => 83,
                'role_id' => 3,
                'created_at' => '2019-02-28 16:16:52',
                'updated_at' => '2019-02-28 16:16:52',
            ),
            67 => 
            array (
                'id' => 71,
                'permission_id' => 84,
                'role_id' => 3,
                'created_at' => '2019-02-28 16:16:52',
                'updated_at' => '2019-02-28 16:16:52',
            ),
            68 => 
            array (
                'id' => 72,
                'permission_id' => 85,
                'role_id' => 3,
                'created_at' => '2019-02-28 16:16:52',
                'updated_at' => '2019-02-28 16:16:52',
            ),
            69 => 
            array (
                'id' => 73,
                'permission_id' => 86,
                'role_id' => 3,
                'created_at' => '2019-02-28 16:16:52',
                'updated_at' => '2019-02-28 16:16:52',
            ),
            70 => 
            array (
                'id' => 74,
                'permission_id' => 87,
                'role_id' => 3,
                'created_at' => '2019-03-02 13:02:25',
                'updated_at' => '2019-03-02 13:02:25',
            ),
            71 => 
            array (
                'id' => 75,
                'permission_id' => 82,
                'role_id' => 2,
                'created_at' => '2019-03-02 13:46:37',
                'updated_at' => '2019-03-02 13:46:37',
            ),
            72 => 
            array (
                'id' => 76,
                'permission_id' => 83,
                'role_id' => 2,
                'created_at' => '2019-03-02 13:46:37',
                'updated_at' => '2019-03-02 13:46:37',
            ),
            73 => 
            array (
                'id' => 77,
                'permission_id' => 84,
                'role_id' => 2,
                'created_at' => '2019-03-02 13:46:37',
                'updated_at' => '2019-03-02 13:46:37',
            ),
            74 => 
            array (
                'id' => 78,
                'permission_id' => 85,
                'role_id' => 2,
                'created_at' => '2019-03-02 13:46:37',
                'updated_at' => '2019-03-02 13:46:37',
            ),
            75 => 
            array (
                'id' => 79,
                'permission_id' => 86,
                'role_id' => 2,
                'created_at' => '2019-03-02 13:46:37',
                'updated_at' => '2019-03-02 13:46:37',
            ),
            76 => 
            array (
                'id' => 80,
                'permission_id' => 87,
                'role_id' => 2,
                'created_at' => '2019-03-02 13:46:37',
                'updated_at' => '2019-03-02 13:46:37',
            ),
            77 => 
            array (
                'id' => 81,
                'permission_id' => 88,
                'role_id' => 2,
                'created_at' => '2019-03-02 13:46:37',
                'updated_at' => '2019-03-02 13:46:37',
            ),
            78 => 
            array (
                'id' => 82,
                'permission_id' => 53,
                'role_id' => 3,
                'created_at' => '2019-03-03 19:13:57',
                'updated_at' => '2019-03-03 19:13:57',
            ),
            79 => 
            array (
                'id' => 83,
                'permission_id' => 54,
                'role_id' => 3,
                'created_at' => '2019-03-03 19:13:57',
                'updated_at' => '2019-03-03 19:13:57',
            ),
            80 => 
            array (
                'id' => 84,
                'permission_id' => 58,
                'role_id' => 3,
                'created_at' => '2019-03-03 19:13:57',
                'updated_at' => '2019-03-03 19:13:57',
            ),
            81 => 
            array (
                'id' => 85,
                'permission_id' => 59,
                'role_id' => 3,
                'created_at' => '2019-03-03 19:13:57',
                'updated_at' => '2019-03-03 19:13:57',
            ),
            82 => 
            array (
                'id' => 86,
                'permission_id' => 69,
                'role_id' => 3,
                'created_at' => '2019-03-03 19:13:57',
                'updated_at' => '2019-03-03 19:13:57',
            ),
            83 => 
            array (
                'id' => 87,
                'permission_id' => 50,
                'role_id' => 2,
                'created_at' => '2019-03-03 19:50:48',
                'updated_at' => '2019-03-03 19:50:48',
            ),
            84 => 
            array (
                'id' => 88,
                'permission_id' => 77,
                'role_id' => 2,
                'created_at' => '2019-03-03 19:50:48',
                'updated_at' => '2019-03-03 19:50:48',
            ),
            85 => 
            array (
                'id' => 89,
                'permission_id' => 78,
                'role_id' => 2,
                'created_at' => '2019-03-03 19:50:48',
                'updated_at' => '2019-03-03 19:50:48',
            ),
            86 => 
            array (
                'id' => 90,
                'permission_id' => 79,
                'role_id' => 2,
                'created_at' => '2019-03-03 19:50:48',
                'updated_at' => '2019-03-03 19:50:48',
            ),
            87 => 
            array (
                'id' => 91,
                'permission_id' => 80,
                'role_id' => 2,
                'created_at' => '2019-03-03 19:50:48',
                'updated_at' => '2019-03-03 19:50:48',
            ),
            88 => 
            array (
                'id' => 93,
                'permission_id' => 31,
                'role_id' => 4,
                'created_at' => '2019-03-04 09:05:53',
                'updated_at' => '2019-03-04 09:05:53',
            ),
            89 => 
            array (
                'id' => 94,
                'permission_id' => 32,
                'role_id' => 4,
                'created_at' => '2019-03-04 09:05:53',
                'updated_at' => '2019-03-04 09:05:53',
            ),
            90 => 
            array (
                'id' => 95,
                'permission_id' => 33,
                'role_id' => 4,
                'created_at' => '2019-03-04 09:05:53',
                'updated_at' => '2019-03-04 09:05:53',
            ),
            91 => 
            array (
                'id' => 96,
                'permission_id' => 34,
                'role_id' => 4,
                'created_at' => '2019-03-04 09:05:53',
                'updated_at' => '2019-03-04 09:05:53',
            ),
            92 => 
            array (
                'id' => 97,
                'permission_id' => 35,
                'role_id' => 4,
                'created_at' => '2019-03-04 09:05:53',
                'updated_at' => '2019-03-04 09:05:53',
            ),
            93 => 
            array (
                'id' => 98,
                'permission_id' => 51,
                'role_id' => 4,
                'created_at' => '2019-03-04 09:05:53',
                'updated_at' => '2019-03-04 09:05:53',
            ),
            94 => 
            array (
                'id' => 99,
                'permission_id' => 52,
                'role_id' => 4,
                'created_at' => '2019-03-04 09:05:53',
                'updated_at' => '2019-03-04 09:05:53',
            ),
            95 => 
            array (
                'id' => 100,
                'permission_id' => 53,
                'role_id' => 4,
                'created_at' => '2019-03-04 09:05:53',
                'updated_at' => '2019-03-04 09:05:53',
            ),
            96 => 
            array (
                'id' => 101,
                'permission_id' => 54,
                'role_id' => 4,
                'created_at' => '2019-03-04 09:05:53',
                'updated_at' => '2019-03-04 09:05:53',
            ),
            97 => 
            array (
                'id' => 102,
                'permission_id' => 56,
                'role_id' => 4,
                'created_at' => '2019-03-04 09:05:53',
                'updated_at' => '2019-03-04 09:05:53',
            ),
            98 => 
            array (
                'id' => 103,
                'permission_id' => 57,
                'role_id' => 4,
                'created_at' => '2019-03-04 09:05:53',
                'updated_at' => '2019-03-04 09:05:53',
            ),
            99 => 
            array (
                'id' => 104,
                'permission_id' => 58,
                'role_id' => 4,
                'created_at' => '2019-03-04 09:05:53',
                'updated_at' => '2019-03-04 09:05:53',
            ),
            100 => 
            array (
                'id' => 105,
                'permission_id' => 59,
                'role_id' => 4,
                'created_at' => '2019-03-04 09:05:53',
                'updated_at' => '2019-03-04 09:05:53',
            ),
            101 => 
            array (
                'id' => 106,
                'permission_id' => 61,
                'role_id' => 4,
                'created_at' => '2019-03-04 09:05:53',
                'updated_at' => '2019-03-04 09:05:53',
            ),
            102 => 
            array (
                'id' => 107,
                'permission_id' => 62,
                'role_id' => 4,
                'created_at' => '2019-03-04 09:05:53',
                'updated_at' => '2019-03-04 09:05:53',
            ),
            103 => 
            array (
                'id' => 108,
                'permission_id' => 63,
                'role_id' => 4,
                'created_at' => '2019-03-04 09:05:53',
                'updated_at' => '2019-03-04 09:05:53',
            ),
            104 => 
            array (
                'id' => 109,
                'permission_id' => 68,
                'role_id' => 4,
                'created_at' => '2019-03-04 09:05:53',
                'updated_at' => '2019-03-04 09:05:53',
            ),
            105 => 
            array (
                'id' => 110,
                'permission_id' => 69,
                'role_id' => 4,
                'created_at' => '2019-03-04 09:05:53',
                'updated_at' => '2019-03-04 09:05:53',
            ),
            106 => 
            array (
                'id' => 111,
                'permission_id' => 82,
                'role_id' => 4,
                'created_at' => '2019-03-04 09:05:53',
                'updated_at' => '2019-03-04 09:05:53',
            ),
            107 => 
            array (
                'id' => 112,
                'permission_id' => 83,
                'role_id' => 4,
                'created_at' => '2019-03-04 09:05:53',
                'updated_at' => '2019-03-04 09:05:53',
            ),
            108 => 
            array (
                'id' => 113,
                'permission_id' => 84,
                'role_id' => 4,
                'created_at' => '2019-03-04 09:05:53',
                'updated_at' => '2019-03-04 09:05:53',
            ),
            109 => 
            array (
                'id' => 114,
                'permission_id' => 85,
                'role_id' => 4,
                'created_at' => '2019-03-04 09:05:53',
                'updated_at' => '2019-03-04 09:05:53',
            ),
            110 => 
            array (
                'id' => 115,
                'permission_id' => 86,
                'role_id' => 4,
                'created_at' => '2019-03-04 09:05:53',
                'updated_at' => '2019-03-04 09:05:53',
            ),
            111 => 
            array (
                'id' => 116,
                'permission_id' => 87,
                'role_id' => 4,
                'created_at' => '2019-03-04 09:05:53',
                'updated_at' => '2019-03-04 09:05:53',
            ),
            112 => 
            array (
                'id' => 117,
                'permission_id' => 88,
                'role_id' => 4,
                'created_at' => '2019-03-04 09:05:53',
                'updated_at' => '2019-03-04 09:05:53',
            ),
        ));
        
        
    }
}