<?php

use Illuminate\Database\Seeder;

class DenunciaAgresorTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('denuncia_agresor')->delete();
        
        \DB::table('denuncia_agresor')->insert(array (
            0 => 
            array (
                'id' => 1,
                'denuncia_id' => 1,
                'agresor_id' => 1,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:07',
                'updated_at' => '2019-01-29 22:26:07',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'denuncia_id' => 2,
                'agresor_id' => 2,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:07',
                'updated_at' => '2019-01-29 22:26:07',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'denuncia_id' => 3,
                'agresor_id' => 3,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:07',
                'updated_at' => '2019-01-29 22:26:07',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'denuncia_id' => 3,
                'agresor_id' => 4,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:07',
                'updated_at' => '2019-01-29 22:26:07',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'denuncia_id' => 4,
                'agresor_id' => 5,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:07',
                'updated_at' => '2019-01-29 22:26:07',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'denuncia_id' => 5,
                'agresor_id' => 6,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:07',
                'updated_at' => '2019-01-29 22:26:07',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'denuncia_id' => 6,
                'agresor_id' => 7,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:07',
                'updated_at' => '2019-01-29 22:26:07',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'denuncia_id' => 6,
                'agresor_id' => 8,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:07',
                'updated_at' => '2019-01-29 22:26:07',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'denuncia_id' => 7,
                'agresor_id' => 9,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:07',
                'updated_at' => '2019-01-29 22:26:07',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'denuncia_id' => 8,
                'agresor_id' => 10,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:08',
                'updated_at' => '2019-01-29 22:26:08',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'denuncia_id' => 8,
                'agresor_id' => 11,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:08',
                'updated_at' => '2019-01-29 22:26:08',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'denuncia_id' => 9,
                'agresor_id' => 12,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:08',
                'updated_at' => '2019-01-29 22:26:08',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'denuncia_id' => 10,
                'agresor_id' => 13,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:08',
                'updated_at' => '2019-01-29 22:26:08',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'denuncia_id' => 10,
                'agresor_id' => 14,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:08',
                'updated_at' => '2019-01-29 22:26:08',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'denuncia_id' => 11,
                'agresor_id' => 15,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:08',
                'updated_at' => '2019-01-29 22:26:08',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'denuncia_id' => 12,
                'agresor_id' => 16,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:08',
                'updated_at' => '2019-01-29 22:26:08',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'denuncia_id' => 13,
                'agresor_id' => 17,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:08',
                'updated_at' => '2019-01-29 22:26:08',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'denuncia_id' => 13,
                'agresor_id' => 18,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:08',
                'updated_at' => '2019-01-29 22:26:08',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'denuncia_id' => 13,
                'agresor_id' => 19,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:08',
                'updated_at' => '2019-01-29 22:26:08',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'denuncia_id' => 14,
                'agresor_id' => 20,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:08',
                'updated_at' => '2019-01-29 22:26:08',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'denuncia_id' => 15,
                'agresor_id' => 21,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:09',
                'updated_at' => '2019-01-29 22:26:09',
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'denuncia_id' => 15,
                'agresor_id' => 22,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:09',
                'updated_at' => '2019-01-29 22:26:09',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'denuncia_id' => 15,
                'agresor_id' => 23,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:09',
                'updated_at' => '2019-01-29 22:26:09',
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'denuncia_id' => 16,
                'agresor_id' => 24,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:09',
                'updated_at' => '2019-01-29 22:26:09',
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'denuncia_id' => 17,
                'agresor_id' => 25,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:09',
                'updated_at' => '2019-01-29 22:26:09',
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'denuncia_id' => 18,
                'agresor_id' => 26,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:09',
                'updated_at' => '2019-01-29 22:26:09',
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'denuncia_id' => 19,
                'agresor_id' => 27,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:09',
                'updated_at' => '2019-01-29 22:26:09',
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'denuncia_id' => 19,
                'agresor_id' => 28,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:09',
                'updated_at' => '2019-01-29 22:26:09',
                'deleted_at' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'denuncia_id' => 20,
                'agresor_id' => 29,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:09',
                'updated_at' => '2019-01-29 22:26:09',
                'deleted_at' => NULL,
            ),
            29 => 
            array (
                'id' => 30,
                'denuncia_id' => 22,
                'agresor_id' => 30,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:10',
                'updated_at' => '2019-01-29 22:26:10',
                'deleted_at' => NULL,
            ),
            30 => 
            array (
                'id' => 31,
                'denuncia_id' => 23,
                'agresor_id' => 31,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:10',
                'updated_at' => '2019-01-29 22:26:10',
                'deleted_at' => NULL,
            ),
            31 => 
            array (
                'id' => 32,
                'denuncia_id' => 24,
                'agresor_id' => 32,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:10',
                'updated_at' => '2019-01-29 22:26:10',
                'deleted_at' => NULL,
            ),
            32 => 
            array (
                'id' => 33,
                'denuncia_id' => 25,
                'agresor_id' => 33,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:10',
                'updated_at' => '2019-01-29 22:26:10',
                'deleted_at' => NULL,
            ),
            33 => 
            array (
                'id' => 34,
                'denuncia_id' => 26,
                'agresor_id' => 34,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:10',
                'updated_at' => '2019-01-29 22:26:10',
                'deleted_at' => NULL,
            ),
            34 => 
            array (
                'id' => 35,
                'denuncia_id' => 27,
                'agresor_id' => 35,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:11',
                'updated_at' => '2019-01-29 22:26:11',
                'deleted_at' => NULL,
            ),
            35 => 
            array (
                'id' => 36,
                'denuncia_id' => 28,
                'agresor_id' => 36,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:11',
                'updated_at' => '2019-01-29 22:26:11',
                'deleted_at' => NULL,
            ),
            36 => 
            array (
                'id' => 37,
                'denuncia_id' => 29,
                'agresor_id' => 37,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:11',
                'updated_at' => '2019-01-29 22:26:11',
                'deleted_at' => NULL,
            ),
            37 => 
            array (
                'id' => 38,
                'denuncia_id' => 30,
                'agresor_id' => 38,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:11',
                'updated_at' => '2019-01-29 22:26:11',
                'deleted_at' => NULL,
            ),
            38 => 
            array (
                'id' => 39,
                'denuncia_id' => 30,
                'agresor_id' => 39,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:11',
                'updated_at' => '2019-01-29 22:26:11',
                'deleted_at' => NULL,
            ),
            39 => 
            array (
                'id' => 40,
                'denuncia_id' => 31,
                'agresor_id' => 40,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:11',
                'updated_at' => '2019-01-29 22:26:11',
                'deleted_at' => NULL,
            ),
            40 => 
            array (
                'id' => 41,
                'denuncia_id' => 32,
                'agresor_id' => 41,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:11',
                'updated_at' => '2019-01-29 22:26:11',
                'deleted_at' => NULL,
            ),
            41 => 
            array (
                'id' => 42,
                'denuncia_id' => 33,
                'agresor_id' => 42,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:12',
                'updated_at' => '2019-01-29 22:26:12',
                'deleted_at' => NULL,
            ),
            42 => 
            array (
                'id' => 43,
                'denuncia_id' => 34,
                'agresor_id' => 43,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:12',
                'updated_at' => '2019-01-29 22:26:12',
                'deleted_at' => NULL,
            ),
            43 => 
            array (
                'id' => 44,
                'denuncia_id' => 35,
                'agresor_id' => 44,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:12',
                'updated_at' => '2019-01-29 22:26:12',
                'deleted_at' => NULL,
            ),
            44 => 
            array (
                'id' => 45,
                'denuncia_id' => 36,
                'agresor_id' => 45,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:12',
                'updated_at' => '2019-01-29 22:26:12',
                'deleted_at' => NULL,
            ),
            45 => 
            array (
                'id' => 46,
                'denuncia_id' => 37,
                'agresor_id' => 46,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:12',
                'updated_at' => '2019-01-29 22:26:12',
                'deleted_at' => NULL,
            ),
            46 => 
            array (
                'id' => 47,
                'denuncia_id' => 38,
                'agresor_id' => 47,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:12',
                'updated_at' => '2019-01-29 22:26:12',
                'deleted_at' => NULL,
            ),
            47 => 
            array (
                'id' => 48,
                'denuncia_id' => 39,
                'agresor_id' => 48,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:12',
                'updated_at' => '2019-01-29 22:26:12',
                'deleted_at' => NULL,
            ),
            48 => 
            array (
                'id' => 49,
                'denuncia_id' => 40,
                'agresor_id' => 49,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:13',
                'updated_at' => '2019-01-29 22:26:13',
                'deleted_at' => NULL,
            ),
            49 => 
            array (
                'id' => 50,
                'denuncia_id' => 40,
                'agresor_id' => 50,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:13',
                'updated_at' => '2019-01-29 22:26:13',
                'deleted_at' => NULL,
            ),
            50 => 
            array (
                'id' => 51,
                'denuncia_id' => 41,
                'agresor_id' => 51,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:13',
                'updated_at' => '2019-01-29 22:26:13',
                'deleted_at' => NULL,
            ),
            51 => 
            array (
                'id' => 52,
                'denuncia_id' => 42,
                'agresor_id' => 52,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:13',
                'updated_at' => '2019-01-29 22:26:13',
                'deleted_at' => NULL,
            ),
            52 => 
            array (
                'id' => 53,
                'denuncia_id' => 43,
                'agresor_id' => 53,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:13',
                'updated_at' => '2019-01-29 22:26:13',
                'deleted_at' => NULL,
            ),
            53 => 
            array (
                'id' => 54,
                'denuncia_id' => 44,
                'agresor_id' => 54,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:13',
                'updated_at' => '2019-01-29 22:26:13',
                'deleted_at' => NULL,
            ),
            54 => 
            array (
                'id' => 55,
                'denuncia_id' => 45,
                'agresor_id' => 55,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:13',
                'updated_at' => '2019-01-29 22:26:13',
                'deleted_at' => NULL,
            ),
            55 => 
            array (
                'id' => 56,
                'denuncia_id' => 46,
                'agresor_id' => 56,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:14',
                'updated_at' => '2019-01-29 22:26:14',
                'deleted_at' => NULL,
            ),
            56 => 
            array (
                'id' => 57,
                'denuncia_id' => 46,
                'agresor_id' => 57,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:14',
                'updated_at' => '2019-01-29 22:26:14',
                'deleted_at' => NULL,
            ),
            57 => 
            array (
                'id' => 58,
                'denuncia_id' => 47,
                'agresor_id' => 58,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:14',
                'updated_at' => '2019-01-29 22:26:14',
                'deleted_at' => NULL,
            ),
            58 => 
            array (
                'id' => 59,
                'denuncia_id' => 48,
                'agresor_id' => 59,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:14',
                'updated_at' => '2019-01-29 22:26:14',
                'deleted_at' => NULL,
            ),
            59 => 
            array (
                'id' => 60,
                'denuncia_id' => 49,
                'agresor_id' => 60,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:14',
                'updated_at' => '2019-01-29 22:26:14',
                'deleted_at' => NULL,
            ),
            60 => 
            array (
                'id' => 61,
                'denuncia_id' => 50,
                'agresor_id' => 61,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:14',
                'updated_at' => '2019-01-29 22:26:14',
                'deleted_at' => NULL,
            ),
            61 => 
            array (
                'id' => 62,
                'denuncia_id' => 50,
                'agresor_id' => 62,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:14',
                'updated_at' => '2019-01-29 22:26:14',
                'deleted_at' => NULL,
            ),
            62 => 
            array (
                'id' => 63,
                'denuncia_id' => 50,
                'agresor_id' => 63,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:14',
                'updated_at' => '2019-01-29 22:26:14',
                'deleted_at' => NULL,
            ),
            63 => 
            array (
                'id' => 64,
                'denuncia_id' => 51,
                'agresor_id' => 64,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:14',
                'updated_at' => '2019-01-29 22:26:14',
                'deleted_at' => NULL,
            ),
            64 => 
            array (
                'id' => 65,
                'denuncia_id' => 52,
                'agresor_id' => 65,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:14',
                'updated_at' => '2019-01-29 22:26:14',
                'deleted_at' => NULL,
            ),
            65 => 
            array (
                'id' => 66,
                'denuncia_id' => 53,
                'agresor_id' => 66,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:15',
                'updated_at' => '2019-01-29 22:26:15',
                'deleted_at' => NULL,
            ),
            66 => 
            array (
                'id' => 67,
                'denuncia_id' => 53,
                'agresor_id' => 67,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:15',
                'updated_at' => '2019-01-29 22:26:15',
                'deleted_at' => NULL,
            ),
            67 => 
            array (
                'id' => 68,
                'denuncia_id' => 53,
                'agresor_id' => 68,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:15',
                'updated_at' => '2019-01-29 22:26:15',
                'deleted_at' => NULL,
            ),
            68 => 
            array (
                'id' => 69,
                'denuncia_id' => 54,
                'agresor_id' => 69,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:15',
                'updated_at' => '2019-01-29 22:26:15',
                'deleted_at' => NULL,
            ),
            69 => 
            array (
                'id' => 70,
                'denuncia_id' => 55,
                'agresor_id' => 70,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:15',
                'updated_at' => '2019-01-29 22:26:15',
                'deleted_at' => NULL,
            ),
            70 => 
            array (
                'id' => 71,
                'denuncia_id' => 56,
                'agresor_id' => 71,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:15',
                'updated_at' => '2019-01-29 22:26:15',
                'deleted_at' => NULL,
            ),
            71 => 
            array (
                'id' => 72,
                'denuncia_id' => 57,
                'agresor_id' => 72,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:15',
                'updated_at' => '2019-01-29 22:26:15',
                'deleted_at' => NULL,
            ),
            72 => 
            array (
                'id' => 73,
                'denuncia_id' => 57,
                'agresor_id' => 73,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:15',
                'updated_at' => '2019-01-29 22:26:15',
                'deleted_at' => NULL,
            ),
            73 => 
            array (
                'id' => 74,
                'denuncia_id' => 58,
                'agresor_id' => 74,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:15',
                'updated_at' => '2019-01-29 22:26:15',
                'deleted_at' => NULL,
            ),
            74 => 
            array (
                'id' => 75,
                'denuncia_id' => 59,
                'agresor_id' => 75,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:15',
                'updated_at' => '2019-01-29 22:26:15',
                'deleted_at' => NULL,
            ),
            75 => 
            array (
                'id' => 76,
                'denuncia_id' => 60,
                'agresor_id' => 76,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:16',
                'updated_at' => '2019-01-29 22:26:16',
                'deleted_at' => NULL,
            ),
            76 => 
            array (
                'id' => 77,
                'denuncia_id' => 61,
                'agresor_id' => 77,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:16',
                'updated_at' => '2019-01-29 22:26:16',
                'deleted_at' => NULL,
            ),
            77 => 
            array (
                'id' => 78,
                'denuncia_id' => 62,
                'agresor_id' => 78,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:16',
                'updated_at' => '2019-01-29 22:26:16',
                'deleted_at' => NULL,
            ),
            78 => 
            array (
                'id' => 79,
                'denuncia_id' => 63,
                'agresor_id' => 79,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:16',
                'updated_at' => '2019-01-29 22:26:16',
                'deleted_at' => NULL,
            ),
            79 => 
            array (
                'id' => 80,
                'denuncia_id' => 64,
                'agresor_id' => 80,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:16',
                'updated_at' => '2019-01-29 22:26:16',
                'deleted_at' => NULL,
            ),
            80 => 
            array (
                'id' => 81,
                'denuncia_id' => 64,
                'agresor_id' => 81,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:16',
                'updated_at' => '2019-01-29 22:26:16',
                'deleted_at' => NULL,
            ),
            81 => 
            array (
                'id' => 82,
                'denuncia_id' => 65,
                'agresor_id' => 82,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:16',
                'updated_at' => '2019-01-29 22:26:16',
                'deleted_at' => NULL,
            ),
            82 => 
            array (
                'id' => 83,
                'denuncia_id' => 66,
                'agresor_id' => 83,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:16',
                'updated_at' => '2019-01-29 22:26:16',
                'deleted_at' => NULL,
            ),
            83 => 
            array (
                'id' => 84,
                'denuncia_id' => 67,
                'agresor_id' => 84,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:16',
                'updated_at' => '2019-01-29 22:26:16',
                'deleted_at' => NULL,
            ),
            84 => 
            array (
                'id' => 85,
                'denuncia_id' => 67,
                'agresor_id' => 85,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:17',
                'updated_at' => '2019-01-29 22:26:17',
                'deleted_at' => NULL,
            ),
            85 => 
            array (
                'id' => 86,
                'denuncia_id' => 68,
                'agresor_id' => 86,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:17',
                'updated_at' => '2019-01-29 22:26:17',
                'deleted_at' => NULL,
            ),
            86 => 
            array (
                'id' => 87,
                'denuncia_id' => 69,
                'agresor_id' => 87,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:17',
                'updated_at' => '2019-01-29 22:26:17',
                'deleted_at' => NULL,
            ),
            87 => 
            array (
                'id' => 88,
                'denuncia_id' => 70,
                'agresor_id' => 88,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:17',
                'updated_at' => '2019-01-29 22:26:17',
                'deleted_at' => NULL,
            ),
            88 => 
            array (
                'id' => 89,
                'denuncia_id' => 71,
                'agresor_id' => 89,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:17',
                'updated_at' => '2019-01-29 22:26:17',
                'deleted_at' => NULL,
            ),
            89 => 
            array (
                'id' => 90,
                'denuncia_id' => 72,
                'agresor_id' => 90,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:17',
                'updated_at' => '2019-01-29 22:26:17',
                'deleted_at' => NULL,
            ),
            90 => 
            array (
                'id' => 91,
                'denuncia_id' => 73,
                'agresor_id' => 91,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:17',
                'updated_at' => '2019-01-29 22:26:17',
                'deleted_at' => NULL,
            ),
            91 => 
            array (
                'id' => 92,
                'denuncia_id' => 74,
                'agresor_id' => 92,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:18',
                'updated_at' => '2019-01-29 22:26:18',
                'deleted_at' => NULL,
            ),
            92 => 
            array (
                'id' => 93,
                'denuncia_id' => 75,
                'agresor_id' => 93,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:18',
                'updated_at' => '2019-01-29 22:26:18',
                'deleted_at' => NULL,
            ),
            93 => 
            array (
                'id' => 94,
                'denuncia_id' => 76,
                'agresor_id' => 94,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:18',
                'updated_at' => '2019-01-29 22:26:18',
                'deleted_at' => NULL,
            ),
            94 => 
            array (
                'id' => 95,
                'denuncia_id' => 77,
                'agresor_id' => 95,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:18',
                'updated_at' => '2019-01-29 22:26:18',
                'deleted_at' => NULL,
            ),
            95 => 
            array (
                'id' => 96,
                'denuncia_id' => 78,
                'agresor_id' => 96,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:18',
                'updated_at' => '2019-01-29 22:26:18',
                'deleted_at' => NULL,
            ),
            96 => 
            array (
                'id' => 97,
                'denuncia_id' => 79,
                'agresor_id' => 97,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:18',
                'updated_at' => '2019-01-29 22:26:18',
                'deleted_at' => NULL,
            ),
            97 => 
            array (
                'id' => 98,
                'denuncia_id' => 80,
                'agresor_id' => 98,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:18',
                'updated_at' => '2019-01-29 22:26:18',
                'deleted_at' => NULL,
            ),
            98 => 
            array (
                'id' => 99,
                'denuncia_id' => 81,
                'agresor_id' => 99,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:19',
                'updated_at' => '2019-01-29 22:26:19',
                'deleted_at' => NULL,
            ),
            99 => 
            array (
                'id' => 100,
                'denuncia_id' => 82,
                'agresor_id' => 100,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:19',
                'updated_at' => '2019-01-29 22:26:19',
                'deleted_at' => NULL,
            ),
            100 => 
            array (
                'id' => 101,
                'denuncia_id' => 83,
                'agresor_id' => 101,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:19',
                'updated_at' => '2019-01-29 22:26:19',
                'deleted_at' => NULL,
            ),
            101 => 
            array (
                'id' => 102,
                'denuncia_id' => 84,
                'agresor_id' => 102,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:19',
                'updated_at' => '2019-01-29 22:26:19',
                'deleted_at' => NULL,
            ),
            102 => 
            array (
                'id' => 103,
                'denuncia_id' => 85,
                'agresor_id' => 103,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:19',
                'updated_at' => '2019-01-29 22:26:19',
                'deleted_at' => NULL,
            ),
            103 => 
            array (
                'id' => 104,
                'denuncia_id' => 86,
                'agresor_id' => 104,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:19',
                'updated_at' => '2019-01-29 22:26:19',
                'deleted_at' => NULL,
            ),
            104 => 
            array (
                'id' => 105,
                'denuncia_id' => 87,
                'agresor_id' => 105,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:20',
                'updated_at' => '2019-01-29 22:26:20',
                'deleted_at' => NULL,
            ),
            105 => 
            array (
                'id' => 106,
                'denuncia_id' => 88,
                'agresor_id' => 106,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:20',
                'updated_at' => '2019-01-29 22:26:20',
                'deleted_at' => NULL,
            ),
            106 => 
            array (
                'id' => 107,
                'denuncia_id' => 88,
                'agresor_id' => 107,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:20',
                'updated_at' => '2019-01-29 22:26:20',
                'deleted_at' => NULL,
            ),
            107 => 
            array (
                'id' => 108,
                'denuncia_id' => 88,
                'agresor_id' => 108,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:20',
                'updated_at' => '2019-01-29 22:26:20',
                'deleted_at' => NULL,
            ),
            108 => 
            array (
                'id' => 109,
                'denuncia_id' => 89,
                'agresor_id' => 109,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:20',
                'updated_at' => '2019-01-29 22:26:20',
                'deleted_at' => NULL,
            ),
            109 => 
            array (
                'id' => 110,
                'denuncia_id' => 90,
                'agresor_id' => 110,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:20',
                'updated_at' => '2019-01-29 22:26:20',
                'deleted_at' => NULL,
            ),
            110 => 
            array (
                'id' => 111,
                'denuncia_id' => 91,
                'agresor_id' => 111,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:20',
                'updated_at' => '2019-01-29 22:26:20',
                'deleted_at' => NULL,
            ),
            111 => 
            array (
                'id' => 112,
                'denuncia_id' => 92,
                'agresor_id' => 112,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:20',
                'updated_at' => '2019-01-29 22:26:20',
                'deleted_at' => NULL,
            ),
            112 => 
            array (
                'id' => 113,
                'denuncia_id' => 93,
                'agresor_id' => 113,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:21',
                'updated_at' => '2019-01-29 22:26:21',
                'deleted_at' => NULL,
            ),
            113 => 
            array (
                'id' => 114,
                'denuncia_id' => 94,
                'agresor_id' => 114,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:21',
                'updated_at' => '2019-01-29 22:26:21',
                'deleted_at' => NULL,
            ),
            114 => 
            array (
                'id' => 115,
                'denuncia_id' => 95,
                'agresor_id' => 115,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:21',
                'updated_at' => '2019-01-29 22:26:21',
                'deleted_at' => NULL,
            ),
            115 => 
            array (
                'id' => 116,
                'denuncia_id' => 96,
                'agresor_id' => 116,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:21',
                'updated_at' => '2019-01-29 22:26:21',
                'deleted_at' => NULL,
            ),
            116 => 
            array (
                'id' => 117,
                'denuncia_id' => 97,
                'agresor_id' => 117,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:21',
                'updated_at' => '2019-01-29 22:26:21',
                'deleted_at' => NULL,
            ),
            117 => 
            array (
                'id' => 118,
                'denuncia_id' => 98,
                'agresor_id' => 118,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:21',
                'updated_at' => '2019-01-29 22:26:21',
                'deleted_at' => NULL,
            ),
            118 => 
            array (
                'id' => 119,
                'denuncia_id' => 98,
                'agresor_id' => 119,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:21',
                'updated_at' => '2019-01-29 22:26:21',
                'deleted_at' => NULL,
            ),
            119 => 
            array (
                'id' => 120,
                'denuncia_id' => 99,
                'agresor_id' => 120,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:22',
                'updated_at' => '2019-01-29 22:26:22',
                'deleted_at' => NULL,
            ),
            120 => 
            array (
                'id' => 121,
                'denuncia_id' => 100,
                'agresor_id' => 121,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:22',
                'updated_at' => '2019-01-29 22:26:22',
                'deleted_at' => NULL,
            ),
            121 => 
            array (
                'id' => 122,
                'denuncia_id' => 101,
                'agresor_id' => 122,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:22',
                'updated_at' => '2019-01-29 22:26:22',
                'deleted_at' => NULL,
            ),
            122 => 
            array (
                'id' => 123,
                'denuncia_id' => 102,
                'agresor_id' => 123,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:22',
                'updated_at' => '2019-01-29 22:26:22',
                'deleted_at' => NULL,
            ),
            123 => 
            array (
                'id' => 124,
                'denuncia_id' => 103,
                'agresor_id' => 124,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:23',
                'updated_at' => '2019-01-29 22:26:23',
                'deleted_at' => NULL,
            ),
            124 => 
            array (
                'id' => 125,
                'denuncia_id' => 104,
                'agresor_id' => 125,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:23',
                'updated_at' => '2019-01-29 22:26:23',
                'deleted_at' => NULL,
            ),
            125 => 
            array (
                'id' => 126,
                'denuncia_id' => 105,
                'agresor_id' => 126,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:23',
                'updated_at' => '2019-01-29 22:26:23',
                'deleted_at' => NULL,
            ),
            126 => 
            array (
                'id' => 127,
                'denuncia_id' => 105,
                'agresor_id' => 127,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:23',
                'updated_at' => '2019-01-29 22:26:23',
                'deleted_at' => NULL,
            ),
            127 => 
            array (
                'id' => 128,
                'denuncia_id' => 106,
                'agresor_id' => 128,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:23',
                'updated_at' => '2019-01-29 22:26:23',
                'deleted_at' => NULL,
            ),
            128 => 
            array (
                'id' => 129,
                'denuncia_id' => 107,
                'agresor_id' => 129,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:23',
                'updated_at' => '2019-01-29 22:26:23',
                'deleted_at' => NULL,
            ),
            129 => 
            array (
                'id' => 130,
                'denuncia_id' => 108,
                'agresor_id' => 130,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:23',
                'updated_at' => '2019-01-29 22:26:23',
                'deleted_at' => NULL,
            ),
            130 => 
            array (
                'id' => 131,
                'denuncia_id' => 109,
                'agresor_id' => 131,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:24',
                'updated_at' => '2019-01-29 22:26:24',
                'deleted_at' => NULL,
            ),
            131 => 
            array (
                'id' => 132,
                'denuncia_id' => 110,
                'agresor_id' => 132,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:24',
                'updated_at' => '2019-01-29 22:26:24',
                'deleted_at' => NULL,
            ),
            132 => 
            array (
                'id' => 133,
                'denuncia_id' => 111,
                'agresor_id' => 133,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:24',
                'updated_at' => '2019-01-29 22:26:24',
                'deleted_at' => NULL,
            ),
            133 => 
            array (
                'id' => 134,
                'denuncia_id' => 111,
                'agresor_id' => 134,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:24',
                'updated_at' => '2019-01-29 22:26:24',
                'deleted_at' => NULL,
            ),
            134 => 
            array (
                'id' => 135,
                'denuncia_id' => 112,
                'agresor_id' => 135,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:24',
                'updated_at' => '2019-01-29 22:26:24',
                'deleted_at' => NULL,
            ),
            135 => 
            array (
                'id' => 136,
                'denuncia_id' => 113,
                'agresor_id' => 136,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:24',
                'updated_at' => '2019-01-29 22:26:24',
                'deleted_at' => NULL,
            ),
            136 => 
            array (
                'id' => 137,
                'denuncia_id' => 114,
                'agresor_id' => 137,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:25',
                'updated_at' => '2019-01-29 22:26:25',
                'deleted_at' => NULL,
            ),
            137 => 
            array (
                'id' => 138,
                'denuncia_id' => 115,
                'agresor_id' => 138,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:25',
                'updated_at' => '2019-01-29 22:26:25',
                'deleted_at' => NULL,
            ),
            138 => 
            array (
                'id' => 139,
                'denuncia_id' => 116,
                'agresor_id' => 139,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:25',
                'updated_at' => '2019-01-29 22:26:25',
                'deleted_at' => NULL,
            ),
            139 => 
            array (
                'id' => 140,
                'denuncia_id' => 117,
                'agresor_id' => 140,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:25',
                'updated_at' => '2019-01-29 22:26:25',
                'deleted_at' => NULL,
            ),
            140 => 
            array (
                'id' => 141,
                'denuncia_id' => 118,
                'agresor_id' => 141,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:25',
                'updated_at' => '2019-01-29 22:26:25',
                'deleted_at' => NULL,
            ),
            141 => 
            array (
                'id' => 142,
                'denuncia_id' => 119,
                'agresor_id' => 142,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:25',
                'updated_at' => '2019-01-29 22:26:25',
                'deleted_at' => NULL,
            ),
            142 => 
            array (
                'id' => 143,
                'denuncia_id' => 120,
                'agresor_id' => 143,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:26',
                'updated_at' => '2019-01-29 22:26:26',
                'deleted_at' => NULL,
            ),
            143 => 
            array (
                'id' => 144,
                'denuncia_id' => 121,
                'agresor_id' => 144,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:26',
                'updated_at' => '2019-01-29 22:26:26',
                'deleted_at' => NULL,
            ),
            144 => 
            array (
                'id' => 145,
                'denuncia_id' => 122,
                'agresor_id' => 145,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:26',
                'updated_at' => '2019-01-29 22:26:26',
                'deleted_at' => NULL,
            ),
            145 => 
            array (
                'id' => 146,
                'denuncia_id' => 123,
                'agresor_id' => 146,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:26',
                'updated_at' => '2019-01-29 22:26:26',
                'deleted_at' => NULL,
            ),
            146 => 
            array (
                'id' => 147,
                'denuncia_id' => 123,
                'agresor_id' => 147,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:26',
                'updated_at' => '2019-01-29 22:26:26',
                'deleted_at' => NULL,
            ),
            147 => 
            array (
                'id' => 148,
                'denuncia_id' => 124,
                'agresor_id' => 148,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:26',
                'updated_at' => '2019-01-29 22:26:26',
                'deleted_at' => NULL,
            ),
            148 => 
            array (
                'id' => 149,
                'denuncia_id' => 125,
                'agresor_id' => 149,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:26',
                'updated_at' => '2019-01-29 22:26:26',
                'deleted_at' => NULL,
            ),
            149 => 
            array (
                'id' => 150,
                'denuncia_id' => 126,
                'agresor_id' => 150,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:27',
                'updated_at' => '2019-01-29 22:26:27',
                'deleted_at' => NULL,
            ),
            150 => 
            array (
                'id' => 151,
                'denuncia_id' => 126,
                'agresor_id' => 151,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:27',
                'updated_at' => '2019-01-29 22:26:27',
                'deleted_at' => NULL,
            ),
            151 => 
            array (
                'id' => 152,
                'denuncia_id' => 126,
                'agresor_id' => 152,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:27',
                'updated_at' => '2019-01-29 22:26:27',
                'deleted_at' => NULL,
            ),
            152 => 
            array (
                'id' => 153,
                'denuncia_id' => 127,
                'agresor_id' => 153,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:27',
                'updated_at' => '2019-01-29 22:26:27',
                'deleted_at' => NULL,
            ),
            153 => 
            array (
                'id' => 154,
                'denuncia_id' => 128,
                'agresor_id' => 154,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:27',
                'updated_at' => '2019-01-29 22:26:27',
                'deleted_at' => NULL,
            ),
            154 => 
            array (
                'id' => 155,
                'denuncia_id' => 129,
                'agresor_id' => 155,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:27',
                'updated_at' => '2019-01-29 22:26:27',
                'deleted_at' => NULL,
            ),
            155 => 
            array (
                'id' => 156,
                'denuncia_id' => 129,
                'agresor_id' => 156,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:27',
                'updated_at' => '2019-01-29 22:26:27',
                'deleted_at' => NULL,
            ),
            156 => 
            array (
                'id' => 157,
                'denuncia_id' => 129,
                'agresor_id' => 157,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:27',
                'updated_at' => '2019-01-29 22:26:27',
                'deleted_at' => NULL,
            ),
            157 => 
            array (
                'id' => 158,
                'denuncia_id' => 130,
                'agresor_id' => 158,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:27',
                'updated_at' => '2019-01-29 22:26:27',
                'deleted_at' => NULL,
            ),
            158 => 
            array (
                'id' => 159,
                'denuncia_id' => 131,
                'agresor_id' => 159,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:28',
                'updated_at' => '2019-01-29 22:26:28',
                'deleted_at' => NULL,
            ),
            159 => 
            array (
                'id' => 160,
                'denuncia_id' => 132,
                'agresor_id' => 160,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:28',
                'updated_at' => '2019-01-29 22:26:28',
                'deleted_at' => NULL,
            ),
            160 => 
            array (
                'id' => 161,
                'denuncia_id' => 133,
                'agresor_id' => 161,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:28',
                'updated_at' => '2019-01-29 22:26:28',
                'deleted_at' => NULL,
            ),
            161 => 
            array (
                'id' => 162,
                'denuncia_id' => 134,
                'agresor_id' => 162,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:28',
                'updated_at' => '2019-01-29 22:26:28',
                'deleted_at' => NULL,
            ),
            162 => 
            array (
                'id' => 163,
                'denuncia_id' => 135,
                'agresor_id' => 163,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:28',
                'updated_at' => '2019-01-29 22:26:28',
                'deleted_at' => NULL,
            ),
            163 => 
            array (
                'id' => 164,
                'denuncia_id' => 136,
                'agresor_id' => 164,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:28',
                'updated_at' => '2019-01-29 22:26:28',
                'deleted_at' => NULL,
            ),
            164 => 
            array (
                'id' => 165,
                'denuncia_id' => 137,
                'agresor_id' => 165,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:29',
                'updated_at' => '2019-01-29 22:26:29',
                'deleted_at' => NULL,
            ),
            165 => 
            array (
                'id' => 166,
                'denuncia_id' => 138,
                'agresor_id' => 166,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:29',
                'updated_at' => '2019-01-29 22:26:29',
                'deleted_at' => NULL,
            ),
            166 => 
            array (
                'id' => 167,
                'denuncia_id' => 139,
                'agresor_id' => 167,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:29',
                'updated_at' => '2019-01-29 22:26:29',
                'deleted_at' => NULL,
            ),
            167 => 
            array (
                'id' => 168,
                'denuncia_id' => 140,
                'agresor_id' => 168,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:29',
                'updated_at' => '2019-01-29 22:26:29',
                'deleted_at' => NULL,
            ),
            168 => 
            array (
                'id' => 169,
                'denuncia_id' => 141,
                'agresor_id' => 169,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:29',
                'updated_at' => '2019-01-29 22:26:29',
                'deleted_at' => NULL,
            ),
            169 => 
            array (
                'id' => 170,
                'denuncia_id' => 142,
                'agresor_id' => 170,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:30',
                'updated_at' => '2019-01-29 22:26:30',
                'deleted_at' => NULL,
            ),
            170 => 
            array (
                'id' => 171,
                'denuncia_id' => 143,
                'agresor_id' => 171,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:30',
                'updated_at' => '2019-01-29 22:26:30',
                'deleted_at' => NULL,
            ),
            171 => 
            array (
                'id' => 172,
                'denuncia_id' => 144,
                'agresor_id' => 172,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:30',
                'updated_at' => '2019-01-29 22:26:30',
                'deleted_at' => NULL,
            ),
            172 => 
            array (
                'id' => 173,
                'denuncia_id' => 145,
                'agresor_id' => 173,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:30',
                'updated_at' => '2019-01-29 22:26:30',
                'deleted_at' => NULL,
            ),
            173 => 
            array (
                'id' => 174,
                'denuncia_id' => 145,
                'agresor_id' => 174,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:30',
                'updated_at' => '2019-01-29 22:26:30',
                'deleted_at' => NULL,
            ),
            174 => 
            array (
                'id' => 175,
                'denuncia_id' => 146,
                'agresor_id' => 175,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:30',
                'updated_at' => '2019-01-29 22:26:30',
                'deleted_at' => NULL,
            ),
            175 => 
            array (
                'id' => 176,
                'denuncia_id' => 147,
                'agresor_id' => 176,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:30',
                'updated_at' => '2019-01-29 22:26:30',
                'deleted_at' => NULL,
            ),
            176 => 
            array (
                'id' => 177,
                'denuncia_id' => 148,
                'agresor_id' => 177,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:30',
                'updated_at' => '2019-01-29 22:26:30',
                'deleted_at' => NULL,
            ),
            177 => 
            array (
                'id' => 178,
                'denuncia_id' => 149,
                'agresor_id' => 178,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:30',
                'updated_at' => '2019-01-29 22:26:30',
                'deleted_at' => NULL,
            ),
            178 => 
            array (
                'id' => 179,
                'denuncia_id' => 150,
                'agresor_id' => 179,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:31',
                'updated_at' => '2019-01-29 22:26:31',
                'deleted_at' => NULL,
            ),
            179 => 
            array (
                'id' => 180,
                'denuncia_id' => 151,
                'agresor_id' => 180,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:31',
                'updated_at' => '2019-01-29 22:26:31',
                'deleted_at' => NULL,
            ),
            180 => 
            array (
                'id' => 181,
                'denuncia_id' => 152,
                'agresor_id' => 181,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:31',
                'updated_at' => '2019-01-29 22:26:31',
                'deleted_at' => NULL,
            ),
            181 => 
            array (
                'id' => 182,
                'denuncia_id' => 153,
                'agresor_id' => 182,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:31',
                'updated_at' => '2019-01-29 22:26:31',
                'deleted_at' => NULL,
            ),
            182 => 
            array (
                'id' => 183,
                'denuncia_id' => 154,
                'agresor_id' => 183,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:31',
                'updated_at' => '2019-01-29 22:26:31',
                'deleted_at' => NULL,
            ),
            183 => 
            array (
                'id' => 184,
                'denuncia_id' => 155,
                'agresor_id' => 184,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:31',
                'updated_at' => '2019-01-29 22:26:31',
                'deleted_at' => NULL,
            ),
            184 => 
            array (
                'id' => 185,
                'denuncia_id' => 156,
                'agresor_id' => 101,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:31',
                'updated_at' => '2019-01-29 22:26:31',
                'deleted_at' => NULL,
            ),
            185 => 
            array (
                'id' => 186,
                'denuncia_id' => 156,
                'agresor_id' => 185,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:31',
                'updated_at' => '2019-01-29 22:26:31',
                'deleted_at' => NULL,
            ),
            186 => 
            array (
                'id' => 187,
                'denuncia_id' => 156,
                'agresor_id' => 186,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:31',
                'updated_at' => '2019-01-29 22:26:31',
                'deleted_at' => NULL,
            ),
            187 => 
            array (
                'id' => 188,
                'denuncia_id' => 156,
                'agresor_id' => 187,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:31',
                'updated_at' => '2019-01-29 22:26:31',
                'deleted_at' => NULL,
            ),
            188 => 
            array (
                'id' => 189,
                'denuncia_id' => 156,
                'agresor_id' => 188,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:31',
                'updated_at' => '2019-01-29 22:26:31',
                'deleted_at' => NULL,
            ),
            189 => 
            array (
                'id' => 190,
                'denuncia_id' => 156,
                'agresor_id' => 189,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:31',
                'updated_at' => '2019-01-29 22:26:31',
                'deleted_at' => NULL,
            ),
            190 => 
            array (
                'id' => 191,
                'denuncia_id' => 156,
                'agresor_id' => 190,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:31',
                'updated_at' => '2019-01-29 22:26:31',
                'deleted_at' => NULL,
            ),
            191 => 
            array (
                'id' => 192,
                'denuncia_id' => 157,
                'agresor_id' => 191,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:31',
                'updated_at' => '2019-01-29 22:26:31',
                'deleted_at' => NULL,
            ),
            192 => 
            array (
                'id' => 193,
                'denuncia_id' => 158,
                'agresor_id' => 192,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:32',
                'updated_at' => '2019-01-29 22:26:32',
                'deleted_at' => NULL,
            ),
            193 => 
            array (
                'id' => 194,
                'denuncia_id' => 159,
                'agresor_id' => 193,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:32',
                'updated_at' => '2019-01-29 22:26:32',
                'deleted_at' => NULL,
            ),
            194 => 
            array (
                'id' => 195,
                'denuncia_id' => 160,
                'agresor_id' => 194,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:32',
                'updated_at' => '2019-01-29 22:26:32',
                'deleted_at' => NULL,
            ),
            195 => 
            array (
                'id' => 196,
                'denuncia_id' => 161,
                'agresor_id' => 195,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:32',
                'updated_at' => '2019-01-29 22:26:32',
                'deleted_at' => NULL,
            ),
            196 => 
            array (
                'id' => 197,
                'denuncia_id' => 162,
                'agresor_id' => 196,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:32',
                'updated_at' => '2019-01-29 22:26:32',
                'deleted_at' => NULL,
            ),
            197 => 
            array (
                'id' => 198,
                'denuncia_id' => 163,
                'agresor_id' => 197,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:32',
                'updated_at' => '2019-01-29 22:26:32',
                'deleted_at' => NULL,
            ),
            198 => 
            array (
                'id' => 199,
                'denuncia_id' => 163,
                'agresor_id' => 198,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:32',
                'updated_at' => '2019-01-29 22:26:32',
                'deleted_at' => NULL,
            ),
            199 => 
            array (
                'id' => 200,
                'denuncia_id' => 164,
                'agresor_id' => 199,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:32',
                'updated_at' => '2019-01-29 22:26:32',
                'deleted_at' => NULL,
            ),
            200 => 
            array (
                'id' => 201,
                'denuncia_id' => 165,
                'agresor_id' => 200,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:33',
                'updated_at' => '2019-01-29 22:26:33',
                'deleted_at' => NULL,
            ),
            201 => 
            array (
                'id' => 202,
                'denuncia_id' => 165,
                'agresor_id' => 201,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:33',
                'updated_at' => '2019-01-29 22:26:33',
                'deleted_at' => NULL,
            ),
            202 => 
            array (
                'id' => 203,
                'denuncia_id' => 165,
                'agresor_id' => 202,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:33',
                'updated_at' => '2019-01-29 22:26:33',
                'deleted_at' => NULL,
            ),
            203 => 
            array (
                'id' => 204,
                'denuncia_id' => 166,
                'agresor_id' => 203,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:33',
                'updated_at' => '2019-01-29 22:26:33',
                'deleted_at' => NULL,
            ),
            204 => 
            array (
                'id' => 205,
                'denuncia_id' => 167,
                'agresor_id' => 204,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:33',
                'updated_at' => '2019-01-29 22:26:33',
                'deleted_at' => NULL,
            ),
            205 => 
            array (
                'id' => 206,
                'denuncia_id' => 168,
                'agresor_id' => 205,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:33',
                'updated_at' => '2019-01-29 22:26:33',
                'deleted_at' => NULL,
            ),
            206 => 
            array (
                'id' => 207,
                'denuncia_id' => 169,
                'agresor_id' => 206,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:33',
                'updated_at' => '2019-01-29 22:26:33',
                'deleted_at' => NULL,
            ),
            207 => 
            array (
                'id' => 208,
                'denuncia_id' => 170,
                'agresor_id' => 207,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:33',
                'updated_at' => '2019-01-29 22:26:33',
                'deleted_at' => NULL,
            ),
            208 => 
            array (
                'id' => 209,
                'denuncia_id' => 170,
                'agresor_id' => 208,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:33',
                'updated_at' => '2019-01-29 22:26:33',
                'deleted_at' => NULL,
            ),
            209 => 
            array (
                'id' => 210,
                'denuncia_id' => 171,
                'agresor_id' => 209,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:33',
                'updated_at' => '2019-01-29 22:26:33',
                'deleted_at' => NULL,
            ),
            210 => 
            array (
                'id' => 211,
                'denuncia_id' => 171,
                'agresor_id' => 210,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:33',
                'updated_at' => '2019-01-29 22:26:33',
                'deleted_at' => NULL,
            ),
            211 => 
            array (
                'id' => 212,
                'denuncia_id' => 172,
                'agresor_id' => 211,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:34',
                'updated_at' => '2019-01-29 22:26:34',
                'deleted_at' => NULL,
            ),
            212 => 
            array (
                'id' => 213,
                'denuncia_id' => 173,
                'agresor_id' => 212,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:34',
                'updated_at' => '2019-01-29 22:26:34',
                'deleted_at' => NULL,
            ),
            213 => 
            array (
                'id' => 214,
                'denuncia_id' => 174,
                'agresor_id' => 213,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:34',
                'updated_at' => '2019-01-29 22:26:34',
                'deleted_at' => NULL,
            ),
            214 => 
            array (
                'id' => 215,
                'denuncia_id' => 175,
                'agresor_id' => 214,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:34',
                'updated_at' => '2019-01-29 22:26:34',
                'deleted_at' => NULL,
            ),
            215 => 
            array (
                'id' => 216,
                'denuncia_id' => 176,
                'agresor_id' => 215,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:34',
                'updated_at' => '2019-01-29 22:26:34',
                'deleted_at' => NULL,
            ),
            216 => 
            array (
                'id' => 217,
                'denuncia_id' => 177,
                'agresor_id' => 216,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:34',
                'updated_at' => '2019-01-29 22:26:34',
                'deleted_at' => NULL,
            ),
            217 => 
            array (
                'id' => 218,
                'denuncia_id' => 178,
                'agresor_id' => 217,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:34',
                'updated_at' => '2019-01-29 22:26:34',
                'deleted_at' => NULL,
            ),
            218 => 
            array (
                'id' => 219,
                'denuncia_id' => 179,
                'agresor_id' => 218,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:35',
                'updated_at' => '2019-01-29 22:26:35',
                'deleted_at' => NULL,
            ),
            219 => 
            array (
                'id' => 220,
                'denuncia_id' => 180,
                'agresor_id' => 219,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:35',
                'updated_at' => '2019-01-29 22:26:35',
                'deleted_at' => NULL,
            ),
            220 => 
            array (
                'id' => 221,
                'denuncia_id' => 181,
                'agresor_id' => 220,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:35',
                'updated_at' => '2019-01-29 22:26:35',
                'deleted_at' => NULL,
            ),
            221 => 
            array (
                'id' => 222,
                'denuncia_id' => 182,
                'agresor_id' => 221,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:35',
                'updated_at' => '2019-01-29 22:26:35',
                'deleted_at' => NULL,
            ),
            222 => 
            array (
                'id' => 223,
                'denuncia_id' => 183,
                'agresor_id' => 222,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:35',
                'updated_at' => '2019-01-29 22:26:35',
                'deleted_at' => NULL,
            ),
            223 => 
            array (
                'id' => 224,
                'denuncia_id' => 183,
                'agresor_id' => 223,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:35',
                'updated_at' => '2019-01-29 22:26:35',
                'deleted_at' => NULL,
            ),
            224 => 
            array (
                'id' => 225,
                'denuncia_id' => 183,
                'agresor_id' => 224,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:35',
                'updated_at' => '2019-01-29 22:26:35',
                'deleted_at' => NULL,
            ),
            225 => 
            array (
                'id' => 226,
                'denuncia_id' => 184,
                'agresor_id' => 225,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:36',
                'updated_at' => '2019-01-29 22:26:36',
                'deleted_at' => NULL,
            ),
            226 => 
            array (
                'id' => 227,
                'denuncia_id' => 185,
                'agresor_id' => 226,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:36',
                'updated_at' => '2019-01-29 22:26:36',
                'deleted_at' => NULL,
            ),
            227 => 
            array (
                'id' => 228,
                'denuncia_id' => 186,
                'agresor_id' => 227,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:36',
                'updated_at' => '2019-01-29 22:26:36',
                'deleted_at' => NULL,
            ),
            228 => 
            array (
                'id' => 229,
                'denuncia_id' => 187,
                'agresor_id' => 228,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:36',
                'updated_at' => '2019-01-29 22:26:36',
                'deleted_at' => NULL,
            ),
            229 => 
            array (
                'id' => 230,
                'denuncia_id' => 188,
                'agresor_id' => 229,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:36',
                'updated_at' => '2019-01-29 22:26:36',
                'deleted_at' => NULL,
            ),
            230 => 
            array (
                'id' => 231,
                'denuncia_id' => 188,
                'agresor_id' => 230,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:36',
                'updated_at' => '2019-01-29 22:26:36',
                'deleted_at' => NULL,
            ),
            231 => 
            array (
                'id' => 232,
                'denuncia_id' => 189,
                'agresor_id' => 231,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:36',
                'updated_at' => '2019-01-29 22:26:36',
                'deleted_at' => NULL,
            ),
            232 => 
            array (
                'id' => 233,
                'denuncia_id' => 190,
                'agresor_id' => 232,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:37',
                'updated_at' => '2019-01-29 22:26:37',
                'deleted_at' => NULL,
            ),
            233 => 
            array (
                'id' => 234,
                'denuncia_id' => 191,
                'agresor_id' => 233,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:37',
                'updated_at' => '2019-01-29 22:26:37',
                'deleted_at' => NULL,
            ),
            234 => 
            array (
                'id' => 235,
                'denuncia_id' => 192,
                'agresor_id' => 234,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:37',
                'updated_at' => '2019-01-29 22:26:37',
                'deleted_at' => NULL,
            ),
            235 => 
            array (
                'id' => 236,
                'denuncia_id' => 193,
                'agresor_id' => 235,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:37',
                'updated_at' => '2019-01-29 22:26:37',
                'deleted_at' => NULL,
            ),
            236 => 
            array (
                'id' => 237,
                'denuncia_id' => 194,
                'agresor_id' => 236,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:37',
                'updated_at' => '2019-01-29 22:26:37',
                'deleted_at' => NULL,
            ),
            237 => 
            array (
                'id' => 238,
                'denuncia_id' => 194,
                'agresor_id' => 237,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:37',
                'updated_at' => '2019-01-29 22:26:37',
                'deleted_at' => NULL,
            ),
            238 => 
            array (
                'id' => 239,
                'denuncia_id' => 194,
                'agresor_id' => 238,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:37',
                'updated_at' => '2019-01-29 22:26:37',
                'deleted_at' => NULL,
            ),
            239 => 
            array (
                'id' => 240,
                'denuncia_id' => 195,
                'agresor_id' => 239,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:37',
                'updated_at' => '2019-01-29 22:26:37',
                'deleted_at' => NULL,
            ),
            240 => 
            array (
                'id' => 241,
                'denuncia_id' => 196,
                'agresor_id' => 240,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:37',
                'updated_at' => '2019-01-29 22:26:37',
                'deleted_at' => NULL,
            ),
            241 => 
            array (
                'id' => 242,
                'denuncia_id' => 197,
                'agresor_id' => 241,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:38',
                'updated_at' => '2019-01-29 22:26:38',
                'deleted_at' => NULL,
            ),
            242 => 
            array (
                'id' => 243,
                'denuncia_id' => 197,
                'agresor_id' => 242,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:38',
                'updated_at' => '2019-01-29 22:26:38',
                'deleted_at' => NULL,
            ),
            243 => 
            array (
                'id' => 244,
                'denuncia_id' => 198,
                'agresor_id' => 242,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:38',
                'updated_at' => '2019-01-29 22:26:38',
                'deleted_at' => NULL,
            ),
            244 => 
            array (
                'id' => 245,
                'denuncia_id' => 199,
                'agresor_id' => 243,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:38',
                'updated_at' => '2019-01-29 22:26:38',
                'deleted_at' => NULL,
            ),
            245 => 
            array (
                'id' => 246,
                'denuncia_id' => 200,
                'agresor_id' => 244,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:38',
                'updated_at' => '2019-01-29 22:26:38',
                'deleted_at' => NULL,
            ),
            246 => 
            array (
                'id' => 247,
                'denuncia_id' => 201,
                'agresor_id' => 245,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:38',
                'updated_at' => '2019-01-29 22:26:38',
                'deleted_at' => NULL,
            ),
            247 => 
            array (
                'id' => 248,
                'denuncia_id' => 202,
                'agresor_id' => 246,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:38',
                'updated_at' => '2019-01-29 22:26:38',
                'deleted_at' => NULL,
            ),
            248 => 
            array (
                'id' => 249,
                'denuncia_id' => 202,
                'agresor_id' => 247,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:38',
                'updated_at' => '2019-01-29 22:26:38',
                'deleted_at' => NULL,
            ),
            249 => 
            array (
                'id' => 250,
                'denuncia_id' => 203,
                'agresor_id' => 248,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:38',
                'updated_at' => '2019-01-29 22:26:38',
                'deleted_at' => NULL,
            ),
            250 => 
            array (
                'id' => 251,
                'denuncia_id' => 204,
                'agresor_id' => 249,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:38',
                'updated_at' => '2019-01-29 22:26:38',
                'deleted_at' => NULL,
            ),
            251 => 
            array (
                'id' => 252,
                'denuncia_id' => 204,
                'agresor_id' => 250,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:38',
                'updated_at' => '2019-01-29 22:26:38',
                'deleted_at' => NULL,
            ),
            252 => 
            array (
                'id' => 253,
                'denuncia_id' => 205,
                'agresor_id' => 251,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:39',
                'updated_at' => '2019-01-29 22:26:39',
                'deleted_at' => NULL,
            ),
            253 => 
            array (
                'id' => 254,
                'denuncia_id' => 206,
                'agresor_id' => 252,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:39',
                'updated_at' => '2019-01-29 22:26:39',
                'deleted_at' => NULL,
            ),
            254 => 
            array (
                'id' => 255,
                'denuncia_id' => 206,
                'agresor_id' => 253,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:39',
                'updated_at' => '2019-01-29 22:26:39',
                'deleted_at' => NULL,
            ),
            255 => 
            array (
                'id' => 256,
                'denuncia_id' => 206,
                'agresor_id' => 254,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:39',
                'updated_at' => '2019-01-29 22:26:39',
                'deleted_at' => NULL,
            ),
            256 => 
            array (
                'id' => 257,
                'denuncia_id' => 207,
                'agresor_id' => 255,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:39',
                'updated_at' => '2019-01-29 22:26:39',
                'deleted_at' => NULL,
            ),
            257 => 
            array (
                'id' => 258,
                'denuncia_id' => 208,
                'agresor_id' => 256,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:39',
                'updated_at' => '2019-01-29 22:26:39',
                'deleted_at' => NULL,
            ),
            258 => 
            array (
                'id' => 259,
                'denuncia_id' => 209,
                'agresor_id' => 257,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:39',
                'updated_at' => '2019-01-29 22:26:39',
                'deleted_at' => NULL,
            ),
            259 => 
            array (
                'id' => 260,
                'denuncia_id' => 210,
                'agresor_id' => 258,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:39',
                'updated_at' => '2019-01-29 22:26:39',
                'deleted_at' => NULL,
            ),
            260 => 
            array (
                'id' => 261,
                'denuncia_id' => 211,
                'agresor_id' => 259,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:40',
                'updated_at' => '2019-01-29 22:26:40',
                'deleted_at' => NULL,
            ),
            261 => 
            array (
                'id' => 262,
                'denuncia_id' => 212,
                'agresor_id' => 260,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:40',
                'updated_at' => '2019-01-29 22:26:40',
                'deleted_at' => NULL,
            ),
            262 => 
            array (
                'id' => 263,
                'denuncia_id' => 213,
                'agresor_id' => 261,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:40',
                'updated_at' => '2019-01-29 22:26:40',
                'deleted_at' => NULL,
            ),
            263 => 
            array (
                'id' => 264,
                'denuncia_id' => 214,
                'agresor_id' => 262,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:40',
                'updated_at' => '2019-01-29 22:26:40',
                'deleted_at' => NULL,
            ),
            264 => 
            array (
                'id' => 265,
                'denuncia_id' => 214,
                'agresor_id' => 263,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:40',
                'updated_at' => '2019-01-29 22:26:40',
                'deleted_at' => NULL,
            ),
            265 => 
            array (
                'id' => 266,
                'denuncia_id' => 215,
                'agresor_id' => 264,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:40',
                'updated_at' => '2019-01-29 22:26:40',
                'deleted_at' => NULL,
            ),
            266 => 
            array (
                'id' => 267,
                'denuncia_id' => 216,
                'agresor_id' => 265,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:40',
                'updated_at' => '2019-01-29 22:26:40',
                'deleted_at' => NULL,
            ),
            267 => 
            array (
                'id' => 268,
                'denuncia_id' => 216,
                'agresor_id' => 266,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:40',
                'updated_at' => '2019-01-29 22:26:40',
                'deleted_at' => NULL,
            ),
            268 => 
            array (
                'id' => 269,
                'denuncia_id' => 217,
                'agresor_id' => 267,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:41',
                'updated_at' => '2019-01-29 22:26:41',
                'deleted_at' => NULL,
            ),
            269 => 
            array (
                'id' => 270,
                'denuncia_id' => 218,
                'agresor_id' => 268,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:41',
                'updated_at' => '2019-01-29 22:26:41',
                'deleted_at' => NULL,
            ),
            270 => 
            array (
                'id' => 271,
                'denuncia_id' => 218,
                'agresor_id' => 269,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:41',
                'updated_at' => '2019-01-29 22:26:41',
                'deleted_at' => NULL,
            ),
            271 => 
            array (
                'id' => 272,
                'denuncia_id' => 219,
                'agresor_id' => 270,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:41',
                'updated_at' => '2019-01-29 22:26:41',
                'deleted_at' => NULL,
            ),
            272 => 
            array (
                'id' => 273,
                'denuncia_id' => 220,
                'agresor_id' => 271,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:41',
                'updated_at' => '2019-01-29 22:26:41',
                'deleted_at' => NULL,
            ),
            273 => 
            array (
                'id' => 274,
                'denuncia_id' => 221,
                'agresor_id' => 272,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:41',
                'updated_at' => '2019-01-29 22:26:41',
                'deleted_at' => NULL,
            ),
            274 => 
            array (
                'id' => 275,
                'denuncia_id' => 222,
                'agresor_id' => 273,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:41',
                'updated_at' => '2019-01-29 22:26:41',
                'deleted_at' => NULL,
            ),
            275 => 
            array (
                'id' => 276,
                'denuncia_id' => 223,
                'agresor_id' => 274,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:41',
                'updated_at' => '2019-01-29 22:26:41',
                'deleted_at' => NULL,
            ),
            276 => 
            array (
                'id' => 277,
                'denuncia_id' => 224,
                'agresor_id' => 275,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:41',
                'updated_at' => '2019-01-29 22:26:41',
                'deleted_at' => NULL,
            ),
            277 => 
            array (
                'id' => 278,
                'denuncia_id' => 225,
                'agresor_id' => 276,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:42',
                'updated_at' => '2019-01-29 22:26:42',
                'deleted_at' => NULL,
            ),
            278 => 
            array (
                'id' => 279,
                'denuncia_id' => 226,
                'agresor_id' => 277,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:42',
                'updated_at' => '2019-01-29 22:26:42',
                'deleted_at' => NULL,
            ),
            279 => 
            array (
                'id' => 280,
                'denuncia_id' => 226,
                'agresor_id' => 278,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:42',
                'updated_at' => '2019-01-29 22:26:42',
                'deleted_at' => NULL,
            ),
            280 => 
            array (
                'id' => 281,
                'denuncia_id' => 227,
                'agresor_id' => 279,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:42',
                'updated_at' => '2019-01-29 22:26:42',
                'deleted_at' => NULL,
            ),
            281 => 
            array (
                'id' => 282,
                'denuncia_id' => 227,
                'agresor_id' => 280,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:42',
                'updated_at' => '2019-01-29 22:26:42',
                'deleted_at' => NULL,
            ),
            282 => 
            array (
                'id' => 283,
                'denuncia_id' => 228,
                'agresor_id' => 281,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:42',
                'updated_at' => '2019-01-29 22:26:42',
                'deleted_at' => NULL,
            ),
            283 => 
            array (
                'id' => 284,
                'denuncia_id' => 229,
                'agresor_id' => 282,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:42',
                'updated_at' => '2019-01-29 22:26:42',
                'deleted_at' => NULL,
            ),
            284 => 
            array (
                'id' => 285,
                'denuncia_id' => 230,
                'agresor_id' => 283,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:42',
                'updated_at' => '2019-01-29 22:26:42',
                'deleted_at' => NULL,
            ),
            285 => 
            array (
                'id' => 286,
                'denuncia_id' => 231,
                'agresor_id' => 284,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:42',
                'updated_at' => '2019-01-29 22:26:42',
                'deleted_at' => NULL,
            ),
            286 => 
            array (
                'id' => 287,
                'denuncia_id' => 232,
                'agresor_id' => 285,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:42',
                'updated_at' => '2019-01-29 22:26:42',
                'deleted_at' => NULL,
            ),
            287 => 
            array (
                'id' => 288,
                'denuncia_id' => 233,
                'agresor_id' => 286,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:43',
                'updated_at' => '2019-01-29 22:26:43',
                'deleted_at' => NULL,
            ),
            288 => 
            array (
                'id' => 289,
                'denuncia_id' => 234,
                'agresor_id' => 287,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:43',
                'updated_at' => '2019-01-29 22:26:43',
                'deleted_at' => NULL,
            ),
            289 => 
            array (
                'id' => 290,
                'denuncia_id' => 234,
                'agresor_id' => 288,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:43',
                'updated_at' => '2019-01-29 22:26:43',
                'deleted_at' => NULL,
            ),
            290 => 
            array (
                'id' => 291,
                'denuncia_id' => 235,
                'agresor_id' => 289,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:43',
                'updated_at' => '2019-01-29 22:26:43',
                'deleted_at' => NULL,
            ),
            291 => 
            array (
                'id' => 292,
                'denuncia_id' => 236,
                'agresor_id' => 290,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:43',
                'updated_at' => '2019-01-29 22:26:43',
                'deleted_at' => NULL,
            ),
            292 => 
            array (
                'id' => 293,
                'denuncia_id' => 237,
                'agresor_id' => 291,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:43',
                'updated_at' => '2019-01-29 22:26:43',
                'deleted_at' => NULL,
            ),
            293 => 
            array (
                'id' => 294,
                'denuncia_id' => 238,
                'agresor_id' => 292,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:43',
                'updated_at' => '2019-01-29 22:26:43',
                'deleted_at' => NULL,
            ),
            294 => 
            array (
                'id' => 295,
                'denuncia_id' => 239,
                'agresor_id' => 293,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:43',
                'updated_at' => '2019-01-29 22:26:43',
                'deleted_at' => NULL,
            ),
            295 => 
            array (
                'id' => 296,
                'denuncia_id' => 240,
                'agresor_id' => 294,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:44',
                'updated_at' => '2019-01-29 22:26:44',
                'deleted_at' => NULL,
            ),
            296 => 
            array (
                'id' => 297,
                'denuncia_id' => 241,
                'agresor_id' => 295,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:44',
                'updated_at' => '2019-01-29 22:26:44',
                'deleted_at' => NULL,
            ),
            297 => 
            array (
                'id' => 298,
                'denuncia_id' => 242,
                'agresor_id' => 296,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:44',
                'updated_at' => '2019-01-29 22:26:44',
                'deleted_at' => NULL,
            ),
            298 => 
            array (
                'id' => 299,
                'denuncia_id' => 243,
                'agresor_id' => 297,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:44',
                'updated_at' => '2019-01-29 22:26:44',
                'deleted_at' => NULL,
            ),
            299 => 
            array (
                'id' => 300,
                'denuncia_id' => 244,
                'agresor_id' => 298,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:44',
                'updated_at' => '2019-01-29 22:26:44',
                'deleted_at' => NULL,
            ),
            300 => 
            array (
                'id' => 301,
                'denuncia_id' => 244,
                'agresor_id' => 299,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:44',
                'updated_at' => '2019-01-29 22:26:44',
                'deleted_at' => NULL,
            ),
            301 => 
            array (
                'id' => 302,
                'denuncia_id' => 245,
                'agresor_id' => 300,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:44',
                'updated_at' => '2019-01-29 22:26:44',
                'deleted_at' => NULL,
            ),
            302 => 
            array (
                'id' => 303,
                'denuncia_id' => 246,
                'agresor_id' => 301,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:44',
                'updated_at' => '2019-01-29 22:26:44',
                'deleted_at' => NULL,
            ),
            303 => 
            array (
                'id' => 304,
                'denuncia_id' => 247,
                'agresor_id' => 253,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:44',
                'updated_at' => '2019-01-29 22:26:44',
                'deleted_at' => NULL,
            ),
            304 => 
            array (
                'id' => 305,
                'denuncia_id' => 248,
                'agresor_id' => 302,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:45',
                'updated_at' => '2019-01-29 22:26:45',
                'deleted_at' => NULL,
            ),
            305 => 
            array (
                'id' => 306,
                'denuncia_id' => 249,
                'agresor_id' => 303,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:45',
                'updated_at' => '2019-01-29 22:26:45',
                'deleted_at' => NULL,
            ),
            306 => 
            array (
                'id' => 307,
                'denuncia_id' => 249,
                'agresor_id' => 304,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:45',
                'updated_at' => '2019-01-29 22:26:45',
                'deleted_at' => NULL,
            ),
            307 => 
            array (
                'id' => 308,
                'denuncia_id' => 250,
                'agresor_id' => 305,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:45',
                'updated_at' => '2019-01-29 22:26:45',
                'deleted_at' => NULL,
            ),
            308 => 
            array (
                'id' => 309,
                'denuncia_id' => 251,
                'agresor_id' => 306,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:45',
                'updated_at' => '2019-01-29 22:26:45',
                'deleted_at' => NULL,
            ),
            309 => 
            array (
                'id' => 310,
                'denuncia_id' => 252,
                'agresor_id' => 307,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:45',
                'updated_at' => '2019-01-29 22:26:45',
                'deleted_at' => NULL,
            ),
            310 => 
            array (
                'id' => 311,
                'denuncia_id' => 253,
                'agresor_id' => 308,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:45',
                'updated_at' => '2019-01-29 22:26:45',
                'deleted_at' => NULL,
            ),
            311 => 
            array (
                'id' => 312,
                'denuncia_id' => 254,
                'agresor_id' => 309,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:45',
                'updated_at' => '2019-01-29 22:26:45',
                'deleted_at' => NULL,
            ),
            312 => 
            array (
                'id' => 313,
                'denuncia_id' => 255,
                'agresor_id' => 310,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:46',
                'updated_at' => '2019-01-29 22:26:46',
                'deleted_at' => NULL,
            ),
            313 => 
            array (
                'id' => 314,
                'denuncia_id' => 256,
                'agresor_id' => 311,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:46',
                'updated_at' => '2019-01-29 22:26:46',
                'deleted_at' => NULL,
            ),
            314 => 
            array (
                'id' => 315,
                'denuncia_id' => 257,
                'agresor_id' => 312,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:46',
                'updated_at' => '2019-01-29 22:26:46',
                'deleted_at' => NULL,
            ),
            315 => 
            array (
                'id' => 316,
                'denuncia_id' => 257,
                'agresor_id' => 313,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:46',
                'updated_at' => '2019-01-29 22:26:46',
                'deleted_at' => NULL,
            ),
            316 => 
            array (
                'id' => 317,
                'denuncia_id' => 258,
                'agresor_id' => 314,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:46',
                'updated_at' => '2019-01-29 22:26:46',
                'deleted_at' => NULL,
            ),
            317 => 
            array (
                'id' => 318,
                'denuncia_id' => 259,
                'agresor_id' => 315,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:46',
                'updated_at' => '2019-01-29 22:26:46',
                'deleted_at' => NULL,
            ),
            318 => 
            array (
                'id' => 319,
                'denuncia_id' => 260,
                'agresor_id' => 316,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:46',
                'updated_at' => '2019-01-29 22:26:46',
                'deleted_at' => NULL,
            ),
            319 => 
            array (
                'id' => 320,
                'denuncia_id' => 261,
                'agresor_id' => 317,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:46',
                'updated_at' => '2019-01-29 22:26:46',
                'deleted_at' => NULL,
            ),
            320 => 
            array (
                'id' => 321,
                'denuncia_id' => 262,
                'agresor_id' => 318,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:46',
                'updated_at' => '2019-01-29 22:26:46',
                'deleted_at' => NULL,
            ),
            321 => 
            array (
                'id' => 322,
                'denuncia_id' => 262,
                'agresor_id' => 319,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:46',
                'updated_at' => '2019-01-29 22:26:46',
                'deleted_at' => NULL,
            ),
            322 => 
            array (
                'id' => 323,
                'denuncia_id' => 263,
                'agresor_id' => 320,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:47',
                'updated_at' => '2019-01-29 22:26:47',
                'deleted_at' => NULL,
            ),
            323 => 
            array (
                'id' => 324,
                'denuncia_id' => 264,
                'agresor_id' => 321,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:47',
                'updated_at' => '2019-01-29 22:26:47',
                'deleted_at' => NULL,
            ),
            324 => 
            array (
                'id' => 325,
                'denuncia_id' => 264,
                'agresor_id' => 322,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:47',
                'updated_at' => '2019-01-29 22:26:47',
                'deleted_at' => NULL,
            ),
            325 => 
            array (
                'id' => 326,
                'denuncia_id' => 265,
                'agresor_id' => 323,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:47',
                'updated_at' => '2019-01-29 22:26:47',
                'deleted_at' => NULL,
            ),
            326 => 
            array (
                'id' => 327,
                'denuncia_id' => 266,
                'agresor_id' => 324,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:47',
                'updated_at' => '2019-01-29 22:26:47',
                'deleted_at' => NULL,
            ),
            327 => 
            array (
                'id' => 328,
                'denuncia_id' => 267,
                'agresor_id' => 325,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:47',
                'updated_at' => '2019-01-29 22:26:47',
                'deleted_at' => NULL,
            ),
            328 => 
            array (
                'id' => 329,
                'denuncia_id' => 268,
                'agresor_id' => 326,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:47',
                'updated_at' => '2019-01-29 22:26:47',
                'deleted_at' => NULL,
            ),
            329 => 
            array (
                'id' => 330,
                'denuncia_id' => 269,
                'agresor_id' => 327,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:47',
                'updated_at' => '2019-01-29 22:26:47',
                'deleted_at' => NULL,
            ),
            330 => 
            array (
                'id' => 331,
                'denuncia_id' => 269,
                'agresor_id' => 328,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:47',
                'updated_at' => '2019-01-29 22:26:47',
                'deleted_at' => NULL,
            ),
            331 => 
            array (
                'id' => 332,
                'denuncia_id' => 269,
                'agresor_id' => 329,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:47',
                'updated_at' => '2019-01-29 22:26:47',
                'deleted_at' => NULL,
            ),
            332 => 
            array (
                'id' => 333,
                'denuncia_id' => 270,
                'agresor_id' => 330,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:48',
                'updated_at' => '2019-01-29 22:26:48',
                'deleted_at' => NULL,
            ),
            333 => 
            array (
                'id' => 334,
                'denuncia_id' => 271,
                'agresor_id' => 331,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:48',
                'updated_at' => '2019-01-29 22:26:48',
                'deleted_at' => NULL,
            ),
            334 => 
            array (
                'id' => 335,
                'denuncia_id' => 272,
                'agresor_id' => 332,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:48',
                'updated_at' => '2019-01-29 22:26:48',
                'deleted_at' => NULL,
            ),
            335 => 
            array (
                'id' => 336,
                'denuncia_id' => 273,
                'agresor_id' => 333,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:48',
                'updated_at' => '2019-01-29 22:26:48',
                'deleted_at' => NULL,
            ),
            336 => 
            array (
                'id' => 337,
                'denuncia_id' => 274,
                'agresor_id' => 334,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:48',
                'updated_at' => '2019-01-29 22:26:48',
                'deleted_at' => NULL,
            ),
            337 => 
            array (
                'id' => 338,
                'denuncia_id' => 275,
                'agresor_id' => 335,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:48',
                'updated_at' => '2019-01-29 22:26:48',
                'deleted_at' => NULL,
            ),
            338 => 
            array (
                'id' => 339,
                'denuncia_id' => 276,
                'agresor_id' => 336,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:49',
                'updated_at' => '2019-01-29 22:26:49',
                'deleted_at' => NULL,
            ),
            339 => 
            array (
                'id' => 340,
                'denuncia_id' => 277,
                'agresor_id' => 337,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:49',
                'updated_at' => '2019-01-29 22:26:49',
                'deleted_at' => NULL,
            ),
            340 => 
            array (
                'id' => 341,
                'denuncia_id' => 278,
                'agresor_id' => 338,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:49',
                'updated_at' => '2019-01-29 22:26:49',
                'deleted_at' => NULL,
            ),
            341 => 
            array (
                'id' => 342,
                'denuncia_id' => 279,
                'agresor_id' => 339,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:49',
                'updated_at' => '2019-01-29 22:26:49',
                'deleted_at' => NULL,
            ),
            342 => 
            array (
                'id' => 343,
                'denuncia_id' => 280,
                'agresor_id' => 340,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:49',
                'updated_at' => '2019-01-29 22:26:49',
                'deleted_at' => NULL,
            ),
            343 => 
            array (
                'id' => 344,
                'denuncia_id' => 281,
                'agresor_id' => 341,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:49',
                'updated_at' => '2019-01-29 22:26:49',
                'deleted_at' => NULL,
            ),
            344 => 
            array (
                'id' => 345,
                'denuncia_id' => 282,
                'agresor_id' => 342,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:50',
                'updated_at' => '2019-01-29 22:26:50',
                'deleted_at' => NULL,
            ),
            345 => 
            array (
                'id' => 346,
                'denuncia_id' => 282,
                'agresor_id' => 343,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:50',
                'updated_at' => '2019-01-29 22:26:50',
                'deleted_at' => NULL,
            ),
            346 => 
            array (
                'id' => 347,
                'denuncia_id' => 282,
                'agresor_id' => 344,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:50',
                'updated_at' => '2019-01-29 22:26:50',
                'deleted_at' => NULL,
            ),
            347 => 
            array (
                'id' => 348,
                'denuncia_id' => 283,
                'agresor_id' => 345,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:50',
                'updated_at' => '2019-01-29 22:26:50',
                'deleted_at' => NULL,
            ),
            348 => 
            array (
                'id' => 349,
                'denuncia_id' => 284,
                'agresor_id' => 346,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:50',
                'updated_at' => '2019-01-29 22:26:50',
                'deleted_at' => NULL,
            ),
            349 => 
            array (
                'id' => 350,
                'denuncia_id' => 284,
                'agresor_id' => 347,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:50',
                'updated_at' => '2019-01-29 22:26:50',
                'deleted_at' => NULL,
            ),
            350 => 
            array (
                'id' => 351,
                'denuncia_id' => 285,
                'agresor_id' => 348,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:50',
                'updated_at' => '2019-01-29 22:26:50',
                'deleted_at' => NULL,
            ),
            351 => 
            array (
                'id' => 352,
                'denuncia_id' => 285,
                'agresor_id' => 349,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:50',
                'updated_at' => '2019-01-29 22:26:50',
                'deleted_at' => NULL,
            ),
            352 => 
            array (
                'id' => 353,
                'denuncia_id' => 286,
                'agresor_id' => 350,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:50',
                'updated_at' => '2019-01-29 22:26:50',
                'deleted_at' => NULL,
            ),
            353 => 
            array (
                'id' => 354,
                'denuncia_id' => 287,
                'agresor_id' => 351,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:51',
                'updated_at' => '2019-01-29 22:26:51',
                'deleted_at' => NULL,
            ),
            354 => 
            array (
                'id' => 355,
                'denuncia_id' => 288,
                'agresor_id' => 352,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:51',
                'updated_at' => '2019-01-29 22:26:51',
                'deleted_at' => NULL,
            ),
            355 => 
            array (
                'id' => 356,
                'denuncia_id' => 289,
                'agresor_id' => 353,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:51',
                'updated_at' => '2019-01-29 22:26:51',
                'deleted_at' => NULL,
            ),
            356 => 
            array (
                'id' => 357,
                'denuncia_id' => 290,
                'agresor_id' => 354,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:51',
                'updated_at' => '2019-01-29 22:26:51',
                'deleted_at' => NULL,
            ),
            357 => 
            array (
                'id' => 358,
                'denuncia_id' => 291,
                'agresor_id' => 355,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:51',
                'updated_at' => '2019-01-29 22:26:51',
                'deleted_at' => NULL,
            ),
            358 => 
            array (
                'id' => 359,
                'denuncia_id' => 291,
                'agresor_id' => 356,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:51',
                'updated_at' => '2019-01-29 22:26:51',
                'deleted_at' => NULL,
            ),
            359 => 
            array (
                'id' => 360,
                'denuncia_id' => 291,
                'agresor_id' => 357,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:51',
                'updated_at' => '2019-01-29 22:26:51',
                'deleted_at' => NULL,
            ),
            360 => 
            array (
                'id' => 361,
                'denuncia_id' => 291,
                'agresor_id' => 358,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:51',
                'updated_at' => '2019-01-29 22:26:51',
                'deleted_at' => NULL,
            ),
            361 => 
            array (
                'id' => 362,
                'denuncia_id' => 291,
                'agresor_id' => 359,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:51',
                'updated_at' => '2019-01-29 22:26:51',
                'deleted_at' => NULL,
            ),
            362 => 
            array (
                'id' => 363,
                'denuncia_id' => 292,
                'agresor_id' => 360,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:51',
                'updated_at' => '2019-01-29 22:26:51',
                'deleted_at' => NULL,
            ),
            363 => 
            array (
                'id' => 364,
                'denuncia_id' => 292,
                'agresor_id' => 361,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:51',
                'updated_at' => '2019-01-29 22:26:51',
                'deleted_at' => NULL,
            ),
            364 => 
            array (
                'id' => 365,
                'denuncia_id' => 293,
                'agresor_id' => 362,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:51',
                'updated_at' => '2019-01-29 22:26:51',
                'deleted_at' => NULL,
            ),
            365 => 
            array (
                'id' => 366,
                'denuncia_id' => 294,
                'agresor_id' => 363,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:51',
                'updated_at' => '2019-01-29 22:26:51',
                'deleted_at' => NULL,
            ),
            366 => 
            array (
                'id' => 367,
                'denuncia_id' => 295,
                'agresor_id' => 364,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:52',
                'updated_at' => '2019-01-29 22:26:52',
                'deleted_at' => NULL,
            ),
            367 => 
            array (
                'id' => 368,
                'denuncia_id' => 296,
                'agresor_id' => 365,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:52',
                'updated_at' => '2019-01-29 22:26:52',
                'deleted_at' => NULL,
            ),
            368 => 
            array (
                'id' => 369,
                'denuncia_id' => 297,
                'agresor_id' => 366,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:52',
                'updated_at' => '2019-01-29 22:26:52',
                'deleted_at' => NULL,
            ),
            369 => 
            array (
                'id' => 370,
                'denuncia_id' => 298,
                'agresor_id' => 367,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:52',
                'updated_at' => '2019-01-29 22:26:52',
                'deleted_at' => NULL,
            ),
            370 => 
            array (
                'id' => 371,
                'denuncia_id' => 299,
                'agresor_id' => 368,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:52',
                'updated_at' => '2019-01-29 22:26:52',
                'deleted_at' => NULL,
            ),
            371 => 
            array (
                'id' => 372,
                'denuncia_id' => 300,
                'agresor_id' => 369,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:52',
                'updated_at' => '2019-01-29 22:26:52',
                'deleted_at' => NULL,
            ),
            372 => 
            array (
                'id' => 373,
                'denuncia_id' => 300,
                'agresor_id' => 370,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:52',
                'updated_at' => '2019-01-29 22:26:52',
                'deleted_at' => NULL,
            ),
            373 => 
            array (
                'id' => 374,
                'denuncia_id' => 300,
                'agresor_id' => 371,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:52',
                'updated_at' => '2019-01-29 22:26:52',
                'deleted_at' => NULL,
            ),
            374 => 
            array (
                'id' => 375,
                'denuncia_id' => 301,
                'agresor_id' => 372,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:53',
                'updated_at' => '2019-01-29 22:26:53',
                'deleted_at' => NULL,
            ),
            375 => 
            array (
                'id' => 376,
                'denuncia_id' => 302,
                'agresor_id' => 373,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:53',
                'updated_at' => '2019-01-29 22:26:53',
                'deleted_at' => NULL,
            ),
            376 => 
            array (
                'id' => 377,
                'denuncia_id' => 302,
                'agresor_id' => 374,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:53',
                'updated_at' => '2019-01-29 22:26:53',
                'deleted_at' => NULL,
            ),
            377 => 
            array (
                'id' => 378,
                'denuncia_id' => 303,
                'agresor_id' => 375,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:53',
                'updated_at' => '2019-01-29 22:26:53',
                'deleted_at' => NULL,
            ),
            378 => 
            array (
                'id' => 379,
                'denuncia_id' => 304,
                'agresor_id' => 376,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:53',
                'updated_at' => '2019-01-29 22:26:53',
                'deleted_at' => NULL,
            ),
            379 => 
            array (
                'id' => 380,
                'denuncia_id' => 305,
                'agresor_id' => 377,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:53',
                'updated_at' => '2019-01-29 22:26:53',
                'deleted_at' => NULL,
            ),
            380 => 
            array (
                'id' => 381,
                'denuncia_id' => 306,
                'agresor_id' => 378,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:53',
                'updated_at' => '2019-01-29 22:26:53',
                'deleted_at' => NULL,
            ),
            381 => 
            array (
                'id' => 382,
                'denuncia_id' => 307,
                'agresor_id' => 379,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:54',
                'updated_at' => '2019-01-29 22:26:54',
                'deleted_at' => NULL,
            ),
            382 => 
            array (
                'id' => 383,
                'denuncia_id' => 308,
                'agresor_id' => 197,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:54',
                'updated_at' => '2019-01-29 22:26:54',
                'deleted_at' => NULL,
            ),
            383 => 
            array (
                'id' => 384,
                'denuncia_id' => 309,
                'agresor_id' => 380,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:54',
                'updated_at' => '2019-01-29 22:26:54',
                'deleted_at' => NULL,
            ),
            384 => 
            array (
                'id' => 385,
                'denuncia_id' => 310,
                'agresor_id' => 381,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:54',
                'updated_at' => '2019-01-29 22:26:54',
                'deleted_at' => NULL,
            ),
            385 => 
            array (
                'id' => 386,
                'denuncia_id' => 310,
                'agresor_id' => 382,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:54',
                'updated_at' => '2019-01-29 22:26:54',
                'deleted_at' => NULL,
            ),
            386 => 
            array (
                'id' => 387,
                'denuncia_id' => 311,
                'agresor_id' => 383,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:54',
                'updated_at' => '2019-01-29 22:26:54',
                'deleted_at' => NULL,
            ),
            387 => 
            array (
                'id' => 388,
                'denuncia_id' => 312,
                'agresor_id' => 384,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:54',
                'updated_at' => '2019-01-29 22:26:54',
                'deleted_at' => NULL,
            ),
            388 => 
            array (
                'id' => 389,
                'denuncia_id' => 313,
                'agresor_id' => 385,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:55',
                'updated_at' => '2019-01-29 22:26:55',
                'deleted_at' => NULL,
            ),
            389 => 
            array (
                'id' => 390,
                'denuncia_id' => 314,
                'agresor_id' => 386,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:55',
                'updated_at' => '2019-01-29 22:26:55',
                'deleted_at' => NULL,
            ),
            390 => 
            array (
                'id' => 391,
                'denuncia_id' => 315,
                'agresor_id' => 387,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:55',
                'updated_at' => '2019-01-29 22:26:55',
                'deleted_at' => NULL,
            ),
            391 => 
            array (
                'id' => 392,
                'denuncia_id' => 316,
                'agresor_id' => 388,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:55',
                'updated_at' => '2019-01-29 22:26:55',
                'deleted_at' => NULL,
            ),
            392 => 
            array (
                'id' => 393,
                'denuncia_id' => 317,
                'agresor_id' => 389,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:55',
                'updated_at' => '2019-01-29 22:26:55',
                'deleted_at' => NULL,
            ),
            393 => 
            array (
                'id' => 394,
                'denuncia_id' => 318,
                'agresor_id' => 390,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:56',
                'updated_at' => '2019-01-29 22:26:56',
                'deleted_at' => NULL,
            ),
            394 => 
            array (
                'id' => 395,
                'denuncia_id' => 318,
                'agresor_id' => 391,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:56',
                'updated_at' => '2019-01-29 22:26:56',
                'deleted_at' => NULL,
            ),
            395 => 
            array (
                'id' => 396,
                'denuncia_id' => 319,
                'agresor_id' => 392,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:56',
                'updated_at' => '2019-01-29 22:26:56',
                'deleted_at' => NULL,
            ),
            396 => 
            array (
                'id' => 397,
                'denuncia_id' => 320,
                'agresor_id' => 393,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:56',
                'updated_at' => '2019-01-29 22:26:56',
                'deleted_at' => NULL,
            ),
            397 => 
            array (
                'id' => 398,
                'denuncia_id' => 321,
                'agresor_id' => 394,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:56',
                'updated_at' => '2019-01-29 22:26:56',
                'deleted_at' => NULL,
            ),
            398 => 
            array (
                'id' => 399,
                'denuncia_id' => 322,
                'agresor_id' => 264,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:56',
                'updated_at' => '2019-01-29 22:26:56',
                'deleted_at' => NULL,
            ),
            399 => 
            array (
                'id' => 400,
                'denuncia_id' => 322,
                'agresor_id' => 395,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:56',
                'updated_at' => '2019-01-29 22:26:56',
                'deleted_at' => NULL,
            ),
            400 => 
            array (
                'id' => 401,
                'denuncia_id' => 323,
                'agresor_id' => 396,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:56',
                'updated_at' => '2019-01-29 22:26:56',
                'deleted_at' => NULL,
            ),
            401 => 
            array (
                'id' => 402,
                'denuncia_id' => 324,
                'agresor_id' => 397,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:57',
                'updated_at' => '2019-01-29 22:26:57',
                'deleted_at' => NULL,
            ),
            402 => 
            array (
                'id' => 403,
                'denuncia_id' => 325,
                'agresor_id' => 398,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:57',
                'updated_at' => '2019-01-29 22:26:57',
                'deleted_at' => NULL,
            ),
            403 => 
            array (
                'id' => 404,
                'denuncia_id' => 326,
                'agresor_id' => 399,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:57',
                'updated_at' => '2019-01-29 22:26:57',
                'deleted_at' => NULL,
            ),
            404 => 
            array (
                'id' => 405,
                'denuncia_id' => 327,
                'agresor_id' => 400,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:57',
                'updated_at' => '2019-01-29 22:26:57',
                'deleted_at' => NULL,
            ),
            405 => 
            array (
                'id' => 406,
                'denuncia_id' => 328,
                'agresor_id' => 401,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:57',
                'updated_at' => '2019-01-29 22:26:57',
                'deleted_at' => NULL,
            ),
            406 => 
            array (
                'id' => 407,
                'denuncia_id' => 329,
                'agresor_id' => 402,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:57',
                'updated_at' => '2019-01-29 22:26:57',
                'deleted_at' => NULL,
            ),
            407 => 
            array (
                'id' => 408,
                'denuncia_id' => 330,
                'agresor_id' => 403,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:58',
                'updated_at' => '2019-01-29 22:26:58',
                'deleted_at' => NULL,
            ),
            408 => 
            array (
                'id' => 409,
                'denuncia_id' => 330,
                'agresor_id' => 404,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:58',
                'updated_at' => '2019-01-29 22:26:58',
                'deleted_at' => NULL,
            ),
            409 => 
            array (
                'id' => 410,
                'denuncia_id' => 330,
                'agresor_id' => 405,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:58',
                'updated_at' => '2019-01-29 22:26:58',
                'deleted_at' => NULL,
            ),
            410 => 
            array (
                'id' => 411,
                'denuncia_id' => 331,
                'agresor_id' => 406,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:58',
                'updated_at' => '2019-01-29 22:26:58',
                'deleted_at' => NULL,
            ),
            411 => 
            array (
                'id' => 412,
                'denuncia_id' => 332,
                'agresor_id' => 407,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:58',
                'updated_at' => '2019-01-29 22:26:58',
                'deleted_at' => NULL,
            ),
            412 => 
            array (
                'id' => 413,
                'denuncia_id' => 333,
                'agresor_id' => 408,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:58',
                'updated_at' => '2019-01-29 22:26:58',
                'deleted_at' => NULL,
            ),
            413 => 
            array (
                'id' => 414,
                'denuncia_id' => 334,
                'agresor_id' => 409,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:59',
                'updated_at' => '2019-01-29 22:26:59',
                'deleted_at' => NULL,
            ),
            414 => 
            array (
                'id' => 415,
                'denuncia_id' => 335,
                'agresor_id' => 410,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:59',
                'updated_at' => '2019-01-29 22:26:59',
                'deleted_at' => NULL,
            ),
            415 => 
            array (
                'id' => 416,
                'denuncia_id' => 336,
                'agresor_id' => 411,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:59',
                'updated_at' => '2019-01-29 22:26:59',
                'deleted_at' => NULL,
            ),
            416 => 
            array (
                'id' => 417,
                'denuncia_id' => 336,
                'agresor_id' => 412,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:59',
                'updated_at' => '2019-01-29 22:26:59',
                'deleted_at' => NULL,
            ),
            417 => 
            array (
                'id' => 418,
                'denuncia_id' => 337,
                'agresor_id' => 413,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:59',
                'updated_at' => '2019-01-29 22:26:59',
                'deleted_at' => NULL,
            ),
            418 => 
            array (
                'id' => 419,
                'denuncia_id' => 338,
                'agresor_id' => 414,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:59',
                'updated_at' => '2019-01-29 22:26:59',
                'deleted_at' => NULL,
            ),
            419 => 
            array (
                'id' => 420,
                'denuncia_id' => 339,
                'agresor_id' => 415,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:26:59',
                'updated_at' => '2019-01-29 22:26:59',
                'deleted_at' => NULL,
            ),
            420 => 
            array (
                'id' => 421,
                'denuncia_id' => 340,
                'agresor_id' => 416,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:00',
                'updated_at' => '2019-01-29 22:27:00',
                'deleted_at' => NULL,
            ),
            421 => 
            array (
                'id' => 422,
                'denuncia_id' => 341,
                'agresor_id' => 417,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:00',
                'updated_at' => '2019-01-29 22:27:00',
                'deleted_at' => NULL,
            ),
            422 => 
            array (
                'id' => 423,
                'denuncia_id' => 342,
                'agresor_id' => 418,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:00',
                'updated_at' => '2019-01-29 22:27:00',
                'deleted_at' => NULL,
            ),
            423 => 
            array (
                'id' => 424,
                'denuncia_id' => 343,
                'agresor_id' => 419,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:00',
                'updated_at' => '2019-01-29 22:27:00',
                'deleted_at' => NULL,
            ),
            424 => 
            array (
                'id' => 425,
                'denuncia_id' => 344,
                'agresor_id' => 420,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:00',
                'updated_at' => '2019-01-29 22:27:00',
                'deleted_at' => NULL,
            ),
            425 => 
            array (
                'id' => 426,
                'denuncia_id' => 344,
                'agresor_id' => 421,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:00',
                'updated_at' => '2019-01-29 22:27:00',
                'deleted_at' => NULL,
            ),
            426 => 
            array (
                'id' => 427,
                'denuncia_id' => 345,
                'agresor_id' => 422,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:01',
                'updated_at' => '2019-01-29 22:27:01',
                'deleted_at' => NULL,
            ),
            427 => 
            array (
                'id' => 428,
                'denuncia_id' => 346,
                'agresor_id' => 423,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:01',
                'updated_at' => '2019-01-29 22:27:01',
                'deleted_at' => NULL,
            ),
            428 => 
            array (
                'id' => 429,
                'denuncia_id' => 347,
                'agresor_id' => 424,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:01',
                'updated_at' => '2019-01-29 22:27:01',
                'deleted_at' => NULL,
            ),
            429 => 
            array (
                'id' => 430,
                'denuncia_id' => 347,
                'agresor_id' => 425,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:01',
                'updated_at' => '2019-01-29 22:27:01',
                'deleted_at' => NULL,
            ),
            430 => 
            array (
                'id' => 431,
                'denuncia_id' => 348,
                'agresor_id' => 426,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:01',
                'updated_at' => '2019-01-29 22:27:01',
                'deleted_at' => NULL,
            ),
            431 => 
            array (
                'id' => 432,
                'denuncia_id' => 349,
                'agresor_id' => 427,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:01',
                'updated_at' => '2019-01-29 22:27:01',
                'deleted_at' => NULL,
            ),
            432 => 
            array (
                'id' => 433,
                'denuncia_id' => 350,
                'agresor_id' => 428,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:01',
                'updated_at' => '2019-01-29 22:27:01',
                'deleted_at' => NULL,
            ),
            433 => 
            array (
                'id' => 434,
                'denuncia_id' => 351,
                'agresor_id' => 429,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:02',
                'updated_at' => '2019-01-29 22:27:02',
                'deleted_at' => NULL,
            ),
            434 => 
            array (
                'id' => 435,
                'denuncia_id' => 351,
                'agresor_id' => 430,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:02',
                'updated_at' => '2019-01-29 22:27:02',
                'deleted_at' => NULL,
            ),
            435 => 
            array (
                'id' => 436,
                'denuncia_id' => 352,
                'agresor_id' => 431,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:02',
                'updated_at' => '2019-01-29 22:27:02',
                'deleted_at' => NULL,
            ),
            436 => 
            array (
                'id' => 437,
                'denuncia_id' => 353,
                'agresor_id' => 432,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:02',
                'updated_at' => '2019-01-29 22:27:02',
                'deleted_at' => NULL,
            ),
            437 => 
            array (
                'id' => 438,
                'denuncia_id' => 354,
                'agresor_id' => 433,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:02',
                'updated_at' => '2019-01-29 22:27:02',
                'deleted_at' => NULL,
            ),
            438 => 
            array (
                'id' => 439,
                'denuncia_id' => 355,
                'agresor_id' => 434,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:03',
                'updated_at' => '2019-01-29 22:27:03',
                'deleted_at' => NULL,
            ),
            439 => 
            array (
                'id' => 440,
                'denuncia_id' => 355,
                'agresor_id' => 435,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:03',
                'updated_at' => '2019-01-29 22:27:03',
                'deleted_at' => NULL,
            ),
            440 => 
            array (
                'id' => 441,
                'denuncia_id' => 356,
                'agresor_id' => 436,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:03',
                'updated_at' => '2019-01-29 22:27:03',
                'deleted_at' => NULL,
            ),
            441 => 
            array (
                'id' => 442,
                'denuncia_id' => 357,
                'agresor_id' => 437,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:03',
                'updated_at' => '2019-01-29 22:27:03',
                'deleted_at' => NULL,
            ),
            442 => 
            array (
                'id' => 443,
                'denuncia_id' => 357,
                'agresor_id' => 438,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:03',
                'updated_at' => '2019-01-29 22:27:03',
                'deleted_at' => NULL,
            ),
            443 => 
            array (
                'id' => 444,
                'denuncia_id' => 358,
                'agresor_id' => 439,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:03',
                'updated_at' => '2019-01-29 22:27:03',
                'deleted_at' => NULL,
            ),
            444 => 
            array (
                'id' => 445,
                'denuncia_id' => 359,
                'agresor_id' => 440,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:03',
                'updated_at' => '2019-01-29 22:27:03',
                'deleted_at' => NULL,
            ),
            445 => 
            array (
                'id' => 446,
                'denuncia_id' => 360,
                'agresor_id' => 441,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:03',
                'updated_at' => '2019-01-29 22:27:03',
                'deleted_at' => NULL,
            ),
            446 => 
            array (
                'id' => 447,
                'denuncia_id' => 361,
                'agresor_id' => 442,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:04',
                'updated_at' => '2019-01-29 22:27:04',
                'deleted_at' => NULL,
            ),
            447 => 
            array (
                'id' => 448,
                'denuncia_id' => 362,
                'agresor_id' => 443,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:04',
                'updated_at' => '2019-01-29 22:27:04',
                'deleted_at' => NULL,
            ),
            448 => 
            array (
                'id' => 449,
                'denuncia_id' => 363,
                'agresor_id' => 444,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:04',
                'updated_at' => '2019-01-29 22:27:04',
                'deleted_at' => NULL,
            ),
            449 => 
            array (
                'id' => 450,
                'denuncia_id' => 364,
                'agresor_id' => 445,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:04',
                'updated_at' => '2019-01-29 22:27:04',
                'deleted_at' => NULL,
            ),
            450 => 
            array (
                'id' => 451,
                'denuncia_id' => 365,
                'agresor_id' => 446,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:04',
                'updated_at' => '2019-01-29 22:27:04',
                'deleted_at' => NULL,
            ),
            451 => 
            array (
                'id' => 452,
                'denuncia_id' => 366,
                'agresor_id' => 447,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:04',
                'updated_at' => '2019-01-29 22:27:04',
                'deleted_at' => NULL,
            ),
            452 => 
            array (
                'id' => 453,
                'denuncia_id' => 367,
                'agresor_id' => 448,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:05',
                'updated_at' => '2019-01-29 22:27:05',
                'deleted_at' => NULL,
            ),
            453 => 
            array (
                'id' => 454,
                'denuncia_id' => 368,
                'agresor_id' => 449,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:05',
                'updated_at' => '2019-01-29 22:27:05',
                'deleted_at' => NULL,
            ),
            454 => 
            array (
                'id' => 455,
                'denuncia_id' => 369,
                'agresor_id' => 450,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:05',
                'updated_at' => '2019-01-29 22:27:05',
                'deleted_at' => NULL,
            ),
            455 => 
            array (
                'id' => 456,
                'denuncia_id' => 370,
                'agresor_id' => 451,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:05',
                'updated_at' => '2019-01-29 22:27:05',
                'deleted_at' => NULL,
            ),
            456 => 
            array (
                'id' => 457,
                'denuncia_id' => 371,
                'agresor_id' => 452,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:05',
                'updated_at' => '2019-01-29 22:27:05',
                'deleted_at' => NULL,
            ),
            457 => 
            array (
                'id' => 458,
                'denuncia_id' => 372,
                'agresor_id' => 453,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:06',
                'updated_at' => '2019-01-29 22:27:06',
                'deleted_at' => NULL,
            ),
            458 => 
            array (
                'id' => 459,
                'denuncia_id' => 373,
                'agresor_id' => 454,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:06',
                'updated_at' => '2019-01-29 22:27:06',
                'deleted_at' => NULL,
            ),
            459 => 
            array (
                'id' => 460,
                'denuncia_id' => 374,
                'agresor_id' => 455,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:06',
                'updated_at' => '2019-01-29 22:27:06',
                'deleted_at' => NULL,
            ),
            460 => 
            array (
                'id' => 461,
                'denuncia_id' => 375,
                'agresor_id' => 456,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:06',
                'updated_at' => '2019-01-29 22:27:06',
                'deleted_at' => NULL,
            ),
            461 => 
            array (
                'id' => 462,
                'denuncia_id' => 376,
                'agresor_id' => 457,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:06',
                'updated_at' => '2019-01-29 22:27:06',
                'deleted_at' => NULL,
            ),
            462 => 
            array (
                'id' => 463,
                'denuncia_id' => 377,
                'agresor_id' => 458,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:06',
                'updated_at' => '2019-01-29 22:27:06',
                'deleted_at' => NULL,
            ),
            463 => 
            array (
                'id' => 464,
                'denuncia_id' => 378,
                'agresor_id' => 459,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:07',
                'updated_at' => '2019-01-29 22:27:07',
                'deleted_at' => NULL,
            ),
            464 => 
            array (
                'id' => 465,
                'denuncia_id' => 379,
                'agresor_id' => 460,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:07',
                'updated_at' => '2019-01-29 22:27:07',
                'deleted_at' => NULL,
            ),
            465 => 
            array (
                'id' => 466,
                'denuncia_id' => 380,
                'agresor_id' => 461,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:07',
                'updated_at' => '2019-01-29 22:27:07',
                'deleted_at' => NULL,
            ),
            466 => 
            array (
                'id' => 467,
                'denuncia_id' => 380,
                'agresor_id' => 462,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:07',
                'updated_at' => '2019-01-29 22:27:07',
                'deleted_at' => NULL,
            ),
            467 => 
            array (
                'id' => 468,
                'denuncia_id' => 381,
                'agresor_id' => 463,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:07',
                'updated_at' => '2019-01-29 22:27:07',
                'deleted_at' => NULL,
            ),
            468 => 
            array (
                'id' => 469,
                'denuncia_id' => 382,
                'agresor_id' => 464,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:07',
                'updated_at' => '2019-01-29 22:27:07',
                'deleted_at' => NULL,
            ),
            469 => 
            array (
                'id' => 470,
                'denuncia_id' => 383,
                'agresor_id' => 465,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:08',
                'updated_at' => '2019-01-29 22:27:08',
                'deleted_at' => NULL,
            ),
            470 => 
            array (
                'id' => 471,
                'denuncia_id' => 384,
                'agresor_id' => 399,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:08',
                'updated_at' => '2019-01-29 22:27:08',
                'deleted_at' => NULL,
            ),
            471 => 
            array (
                'id' => 472,
                'denuncia_id' => 385,
                'agresor_id' => 466,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:08',
                'updated_at' => '2019-01-29 22:27:08',
                'deleted_at' => NULL,
            ),
            472 => 
            array (
                'id' => 473,
                'denuncia_id' => 385,
                'agresor_id' => 467,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:08',
                'updated_at' => '2019-01-29 22:27:08',
                'deleted_at' => NULL,
            ),
            473 => 
            array (
                'id' => 474,
                'denuncia_id' => 385,
                'agresor_id' => 468,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:08',
                'updated_at' => '2019-01-29 22:27:08',
                'deleted_at' => NULL,
            ),
            474 => 
            array (
                'id' => 475,
                'denuncia_id' => 386,
                'agresor_id' => 469,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:08',
                'updated_at' => '2019-01-29 22:27:08',
                'deleted_at' => NULL,
            ),
            475 => 
            array (
                'id' => 476,
                'denuncia_id' => 386,
                'agresor_id' => 470,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:08',
                'updated_at' => '2019-01-29 22:27:08',
                'deleted_at' => NULL,
            ),
            476 => 
            array (
                'id' => 477,
                'denuncia_id' => 387,
                'agresor_id' => 471,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:08',
                'updated_at' => '2019-01-29 22:27:08',
                'deleted_at' => NULL,
            ),
            477 => 
            array (
                'id' => 478,
                'denuncia_id' => 387,
                'agresor_id' => 472,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:08',
                'updated_at' => '2019-01-29 22:27:08',
                'deleted_at' => NULL,
            ),
            478 => 
            array (
                'id' => 479,
                'denuncia_id' => 388,
                'agresor_id' => 473,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:09',
                'updated_at' => '2019-01-29 22:27:09',
                'deleted_at' => NULL,
            ),
            479 => 
            array (
                'id' => 480,
                'denuncia_id' => 389,
                'agresor_id' => 474,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:09',
                'updated_at' => '2019-01-29 22:27:09',
                'deleted_at' => NULL,
            ),
            480 => 
            array (
                'id' => 481,
                'denuncia_id' => 390,
                'agresor_id' => 475,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:09',
                'updated_at' => '2019-01-29 22:27:09',
                'deleted_at' => NULL,
            ),
            481 => 
            array (
                'id' => 482,
                'denuncia_id' => 390,
                'agresor_id' => 476,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:09',
                'updated_at' => '2019-01-29 22:27:09',
                'deleted_at' => NULL,
            ),
            482 => 
            array (
                'id' => 483,
                'denuncia_id' => 391,
                'agresor_id' => 477,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:09',
                'updated_at' => '2019-01-29 22:27:09',
                'deleted_at' => NULL,
            ),
            483 => 
            array (
                'id' => 484,
                'denuncia_id' => 392,
                'agresor_id' => 478,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:09',
                'updated_at' => '2019-01-29 22:27:09',
                'deleted_at' => NULL,
            ),
            484 => 
            array (
                'id' => 485,
                'denuncia_id' => 393,
                'agresor_id' => 479,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:09',
                'updated_at' => '2019-01-29 22:27:09',
                'deleted_at' => NULL,
            ),
            485 => 
            array (
                'id' => 486,
                'denuncia_id' => 393,
                'agresor_id' => 480,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:09',
                'updated_at' => '2019-01-29 22:27:09',
                'deleted_at' => NULL,
            ),
            486 => 
            array (
                'id' => 487,
                'denuncia_id' => 394,
                'agresor_id' => 481,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:10',
                'updated_at' => '2019-01-29 22:27:10',
                'deleted_at' => NULL,
            ),
            487 => 
            array (
                'id' => 488,
                'denuncia_id' => 395,
                'agresor_id' => 482,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:10',
                'updated_at' => '2019-01-29 22:27:10',
                'deleted_at' => NULL,
            ),
            488 => 
            array (
                'id' => 489,
                'denuncia_id' => 396,
                'agresor_id' => 483,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:10',
                'updated_at' => '2019-01-29 22:27:10',
                'deleted_at' => NULL,
            ),
            489 => 
            array (
                'id' => 490,
                'denuncia_id' => 397,
                'agresor_id' => 484,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:10',
                'updated_at' => '2019-01-29 22:27:10',
                'deleted_at' => NULL,
            ),
            490 => 
            array (
                'id' => 491,
                'denuncia_id' => 398,
                'agresor_id' => 485,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:10',
                'updated_at' => '2019-01-29 22:27:10',
                'deleted_at' => NULL,
            ),
            491 => 
            array (
                'id' => 492,
                'denuncia_id' => 399,
                'agresor_id' => 486,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:10',
                'updated_at' => '2019-01-29 22:27:10',
                'deleted_at' => NULL,
            ),
            492 => 
            array (
                'id' => 493,
                'denuncia_id' => 400,
                'agresor_id' => 487,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:11',
                'updated_at' => '2019-01-29 22:27:11',
                'deleted_at' => NULL,
            ),
            493 => 
            array (
                'id' => 494,
                'denuncia_id' => 401,
                'agresor_id' => 488,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:11',
                'updated_at' => '2019-01-29 22:27:11',
                'deleted_at' => NULL,
            ),
            494 => 
            array (
                'id' => 495,
                'denuncia_id' => 402,
                'agresor_id' => 489,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:11',
                'updated_at' => '2019-01-29 22:27:11',
                'deleted_at' => NULL,
            ),
            495 => 
            array (
                'id' => 496,
                'denuncia_id' => 403,
                'agresor_id' => 490,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:11',
                'updated_at' => '2019-01-29 22:27:11',
                'deleted_at' => NULL,
            ),
            496 => 
            array (
                'id' => 497,
                'denuncia_id' => 404,
                'agresor_id' => 491,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:11',
                'updated_at' => '2019-01-29 22:27:11',
                'deleted_at' => NULL,
            ),
            497 => 
            array (
                'id' => 498,
                'denuncia_id' => 405,
                'agresor_id' => 492,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:11',
                'updated_at' => '2019-01-29 22:27:11',
                'deleted_at' => NULL,
            ),
            498 => 
            array (
                'id' => 499,
                'denuncia_id' => 406,
                'agresor_id' => 493,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:11',
                'updated_at' => '2019-01-29 22:27:11',
                'deleted_at' => NULL,
            ),
            499 => 
            array (
                'id' => 500,
                'denuncia_id' => 407,
                'agresor_id' => 494,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:12',
                'updated_at' => '2019-01-29 22:27:12',
                'deleted_at' => NULL,
            ),
        ));
        \DB::table('denuncia_agresor')->insert(array (
            0 => 
            array (
                'id' => 501,
                'denuncia_id' => 408,
                'agresor_id' => 495,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:12',
                'updated_at' => '2019-01-29 22:27:12',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 502,
                'denuncia_id' => 409,
                'agresor_id' => 496,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:12',
                'updated_at' => '2019-01-29 22:27:12',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 503,
                'denuncia_id' => 410,
                'agresor_id' => 497,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:12',
                'updated_at' => '2019-01-29 22:27:12',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 504,
                'denuncia_id' => 411,
                'agresor_id' => 498,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:12',
                'updated_at' => '2019-01-29 22:27:12',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 505,
                'denuncia_id' => 411,
                'agresor_id' => 499,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:12',
                'updated_at' => '2019-01-29 22:27:12',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 506,
                'denuncia_id' => 411,
                'agresor_id' => 500,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:12',
                'updated_at' => '2019-01-29 22:27:12',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 507,
                'denuncia_id' => 412,
                'agresor_id' => 501,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:13',
                'updated_at' => '2019-01-29 22:27:13',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 508,
                'denuncia_id' => 413,
                'agresor_id' => 502,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:13',
                'updated_at' => '2019-01-29 22:27:13',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 509,
                'denuncia_id' => 413,
                'agresor_id' => 503,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:13',
                'updated_at' => '2019-01-29 22:27:13',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 510,
                'denuncia_id' => 413,
                'agresor_id' => 504,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:13',
                'updated_at' => '2019-01-29 22:27:13',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 511,
                'denuncia_id' => 413,
                'agresor_id' => 505,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:13',
                'updated_at' => '2019-01-29 22:27:13',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 512,
                'denuncia_id' => 414,
                'agresor_id' => 506,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:13',
                'updated_at' => '2019-01-29 22:27:13',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 513,
                'denuncia_id' => 414,
                'agresor_id' => 507,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:13',
                'updated_at' => '2019-01-29 22:27:13',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 514,
                'denuncia_id' => 415,
                'agresor_id' => 508,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:13',
                'updated_at' => '2019-01-29 22:27:13',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 515,
                'denuncia_id' => 415,
                'agresor_id' => 509,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:13',
                'updated_at' => '2019-01-29 22:27:13',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 516,
                'denuncia_id' => 416,
                'agresor_id' => 510,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:13',
                'updated_at' => '2019-01-29 22:27:13',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 517,
                'denuncia_id' => 417,
                'agresor_id' => 511,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:14',
                'updated_at' => '2019-01-29 22:27:14',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 518,
                'denuncia_id' => 417,
                'agresor_id' => 512,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:14',
                'updated_at' => '2019-01-29 22:27:14',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 519,
                'denuncia_id' => 418,
                'agresor_id' => 513,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:14',
                'updated_at' => '2019-01-29 22:27:14',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 520,
                'denuncia_id' => 418,
                'agresor_id' => 514,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:14',
                'updated_at' => '2019-01-29 22:27:14',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 521,
                'denuncia_id' => 419,
                'agresor_id' => 515,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:14',
                'updated_at' => '2019-01-29 22:27:14',
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 522,
                'denuncia_id' => 419,
                'agresor_id' => 516,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:14',
                'updated_at' => '2019-01-29 22:27:14',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 523,
                'denuncia_id' => 419,
                'agresor_id' => 517,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:14',
                'updated_at' => '2019-01-29 22:27:14',
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id' => 524,
                'denuncia_id' => 420,
                'agresor_id' => 343,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:14',
                'updated_at' => '2019-01-29 22:27:14',
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id' => 525,
                'denuncia_id' => 420,
                'agresor_id' => 518,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:14',
                'updated_at' => '2019-01-29 22:27:14',
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id' => 526,
                'denuncia_id' => 420,
                'agresor_id' => 519,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:14',
                'updated_at' => '2019-01-29 22:27:14',
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'id' => 527,
                'denuncia_id' => 421,
                'agresor_id' => 520,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:14',
                'updated_at' => '2019-01-29 22:27:14',
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id' => 528,
                'denuncia_id' => 421,
                'agresor_id' => 521,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:14',
                'updated_at' => '2019-01-29 22:27:14',
                'deleted_at' => NULL,
            ),
            28 => 
            array (
                'id' => 529,
                'denuncia_id' => 422,
                'agresor_id' => 522,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:14',
                'updated_at' => '2019-01-29 22:27:14',
                'deleted_at' => NULL,
            ),
            29 => 
            array (
                'id' => 530,
                'denuncia_id' => 422,
                'agresor_id' => 523,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:15',
                'updated_at' => '2019-01-29 22:27:15',
                'deleted_at' => NULL,
            ),
            30 => 
            array (
                'id' => 531,
                'denuncia_id' => 423,
                'agresor_id' => 524,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:15',
                'updated_at' => '2019-01-29 22:27:15',
                'deleted_at' => NULL,
            ),
            31 => 
            array (
                'id' => 532,
                'denuncia_id' => 424,
                'agresor_id' => 525,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:15',
                'updated_at' => '2019-01-29 22:27:15',
                'deleted_at' => NULL,
            ),
            32 => 
            array (
                'id' => 533,
                'denuncia_id' => 425,
                'agresor_id' => 526,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:15',
                'updated_at' => '2019-01-29 22:27:15',
                'deleted_at' => NULL,
            ),
            33 => 
            array (
                'id' => 534,
                'denuncia_id' => 426,
                'agresor_id' => 527,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:15',
                'updated_at' => '2019-01-29 22:27:15',
                'deleted_at' => NULL,
            ),
            34 => 
            array (
                'id' => 535,
                'denuncia_id' => 427,
                'agresor_id' => 528,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:15',
                'updated_at' => '2019-01-29 22:27:15',
                'deleted_at' => NULL,
            ),
            35 => 
            array (
                'id' => 536,
                'denuncia_id' => 428,
                'agresor_id' => 529,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:16',
                'updated_at' => '2019-01-29 22:27:16',
                'deleted_at' => NULL,
            ),
            36 => 
            array (
                'id' => 537,
                'denuncia_id' => 429,
                'agresor_id' => 530,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:16',
                'updated_at' => '2019-01-29 22:27:16',
                'deleted_at' => NULL,
            ),
            37 => 
            array (
                'id' => 538,
                'denuncia_id' => 429,
                'agresor_id' => 531,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:16',
                'updated_at' => '2019-01-29 22:27:16',
                'deleted_at' => NULL,
            ),
            38 => 
            array (
                'id' => 539,
                'denuncia_id' => 430,
                'agresor_id' => 532,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:16',
                'updated_at' => '2019-01-29 22:27:16',
                'deleted_at' => NULL,
            ),
            39 => 
            array (
                'id' => 540,
                'denuncia_id' => 431,
                'agresor_id' => 533,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:16',
                'updated_at' => '2019-01-29 22:27:16',
                'deleted_at' => NULL,
            ),
            40 => 
            array (
                'id' => 541,
                'denuncia_id' => 432,
                'agresor_id' => 534,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:16',
                'updated_at' => '2019-01-29 22:27:16',
                'deleted_at' => NULL,
            ),
            41 => 
            array (
                'id' => 542,
                'denuncia_id' => 433,
                'agresor_id' => 535,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:16',
                'updated_at' => '2019-01-29 22:27:16',
                'deleted_at' => NULL,
            ),
            42 => 
            array (
                'id' => 543,
                'denuncia_id' => 434,
                'agresor_id' => 536,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:17',
                'updated_at' => '2019-01-29 22:27:17',
                'deleted_at' => NULL,
            ),
            43 => 
            array (
                'id' => 544,
                'denuncia_id' => 435,
                'agresor_id' => 537,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:17',
                'updated_at' => '2019-01-29 22:27:17',
                'deleted_at' => NULL,
            ),
            44 => 
            array (
                'id' => 545,
                'denuncia_id' => 436,
                'agresor_id' => 538,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:17',
                'updated_at' => '2019-01-29 22:27:17',
                'deleted_at' => NULL,
            ),
            45 => 
            array (
                'id' => 546,
                'denuncia_id' => 437,
                'agresor_id' => 539,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:17',
                'updated_at' => '2019-01-29 22:27:17',
                'deleted_at' => NULL,
            ),
            46 => 
            array (
                'id' => 547,
                'denuncia_id' => 438,
                'agresor_id' => 540,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:18',
                'updated_at' => '2019-01-29 22:27:18',
                'deleted_at' => NULL,
            ),
            47 => 
            array (
                'id' => 548,
                'denuncia_id' => 438,
                'agresor_id' => 541,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:18',
                'updated_at' => '2019-01-29 22:27:18',
                'deleted_at' => NULL,
            ),
            48 => 
            array (
                'id' => 549,
                'denuncia_id' => 439,
                'agresor_id' => 542,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:18',
                'updated_at' => '2019-01-29 22:27:18',
                'deleted_at' => NULL,
            ),
            49 => 
            array (
                'id' => 550,
                'denuncia_id' => 440,
                'agresor_id' => 543,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:18',
                'updated_at' => '2019-01-29 22:27:18',
                'deleted_at' => NULL,
            ),
            50 => 
            array (
                'id' => 551,
                'denuncia_id' => 441,
                'agresor_id' => 544,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:18',
                'updated_at' => '2019-01-29 22:27:18',
                'deleted_at' => NULL,
            ),
            51 => 
            array (
                'id' => 552,
                'denuncia_id' => 442,
                'agresor_id' => 545,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:18',
                'updated_at' => '2019-01-29 22:27:18',
                'deleted_at' => NULL,
            ),
            52 => 
            array (
                'id' => 553,
                'denuncia_id' => 443,
                'agresor_id' => 546,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:19',
                'updated_at' => '2019-01-29 22:27:19',
                'deleted_at' => NULL,
            ),
            53 => 
            array (
                'id' => 554,
                'denuncia_id' => 444,
                'agresor_id' => 547,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:19',
                'updated_at' => '2019-01-29 22:27:19',
                'deleted_at' => NULL,
            ),
            54 => 
            array (
                'id' => 555,
                'denuncia_id' => 445,
                'agresor_id' => 548,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:19',
                'updated_at' => '2019-01-29 22:27:19',
                'deleted_at' => NULL,
            ),
            55 => 
            array (
                'id' => 556,
                'denuncia_id' => 446,
                'agresor_id' => 549,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:19',
                'updated_at' => '2019-01-29 22:27:19',
                'deleted_at' => NULL,
            ),
            56 => 
            array (
                'id' => 557,
                'denuncia_id' => 447,
                'agresor_id' => 550,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:19',
                'updated_at' => '2019-01-29 22:27:19',
                'deleted_at' => NULL,
            ),
            57 => 
            array (
                'id' => 558,
                'denuncia_id' => 447,
                'agresor_id' => 551,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:19',
                'updated_at' => '2019-01-29 22:27:19',
                'deleted_at' => NULL,
            ),
            58 => 
            array (
                'id' => 559,
                'denuncia_id' => 448,
                'agresor_id' => 552,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:20',
                'updated_at' => '2019-01-29 22:27:20',
                'deleted_at' => NULL,
            ),
            59 => 
            array (
                'id' => 560,
                'denuncia_id' => 449,
                'agresor_id' => 553,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:20',
                'updated_at' => '2019-01-29 22:27:20',
                'deleted_at' => NULL,
            ),
            60 => 
            array (
                'id' => 561,
                'denuncia_id' => 450,
                'agresor_id' => 554,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:20',
                'updated_at' => '2019-01-29 22:27:20',
                'deleted_at' => NULL,
            ),
            61 => 
            array (
                'id' => 562,
                'denuncia_id' => 450,
                'agresor_id' => 555,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:20',
                'updated_at' => '2019-01-29 22:27:20',
                'deleted_at' => NULL,
            ),
            62 => 
            array (
                'id' => 563,
                'denuncia_id' => 451,
                'agresor_id' => 556,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:20',
                'updated_at' => '2019-01-29 22:27:20',
                'deleted_at' => NULL,
            ),
            63 => 
            array (
                'id' => 564,
                'denuncia_id' => 452,
                'agresor_id' => 557,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:20',
                'updated_at' => '2019-01-29 22:27:20',
                'deleted_at' => NULL,
            ),
            64 => 
            array (
                'id' => 565,
                'denuncia_id' => 453,
                'agresor_id' => 558,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:20',
                'updated_at' => '2019-01-29 22:27:20',
                'deleted_at' => NULL,
            ),
            65 => 
            array (
                'id' => 566,
                'denuncia_id' => 454,
                'agresor_id' => 559,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:21',
                'updated_at' => '2019-01-29 22:27:21',
                'deleted_at' => NULL,
            ),
            66 => 
            array (
                'id' => 567,
                'denuncia_id' => 455,
                'agresor_id' => 560,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:21',
                'updated_at' => '2019-01-29 22:27:21',
                'deleted_at' => NULL,
            ),
            67 => 
            array (
                'id' => 568,
                'denuncia_id' => 456,
                'agresor_id' => 561,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:21',
                'updated_at' => '2019-01-29 22:27:21',
                'deleted_at' => NULL,
            ),
            68 => 
            array (
                'id' => 569,
                'denuncia_id' => 457,
                'agresor_id' => 562,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:21',
                'updated_at' => '2019-01-29 22:27:21',
                'deleted_at' => NULL,
            ),
            69 => 
            array (
                'id' => 570,
                'denuncia_id' => 458,
                'agresor_id' => 413,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:21',
                'updated_at' => '2019-01-29 22:27:21',
                'deleted_at' => NULL,
            ),
            70 => 
            array (
                'id' => 571,
                'denuncia_id' => 459,
                'agresor_id' => 563,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:21',
                'updated_at' => '2019-01-29 22:27:21',
                'deleted_at' => NULL,
            ),
            71 => 
            array (
                'id' => 572,
                'denuncia_id' => 460,
                'agresor_id' => 564,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:22',
                'updated_at' => '2019-01-29 22:27:22',
                'deleted_at' => NULL,
            ),
            72 => 
            array (
                'id' => 573,
                'denuncia_id' => 461,
                'agresor_id' => 565,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:22',
                'updated_at' => '2019-01-29 22:27:22',
                'deleted_at' => NULL,
            ),
            73 => 
            array (
                'id' => 574,
                'denuncia_id' => 462,
                'agresor_id' => 566,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:22',
                'updated_at' => '2019-01-29 22:27:22',
                'deleted_at' => NULL,
            ),
            74 => 
            array (
                'id' => 575,
                'denuncia_id' => 463,
                'agresor_id' => 567,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:22',
                'updated_at' => '2019-01-29 22:27:22',
                'deleted_at' => NULL,
            ),
            75 => 
            array (
                'id' => 576,
                'denuncia_id' => 463,
                'agresor_id' => 568,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:22',
                'updated_at' => '2019-01-29 22:27:22',
                'deleted_at' => NULL,
            ),
            76 => 
            array (
                'id' => 577,
                'denuncia_id' => 463,
                'agresor_id' => 569,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:22',
                'updated_at' => '2019-01-29 22:27:22',
                'deleted_at' => NULL,
            ),
            77 => 
            array (
                'id' => 578,
                'denuncia_id' => 463,
                'agresor_id' => 570,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:22',
                'updated_at' => '2019-01-29 22:27:22',
                'deleted_at' => NULL,
            ),
            78 => 
            array (
                'id' => 579,
                'denuncia_id' => 463,
                'agresor_id' => 571,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:22',
                'updated_at' => '2019-01-29 22:27:22',
                'deleted_at' => NULL,
            ),
            79 => 
            array (
                'id' => 580,
                'denuncia_id' => 463,
                'agresor_id' => 572,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:22',
                'updated_at' => '2019-01-29 22:27:22',
                'deleted_at' => NULL,
            ),
            80 => 
            array (
                'id' => 581,
                'denuncia_id' => 463,
                'agresor_id' => 573,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:22',
                'updated_at' => '2019-01-29 22:27:22',
                'deleted_at' => NULL,
            ),
            81 => 
            array (
                'id' => 582,
                'denuncia_id' => 463,
                'agresor_id' => 574,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:22',
                'updated_at' => '2019-01-29 22:27:22',
                'deleted_at' => NULL,
            ),
            82 => 
            array (
                'id' => 583,
                'denuncia_id' => 463,
                'agresor_id' => 575,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:22',
                'updated_at' => '2019-01-29 22:27:22',
                'deleted_at' => NULL,
            ),
            83 => 
            array (
                'id' => 584,
                'denuncia_id' => 464,
                'agresor_id' => 576,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:23',
                'updated_at' => '2019-01-29 22:27:23',
                'deleted_at' => NULL,
            ),
            84 => 
            array (
                'id' => 585,
                'denuncia_id' => 465,
                'agresor_id' => 577,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:23',
                'updated_at' => '2019-01-29 22:27:23',
                'deleted_at' => NULL,
            ),
            85 => 
            array (
                'id' => 586,
                'denuncia_id' => 465,
                'agresor_id' => 578,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:23',
                'updated_at' => '2019-01-29 22:27:23',
                'deleted_at' => NULL,
            ),
            86 => 
            array (
                'id' => 587,
                'denuncia_id' => 465,
                'agresor_id' => 579,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:23',
                'updated_at' => '2019-01-29 22:27:23',
                'deleted_at' => NULL,
            ),
            87 => 
            array (
                'id' => 588,
                'denuncia_id' => 466,
                'agresor_id' => 580,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:23',
                'updated_at' => '2019-01-29 22:27:23',
                'deleted_at' => NULL,
            ),
            88 => 
            array (
                'id' => 589,
                'denuncia_id' => 467,
                'agresor_id' => 581,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:23',
                'updated_at' => '2019-01-29 22:27:23',
                'deleted_at' => NULL,
            ),
            89 => 
            array (
                'id' => 590,
                'denuncia_id' => 467,
                'agresor_id' => 582,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:23',
                'updated_at' => '2019-01-29 22:27:23',
                'deleted_at' => NULL,
            ),
            90 => 
            array (
                'id' => 591,
                'denuncia_id' => 467,
                'agresor_id' => 583,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:23',
                'updated_at' => '2019-01-29 22:27:23',
                'deleted_at' => NULL,
            ),
            91 => 
            array (
                'id' => 592,
                'denuncia_id' => 467,
                'agresor_id' => 584,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:23',
                'updated_at' => '2019-01-29 22:27:23',
                'deleted_at' => NULL,
            ),
            92 => 
            array (
                'id' => 593,
                'denuncia_id' => 468,
                'agresor_id' => 585,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:23',
                'updated_at' => '2019-01-29 22:27:23',
                'deleted_at' => NULL,
            ),
            93 => 
            array (
                'id' => 594,
                'denuncia_id' => 468,
                'agresor_id' => 586,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:23',
                'updated_at' => '2019-01-29 22:27:23',
                'deleted_at' => NULL,
            ),
            94 => 
            array (
                'id' => 595,
                'denuncia_id' => 469,
                'agresor_id' => 587,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:24',
                'updated_at' => '2019-01-29 22:27:24',
                'deleted_at' => NULL,
            ),
            95 => 
            array (
                'id' => 596,
                'denuncia_id' => 469,
                'agresor_id' => 588,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:24',
                'updated_at' => '2019-01-29 22:27:24',
                'deleted_at' => NULL,
            ),
            96 => 
            array (
                'id' => 597,
                'denuncia_id' => 470,
                'agresor_id' => 589,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:24',
                'updated_at' => '2019-01-29 22:27:24',
                'deleted_at' => NULL,
            ),
            97 => 
            array (
                'id' => 598,
                'denuncia_id' => 471,
                'agresor_id' => 590,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:24',
                'updated_at' => '2019-01-29 22:27:24',
                'deleted_at' => NULL,
            ),
            98 => 
            array (
                'id' => 599,
                'denuncia_id' => 472,
                'agresor_id' => 591,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:24',
                'updated_at' => '2019-01-29 22:27:24',
                'deleted_at' => NULL,
            ),
            99 => 
            array (
                'id' => 600,
                'denuncia_id' => 473,
                'agresor_id' => 592,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:24',
                'updated_at' => '2019-01-29 22:27:24',
                'deleted_at' => NULL,
            ),
            100 => 
            array (
                'id' => 601,
                'denuncia_id' => 473,
                'agresor_id' => 593,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:24',
                'updated_at' => '2019-01-29 22:27:24',
                'deleted_at' => NULL,
            ),
            101 => 
            array (
                'id' => 602,
                'denuncia_id' => 474,
                'agresor_id' => 594,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:25',
                'updated_at' => '2019-01-29 22:27:25',
                'deleted_at' => NULL,
            ),
            102 => 
            array (
                'id' => 603,
                'denuncia_id' => 475,
                'agresor_id' => 595,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:25',
                'updated_at' => '2019-01-29 22:27:25',
                'deleted_at' => NULL,
            ),
            103 => 
            array (
                'id' => 604,
                'denuncia_id' => 476,
                'agresor_id' => 596,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:25',
                'updated_at' => '2019-01-29 22:27:25',
                'deleted_at' => NULL,
            ),
            104 => 
            array (
                'id' => 605,
                'denuncia_id' => 477,
                'agresor_id' => 597,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:25',
                'updated_at' => '2019-01-29 22:27:25',
                'deleted_at' => NULL,
            ),
            105 => 
            array (
                'id' => 606,
                'denuncia_id' => 478,
                'agresor_id' => 598,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:25',
                'updated_at' => '2019-01-29 22:27:25',
                'deleted_at' => NULL,
            ),
            106 => 
            array (
                'id' => 607,
                'denuncia_id' => 478,
                'agresor_id' => 599,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:25',
                'updated_at' => '2019-01-29 22:27:25',
                'deleted_at' => NULL,
            ),
            107 => 
            array (
                'id' => 608,
                'denuncia_id' => 479,
                'agresor_id' => 600,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:25',
                'updated_at' => '2019-01-29 22:27:25',
                'deleted_at' => NULL,
            ),
            108 => 
            array (
                'id' => 609,
                'denuncia_id' => 480,
                'agresor_id' => 601,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:26',
                'updated_at' => '2019-01-29 22:27:26',
                'deleted_at' => NULL,
            ),
            109 => 
            array (
                'id' => 610,
                'denuncia_id' => 481,
                'agresor_id' => 602,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:26',
                'updated_at' => '2019-01-29 22:27:26',
                'deleted_at' => NULL,
            ),
            110 => 
            array (
                'id' => 611,
                'denuncia_id' => 482,
                'agresor_id' => 603,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:26',
                'updated_at' => '2019-01-29 22:27:26',
                'deleted_at' => NULL,
            ),
            111 => 
            array (
                'id' => 612,
                'denuncia_id' => 483,
                'agresor_id' => 604,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:26',
                'updated_at' => '2019-01-29 22:27:26',
                'deleted_at' => NULL,
            ),
            112 => 
            array (
                'id' => 613,
                'denuncia_id' => 484,
                'agresor_id' => 605,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:26',
                'updated_at' => '2019-01-29 22:27:26',
                'deleted_at' => NULL,
            ),
            113 => 
            array (
                'id' => 614,
                'denuncia_id' => 485,
                'agresor_id' => 606,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:27',
                'updated_at' => '2019-01-29 22:27:27',
                'deleted_at' => NULL,
            ),
            114 => 
            array (
                'id' => 615,
                'denuncia_id' => 486,
                'agresor_id' => 607,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:27',
                'updated_at' => '2019-01-29 22:27:27',
                'deleted_at' => NULL,
            ),
            115 => 
            array (
                'id' => 616,
                'denuncia_id' => 487,
                'agresor_id' => 608,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:27',
                'updated_at' => '2019-01-29 22:27:27',
                'deleted_at' => NULL,
            ),
            116 => 
            array (
                'id' => 617,
                'denuncia_id' => 488,
                'agresor_id' => 609,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:27',
                'updated_at' => '2019-01-29 22:27:27',
                'deleted_at' => NULL,
            ),
            117 => 
            array (
                'id' => 618,
                'denuncia_id' => 489,
                'agresor_id' => 610,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:28',
                'updated_at' => '2019-01-29 22:27:28',
                'deleted_at' => NULL,
            ),
            118 => 
            array (
                'id' => 619,
                'denuncia_id' => 490,
                'agresor_id' => 611,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:28',
                'updated_at' => '2019-01-29 22:27:28',
                'deleted_at' => NULL,
            ),
            119 => 
            array (
                'id' => 620,
                'denuncia_id' => 491,
                'agresor_id' => 612,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:28',
                'updated_at' => '2019-01-29 22:27:28',
                'deleted_at' => NULL,
            ),
            120 => 
            array (
                'id' => 621,
                'denuncia_id' => 492,
                'agresor_id' => 9,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:28',
                'updated_at' => '2019-01-29 22:27:28',
                'deleted_at' => NULL,
            ),
            121 => 
            array (
                'id' => 622,
                'denuncia_id' => 493,
                'agresor_id' => 509,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:28',
                'updated_at' => '2019-01-29 22:27:28',
                'deleted_at' => NULL,
            ),
            122 => 
            array (
                'id' => 623,
                'denuncia_id' => 493,
                'agresor_id' => 613,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:28',
                'updated_at' => '2019-01-29 22:27:28',
                'deleted_at' => NULL,
            ),
            123 => 
            array (
                'id' => 624,
                'denuncia_id' => 494,
                'agresor_id' => 614,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:29',
                'updated_at' => '2019-01-29 22:27:29',
                'deleted_at' => NULL,
            ),
            124 => 
            array (
                'id' => 625,
                'denuncia_id' => 495,
                'agresor_id' => 615,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:29',
                'updated_at' => '2019-01-29 22:27:29',
                'deleted_at' => NULL,
            ),
            125 => 
            array (
                'id' => 626,
                'denuncia_id' => 496,
                'agresor_id' => 616,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:29',
                'updated_at' => '2019-01-29 22:27:29',
                'deleted_at' => NULL,
            ),
            126 => 
            array (
                'id' => 627,
                'denuncia_id' => 497,
                'agresor_id' => 617,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:29',
                'updated_at' => '2019-01-29 22:27:29',
                'deleted_at' => NULL,
            ),
            127 => 
            array (
                'id' => 628,
                'denuncia_id' => 498,
                'agresor_id' => 618,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:29',
                'updated_at' => '2019-01-29 22:27:29',
                'deleted_at' => NULL,
            ),
            128 => 
            array (
                'id' => 629,
                'denuncia_id' => 499,
                'agresor_id' => 619,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:29',
                'updated_at' => '2019-01-29 22:27:29',
                'deleted_at' => NULL,
            ),
            129 => 
            array (
                'id' => 630,
                'denuncia_id' => 500,
                'agresor_id' => 620,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:30',
                'updated_at' => '2019-01-29 22:27:30',
                'deleted_at' => NULL,
            ),
            130 => 
            array (
                'id' => 631,
                'denuncia_id' => 501,
                'agresor_id' => 621,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:30',
                'updated_at' => '2019-01-29 22:27:30',
                'deleted_at' => NULL,
            ),
            131 => 
            array (
                'id' => 632,
                'denuncia_id' => 502,
                'agresor_id' => 622,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:30',
                'updated_at' => '2019-01-29 22:27:30',
                'deleted_at' => NULL,
            ),
            132 => 
            array (
                'id' => 633,
                'denuncia_id' => 503,
                'agresor_id' => 623,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:30',
                'updated_at' => '2019-01-29 22:27:30',
                'deleted_at' => NULL,
            ),
            133 => 
            array (
                'id' => 634,
                'denuncia_id' => 504,
                'agresor_id' => 624,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:30',
                'updated_at' => '2019-01-29 22:27:30',
                'deleted_at' => NULL,
            ),
            134 => 
            array (
                'id' => 635,
                'denuncia_id' => 504,
                'agresor_id' => 625,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:30',
                'updated_at' => '2019-01-29 22:27:30',
                'deleted_at' => NULL,
            ),
            135 => 
            array (
                'id' => 636,
                'denuncia_id' => 504,
                'agresor_id' => 626,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:30',
                'updated_at' => '2019-01-29 22:27:30',
                'deleted_at' => NULL,
            ),
            136 => 
            array (
                'id' => 637,
                'denuncia_id' => 504,
                'agresor_id' => 627,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:30',
                'updated_at' => '2019-01-29 22:27:30',
                'deleted_at' => NULL,
            ),
            137 => 
            array (
                'id' => 638,
                'denuncia_id' => 505,
                'agresor_id' => 628,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:31',
                'updated_at' => '2019-01-29 22:27:31',
                'deleted_at' => NULL,
            ),
            138 => 
            array (
                'id' => 639,
                'denuncia_id' => 506,
                'agresor_id' => 629,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:31',
                'updated_at' => '2019-01-29 22:27:31',
                'deleted_at' => NULL,
            ),
            139 => 
            array (
                'id' => 640,
                'denuncia_id' => 507,
                'agresor_id' => 630,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:31',
                'updated_at' => '2019-01-29 22:27:31',
                'deleted_at' => NULL,
            ),
            140 => 
            array (
                'id' => 641,
                'denuncia_id' => 507,
                'agresor_id' => 631,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:31',
                'updated_at' => '2019-01-29 22:27:31',
                'deleted_at' => NULL,
            ),
            141 => 
            array (
                'id' => 642,
                'denuncia_id' => 507,
                'agresor_id' => 632,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:31',
                'updated_at' => '2019-01-29 22:27:31',
                'deleted_at' => NULL,
            ),
            142 => 
            array (
                'id' => 643,
                'denuncia_id' => 508,
                'agresor_id' => 633,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:31',
                'updated_at' => '2019-01-29 22:27:31',
                'deleted_at' => NULL,
            ),
            143 => 
            array (
                'id' => 644,
                'denuncia_id' => 509,
                'agresor_id' => 634,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:32',
                'updated_at' => '2019-01-29 22:27:32',
                'deleted_at' => NULL,
            ),
            144 => 
            array (
                'id' => 645,
                'denuncia_id' => 509,
                'agresor_id' => 635,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:32',
                'updated_at' => '2019-01-29 22:27:32',
                'deleted_at' => NULL,
            ),
            145 => 
            array (
                'id' => 646,
                'denuncia_id' => 510,
                'agresor_id' => 636,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:32',
                'updated_at' => '2019-01-29 22:27:32',
                'deleted_at' => NULL,
            ),
            146 => 
            array (
                'id' => 647,
                'denuncia_id' => 511,
                'agresor_id' => 637,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:32',
                'updated_at' => '2019-01-29 22:27:32',
                'deleted_at' => NULL,
            ),
            147 => 
            array (
                'id' => 648,
                'denuncia_id' => 512,
                'agresor_id' => 638,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:32',
                'updated_at' => '2019-01-29 22:27:32',
                'deleted_at' => NULL,
            ),
            148 => 
            array (
                'id' => 649,
                'denuncia_id' => 513,
                'agresor_id' => 639,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:32',
                'updated_at' => '2019-01-29 22:27:32',
                'deleted_at' => NULL,
            ),
            149 => 
            array (
                'id' => 650,
                'denuncia_id' => 514,
                'agresor_id' => 640,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:32',
                'updated_at' => '2019-01-29 22:27:32',
                'deleted_at' => NULL,
            ),
            150 => 
            array (
                'id' => 651,
                'denuncia_id' => 515,
                'agresor_id' => 641,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:33',
                'updated_at' => '2019-01-29 22:27:33',
                'deleted_at' => NULL,
            ),
            151 => 
            array (
                'id' => 652,
                'denuncia_id' => 515,
                'agresor_id' => 642,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:33',
                'updated_at' => '2019-01-29 22:27:33',
                'deleted_at' => NULL,
            ),
            152 => 
            array (
                'id' => 653,
                'denuncia_id' => 516,
                'agresor_id' => 643,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:33',
                'updated_at' => '2019-01-29 22:27:33',
                'deleted_at' => NULL,
            ),
            153 => 
            array (
                'id' => 654,
                'denuncia_id' => 517,
                'agresor_id' => 644,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:33',
                'updated_at' => '2019-01-29 22:27:33',
                'deleted_at' => NULL,
            ),
            154 => 
            array (
                'id' => 655,
                'denuncia_id' => 518,
                'agresor_id' => 645,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:33',
                'updated_at' => '2019-01-29 22:27:33',
                'deleted_at' => NULL,
            ),
            155 => 
            array (
                'id' => 656,
                'denuncia_id' => 519,
                'agresor_id' => 646,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:33',
                'updated_at' => '2019-01-29 22:27:33',
                'deleted_at' => NULL,
            ),
            156 => 
            array (
                'id' => 657,
                'denuncia_id' => 520,
                'agresor_id' => 647,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:34',
                'updated_at' => '2019-01-29 22:27:34',
                'deleted_at' => NULL,
            ),
            157 => 
            array (
                'id' => 658,
                'denuncia_id' => 521,
                'agresor_id' => 648,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:34',
                'updated_at' => '2019-01-29 22:27:34',
                'deleted_at' => NULL,
            ),
            158 => 
            array (
                'id' => 659,
                'denuncia_id' => 522,
                'agresor_id' => 649,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:34',
                'updated_at' => '2019-01-29 22:27:34',
                'deleted_at' => NULL,
            ),
            159 => 
            array (
                'id' => 660,
                'denuncia_id' => 523,
                'agresor_id' => 650,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:34',
                'updated_at' => '2019-01-29 22:27:34',
                'deleted_at' => NULL,
            ),
            160 => 
            array (
                'id' => 661,
                'denuncia_id' => 524,
                'agresor_id' => 651,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:34',
                'updated_at' => '2019-01-29 22:27:34',
                'deleted_at' => NULL,
            ),
            161 => 
            array (
                'id' => 662,
                'denuncia_id' => 525,
                'agresor_id' => 652,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:35',
                'updated_at' => '2019-01-29 22:27:35',
                'deleted_at' => NULL,
            ),
            162 => 
            array (
                'id' => 663,
                'denuncia_id' => 526,
                'agresor_id' => 653,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:35',
                'updated_at' => '2019-01-29 22:27:35',
                'deleted_at' => NULL,
            ),
            163 => 
            array (
                'id' => 664,
                'denuncia_id' => 527,
                'agresor_id' => 654,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:35',
                'updated_at' => '2019-01-29 22:27:35',
                'deleted_at' => NULL,
            ),
            164 => 
            array (
                'id' => 665,
                'denuncia_id' => 528,
                'agresor_id' => 655,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:35',
                'updated_at' => '2019-01-29 22:27:35',
                'deleted_at' => NULL,
            ),
            165 => 
            array (
                'id' => 666,
                'denuncia_id' => 529,
                'agresor_id' => 656,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:35',
                'updated_at' => '2019-01-29 22:27:35',
                'deleted_at' => NULL,
            ),
            166 => 
            array (
                'id' => 667,
                'denuncia_id' => 530,
                'agresor_id' => 657,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:36',
                'updated_at' => '2019-01-29 22:27:36',
                'deleted_at' => NULL,
            ),
            167 => 
            array (
                'id' => 668,
                'denuncia_id' => 531,
                'agresor_id' => 658,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:36',
                'updated_at' => '2019-01-29 22:27:36',
                'deleted_at' => NULL,
            ),
            168 => 
            array (
                'id' => 669,
                'denuncia_id' => 532,
                'agresor_id' => 659,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:36',
                'updated_at' => '2019-01-29 22:27:36',
                'deleted_at' => NULL,
            ),
            169 => 
            array (
                'id' => 670,
                'denuncia_id' => 533,
                'agresor_id' => 660,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:36',
                'updated_at' => '2019-01-29 22:27:36',
                'deleted_at' => NULL,
            ),
            170 => 
            array (
                'id' => 671,
                'denuncia_id' => 533,
                'agresor_id' => 661,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:36',
                'updated_at' => '2019-01-29 22:27:36',
                'deleted_at' => NULL,
            ),
            171 => 
            array (
                'id' => 672,
                'denuncia_id' => 533,
                'agresor_id' => 662,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:36',
                'updated_at' => '2019-01-29 22:27:36',
                'deleted_at' => NULL,
            ),
            172 => 
            array (
                'id' => 673,
                'denuncia_id' => 534,
                'agresor_id' => 663,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:37',
                'updated_at' => '2019-01-29 22:27:37',
                'deleted_at' => NULL,
            ),
            173 => 
            array (
                'id' => 674,
                'denuncia_id' => 535,
                'agresor_id' => 664,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:37',
                'updated_at' => '2019-01-29 22:27:37',
                'deleted_at' => NULL,
            ),
            174 => 
            array (
                'id' => 675,
                'denuncia_id' => 536,
                'agresor_id' => 665,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:37',
                'updated_at' => '2019-01-29 22:27:37',
                'deleted_at' => NULL,
            ),
            175 => 
            array (
                'id' => 676,
                'denuncia_id' => 536,
                'agresor_id' => 666,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:37',
                'updated_at' => '2019-01-29 22:27:37',
                'deleted_at' => NULL,
            ),
            176 => 
            array (
                'id' => 677,
                'denuncia_id' => 537,
                'agresor_id' => 667,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:37',
                'updated_at' => '2019-01-29 22:27:37',
                'deleted_at' => NULL,
            ),
            177 => 
            array (
                'id' => 678,
                'denuncia_id' => 538,
                'agresor_id' => 668,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:38',
                'updated_at' => '2019-01-29 22:27:38',
                'deleted_at' => NULL,
            ),
            178 => 
            array (
                'id' => 679,
                'denuncia_id' => 539,
                'agresor_id' => 669,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:38',
                'updated_at' => '2019-01-29 22:27:38',
                'deleted_at' => NULL,
            ),
            179 => 
            array (
                'id' => 680,
                'denuncia_id' => 540,
                'agresor_id' => 670,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:38',
                'updated_at' => '2019-01-29 22:27:38',
                'deleted_at' => NULL,
            ),
            180 => 
            array (
                'id' => 681,
                'denuncia_id' => 541,
                'agresor_id' => 671,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:38',
                'updated_at' => '2019-01-29 22:27:38',
                'deleted_at' => NULL,
            ),
            181 => 
            array (
                'id' => 682,
                'denuncia_id' => 542,
                'agresor_id' => 672,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:38',
                'updated_at' => '2019-01-29 22:27:38',
                'deleted_at' => NULL,
            ),
            182 => 
            array (
                'id' => 683,
                'denuncia_id' => 543,
                'agresor_id' => 673,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:38',
                'updated_at' => '2019-01-29 22:27:38',
                'deleted_at' => NULL,
            ),
            183 => 
            array (
                'id' => 684,
                'denuncia_id' => 544,
                'agresor_id' => 674,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:39',
                'updated_at' => '2019-01-29 22:27:39',
                'deleted_at' => NULL,
            ),
            184 => 
            array (
                'id' => 685,
                'denuncia_id' => 544,
                'agresor_id' => 675,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:39',
                'updated_at' => '2019-01-29 22:27:39',
                'deleted_at' => NULL,
            ),
            185 => 
            array (
                'id' => 686,
                'denuncia_id' => 545,
                'agresor_id' => 676,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:39',
                'updated_at' => '2019-01-29 22:27:39',
                'deleted_at' => NULL,
            ),
            186 => 
            array (
                'id' => 687,
                'denuncia_id' => 546,
                'agresor_id' => 677,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:39',
                'updated_at' => '2019-01-29 22:27:39',
                'deleted_at' => NULL,
            ),
            187 => 
            array (
                'id' => 688,
                'denuncia_id' => 547,
                'agresor_id' => 678,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:39',
                'updated_at' => '2019-01-29 22:27:39',
                'deleted_at' => NULL,
            ),
            188 => 
            array (
                'id' => 689,
                'denuncia_id' => 547,
                'agresor_id' => 679,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:39',
                'updated_at' => '2019-01-29 22:27:39',
                'deleted_at' => NULL,
            ),
            189 => 
            array (
                'id' => 690,
                'denuncia_id' => 547,
                'agresor_id' => 680,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:39',
                'updated_at' => '2019-01-29 22:27:39',
                'deleted_at' => NULL,
            ),
            190 => 
            array (
                'id' => 691,
                'denuncia_id' => 548,
                'agresor_id' => 681,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:39',
                'updated_at' => '2019-01-29 22:27:39',
                'deleted_at' => NULL,
            ),
            191 => 
            array (
                'id' => 692,
                'denuncia_id' => 549,
                'agresor_id' => 682,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:40',
                'updated_at' => '2019-01-29 22:27:40',
                'deleted_at' => NULL,
            ),
            192 => 
            array (
                'id' => 693,
                'denuncia_id' => 550,
                'agresor_id' => 683,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:40',
                'updated_at' => '2019-01-29 22:27:40',
                'deleted_at' => NULL,
            ),
            193 => 
            array (
                'id' => 694,
                'denuncia_id' => 551,
                'agresor_id' => 684,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:40',
                'updated_at' => '2019-01-29 22:27:40',
                'deleted_at' => NULL,
            ),
            194 => 
            array (
                'id' => 695,
                'denuncia_id' => 552,
                'agresor_id' => 685,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:40',
                'updated_at' => '2019-01-29 22:27:40',
                'deleted_at' => NULL,
            ),
            195 => 
            array (
                'id' => 696,
                'denuncia_id' => 553,
                'agresor_id' => 686,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:40',
                'updated_at' => '2019-01-29 22:27:40',
                'deleted_at' => NULL,
            ),
            196 => 
            array (
                'id' => 697,
                'denuncia_id' => 554,
                'agresor_id' => 687,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:40',
                'updated_at' => '2019-01-29 22:27:40',
                'deleted_at' => NULL,
            ),
            197 => 
            array (
                'id' => 698,
                'denuncia_id' => 555,
                'agresor_id' => 688,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:41',
                'updated_at' => '2019-01-29 22:27:41',
                'deleted_at' => NULL,
            ),
            198 => 
            array (
                'id' => 699,
                'denuncia_id' => 556,
                'agresor_id' => 689,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:41',
                'updated_at' => '2019-01-29 22:27:41',
                'deleted_at' => NULL,
            ),
            199 => 
            array (
                'id' => 700,
                'denuncia_id' => 557,
                'agresor_id' => 690,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:41',
                'updated_at' => '2019-01-29 22:27:41',
                'deleted_at' => NULL,
            ),
            200 => 
            array (
                'id' => 701,
                'denuncia_id' => 558,
                'agresor_id' => 691,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:41',
                'updated_at' => '2019-01-29 22:27:41',
                'deleted_at' => NULL,
            ),
            201 => 
            array (
                'id' => 702,
                'denuncia_id' => 559,
                'agresor_id' => 692,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:41',
                'updated_at' => '2019-01-29 22:27:41',
                'deleted_at' => NULL,
            ),
            202 => 
            array (
                'id' => 703,
                'denuncia_id' => 560,
                'agresor_id' => 693,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:42',
                'updated_at' => '2019-01-29 22:27:42',
                'deleted_at' => NULL,
            ),
            203 => 
            array (
                'id' => 704,
                'denuncia_id' => 561,
                'agresor_id' => 694,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:42',
                'updated_at' => '2019-01-29 22:27:42',
                'deleted_at' => NULL,
            ),
            204 => 
            array (
                'id' => 705,
                'denuncia_id' => 563,
                'agresor_id' => 695,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:42',
                'updated_at' => '2019-01-29 22:27:42',
                'deleted_at' => NULL,
            ),
            205 => 
            array (
                'id' => 706,
                'denuncia_id' => 564,
                'agresor_id' => 696,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:42',
                'updated_at' => '2019-01-29 22:27:42',
                'deleted_at' => NULL,
            ),
            206 => 
            array (
                'id' => 707,
                'denuncia_id' => 565,
                'agresor_id' => 697,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:43',
                'updated_at' => '2019-01-29 22:27:43',
                'deleted_at' => NULL,
            ),
            207 => 
            array (
                'id' => 708,
                'denuncia_id' => 566,
                'agresor_id' => 698,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:43',
                'updated_at' => '2019-01-29 22:27:43',
                'deleted_at' => NULL,
            ),
            208 => 
            array (
                'id' => 709,
                'denuncia_id' => 567,
                'agresor_id' => 699,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:43',
                'updated_at' => '2019-01-29 22:27:43',
                'deleted_at' => NULL,
            ),
            209 => 
            array (
                'id' => 710,
                'denuncia_id' => 568,
                'agresor_id' => 700,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:43',
                'updated_at' => '2019-01-29 22:27:43',
                'deleted_at' => NULL,
            ),
            210 => 
            array (
                'id' => 711,
                'denuncia_id' => 569,
                'agresor_id' => 701,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:43',
                'updated_at' => '2019-01-29 22:27:43',
                'deleted_at' => NULL,
            ),
            211 => 
            array (
                'id' => 712,
                'denuncia_id' => 570,
                'agresor_id' => 702,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:43',
                'updated_at' => '2019-01-29 22:27:43',
                'deleted_at' => NULL,
            ),
            212 => 
            array (
                'id' => 713,
                'denuncia_id' => 571,
                'agresor_id' => 703,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:44',
                'updated_at' => '2019-01-29 22:27:44',
                'deleted_at' => NULL,
            ),
            213 => 
            array (
                'id' => 714,
                'denuncia_id' => 571,
                'agresor_id' => 704,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:44',
                'updated_at' => '2019-01-29 22:27:44',
                'deleted_at' => NULL,
            ),
            214 => 
            array (
                'id' => 715,
                'denuncia_id' => 572,
                'agresor_id' => 705,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:44',
                'updated_at' => '2019-01-29 22:27:44',
                'deleted_at' => NULL,
            ),
            215 => 
            array (
                'id' => 716,
                'denuncia_id' => 573,
                'agresor_id' => 706,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:44',
                'updated_at' => '2019-01-29 22:27:44',
                'deleted_at' => NULL,
            ),
            216 => 
            array (
                'id' => 717,
                'denuncia_id' => 574,
                'agresor_id' => 707,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:44',
                'updated_at' => '2019-01-29 22:27:44',
                'deleted_at' => NULL,
            ),
            217 => 
            array (
                'id' => 718,
                'denuncia_id' => 575,
                'agresor_id' => 708,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:44',
                'updated_at' => '2019-01-29 22:27:44',
                'deleted_at' => NULL,
            ),
            218 => 
            array (
                'id' => 719,
                'denuncia_id' => 575,
                'agresor_id' => 709,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:44',
                'updated_at' => '2019-01-29 22:27:44',
                'deleted_at' => NULL,
            ),
            219 => 
            array (
                'id' => 720,
                'denuncia_id' => 575,
                'agresor_id' => 710,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:44',
                'updated_at' => '2019-01-29 22:27:44',
                'deleted_at' => NULL,
            ),
            220 => 
            array (
                'id' => 721,
                'denuncia_id' => 575,
                'agresor_id' => 711,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:44',
                'updated_at' => '2019-01-29 22:27:44',
                'deleted_at' => NULL,
            ),
            221 => 
            array (
                'id' => 722,
                'denuncia_id' => 576,
                'agresor_id' => 712,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:45',
                'updated_at' => '2019-01-29 22:27:45',
                'deleted_at' => NULL,
            ),
            222 => 
            array (
                'id' => 723,
                'denuncia_id' => 577,
                'agresor_id' => 713,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:45',
                'updated_at' => '2019-01-29 22:27:45',
                'deleted_at' => NULL,
            ),
            223 => 
            array (
                'id' => 724,
                'denuncia_id' => 578,
                'agresor_id' => 714,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:45',
                'updated_at' => '2019-01-29 22:27:45',
                'deleted_at' => NULL,
            ),
            224 => 
            array (
                'id' => 725,
                'denuncia_id' => 579,
                'agresor_id' => 715,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:45',
                'updated_at' => '2019-01-29 22:27:45',
                'deleted_at' => NULL,
            ),
            225 => 
            array (
                'id' => 726,
                'denuncia_id' => 580,
                'agresor_id' => 716,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:45',
                'updated_at' => '2019-01-29 22:27:45',
                'deleted_at' => NULL,
            ),
            226 => 
            array (
                'id' => 727,
                'denuncia_id' => 581,
                'agresor_id' => 717,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:46',
                'updated_at' => '2019-01-29 22:27:46',
                'deleted_at' => NULL,
            ),
            227 => 
            array (
                'id' => 728,
                'denuncia_id' => 581,
                'agresor_id' => 718,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:46',
                'updated_at' => '2019-01-29 22:27:46',
                'deleted_at' => NULL,
            ),
            228 => 
            array (
                'id' => 729,
                'denuncia_id' => 581,
                'agresor_id' => 719,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:46',
                'updated_at' => '2019-01-29 22:27:46',
                'deleted_at' => NULL,
            ),
            229 => 
            array (
                'id' => 730,
                'denuncia_id' => 582,
                'agresor_id' => 720,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:46',
                'updated_at' => '2019-01-29 22:27:46',
                'deleted_at' => NULL,
            ),
            230 => 
            array (
                'id' => 731,
                'denuncia_id' => 583,
                'agresor_id' => 721,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:46',
                'updated_at' => '2019-01-29 22:27:46',
                'deleted_at' => NULL,
            ),
            231 => 
            array (
                'id' => 732,
                'denuncia_id' => 584,
                'agresor_id' => 722,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:46',
                'updated_at' => '2019-01-29 22:27:46',
                'deleted_at' => NULL,
            ),
            232 => 
            array (
                'id' => 733,
                'denuncia_id' => 585,
                'agresor_id' => 723,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:46',
                'updated_at' => '2019-01-29 22:27:46',
                'deleted_at' => NULL,
            ),
            233 => 
            array (
                'id' => 734,
                'denuncia_id' => 586,
                'agresor_id' => 724,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:47',
                'updated_at' => '2019-01-29 22:27:47',
                'deleted_at' => NULL,
            ),
            234 => 
            array (
                'id' => 735,
                'denuncia_id' => 587,
                'agresor_id' => 725,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:47',
                'updated_at' => '2019-01-29 22:27:47',
                'deleted_at' => NULL,
            ),
            235 => 
            array (
                'id' => 736,
                'denuncia_id' => 588,
                'agresor_id' => 726,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:47',
                'updated_at' => '2019-01-29 22:27:47',
                'deleted_at' => NULL,
            ),
            236 => 
            array (
                'id' => 737,
                'denuncia_id' => 589,
                'agresor_id' => 727,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:47',
                'updated_at' => '2019-01-29 22:27:47',
                'deleted_at' => NULL,
            ),
            237 => 
            array (
                'id' => 738,
                'denuncia_id' => 589,
                'agresor_id' => 728,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:47',
                'updated_at' => '2019-01-29 22:27:47',
                'deleted_at' => NULL,
            ),
            238 => 
            array (
                'id' => 739,
                'denuncia_id' => 589,
                'agresor_id' => 729,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:47',
                'updated_at' => '2019-01-29 22:27:47',
                'deleted_at' => NULL,
            ),
            239 => 
            array (
                'id' => 740,
                'denuncia_id' => 590,
                'agresor_id' => 730,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:47',
                'updated_at' => '2019-01-29 22:27:47',
                'deleted_at' => NULL,
            ),
            240 => 
            array (
                'id' => 741,
                'denuncia_id' => 591,
                'agresor_id' => 731,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:48',
                'updated_at' => '2019-01-29 22:27:48',
                'deleted_at' => NULL,
            ),
            241 => 
            array (
                'id' => 742,
                'denuncia_id' => 592,
                'agresor_id' => 732,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:48',
                'updated_at' => '2019-01-29 22:27:48',
                'deleted_at' => NULL,
            ),
            242 => 
            array (
                'id' => 743,
                'denuncia_id' => 593,
                'agresor_id' => 733,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:48',
                'updated_at' => '2019-01-29 22:27:48',
                'deleted_at' => NULL,
            ),
            243 => 
            array (
                'id' => 744,
                'denuncia_id' => 594,
                'agresor_id' => 734,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:48',
                'updated_at' => '2019-01-29 22:27:48',
                'deleted_at' => NULL,
            ),
            244 => 
            array (
                'id' => 745,
                'denuncia_id' => 595,
                'agresor_id' => 735,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:49',
                'updated_at' => '2019-01-29 22:27:49',
                'deleted_at' => NULL,
            ),
            245 => 
            array (
                'id' => 746,
                'denuncia_id' => 596,
                'agresor_id' => 736,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:49',
                'updated_at' => '2019-01-29 22:27:49',
                'deleted_at' => NULL,
            ),
            246 => 
            array (
                'id' => 747,
                'denuncia_id' => 597,
                'agresor_id' => 737,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:49',
                'updated_at' => '2019-01-29 22:27:49',
                'deleted_at' => NULL,
            ),
            247 => 
            array (
                'id' => 748,
                'denuncia_id' => 597,
                'agresor_id' => 738,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:49',
                'updated_at' => '2019-01-29 22:27:49',
                'deleted_at' => NULL,
            ),
            248 => 
            array (
                'id' => 749,
                'denuncia_id' => 598,
                'agresor_id' => 739,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:49',
                'updated_at' => '2019-01-29 22:27:49',
                'deleted_at' => NULL,
            ),
            249 => 
            array (
                'id' => 750,
                'denuncia_id' => 599,
                'agresor_id' => 740,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:49',
                'updated_at' => '2019-01-29 22:27:49',
                'deleted_at' => NULL,
            ),
            250 => 
            array (
                'id' => 751,
                'denuncia_id' => 600,
                'agresor_id' => 741,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:49',
                'updated_at' => '2019-01-29 22:27:49',
                'deleted_at' => NULL,
            ),
            251 => 
            array (
                'id' => 752,
                'denuncia_id' => 601,
                'agresor_id' => 742,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:50',
                'updated_at' => '2019-01-29 22:27:50',
                'deleted_at' => NULL,
            ),
            252 => 
            array (
                'id' => 753,
                'denuncia_id' => 602,
                'agresor_id' => 743,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:50',
                'updated_at' => '2019-01-29 22:27:50',
                'deleted_at' => NULL,
            ),
            253 => 
            array (
                'id' => 754,
                'denuncia_id' => 602,
                'agresor_id' => 744,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:50',
                'updated_at' => '2019-01-29 22:27:50',
                'deleted_at' => NULL,
            ),
            254 => 
            array (
                'id' => 755,
                'denuncia_id' => 603,
                'agresor_id' => 745,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:50',
                'updated_at' => '2019-01-29 22:27:50',
                'deleted_at' => NULL,
            ),
            255 => 
            array (
                'id' => 756,
                'denuncia_id' => 604,
                'agresor_id' => 746,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:50',
                'updated_at' => '2019-01-29 22:27:50',
                'deleted_at' => NULL,
            ),
            256 => 
            array (
                'id' => 757,
                'denuncia_id' => 605,
                'agresor_id' => 747,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:50',
                'updated_at' => '2019-01-29 22:27:50',
                'deleted_at' => NULL,
            ),
            257 => 
            array (
                'id' => 758,
                'denuncia_id' => 606,
                'agresor_id' => 748,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:51',
                'updated_at' => '2019-01-29 22:27:51',
                'deleted_at' => NULL,
            ),
            258 => 
            array (
                'id' => 759,
                'denuncia_id' => 607,
                'agresor_id' => 749,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:51',
                'updated_at' => '2019-01-29 22:27:51',
                'deleted_at' => NULL,
            ),
            259 => 
            array (
                'id' => 760,
                'denuncia_id' => 608,
                'agresor_id' => 750,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:51',
                'updated_at' => '2019-01-29 22:27:51',
                'deleted_at' => NULL,
            ),
            260 => 
            array (
                'id' => 761,
                'denuncia_id' => 608,
                'agresor_id' => 751,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:51',
                'updated_at' => '2019-01-29 22:27:51',
                'deleted_at' => NULL,
            ),
            261 => 
            array (
                'id' => 762,
                'denuncia_id' => 608,
                'agresor_id' => 752,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:51',
                'updated_at' => '2019-01-29 22:27:51',
                'deleted_at' => NULL,
            ),
            262 => 
            array (
                'id' => 763,
                'denuncia_id' => 609,
                'agresor_id' => 753,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:51',
                'updated_at' => '2019-01-29 22:27:51',
                'deleted_at' => NULL,
            ),
            263 => 
            array (
                'id' => 764,
                'denuncia_id' => 610,
                'agresor_id' => 754,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:51',
                'updated_at' => '2019-01-29 22:27:51',
                'deleted_at' => NULL,
            ),
            264 => 
            array (
                'id' => 765,
                'denuncia_id' => 610,
                'agresor_id' => 755,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:51',
                'updated_at' => '2019-01-29 22:27:51',
                'deleted_at' => NULL,
            ),
            265 => 
            array (
                'id' => 766,
                'denuncia_id' => 610,
                'agresor_id' => 756,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:51',
                'updated_at' => '2019-01-29 22:27:51',
                'deleted_at' => NULL,
            ),
            266 => 
            array (
                'id' => 767,
                'denuncia_id' => 610,
                'agresor_id' => 757,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:51',
                'updated_at' => '2019-01-29 22:27:51',
                'deleted_at' => NULL,
            ),
            267 => 
            array (
                'id' => 768,
                'denuncia_id' => 611,
                'agresor_id' => 758,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:51',
                'updated_at' => '2019-01-29 22:27:51',
                'deleted_at' => NULL,
            ),
            268 => 
            array (
                'id' => 769,
                'denuncia_id' => 612,
                'agresor_id' => 759,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:52',
                'updated_at' => '2019-01-29 22:27:52',
                'deleted_at' => NULL,
            ),
            269 => 
            array (
                'id' => 770,
                'denuncia_id' => 613,
                'agresor_id' => 760,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:52',
                'updated_at' => '2019-01-29 22:27:52',
                'deleted_at' => NULL,
            ),
            270 => 
            array (
                'id' => 771,
                'denuncia_id' => 614,
                'agresor_id' => 761,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:52',
                'updated_at' => '2019-01-29 22:27:52',
                'deleted_at' => NULL,
            ),
            271 => 
            array (
                'id' => 772,
                'denuncia_id' => 614,
                'agresor_id' => 762,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:52',
                'updated_at' => '2019-01-29 22:27:52',
                'deleted_at' => NULL,
            ),
            272 => 
            array (
                'id' => 773,
                'denuncia_id' => 615,
                'agresor_id' => 763,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:52',
                'updated_at' => '2019-01-29 22:27:52',
                'deleted_at' => NULL,
            ),
            273 => 
            array (
                'id' => 774,
                'denuncia_id' => 616,
                'agresor_id' => 764,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:52',
                'updated_at' => '2019-01-29 22:27:52',
                'deleted_at' => NULL,
            ),
            274 => 
            array (
                'id' => 775,
                'denuncia_id' => 617,
                'agresor_id' => 765,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:53',
                'updated_at' => '2019-01-29 22:27:53',
                'deleted_at' => NULL,
            ),
            275 => 
            array (
                'id' => 776,
                'denuncia_id' => 618,
                'agresor_id' => 766,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:53',
                'updated_at' => '2019-01-29 22:27:53',
                'deleted_at' => NULL,
            ),
            276 => 
            array (
                'id' => 777,
                'denuncia_id' => 619,
                'agresor_id' => 767,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:53',
                'updated_at' => '2019-01-29 22:27:53',
                'deleted_at' => NULL,
            ),
            277 => 
            array (
                'id' => 778,
                'denuncia_id' => 620,
                'agresor_id' => 768,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:53',
                'updated_at' => '2019-01-29 22:27:53',
                'deleted_at' => NULL,
            ),
            278 => 
            array (
                'id' => 779,
                'denuncia_id' => 621,
                'agresor_id' => 769,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:53',
                'updated_at' => '2019-01-29 22:27:53',
                'deleted_at' => NULL,
            ),
            279 => 
            array (
                'id' => 780,
                'denuncia_id' => 622,
                'agresor_id' => 770,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:53',
                'updated_at' => '2019-01-29 22:27:53',
                'deleted_at' => NULL,
            ),
            280 => 
            array (
                'id' => 781,
                'denuncia_id' => 623,
                'agresor_id' => 771,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:54',
                'updated_at' => '2019-01-29 22:27:54',
                'deleted_at' => NULL,
            ),
            281 => 
            array (
                'id' => 782,
                'denuncia_id' => 624,
                'agresor_id' => 772,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:54',
                'updated_at' => '2019-01-29 22:27:54',
                'deleted_at' => NULL,
            ),
            282 => 
            array (
                'id' => 783,
                'denuncia_id' => 624,
                'agresor_id' => 773,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:54',
                'updated_at' => '2019-01-29 22:27:54',
                'deleted_at' => NULL,
            ),
            283 => 
            array (
                'id' => 784,
                'denuncia_id' => 625,
                'agresor_id' => 774,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:54',
                'updated_at' => '2019-01-29 22:27:54',
                'deleted_at' => NULL,
            ),
            284 => 
            array (
                'id' => 785,
                'denuncia_id' => 626,
                'agresor_id' => 775,
                'tblparentesco_id' => 9,
                'created_at' => '2019-01-29 22:27:54',
                'updated_at' => '2019-01-29 22:27:54',
                'deleted_at' => NULL,
            ),
            285 => 
            array (
                'id' => 786,
                'denuncia_id' => 1122,
                'agresor_id' => 776,
                'tblparentesco_id' => 2,
                'created_at' => '2019-01-29 22:29:15',
                'updated_at' => '2019-01-29 22:29:15',
                'deleted_at' => NULL,
            ),
            286 => 
            array (
                'id' => 787,
                'denuncia_id' => 1123,
                'agresor_id' => 777,
                'tblparentesco_id' => 2,
                'created_at' => '2019-01-29 22:29:15',
                'updated_at' => '2019-01-29 22:29:15',
                'deleted_at' => NULL,
            ),
            287 => 
            array (
                'id' => 788,
                'denuncia_id' => 1124,
                'agresor_id' => 778,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:16',
                'updated_at' => '2019-01-29 22:29:16',
                'deleted_at' => NULL,
            ),
            288 => 
            array (
                'id' => 789,
                'denuncia_id' => 1125,
                'agresor_id' => 779,
                'tblparentesco_id' => 3,
                'created_at' => '2019-01-29 22:29:16',
                'updated_at' => '2019-01-29 22:29:16',
                'deleted_at' => NULL,
            ),
            289 => 
            array (
                'id' => 790,
                'denuncia_id' => 1126,
                'agresor_id' => 780,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:16',
                'updated_at' => '2019-01-29 22:29:16',
                'deleted_at' => NULL,
            ),
            290 => 
            array (
                'id' => 791,
                'denuncia_id' => 1127,
                'agresor_id' => 781,
                'tblparentesco_id' => 2,
                'created_at' => '2019-01-29 22:29:16',
                'updated_at' => '2019-01-29 22:29:16',
                'deleted_at' => NULL,
            ),
            291 => 
            array (
                'id' => 792,
                'denuncia_id' => 1128,
                'agresor_id' => 782,
                'tblparentesco_id' => 2,
                'created_at' => '2019-01-29 22:29:16',
                'updated_at' => '2019-01-29 22:29:16',
                'deleted_at' => NULL,
            ),
            292 => 
            array (
                'id' => 793,
                'denuncia_id' => 1129,
                'agresor_id' => 783,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:16',
                'updated_at' => '2019-01-29 22:29:16',
                'deleted_at' => NULL,
            ),
            293 => 
            array (
                'id' => 794,
                'denuncia_id' => 1130,
                'agresor_id' => 784,
                'tblparentesco_id' => 4,
                'created_at' => '2019-01-29 22:29:16',
                'updated_at' => '2019-01-29 22:29:16',
                'deleted_at' => NULL,
            ),
            294 => 
            array (
                'id' => 795,
                'denuncia_id' => 1131,
                'agresor_id' => 785,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:17',
                'updated_at' => '2019-01-29 22:29:17',
                'deleted_at' => NULL,
            ),
            295 => 
            array (
                'id' => 796,
                'denuncia_id' => 1132,
                'agresor_id' => 786,
                'tblparentesco_id' => 8,
                'created_at' => '2019-01-29 22:29:17',
                'updated_at' => '2019-01-29 22:29:17',
                'deleted_at' => NULL,
            ),
            296 => 
            array (
                'id' => 797,
                'denuncia_id' => 1133,
                'agresor_id' => 787,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:17',
                'updated_at' => '2019-01-29 22:29:17',
                'deleted_at' => NULL,
            ),
            297 => 
            array (
                'id' => 798,
                'denuncia_id' => 1134,
                'agresor_id' => 788,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:17',
                'updated_at' => '2019-01-29 22:29:17',
                'deleted_at' => NULL,
            ),
            298 => 
            array (
                'id' => 799,
                'denuncia_id' => 1135,
                'agresor_id' => 789,
                'tblparentesco_id' => 3,
                'created_at' => '2019-01-29 22:29:17',
                'updated_at' => '2019-01-29 22:29:17',
                'deleted_at' => NULL,
            ),
            299 => 
            array (
                'id' => 800,
                'denuncia_id' => 1136,
                'agresor_id' => 790,
                'tblparentesco_id' => 2,
                'created_at' => '2019-01-29 22:29:17',
                'updated_at' => '2019-01-29 22:29:17',
                'deleted_at' => NULL,
            ),
            300 => 
            array (
                'id' => 801,
                'denuncia_id' => 1136,
                'agresor_id' => 791,
                'tblparentesco_id' => 2,
                'created_at' => '2019-01-29 22:29:17',
                'updated_at' => '2019-01-29 22:29:17',
                'deleted_at' => NULL,
            ),
            301 => 
            array (
                'id' => 802,
                'denuncia_id' => 1137,
                'agresor_id' => 792,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:17',
                'updated_at' => '2019-01-29 22:29:17',
                'deleted_at' => NULL,
            ),
            302 => 
            array (
                'id' => 803,
                'denuncia_id' => 1138,
                'agresor_id' => 793,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:17',
                'updated_at' => '2019-01-29 22:29:17',
                'deleted_at' => NULL,
            ),
            303 => 
            array (
                'id' => 804,
                'denuncia_id' => 1139,
                'agresor_id' => 794,
                'tblparentesco_id' => 2,
                'created_at' => '2019-01-29 22:29:18',
                'updated_at' => '2019-01-29 22:29:18',
                'deleted_at' => NULL,
            ),
            304 => 
            array (
                'id' => 805,
                'denuncia_id' => 1140,
                'agresor_id' => 795,
                'tblparentesco_id' => 3,
                'created_at' => '2019-01-29 22:29:18',
                'updated_at' => '2019-01-29 22:29:18',
                'deleted_at' => NULL,
            ),
            305 => 
            array (
                'id' => 806,
                'denuncia_id' => 1141,
                'agresor_id' => 796,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:18',
                'updated_at' => '2019-01-29 22:29:18',
                'deleted_at' => NULL,
            ),
            306 => 
            array (
                'id' => 807,
                'denuncia_id' => 1142,
                'agresor_id' => 797,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:18',
                'updated_at' => '2019-01-29 22:29:18',
                'deleted_at' => NULL,
            ),
            307 => 
            array (
                'id' => 808,
                'denuncia_id' => 1143,
                'agresor_id' => 798,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:18',
                'updated_at' => '2019-01-29 22:29:18',
                'deleted_at' => NULL,
            ),
            308 => 
            array (
                'id' => 809,
                'denuncia_id' => 1144,
                'agresor_id' => 799,
                'tblparentesco_id' => 4,
                'created_at' => '2019-01-29 22:29:18',
                'updated_at' => '2019-01-29 22:29:18',
                'deleted_at' => NULL,
            ),
            309 => 
            array (
                'id' => 810,
                'denuncia_id' => 1145,
                'agresor_id' => 800,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:18',
                'updated_at' => '2019-01-29 22:29:18',
                'deleted_at' => NULL,
            ),
            310 => 
            array (
                'id' => 811,
                'denuncia_id' => 1145,
                'agresor_id' => 801,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:18',
                'updated_at' => '2019-01-29 22:29:18',
                'deleted_at' => NULL,
            ),
            311 => 
            array (
                'id' => 812,
                'denuncia_id' => 1146,
                'agresor_id' => 802,
                'tblparentesco_id' => 8,
                'created_at' => '2019-01-29 22:29:19',
                'updated_at' => '2019-01-29 22:29:19',
                'deleted_at' => NULL,
            ),
            312 => 
            array (
                'id' => 813,
                'denuncia_id' => 1147,
                'agresor_id' => 803,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:19',
                'updated_at' => '2019-01-29 22:29:19',
                'deleted_at' => NULL,
            ),
            313 => 
            array (
                'id' => 814,
                'denuncia_id' => 1148,
                'agresor_id' => 804,
                'tblparentesco_id' => 2,
                'created_at' => '2019-01-29 22:29:19',
                'updated_at' => '2019-01-29 22:29:19',
                'deleted_at' => NULL,
            ),
            314 => 
            array (
                'id' => 815,
                'denuncia_id' => 1149,
                'agresor_id' => 805,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:19',
                'updated_at' => '2019-01-29 22:29:19',
                'deleted_at' => NULL,
            ),
            315 => 
            array (
                'id' => 816,
                'denuncia_id' => 1150,
                'agresor_id' => 806,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:19',
                'updated_at' => '2019-01-29 22:29:19',
                'deleted_at' => NULL,
            ),
            316 => 
            array (
                'id' => 817,
                'denuncia_id' => 1151,
                'agresor_id' => 807,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:19',
                'updated_at' => '2019-01-29 22:29:19',
                'deleted_at' => NULL,
            ),
            317 => 
            array (
                'id' => 818,
                'denuncia_id' => 1152,
                'agresor_id' => 808,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:19',
                'updated_at' => '2019-01-29 22:29:19',
                'deleted_at' => NULL,
            ),
            318 => 
            array (
                'id' => 819,
                'denuncia_id' => 1153,
                'agresor_id' => 809,
                'tblparentesco_id' => 4,
                'created_at' => '2019-01-29 22:29:19',
                'updated_at' => '2019-01-29 22:29:19',
                'deleted_at' => NULL,
            ),
            319 => 
            array (
                'id' => 820,
                'denuncia_id' => 1154,
                'agresor_id' => 810,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:20',
                'updated_at' => '2019-01-29 22:29:20',
                'deleted_at' => NULL,
            ),
            320 => 
            array (
                'id' => 821,
                'denuncia_id' => 1155,
                'agresor_id' => 811,
                'tblparentesco_id' => 4,
                'created_at' => '2019-01-29 22:29:20',
                'updated_at' => '2019-01-29 22:29:20',
                'deleted_at' => NULL,
            ),
            321 => 
            array (
                'id' => 822,
                'denuncia_id' => 1156,
                'agresor_id' => 812,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:20',
                'updated_at' => '2019-01-29 22:29:20',
                'deleted_at' => NULL,
            ),
            322 => 
            array (
                'id' => 823,
                'denuncia_id' => 1157,
                'agresor_id' => 813,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:20',
                'updated_at' => '2019-01-29 22:29:20',
                'deleted_at' => NULL,
            ),
            323 => 
            array (
                'id' => 824,
                'denuncia_id' => 1158,
                'agresor_id' => 814,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:20',
                'updated_at' => '2019-01-29 22:29:20',
                'deleted_at' => NULL,
            ),
            324 => 
            array (
                'id' => 825,
                'denuncia_id' => 1159,
                'agresor_id' => 815,
                'tblparentesco_id' => 4,
                'created_at' => '2019-01-29 22:29:20',
                'updated_at' => '2019-01-29 22:29:20',
                'deleted_at' => NULL,
            ),
            325 => 
            array (
                'id' => 826,
                'denuncia_id' => 1160,
                'agresor_id' => 816,
                'tblparentesco_id' => 4,
                'created_at' => '2019-01-29 22:29:20',
                'updated_at' => '2019-01-29 22:29:20',
                'deleted_at' => NULL,
            ),
            326 => 
            array (
                'id' => 827,
                'denuncia_id' => 1161,
                'agresor_id' => 817,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:21',
                'updated_at' => '2019-01-29 22:29:21',
                'deleted_at' => NULL,
            ),
            327 => 
            array (
                'id' => 828,
                'denuncia_id' => 1162,
                'agresor_id' => 818,
                'tblparentesco_id' => 4,
                'created_at' => '2019-01-29 22:29:21',
                'updated_at' => '2019-01-29 22:29:21',
                'deleted_at' => NULL,
            ),
            328 => 
            array (
                'id' => 829,
                'denuncia_id' => 1164,
                'agresor_id' => 819,
                'tblparentesco_id' => 3,
                'created_at' => '2019-01-29 22:29:21',
                'updated_at' => '2019-01-29 22:29:21',
                'deleted_at' => NULL,
            ),
            329 => 
            array (
                'id' => 830,
                'denuncia_id' => 1165,
                'agresor_id' => 820,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:21',
                'updated_at' => '2019-01-29 22:29:21',
                'deleted_at' => NULL,
            ),
            330 => 
            array (
                'id' => 831,
                'denuncia_id' => 1166,
                'agresor_id' => 586,
                'tblparentesco_id' => 2,
                'created_at' => '2019-01-29 22:29:21',
                'updated_at' => '2019-01-29 22:29:21',
                'deleted_at' => NULL,
            ),
            331 => 
            array (
                'id' => 832,
                'denuncia_id' => 1167,
                'agresor_id' => 821,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:22',
                'updated_at' => '2019-01-29 22:29:22',
                'deleted_at' => NULL,
            ),
            332 => 
            array (
                'id' => 833,
                'denuncia_id' => 1168,
                'agresor_id' => 822,
                'tblparentesco_id' => 4,
                'created_at' => '2019-01-29 22:29:22',
                'updated_at' => '2019-01-29 22:29:22',
                'deleted_at' => NULL,
            ),
            333 => 
            array (
                'id' => 834,
                'denuncia_id' => 1169,
                'agresor_id' => 823,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:22',
                'updated_at' => '2019-01-29 22:29:22',
                'deleted_at' => NULL,
            ),
            334 => 
            array (
                'id' => 835,
                'denuncia_id' => 1169,
                'agresor_id' => 824,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:22',
                'updated_at' => '2019-01-29 22:29:22',
                'deleted_at' => NULL,
            ),
            335 => 
            array (
                'id' => 836,
                'denuncia_id' => 1170,
                'agresor_id' => 825,
                'tblparentesco_id' => 8,
                'created_at' => '2019-01-29 22:29:22',
                'updated_at' => '2019-01-29 22:29:22',
                'deleted_at' => NULL,
            ),
            336 => 
            array (
                'id' => 837,
                'denuncia_id' => 1171,
                'agresor_id' => 826,
                'tblparentesco_id' => 8,
                'created_at' => '2019-01-29 22:29:22',
                'updated_at' => '2019-01-29 22:29:22',
                'deleted_at' => NULL,
            ),
            337 => 
            array (
                'id' => 838,
                'denuncia_id' => 1172,
                'agresor_id' => 827,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:23',
                'updated_at' => '2019-01-29 22:29:23',
                'deleted_at' => NULL,
            ),
            338 => 
            array (
                'id' => 839,
                'denuncia_id' => 1173,
                'agresor_id' => 828,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:23',
                'updated_at' => '2019-01-29 22:29:23',
                'deleted_at' => NULL,
            ),
            339 => 
            array (
                'id' => 840,
                'denuncia_id' => 1174,
                'agresor_id' => 829,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:23',
                'updated_at' => '2019-01-29 22:29:23',
                'deleted_at' => NULL,
            ),
            340 => 
            array (
                'id' => 841,
                'denuncia_id' => 1175,
                'agresor_id' => 830,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:23',
                'updated_at' => '2019-01-29 22:29:23',
                'deleted_at' => NULL,
            ),
            341 => 
            array (
                'id' => 842,
                'denuncia_id' => 1175,
                'agresor_id' => 831,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:23',
                'updated_at' => '2019-01-29 22:29:23',
                'deleted_at' => NULL,
            ),
            342 => 
            array (
                'id' => 843,
                'denuncia_id' => 1176,
                'agresor_id' => 832,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:23',
                'updated_at' => '2019-01-29 22:29:23',
                'deleted_at' => NULL,
            ),
            343 => 
            array (
                'id' => 844,
                'denuncia_id' => 1176,
                'agresor_id' => 833,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:23',
                'updated_at' => '2019-01-29 22:29:23',
                'deleted_at' => NULL,
            ),
            344 => 
            array (
                'id' => 845,
                'denuncia_id' => 1177,
                'agresor_id' => 834,
                'tblparentesco_id' => 4,
                'created_at' => '2019-01-29 22:29:24',
                'updated_at' => '2019-01-29 22:29:24',
                'deleted_at' => NULL,
            ),
            345 => 
            array (
                'id' => 846,
                'denuncia_id' => 1178,
                'agresor_id' => 835,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:24',
                'updated_at' => '2019-01-29 22:29:24',
                'deleted_at' => NULL,
            ),
            346 => 
            array (
                'id' => 847,
                'denuncia_id' => 1179,
                'agresor_id' => 836,
                'tblparentesco_id' => 4,
                'created_at' => '2019-01-29 22:29:24',
                'updated_at' => '2019-01-29 22:29:24',
                'deleted_at' => NULL,
            ),
            347 => 
            array (
                'id' => 848,
                'denuncia_id' => 1180,
                'agresor_id' => 837,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:24',
                'updated_at' => '2019-01-29 22:29:24',
                'deleted_at' => NULL,
            ),
            348 => 
            array (
                'id' => 849,
                'denuncia_id' => 1181,
                'agresor_id' => 838,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:24',
                'updated_at' => '2019-01-29 22:29:24',
                'deleted_at' => NULL,
            ),
            349 => 
            array (
                'id' => 850,
                'denuncia_id' => 1182,
                'agresor_id' => 839,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:25',
                'updated_at' => '2019-01-29 22:29:25',
                'deleted_at' => NULL,
            ),
            350 => 
            array (
                'id' => 851,
                'denuncia_id' => 1183,
                'agresor_id' => 840,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:25',
                'updated_at' => '2019-01-29 22:29:25',
                'deleted_at' => NULL,
            ),
            351 => 
            array (
                'id' => 852,
                'denuncia_id' => 1183,
                'agresor_id' => 841,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:25',
                'updated_at' => '2019-01-29 22:29:25',
                'deleted_at' => NULL,
            ),
            352 => 
            array (
                'id' => 853,
                'denuncia_id' => 1183,
                'agresor_id' => 842,
                'tblparentesco_id' => 6,
                'created_at' => '2019-01-29 22:29:25',
                'updated_at' => '2019-01-29 22:29:25',
                'deleted_at' => NULL,
            ),
            353 => 
            array (
                'id' => 854,
                'denuncia_id' => 1183,
                'agresor_id' => 843,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:25',
                'updated_at' => '2019-01-29 22:29:25',
                'deleted_at' => NULL,
            ),
            354 => 
            array (
                'id' => 855,
                'denuncia_id' => 1184,
                'agresor_id' => 844,
                'tblparentesco_id' => 6,
                'created_at' => '2019-01-29 22:29:25',
                'updated_at' => '2019-01-29 22:29:25',
                'deleted_at' => NULL,
            ),
            355 => 
            array (
                'id' => 856,
                'denuncia_id' => 1185,
                'agresor_id' => 845,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:25',
                'updated_at' => '2019-01-29 22:29:25',
                'deleted_at' => NULL,
            ),
            356 => 
            array (
                'id' => 857,
                'denuncia_id' => 1186,
                'agresor_id' => 846,
                'tblparentesco_id' => 2,
                'created_at' => '2019-01-29 22:29:25',
                'updated_at' => '2019-01-29 22:29:25',
                'deleted_at' => NULL,
            ),
            357 => 
            array (
                'id' => 858,
                'denuncia_id' => 1187,
                'agresor_id' => 847,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:25',
                'updated_at' => '2019-01-29 22:29:25',
                'deleted_at' => NULL,
            ),
            358 => 
            array (
                'id' => 859,
                'denuncia_id' => 1188,
                'agresor_id' => 848,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:26',
                'updated_at' => '2019-01-29 22:29:26',
                'deleted_at' => NULL,
            ),
            359 => 
            array (
                'id' => 860,
                'denuncia_id' => 1189,
                'agresor_id' => 849,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:26',
                'updated_at' => '2019-01-29 22:29:26',
                'deleted_at' => NULL,
            ),
            360 => 
            array (
                'id' => 861,
                'denuncia_id' => 1190,
                'agresor_id' => 850,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:26',
                'updated_at' => '2019-01-29 22:29:26',
                'deleted_at' => NULL,
            ),
            361 => 
            array (
                'id' => 862,
                'denuncia_id' => 1191,
                'agresor_id' => 851,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:26',
                'updated_at' => '2019-01-29 22:29:26',
                'deleted_at' => NULL,
            ),
            362 => 
            array (
                'id' => 863,
                'denuncia_id' => 1192,
                'agresor_id' => 852,
                'tblparentesco_id' => 2,
                'created_at' => '2019-01-29 22:29:26',
                'updated_at' => '2019-01-29 22:29:26',
                'deleted_at' => NULL,
            ),
            363 => 
            array (
                'id' => 864,
                'denuncia_id' => 1193,
                'agresor_id' => 853,
                'tblparentesco_id' => 4,
                'created_at' => '2019-01-29 22:29:26',
                'updated_at' => '2019-01-29 22:29:26',
                'deleted_at' => NULL,
            ),
            364 => 
            array (
                'id' => 865,
                'denuncia_id' => 1194,
                'agresor_id' => 854,
                'tblparentesco_id' => 4,
                'created_at' => '2019-01-29 22:29:26',
                'updated_at' => '2019-01-29 22:29:26',
                'deleted_at' => NULL,
            ),
            365 => 
            array (
                'id' => 866,
                'denuncia_id' => 1194,
                'agresor_id' => 855,
                'tblparentesco_id' => 4,
                'created_at' => '2019-01-29 22:29:26',
                'updated_at' => '2019-01-29 22:29:26',
                'deleted_at' => NULL,
            ),
            366 => 
            array (
                'id' => 867,
                'denuncia_id' => 1195,
                'agresor_id' => 856,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:26',
                'updated_at' => '2019-01-29 22:29:26',
                'deleted_at' => NULL,
            ),
            367 => 
            array (
                'id' => 868,
                'denuncia_id' => 1195,
                'agresor_id' => 857,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:26',
                'updated_at' => '2019-01-29 22:29:26',
                'deleted_at' => NULL,
            ),
            368 => 
            array (
                'id' => 869,
                'denuncia_id' => 1196,
                'agresor_id' => 858,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:27',
                'updated_at' => '2019-01-29 22:29:27',
                'deleted_at' => NULL,
            ),
            369 => 
            array (
                'id' => 870,
                'denuncia_id' => 1197,
                'agresor_id' => 859,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:27',
                'updated_at' => '2019-01-29 22:29:27',
                'deleted_at' => NULL,
            ),
            370 => 
            array (
                'id' => 871,
                'denuncia_id' => 1198,
                'agresor_id' => 860,
                'tblparentesco_id' => 3,
                'created_at' => '2019-01-29 22:29:27',
                'updated_at' => '2019-01-29 22:29:27',
                'deleted_at' => NULL,
            ),
            371 => 
            array (
                'id' => 872,
                'denuncia_id' => 1199,
                'agresor_id' => 861,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:27',
                'updated_at' => '2019-01-29 22:29:27',
                'deleted_at' => NULL,
            ),
            372 => 
            array (
                'id' => 873,
                'denuncia_id' => 1200,
                'agresor_id' => 862,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:27',
                'updated_at' => '2019-01-29 22:29:27',
                'deleted_at' => NULL,
            ),
            373 => 
            array (
                'id' => 874,
                'denuncia_id' => 1201,
                'agresor_id' => 863,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:28',
                'updated_at' => '2019-01-29 22:29:28',
                'deleted_at' => NULL,
            ),
            374 => 
            array (
                'id' => 875,
                'denuncia_id' => 1202,
                'agresor_id' => 864,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:28',
                'updated_at' => '2019-01-29 22:29:28',
                'deleted_at' => NULL,
            ),
            375 => 
            array (
                'id' => 876,
                'denuncia_id' => 1203,
                'agresor_id' => 865,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:28',
                'updated_at' => '2019-01-29 22:29:28',
                'deleted_at' => NULL,
            ),
            376 => 
            array (
                'id' => 877,
                'denuncia_id' => 1204,
                'agresor_id' => 866,
                'tblparentesco_id' => 2,
                'created_at' => '2019-01-29 22:29:28',
                'updated_at' => '2019-01-29 22:29:28',
                'deleted_at' => NULL,
            ),
            377 => 
            array (
                'id' => 878,
                'denuncia_id' => 1205,
                'agresor_id' => 867,
                'tblparentesco_id' => 8,
                'created_at' => '2019-01-29 22:29:28',
                'updated_at' => '2019-01-29 22:29:28',
                'deleted_at' => NULL,
            ),
            378 => 
            array (
                'id' => 879,
                'denuncia_id' => 1206,
                'agresor_id' => 868,
                'tblparentesco_id' => 4,
                'created_at' => '2019-01-29 22:29:28',
                'updated_at' => '2019-01-29 22:29:28',
                'deleted_at' => NULL,
            ),
            379 => 
            array (
                'id' => 880,
                'denuncia_id' => 1207,
                'agresor_id' => 869,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:29',
                'updated_at' => '2019-01-29 22:29:29',
                'deleted_at' => NULL,
            ),
            380 => 
            array (
                'id' => 881,
                'denuncia_id' => 1208,
                'agresor_id' => 870,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:29',
                'updated_at' => '2019-01-29 22:29:29',
                'deleted_at' => NULL,
            ),
            381 => 
            array (
                'id' => 882,
                'denuncia_id' => 1209,
                'agresor_id' => 871,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:29',
                'updated_at' => '2019-01-29 22:29:29',
                'deleted_at' => NULL,
            ),
            382 => 
            array (
                'id' => 883,
                'denuncia_id' => 1210,
                'agresor_id' => 872,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:29',
                'updated_at' => '2019-01-29 22:29:29',
                'deleted_at' => NULL,
            ),
            383 => 
            array (
                'id' => 884,
                'denuncia_id' => 1211,
                'agresor_id' => 873,
                'tblparentesco_id' => 4,
                'created_at' => '2019-01-29 22:29:29',
                'updated_at' => '2019-01-29 22:29:29',
                'deleted_at' => NULL,
            ),
            384 => 
            array (
                'id' => 885,
                'denuncia_id' => 1212,
                'agresor_id' => 874,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:29',
                'updated_at' => '2019-01-29 22:29:29',
                'deleted_at' => NULL,
            ),
            385 => 
            array (
                'id' => 886,
                'denuncia_id' => 1213,
                'agresor_id' => 875,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:29',
                'updated_at' => '2019-01-29 22:29:29',
                'deleted_at' => NULL,
            ),
            386 => 
            array (
                'id' => 887,
                'denuncia_id' => 1214,
                'agresor_id' => 876,
                'tblparentesco_id' => 2,
                'created_at' => '2019-01-29 22:29:30',
                'updated_at' => '2019-01-29 22:29:30',
                'deleted_at' => NULL,
            ),
            387 => 
            array (
                'id' => 888,
                'denuncia_id' => 1215,
                'agresor_id' => 877,
                'tblparentesco_id' => 4,
                'created_at' => '2019-01-29 22:29:30',
                'updated_at' => '2019-01-29 22:29:30',
                'deleted_at' => NULL,
            ),
            388 => 
            array (
                'id' => 889,
                'denuncia_id' => 1216,
                'agresor_id' => 878,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:30',
                'updated_at' => '2019-01-29 22:29:30',
                'deleted_at' => NULL,
            ),
            389 => 
            array (
                'id' => 890,
                'denuncia_id' => 1217,
                'agresor_id' => 879,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:30',
                'updated_at' => '2019-01-29 22:29:30',
                'deleted_at' => NULL,
            ),
            390 => 
            array (
                'id' => 891,
                'denuncia_id' => 1218,
                'agresor_id' => 880,
                'tblparentesco_id' => 4,
                'created_at' => '2019-01-29 22:29:30',
                'updated_at' => '2019-01-29 22:29:30',
                'deleted_at' => NULL,
            ),
            391 => 
            array (
                'id' => 892,
                'denuncia_id' => 1219,
                'agresor_id' => 881,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:30',
                'updated_at' => '2019-01-29 22:29:30',
                'deleted_at' => NULL,
            ),
            392 => 
            array (
                'id' => 893,
                'denuncia_id' => 1220,
                'agresor_id' => 882,
                'tblparentesco_id' => 2,
                'created_at' => '2019-01-29 22:29:31',
                'updated_at' => '2019-01-29 22:29:31',
                'deleted_at' => NULL,
            ),
            393 => 
            array (
                'id' => 894,
                'denuncia_id' => 1220,
                'agresor_id' => 883,
                'tblparentesco_id' => 2,
                'created_at' => '2019-01-29 22:29:31',
                'updated_at' => '2019-01-29 22:29:31',
                'deleted_at' => NULL,
            ),
            394 => 
            array (
                'id' => 895,
                'denuncia_id' => 1220,
                'agresor_id' => 884,
                'tblparentesco_id' => 2,
                'created_at' => '2019-01-29 22:29:31',
                'updated_at' => '2019-01-29 22:29:31',
                'deleted_at' => NULL,
            ),
            395 => 
            array (
                'id' => 896,
                'denuncia_id' => 1220,
                'agresor_id' => 885,
                'tblparentesco_id' => 2,
                'created_at' => '2019-01-29 22:29:31',
                'updated_at' => '2019-01-29 22:29:31',
                'deleted_at' => NULL,
            ),
            396 => 
            array (
                'id' => 897,
                'denuncia_id' => 1221,
                'agresor_id' => 886,
                'tblparentesco_id' => 2,
                'created_at' => '2019-01-29 22:29:31',
                'updated_at' => '2019-01-29 22:29:31',
                'deleted_at' => NULL,
            ),
            397 => 
            array (
                'id' => 898,
                'denuncia_id' => 1222,
                'agresor_id' => 887,
                'tblparentesco_id' => 4,
                'created_at' => '2019-01-29 22:29:31',
                'updated_at' => '2019-01-29 22:29:31',
                'deleted_at' => NULL,
            ),
            398 => 
            array (
                'id' => 899,
                'denuncia_id' => 1223,
                'agresor_id' => 888,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:31',
                'updated_at' => '2019-01-29 22:29:31',
                'deleted_at' => NULL,
            ),
            399 => 
            array (
                'id' => 900,
                'denuncia_id' => 1224,
                'agresor_id' => 889,
                'tblparentesco_id' => 4,
                'created_at' => '2019-01-29 22:29:31',
                'updated_at' => '2019-01-29 22:29:31',
                'deleted_at' => NULL,
            ),
            400 => 
            array (
                'id' => 901,
                'denuncia_id' => 1225,
                'agresor_id' => 890,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:31',
                'updated_at' => '2019-01-29 22:29:31',
                'deleted_at' => NULL,
            ),
            401 => 
            array (
                'id' => 902,
                'denuncia_id' => 1226,
                'agresor_id' => 891,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:32',
                'updated_at' => '2019-01-29 22:29:32',
                'deleted_at' => NULL,
            ),
            402 => 
            array (
                'id' => 903,
                'denuncia_id' => 1227,
                'agresor_id' => 892,
                'tblparentesco_id' => 4,
                'created_at' => '2019-01-29 22:29:32',
                'updated_at' => '2019-01-29 22:29:32',
                'deleted_at' => NULL,
            ),
            403 => 
            array (
                'id' => 904,
                'denuncia_id' => 1228,
                'agresor_id' => 893,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:32',
                'updated_at' => '2019-01-29 22:29:32',
                'deleted_at' => NULL,
            ),
            404 => 
            array (
                'id' => 905,
                'denuncia_id' => 1229,
                'agresor_id' => 894,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:32',
                'updated_at' => '2019-01-29 22:29:32',
                'deleted_at' => NULL,
            ),
            405 => 
            array (
                'id' => 906,
                'denuncia_id' => 1230,
                'agresor_id' => 895,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:32',
                'updated_at' => '2019-01-29 22:29:32',
                'deleted_at' => NULL,
            ),
            406 => 
            array (
                'id' => 907,
                'denuncia_id' => 1231,
                'agresor_id' => 896,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:32',
                'updated_at' => '2019-01-29 22:29:32',
                'deleted_at' => NULL,
            ),
            407 => 
            array (
                'id' => 908,
                'denuncia_id' => 1232,
                'agresor_id' => 897,
                'tblparentesco_id' => 8,
                'created_at' => '2019-01-29 22:29:33',
                'updated_at' => '2019-01-29 22:29:33',
                'deleted_at' => NULL,
            ),
            408 => 
            array (
                'id' => 909,
                'denuncia_id' => 1233,
                'agresor_id' => 898,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:33',
                'updated_at' => '2019-01-29 22:29:33',
                'deleted_at' => NULL,
            ),
            409 => 
            array (
                'id' => 910,
                'denuncia_id' => 1234,
                'agresor_id' => 899,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:33',
                'updated_at' => '2019-01-29 22:29:33',
                'deleted_at' => NULL,
            ),
            410 => 
            array (
                'id' => 911,
                'denuncia_id' => 1235,
                'agresor_id' => 900,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:33',
                'updated_at' => '2019-01-29 22:29:33',
                'deleted_at' => NULL,
            ),
            411 => 
            array (
                'id' => 912,
                'denuncia_id' => 1236,
                'agresor_id' => 901,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:33',
                'updated_at' => '2019-01-29 22:29:33',
                'deleted_at' => NULL,
            ),
            412 => 
            array (
                'id' => 913,
                'denuncia_id' => 1237,
                'agresor_id' => 902,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:34',
                'updated_at' => '2019-01-29 22:29:34',
                'deleted_at' => NULL,
            ),
            413 => 
            array (
                'id' => 914,
                'denuncia_id' => 1238,
                'agresor_id' => 903,
                'tblparentesco_id' => 6,
                'created_at' => '2019-01-29 22:29:34',
                'updated_at' => '2019-01-29 22:29:34',
                'deleted_at' => NULL,
            ),
            414 => 
            array (
                'id' => 915,
                'denuncia_id' => 1238,
                'agresor_id' => 904,
                'tblparentesco_id' => 6,
                'created_at' => '2019-01-29 22:29:34',
                'updated_at' => '2019-01-29 22:29:34',
                'deleted_at' => NULL,
            ),
            415 => 
            array (
                'id' => 916,
                'denuncia_id' => 1239,
                'agresor_id' => 905,
                'tblparentesco_id' => 3,
                'created_at' => '2019-01-29 22:29:34',
                'updated_at' => '2019-01-29 22:29:34',
                'deleted_at' => NULL,
            ),
            416 => 
            array (
                'id' => 917,
                'denuncia_id' => 1240,
                'agresor_id' => 906,
                'tblparentesco_id' => 8,
                'created_at' => '2019-01-29 22:29:34',
                'updated_at' => '2019-01-29 22:29:34',
                'deleted_at' => NULL,
            ),
            417 => 
            array (
                'id' => 918,
                'denuncia_id' => 1241,
                'agresor_id' => 907,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:34',
                'updated_at' => '2019-01-29 22:29:34',
                'deleted_at' => NULL,
            ),
            418 => 
            array (
                'id' => 919,
                'denuncia_id' => 1242,
                'agresor_id' => 908,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:34',
                'updated_at' => '2019-01-29 22:29:34',
                'deleted_at' => NULL,
            ),
            419 => 
            array (
                'id' => 920,
                'denuncia_id' => 1243,
                'agresor_id' => 909,
                'tblparentesco_id' => 8,
                'created_at' => '2019-01-29 22:29:34',
                'updated_at' => '2019-01-29 22:29:34',
                'deleted_at' => NULL,
            ),
            420 => 
            array (
                'id' => 921,
                'denuncia_id' => 1244,
                'agresor_id' => 910,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:35',
                'updated_at' => '2019-01-29 22:29:35',
                'deleted_at' => NULL,
            ),
            421 => 
            array (
                'id' => 922,
                'denuncia_id' => 1245,
                'agresor_id' => 911,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:35',
                'updated_at' => '2019-01-29 22:29:35',
                'deleted_at' => NULL,
            ),
            422 => 
            array (
                'id' => 923,
                'denuncia_id' => 1246,
                'agresor_id' => 912,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:35',
                'updated_at' => '2019-01-29 22:29:35',
                'deleted_at' => NULL,
            ),
            423 => 
            array (
                'id' => 924,
                'denuncia_id' => 1247,
                'agresor_id' => 913,
                'tblparentesco_id' => 4,
                'created_at' => '2019-01-29 22:29:35',
                'updated_at' => '2019-01-29 22:29:35',
                'deleted_at' => NULL,
            ),
            424 => 
            array (
                'id' => 925,
                'denuncia_id' => 1248,
                'agresor_id' => 914,
                'tblparentesco_id' => 4,
                'created_at' => '2019-01-29 22:29:35',
                'updated_at' => '2019-01-29 22:29:35',
                'deleted_at' => NULL,
            ),
            425 => 
            array (
                'id' => 926,
                'denuncia_id' => 1249,
                'agresor_id' => 915,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:36',
                'updated_at' => '2019-01-29 22:29:36',
                'deleted_at' => NULL,
            ),
            426 => 
            array (
                'id' => 927,
                'denuncia_id' => 1250,
                'agresor_id' => 916,
                'tblparentesco_id' => 4,
                'created_at' => '2019-01-29 22:29:36',
                'updated_at' => '2019-01-29 22:29:36',
                'deleted_at' => NULL,
            ),
            427 => 
            array (
                'id' => 928,
                'denuncia_id' => 1251,
                'agresor_id' => 917,
                'tblparentesco_id' => 4,
                'created_at' => '2019-01-29 22:29:36',
                'updated_at' => '2019-01-29 22:29:36',
                'deleted_at' => NULL,
            ),
            428 => 
            array (
                'id' => 929,
                'denuncia_id' => 1252,
                'agresor_id' => 918,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:36',
                'updated_at' => '2019-01-29 22:29:36',
                'deleted_at' => NULL,
            ),
            429 => 
            array (
                'id' => 930,
                'denuncia_id' => 1253,
                'agresor_id' => 919,
                'tblparentesco_id' => 8,
                'created_at' => '2019-01-29 22:29:36',
                'updated_at' => '2019-01-29 22:29:36',
                'deleted_at' => NULL,
            ),
            430 => 
            array (
                'id' => 931,
                'denuncia_id' => 1254,
                'agresor_id' => 920,
                'tblparentesco_id' => 6,
                'created_at' => '2019-01-29 22:29:36',
                'updated_at' => '2019-01-29 22:29:36',
                'deleted_at' => NULL,
            ),
            431 => 
            array (
                'id' => 932,
                'denuncia_id' => 1255,
                'agresor_id' => 921,
                'tblparentesco_id' => 3,
                'created_at' => '2019-01-29 22:29:37',
                'updated_at' => '2019-01-29 22:29:37',
                'deleted_at' => NULL,
            ),
            432 => 
            array (
                'id' => 933,
                'denuncia_id' => 1256,
                'agresor_id' => 922,
                'tblparentesco_id' => 4,
                'created_at' => '2019-01-29 22:29:37',
                'updated_at' => '2019-01-29 22:29:37',
                'deleted_at' => NULL,
            ),
            433 => 
            array (
                'id' => 934,
                'denuncia_id' => 1257,
                'agresor_id' => 923,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:37',
                'updated_at' => '2019-01-29 22:29:37',
                'deleted_at' => NULL,
            ),
            434 => 
            array (
                'id' => 935,
                'denuncia_id' => 1258,
                'agresor_id' => 924,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:37',
                'updated_at' => '2019-01-29 22:29:37',
                'deleted_at' => NULL,
            ),
            435 => 
            array (
                'id' => 936,
                'denuncia_id' => 1259,
                'agresor_id' => 925,
                'tblparentesco_id' => 4,
                'created_at' => '2019-01-29 22:29:37',
                'updated_at' => '2019-01-29 22:29:37',
                'deleted_at' => NULL,
            ),
            436 => 
            array (
                'id' => 937,
                'denuncia_id' => 1260,
                'agresor_id' => 926,
                'tblparentesco_id' => 2,
                'created_at' => '2019-01-29 22:29:37',
                'updated_at' => '2019-01-29 22:29:37',
                'deleted_at' => NULL,
            ),
            437 => 
            array (
                'id' => 938,
                'denuncia_id' => 1261,
                'agresor_id' => 927,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:37',
                'updated_at' => '2019-01-29 22:29:37',
                'deleted_at' => NULL,
            ),
            438 => 
            array (
                'id' => 939,
                'denuncia_id' => 1262,
                'agresor_id' => 928,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:38',
                'updated_at' => '2019-01-29 22:29:38',
                'deleted_at' => NULL,
            ),
            439 => 
            array (
                'id' => 940,
                'denuncia_id' => 1263,
                'agresor_id' => 929,
                'tblparentesco_id' => 2,
                'created_at' => '2019-01-29 22:29:38',
                'updated_at' => '2019-01-29 22:29:38',
                'deleted_at' => NULL,
            ),
            440 => 
            array (
                'id' => 941,
                'denuncia_id' => 1264,
                'agresor_id' => 930,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:38',
                'updated_at' => '2019-01-29 22:29:38',
                'deleted_at' => NULL,
            ),
            441 => 
            array (
                'id' => 942,
                'denuncia_id' => 1265,
                'agresor_id' => 931,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:38',
                'updated_at' => '2019-01-29 22:29:38',
                'deleted_at' => NULL,
            ),
            442 => 
            array (
                'id' => 943,
                'denuncia_id' => 1266,
                'agresor_id' => 932,
                'tblparentesco_id' => 8,
                'created_at' => '2019-01-29 22:29:38',
                'updated_at' => '2019-01-29 22:29:38',
                'deleted_at' => NULL,
            ),
            443 => 
            array (
                'id' => 944,
                'denuncia_id' => 1267,
                'agresor_id' => 933,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:39',
                'updated_at' => '2019-01-29 22:29:39',
                'deleted_at' => NULL,
            ),
            444 => 
            array (
                'id' => 945,
                'denuncia_id' => 1268,
                'agresor_id' => 934,
                'tblparentesco_id' => 2,
                'created_at' => '2019-01-29 22:29:39',
                'updated_at' => '2019-01-29 22:29:39',
                'deleted_at' => NULL,
            ),
            445 => 
            array (
                'id' => 946,
                'denuncia_id' => 1269,
                'agresor_id' => 935,
                'tblparentesco_id' => 8,
                'created_at' => '2019-01-29 22:29:39',
                'updated_at' => '2019-01-29 22:29:39',
                'deleted_at' => NULL,
            ),
            446 => 
            array (
                'id' => 947,
                'denuncia_id' => 1270,
                'agresor_id' => 936,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:39',
                'updated_at' => '2019-01-29 22:29:39',
                'deleted_at' => NULL,
            ),
            447 => 
            array (
                'id' => 948,
                'denuncia_id' => 1271,
                'agresor_id' => 937,
                'tblparentesco_id' => 3,
                'created_at' => '2019-01-29 22:29:39',
                'updated_at' => '2019-01-29 22:29:39',
                'deleted_at' => NULL,
            ),
            448 => 
            array (
                'id' => 949,
                'denuncia_id' => 1272,
                'agresor_id' => 938,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:39',
                'updated_at' => '2019-01-29 22:29:39',
                'deleted_at' => NULL,
            ),
            449 => 
            array (
                'id' => 950,
                'denuncia_id' => 1273,
                'agresor_id' => 939,
                'tblparentesco_id' => 4,
                'created_at' => '2019-01-29 22:29:40',
                'updated_at' => '2019-01-29 22:29:40',
                'deleted_at' => NULL,
            ),
            450 => 
            array (
                'id' => 951,
                'denuncia_id' => 1274,
                'agresor_id' => 940,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:40',
                'updated_at' => '2019-01-29 22:29:40',
                'deleted_at' => NULL,
            ),
            451 => 
            array (
                'id' => 952,
                'denuncia_id' => 1275,
                'agresor_id' => 941,
                'tblparentesco_id' => 4,
                'created_at' => '2019-01-29 22:29:40',
                'updated_at' => '2019-01-29 22:29:40',
                'deleted_at' => NULL,
            ),
            452 => 
            array (
                'id' => 953,
                'denuncia_id' => 1276,
                'agresor_id' => 942,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:40',
                'updated_at' => '2019-01-29 22:29:40',
                'deleted_at' => NULL,
            ),
            453 => 
            array (
                'id' => 954,
                'denuncia_id' => 1277,
                'agresor_id' => 943,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:40',
                'updated_at' => '2019-01-29 22:29:40',
                'deleted_at' => NULL,
            ),
            454 => 
            array (
                'id' => 955,
                'denuncia_id' => 1277,
                'agresor_id' => 944,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:40',
                'updated_at' => '2019-01-29 22:29:40',
                'deleted_at' => NULL,
            ),
            455 => 
            array (
                'id' => 956,
                'denuncia_id' => 1278,
                'agresor_id' => 945,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:41',
                'updated_at' => '2019-01-29 22:29:41',
                'deleted_at' => NULL,
            ),
            456 => 
            array (
                'id' => 957,
                'denuncia_id' => 1279,
                'agresor_id' => 946,
                'tblparentesco_id' => 3,
                'created_at' => '2019-01-29 22:29:41',
                'updated_at' => '2019-01-29 22:29:41',
                'deleted_at' => NULL,
            ),
            457 => 
            array (
                'id' => 958,
                'denuncia_id' => 1280,
                'agresor_id' => 947,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:41',
                'updated_at' => '2019-01-29 22:29:41',
                'deleted_at' => NULL,
            ),
            458 => 
            array (
                'id' => 959,
                'denuncia_id' => 1281,
                'agresor_id' => 948,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:41',
                'updated_at' => '2019-01-29 22:29:41',
                'deleted_at' => NULL,
            ),
            459 => 
            array (
                'id' => 960,
                'denuncia_id' => 1282,
                'agresor_id' => 949,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:41',
                'updated_at' => '2019-01-29 22:29:41',
                'deleted_at' => NULL,
            ),
            460 => 
            array (
                'id' => 961,
                'denuncia_id' => 1283,
                'agresor_id' => 950,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:42',
                'updated_at' => '2019-01-29 22:29:42',
                'deleted_at' => NULL,
            ),
            461 => 
            array (
                'id' => 962,
                'denuncia_id' => 1284,
                'agresor_id' => 951,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:42',
                'updated_at' => '2019-01-29 22:29:42',
                'deleted_at' => NULL,
            ),
            462 => 
            array (
                'id' => 963,
                'denuncia_id' => 1284,
                'agresor_id' => 952,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:42',
                'updated_at' => '2019-01-29 22:29:42',
                'deleted_at' => NULL,
            ),
            463 => 
            array (
                'id' => 964,
                'denuncia_id' => 1285,
                'agresor_id' => 953,
                'tblparentesco_id' => 4,
                'created_at' => '2019-01-29 22:29:42',
                'updated_at' => '2019-01-29 22:29:42',
                'deleted_at' => NULL,
            ),
            464 => 
            array (
                'id' => 965,
                'denuncia_id' => 1286,
                'agresor_id' => 954,
                'tblparentesco_id' => 4,
                'created_at' => '2019-01-29 22:29:42',
                'updated_at' => '2019-01-29 22:29:42',
                'deleted_at' => NULL,
            ),
            465 => 
            array (
                'id' => 966,
                'denuncia_id' => 1287,
                'agresor_id' => 955,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:42',
                'updated_at' => '2019-01-29 22:29:42',
                'deleted_at' => NULL,
            ),
            466 => 
            array (
                'id' => 967,
                'denuncia_id' => 1288,
                'agresor_id' => 956,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:42',
                'updated_at' => '2019-01-29 22:29:42',
                'deleted_at' => NULL,
            ),
            467 => 
            array (
                'id' => 968,
                'denuncia_id' => 1289,
                'agresor_id' => 957,
                'tblparentesco_id' => 3,
                'created_at' => '2019-01-29 22:29:42',
                'updated_at' => '2019-01-29 22:29:42',
                'deleted_at' => NULL,
            ),
            468 => 
            array (
                'id' => 969,
                'denuncia_id' => 1290,
                'agresor_id' => 958,
                'tblparentesco_id' => 2,
                'created_at' => '2019-01-29 22:29:43',
                'updated_at' => '2019-01-29 22:29:43',
                'deleted_at' => NULL,
            ),
            469 => 
            array (
                'id' => 970,
                'denuncia_id' => 1291,
                'agresor_id' => 959,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:43',
                'updated_at' => '2019-01-29 22:29:43',
                'deleted_at' => NULL,
            ),
            470 => 
            array (
                'id' => 971,
                'denuncia_id' => 1292,
                'agresor_id' => 960,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:43',
                'updated_at' => '2019-01-29 22:29:43',
                'deleted_at' => NULL,
            ),
            471 => 
            array (
                'id' => 972,
                'denuncia_id' => 1293,
                'agresor_id' => 961,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:43',
                'updated_at' => '2019-01-29 22:29:43',
                'deleted_at' => NULL,
            ),
            472 => 
            array (
                'id' => 973,
                'denuncia_id' => 1294,
                'agresor_id' => 962,
                'tblparentesco_id' => 4,
                'created_at' => '2019-01-29 22:29:43',
                'updated_at' => '2019-01-29 22:29:43',
                'deleted_at' => NULL,
            ),
            473 => 
            array (
                'id' => 974,
                'denuncia_id' => 1295,
                'agresor_id' => 963,
                'tblparentesco_id' => 1,
                'created_at' => '2019-01-29 22:29:43',
                'updated_at' => '2019-01-29 22:29:43',
                'deleted_at' => NULL,
            ),
            474 => 
            array (
                'id' => 975,
                'denuncia_id' => 1296,
                'agresor_id' => 964,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:44',
                'updated_at' => '2019-01-29 22:29:44',
                'deleted_at' => NULL,
            ),
            475 => 
            array (
                'id' => 976,
                'denuncia_id' => 1297,
                'agresor_id' => 965,
                'tblparentesco_id' => 2,
                'created_at' => '2019-01-29 22:29:44',
                'updated_at' => '2019-01-29 22:29:44',
                'deleted_at' => NULL,
            ),
            476 => 
            array (
                'id' => 977,
                'denuncia_id' => 1298,
                'agresor_id' => 966,
                'tblparentesco_id' => 4,
                'created_at' => '2019-01-29 22:29:44',
                'updated_at' => '2019-01-29 22:29:44',
                'deleted_at' => NULL,
            ),
            477 => 
            array (
                'id' => 978,
                'denuncia_id' => 1299,
                'agresor_id' => 967,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:44',
                'updated_at' => '2019-01-29 22:29:44',
                'deleted_at' => NULL,
            ),
            478 => 
            array (
                'id' => 979,
                'denuncia_id' => 1299,
                'agresor_id' => 968,
                'tblparentesco_id' => 5,
                'created_at' => '2019-01-29 22:29:44',
                'updated_at' => '2019-01-29 22:29:44',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}